﻿use thecoffeeson
go


  CREATE PROCEDURE findOrderByCustomer @id_cus int
 as
 select * from orders where id_cus = @id_cus
 go

 --exec findOrderByCustomer @id_cus=1
 go


--view product theo status va loai
--drop view product_view
--select * from product_view
go

--drop view product_view
create view product_view as
select pr.id, pr.name, pr.image, pr.price, pr.status,pr.id_categories
from Products pr
inner join Categories ctl on pr.id_categories = ctl.id
go

--drop view productTrue_view
create view productTrue_view as
--select *
select pr.id, pr.name, pr.image, pr.price, pr.status,pr.id_categories
from Products pr
inner join Categories ctl on pr.id_categories = ctl.id
where pr.status =1 or pr.status =2 and ctl.status =1
 
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 
 go

  CREATE PROCEDURE softProduct 
 as
 select *
 from product_view 
 order by status desc 
 go
 --exec softProduct

 --find san pham theo ten
 CREATE PROCEDURE findProduct @pr nvarchar(50)
 as
 select *
 from product_view 
 where name like '%'+@pr+'%' 
 go
 --exec findProduct @pr = ca



-- tìm kiếm thông tin ship theo đơn hàng
 CREATE PROCEDURE findShipByIdOrder @id int
 as
select* from Ships_info
 where id_order= @id 
 go


 exec findShipByIdOrder @id=1
 go

 -- view bill
--view tong thong ke
--drop view bills_view
--select * from bills_view
create view bills_view as
select b.id, b.date_create,sum( odt.amount*pr.price*sz.Product_price) as total,b.id_order
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
group by b.id , b.date_create ,b.id_order ,ost.comment
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 
 go
use thecoffeeson
go
 --drop PROCEDURE findOrder_status
CREATE PROCEDURE findOrder_status @status int
 as
 --select  b.id, b.date_create,sum( odt.amount*pr.price*sz.Product_price) as total,b.id_order
 select od.id ,od.date_created,od.payment,od.id_cus,od.id_voucher
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
where ost.status = @status
--group by b.id , b.date_create ,b.id_order 
go

--exec findOrder_status @status =0
--drop PROCEDURE findOrder_statusUser
CREATE PROCEDURE findOrder_statusUser @status int ,@id_cus int
 as
 select ost.id,ost.comment,ost.status,ost.id_order
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
where ost.status = @status and od.id_cus =@id_cus
--group by b.id , b.date_create ,b.id_order 
go
--exec findOrder_statusUser @status =1 ,@id_cus =7
--select*from orders_status
 --Voucher======================

  --select * from Vouchers

  --select * from Vouchers where status =1
--=======
-- select * from Feedbacks where id_cus =1
CREATE PROCEDURE findFeedbacksbyProduct @id int
 as
select * from FeedBacks where id_product = @id
go
--exec findFeedbacksbyProduct @id=4
create view VoucherUser_View as
select v.id, v.status, v.value, v.start_time, v.end_time, quanity 
from Orders o 
RIGHT JOIN Vouchers v 
on O.id_voucher = V.id  where v.status =1 and end_time> GETDATE()
Group by v.id, v.status, v.value, v.start_time, v.end_time, quanity
Having count(id_voucher) < (select quanity from Vouchers where id = v.id)
go

--select * from VoucherUser_View

CREATE PROCEDURE checkOrder
 as
select od.id,od.date_created ,ship.id as ship ,b.id as id_bill,ost.id as status 
--select*
from Orders od
--full join Orders_Detail odt on od.id = odt.id_order
full join Ships_info ship on ship.id_order=od.id
full join bills b on b.id_order = od.id
full join Orders_status ost on ost.id_order = od.id
go
--drop PROCEDURE searchOrders
--CREATE PROCEDURE searchOrders @id int
-- as

--exec searchOrders @id =0