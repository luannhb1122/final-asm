use thecoffeeson
go
-- data cua cửa hàng
INSERT INTO Stores (name ,phone ,email ,address )
    VALUES ('TheCoffeeSon','0989697995','TheSonCoffee@gmail.com',N'2 đường Trường Sơn Quận tân Bình TP.HCM'),
	('TheCoffeeLuan','0764265512','TheLuanCoffee@gmail.com',N'1 Đường Nguyên Hồng Phường 1 Quận Gò Vấp TP.HCM'),
	('TheCoffeeHau','0794751044','TheHauCoffee@gmail.com',N'225 Đường Lê Văn Quới  Phường Bình Trị Đông Quận Bình Tân TP.HCM'),
	('TheCoffeeMinh','0376571409','TheMinhCoffee@gmail.com',N'1 Đường Chương Dương Phường Linh Chiểu TP.ThuDuc TP.HCM'),
	('TheCoffeeDat','0964138876','ThedatCoffee@gmail.com',N' Đường số 9 Phường 6 Quận 4 TP.HCM');
go
--data của Roles, Accuont 
INSERT INTO Roles (name)
    VALUES 
	('ROLE_ADMIN'),
	('ROLE_USER'),
	('ROLE_SUPERADMIN')
	
INSERT INTO accounts_info (name, phone, email,picture, birthday, gender,username,password,id_role,create_date)
    VALUES
	(N'SUPERADMIN','0964138876','nguyentiedat03171999@gmail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','admin','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',3,'2020-12-01'),
	(N'Nguyễn Tiến Đạt','0964138876','nguyentiedat03171999@gmail.com','https://res.cloudinary.com/minhchon/image/upload/v1619507468/Thecoffeeson/NguyenTienDat_iybjcp.png','2021-01-04','0','dat','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',1,'2020-12-01'),	
	(N'Ngô Hoàng Bảo Luân','0764265512','ghost451201@gmail.com','https://res.cloudinary.com/minhchon/image/upload/v1619507461/Thecoffeeson/NgoHoangBaoLuan_m2eqio.jpg','2021-02-04','1','luan','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',1,'2021-01-02'),
	(N'Võ Trường Sơn','0989697995','votruongson264@gmail.com','https://res.cloudinary.com/minhchon/image/upload/v1619507451/Thecoffeeson/VoTruong_Son_eqlkrh.png','2021-01-04','0','son','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',1,'2021-02-03'),	
	(N'Nguyễn Thanh Hậu','0794751044','nguyenvan3@gmail.com','https://res.cloudinary.com/minhchon/image/upload/v1619711414/Thecoffeeson/NguyenThanhHau_iyzxe8.jpg','2021-02-04','1','hau','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',1,'2021-02-04'),
	(N'Nguyễn Nhật Minh','0376571409','mdung2406@gmail.com','https://res.cloudinary.com/minhchon/image/upload/v1619715988/Thecoffeeson/NguyenNhatMinh_iqy04w.jpg','2021-02-04','1','minh','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',1,'2021-02-04');

INSERT INTO accounts_info (name, phone, email,picture, birthday, gender,username,password,id_role)
    VALUES
	(N'Nguyễn Văn Cường','0761238764','Cuongnv0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-01-04','0','cuonglg','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Linh Ngọc Vân','0257894456','vanln0@gail.com',		'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-01-04','0','cus0','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Vũ Minh Tiến','0641238643','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-02-04','1','cus1','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Đặng Ngọc Mai','0123257789','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-01-04','0','cus2','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Huỳnh Quốc Đạt','0863452251','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-01-04','1','cus3','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Nguyễn Mai Oanh','0642589954','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-02-04','0','cus4','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Phan Minh Tú','0168947729','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-02-04','0','cus5','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Hoàng Tú Lan','0753214456','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png','2021-01-04','1','cus6','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Đô Tuấn Nam','0985735561','nguyenvan2@gail.com',		'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','0','cus7','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Đổ Thị Ánh Thu','0123457654','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','1','cus8','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Phan Ngọc Tuyến','0123562234','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','0','cus9','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Lê Minh Nguyệt','098752568','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','0','cus10','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Lê Thiết Vương','098758853','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','1','cus11','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Nguyễn Vân Diểm Hương','097358841','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','1','cus12','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Nguyễn Tuyên','0987865435','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','0','cus13','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Trần Thi Phương Uyên','0834562234','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','1','cus14','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Đăng Văn Trung','0978956432','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','0','cus15','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Võ Ánh Hằng','0978905532','nguyenvan2@gail.com',		'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','1','cus16','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Đinh Quốc Hoàng','0870981234','nguyenvan0@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','0','cus18','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Ngô Hoàng Kim Vân','0965790122','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','1','cus19','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Nguyên Đông Quân','0998877886','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','1','cus20','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Huỳnh Liên Nguyện','0789065312','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-01-04','0','cus21','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'Phan Văn Kiên','0098999333','nguyenvan2@gail.com',	'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','1','cus22','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),
	(N'Ngô Hoàng Thanh Liên','090088642','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg','2021-02-04','0','cus23','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2);

go
-- data sản phẩm ,categories
INSERT INTO Categories (type)
    VALUES
	(N'Cafe'),
	(N'Trà'),
	(N'Yaourt'),
	(N'Sữa'),
	(N'Sinh tố'),
	(N'Nước ép'),
	(N'Thức uống khác');

	
INSERT INTO Products(name, image, price, id_categories)
VALUES 
(N'Cà Phê Đen Đá','https://res.cloudinary.com/minhchon/image/upload/v1619173944/Thecoffeeson/Cafeden_rwfthx.png',28000,'1'),
(N'Cà Phê Sữa Đá','https://res.cloudinary.com/minhchon/image/upload/v1619162162/Thecoffeeson/Cafesua_qvkole.png',30000,'1'),
(N'Bạc Xỉu','https://res.cloudinary.com/minhchon/image/upload/v1619162144/Thecoffeeson/Bacxiu_ci7zk5.png',32000,'1'),
(N'Capuchino','https://res.cloudinary.com/minhchon/image/upload/v1619162173/Thecoffeeson/Capuchino_vypirr.png',35000,'1'),
(N'Sữa Tươi Cà Phê','	https://res.cloudinary.com/minhchon/image/upload/v1619162451/Thecoffeeson/Suatuoicafe_cv8qi2.png',33000,'1'),
(N'Trà Vải','https://res.cloudinary.com/minhchon/image/upload/v1619162325/Thecoffeeson/Travaihoahong_e8ctw7.png',31000,'2'),
(N'Trà Olong Sen Vàng','	https://res.cloudinary.com/minhchon/image/upload/v1619162337/Thecoffeeson/Traolongsenvang_lb4ky6.png',33000,'2'),
(N'Trà Đào cam Sả','	https://res.cloudinary.com/minhchon/image/upload/v1619162369/Thecoffeeson/Tradaocamsa_w1lmmf.png',33000,'2'),
(N'Trà Sữa Trân Châu	','https://res.cloudinary.com/minhchon/image/upload/v1619162811/Thecoffeeson/Trasuathapcam_bykpa5.png',33000,'2'),
(N'Yaourt Dâu','	https://res.cloudinary.com/minhchon/image/upload/v1619162194/Thecoffeeson/yaourtdau_u33bnc.png',33000,'3'),
(N'Yaourt Kiwi','https://res.cloudinary.com/minhchon/image/upload/v1619173560/Thecoffeeson/yaourtKiwi_th0lwc.png',33000,'3'),
(N'Yaourt Việt Quất	','https://res.cloudinary.com/minhchon/image/upload/v1619173549/Thecoffeeson/yaourtvietquat_jbywen.png',33000,'3'),
(N'Sữa Tươi','https://res.cloudinary.com/minhchon/image/upload/v1619173498/Thecoffeeson/Suatuoi_hc0rmq.png',33000,'4'),
(N'Sinh Tố Phúc Bồn Tử','	https://res.cloudinary.com/minhchon/image/upload/v1619162426/Thecoffeeson/Phucbontu_ixl8p0.png',33000,'5'),
(N'Sinh Tố Việt Quất','	https://res.cloudinary.com/minhchon/image/upload/v1619162302/Thecoffeeson/Vietquat_la7iz7.png',33000,'5'),
(N'Sinh Tố Xoài','	https://res.cloudinary.com/minhchon/image/upload/v1619162292/Thecoffeeson/Xoaidaxay_g5bxjs.png',33000,'5'),
(N'Nước Chanh Dây','	https://res.cloudinary.com/minhchon/image/upload/v1619162403/Thecoffeeson/Sodachanhday_gvliuy.png',33000,'6'),
(N'Nước Ép Thơm','	https://res.cloudinary.com/minhchon/image/upload/v1619162207/Thecoffeeson/Nuocepthom_lusahz.png',33000,'6'),
(N'Nước Dưa Hấu	','https://res.cloudinary.com/minhchon/image/upload/v1619162246/Thecoffeeson/Nuocduahau_fdxksm.png',33000,'6'),
(N'Nước Cam Ép','	https://res.cloudinary.com/minhchon/image/upload/v1619162386/Thecoffeeson/Camvat_gweuve.png',33000,'6'),
(N'Soda','	https://res.cloudinary.com/minhchon/image/upload/v1619162184/Thecoffeeson/Soda_c1rixd.png',33000,'7'),
(N'Khoai Môn Cream Cheese','	https://res.cloudinary.com/minhchon/image/upload/v1619162437/Thecoffeeson/Khoaimonkemcheese_iuwzpx.png',33000,'7'),
(N'Chocolate Đá Xay','	https://res.cloudinary.com/minhchon/image/upload/v1619162798/Thecoffeeson/Chocolatedaxay_jxfii9.png',33000,'7'),
(N'Chocolate Cream Cheese','	https://res.cloudinary.com/minhchon/image/upload/v1619162779/Thecoffeeson/Chocoladaxayphomai_w3sctu.png',33000,'7');

INSERT INTO Products(name, image, price, id_categories,status)
VALUES 
	(N'Latte','https://product.hstatic.net/1000075078/product/latte-nong_ffcd92de11f74937bce4197823246d07_large.jpg',38000,'1',1),
	(N'Mocha','https://product.hstatic.net/1000075078/product/mocha-nong_66ebb6f03a874a4391fc80ad69264ea5_master.jpg',42000,'1',1),
	(N'Espresso','https://product.hstatic.net/1000075078/product/espresso-nong_4b32833e9a5f48768ea5d5d2a4df0303_large.jpg',38000,'1',1),
	(N'Cookies Đá Xay','https://product.hstatic.net/1000075078/product/cookie-da-xay_43c2bc99f313405aa253b803dcd59030_large.jpg',33000,'1',2),
	(N'Hồng trà latte ','https://product.hstatic.net/1000075078/product/ht-latte-macchiato_fe7fa1571b974b48a5d750bd2e9e84eb_large.jpg',31000,'2',2),
	(N'Trà sưa Olong Nướng','https://product.hstatic.net/1000075078/product/oolong-nuong_6ce4a7b9cfba44509fcf1886a25e5b00_large.jpg',33000,'2',2),
	(N'Trà Đen MachiaTo cam Sả','https://product.hstatic.net/1000075078/product/tra-den-macchiato_facfdd980ca547be93974edb7f16d3b7_large.jpg',33000,'2',2);
;

INSERT INTO Categories(type)
VALUES 
	(N'Thức uống Trái cây')

INSERT INTO Products(name, image, price, id_categories,status)
VALUES 
	(N'Chanh sả đá xay','https://product.hstatic.net/1000075078/product/chanh-sa-da-xay_fd95c70496714848a16a64c60b04ccf6_large.jpg',38000,'8',2),
	(N'Đào việt quất đá xay','https://product.hstatic.net/1000075078/product/dao-viet-quat-da-xay_7e464338009c432985f8e3cdba6acb38_large.jpg',42000,'8',2),
	(N'Phúc bồn tử đá xay','https://product.hstatic.net/1000075078/product/cam-pbt-da-xay_06ca55fce8e84389ab9d707f4bd753a7_large.jpg',38000,'8',2),
	(N'Sinh tố việt quất','https://product.hstatic.net/1000075078/product/sinh-to-viet-quoc_75c00683e2aa4b8eb10e2b0d0e568a0c_large.jpg',33000,'8',2),
	(N'Yogurt dưa lưới phát tài ','https://product.hstatic.net/1000075078/product/yogurt-dua-luoi_30e104322e764a8aa4794e09c1ff74ab_large.jpg',31000,'8',2);
INSERT INTO feedbacks (note,rate,id_cus,id_product)
    VALUES
	(N'coffee nay ...',4,1,9),
	(N'coffee nay ...',4,3,1),
	(N'coffee nay ...',4,2,3),
	(N'coffee nay ...',4,4,4),
	(N'coffee nay ...',4,1,4),
	(N'coffee nay ...',4,1,1),	
	(N'coffee nay ...',4,1,12);
go
-- Thanh toán ,order ,vourcher ,order_status,ship,order_detail
INSERT INTO Sizes (Product_size,Product_price )
    VALUES
	('S',1.0),
	('M',1.2),
	('L',1.25),
	('XL',1.3);
	
INSERT INTO Vouchers ( value ,start_time ,end_time ,quanity)
    VALUES
	(5000,'2020-12-04','2021-05-10',10),
	(10000,'2020-11-04','2021-05-10',5),
	(7000,'2020-12-04','2021-05-10',10),
	(12000,'2020-09-04','2021-05-10',5),
	(4000,'2020-10-04','2021-05-10',10),
	(15000,'2020-02-04','2021-05-10',5);

INSERT INTO orders (date_created,id_cus,id_voucher)
    VALUES
	('2020-01-04',24,1),
	('2020-02-04',2,2),
	('2020-03-04',3,3),
	('2020-04-04',4,4),
	('2020-05-04',5,5),
	('2020-06-04',6,6),
	('2020-07-04',7,1),
	('2020-08-04',8,2),
	('2020-09-04',9,3),
	('2020-10-04',10,4),
	('2020-11-04',13,5),
	('2020-12-04',11,6),
	('2021-01-04',12,1),
	('2021-02-04',13,2),
	('2021-03-04',14,3),
	('2021-04-04',15,4),
	('2021-04-05',16,5)
	;
	
INSERT INTO orders_detail (amount,id_product,id_order)
    VALUES
	(3,2,1),
	(5,1,1),
	(1,3,1),
	(2,4,1),
	(5,9,2),
	(1,6,2),
	(2,7,2),
	(5,11,3),
	(1,6,3),
	(2,7,3),
	(5,11,4),
	(1,24,4),	
	(2,17,4),
	(5,1,5),
	(1,22,5),	
	(2,3,6),
	(1,4,6),
	(2,6,7),
	(3,7,7),	
	(6,5,8),
	(1,8,8),
	(7,9,9),	
	(2,10,9),
	(5,11,10),
	(1,12,10),	
	(2,13,10),
	(1,14,11),
	(1,15,11),	
	(4,16,11),
	(2,17,12),
	(3,18,12),	
	(4,19,12),
	(3,20,13),
	(3,21,13),	
	(1,22,13),
	(1,23,14),
	(2,24,14),	
	(2,21,14),
	(6,1,15),
	(2,24,15),	
	(2,3,15),
	(1,10,16),
	(1,24,16),
	(1,10,16);
	--(6,1,17),
	--(2,24,17),	
	--(2,3,17)
	

INSERT INTO ships_info (name_cus ,address ,	phone_cus ,	note ,price ,id_order )
    VALUES
	(N'Nguyên Thi Mai',N'214 Nguyễn Trãi Phường 2 Quận 5','0765258841',N'Giao nhanh',15000,1),
	(N'Hải Nam',N'33 Ngô Gia Tự Phương 4 Quận 5','0111222322',N'Giao bình thường',15000,2),
	(N'Nguyễn Thị Cúc',N'12 Đương quãn hàm f6  quận Gò Vấp','0111222311',N'Giao bình thường',15000,3),
	(N'Trần thị Diệp',N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3','0665559982',N'Giao Bình thường',15000,4),
	(N'Ngô Lâm',N'004 Phan Văn Trị f1 Quận Gò Vấp','0254785541',N'Giao Bình thường',15000,5),
	(N'Nguyên Hông Mai',N'11 Đinh Tiên Hoàng F2 Quận 1','065794452',N'Giao Bình thường',15000,6),
	(N'Nguyễn Minh',N'312 Cống lở f6 Quận6','053258674',N'Giao Bình thường',15000,7),
	(N'Trần Hoàng Yến',N'24 Trần Huy Liệu F1 Quận 3','0986865547',N'Giao Bình thường',15000,8),
	(N'Nguyễn thị Ngọc Mai',N'14 Ngô Quyền f2 Quận tân Bình','0999856441',N'Giao Bình thường',15000,9),
	(N'Đinh bá thành',N'112 Nở Trang Long f9 Quận Bình Thạnh','0989856332',N'Giao Bình thường',15000,10),
	(N'Nguyễn Thị Phương Uyên',N'21 Nguyễn Thái Sơn f7 Gò Vấp','0989547761',N'Giao Bình thường',15000,11),
	(N'Trần Hào',N'233 Quang Trung f5 Gò Vấp','0966855788',N'Giao Bình thường',15000,12),
	(N'Hắc Lịch',N'002 Thái Thượng Lãng Ong Quận 8','0966888444',N'Giao Bình thường',15000,13),
	(N'Minh Ngọc',N'214/12 Lê Quang Định F1 Quận Gò Vấp','0933888426',N'Giao Bình thường',15000,14),
	(N'Thái Vũ',N'11 Nguyên Hồng F1 Gò Vấp','0736652477',N'Giao Bình thường',15000,15),
	(N'Minh Lan',N'22 Hoàng Sa F4 Quận 3','0778996542',N'Giao Bình thường',15000,16);

	
	
INSERT INTO Orders_status (status,comment,id_order)
    VALUES
	('2',N'success',1),
	('1',N'on working',2),
	('2',N'success',3),
	('0',N'fail',4),
	('2',N'success',5),
	('2',N'success',6),
	('2',N'success',7),
	('2',N'success',8),
	('0',N'fail',9),
	('0',N'fail',10),
	('2',N'success',11),
	('1',N'on working',12),
	('2',N'success',13),
	('2',N'success',14),
	('2',N'success',15),	
	('0',N'fail',16);
	

	
INSERT INTO bills (	date_create ,id_order)
    VALUES
	('2020-01-04',1),
	('2020-02-04',2),
	('2020-03-04',3),
	('2020-04-04',4),
	('2020-05-04',5),
	('2020-06-04',6),
	('2020-07-04',7),
	('2020-08-04',8),
	('2020-09-04',9),
	('2020-10-04',10),
	('2020-11-04',11),
	('2020-12-04',12),
	('2021-01-04',13),
	('2021-02-03',14),
	('2021-03-04',15),
	('2021-04-04',16);



	
	
-- 	select* , concat(key, id) as new  from customers 
-- 	select * from customers;
-- 	select * from accounts_info
-- 	inner join customers on accounts_info.id_cus = customers.id;

