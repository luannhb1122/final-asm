CREATE DATABASE thecoffeeson;
go
-- HOME PAGE INFO
-- Create table Stores
-- (
-- 	id serial primary key,
-- 	name varchar(50),
-- 	phone varchar(11),
-- 	email varchar(50),
-- 	address varchar(50),
-- 	website varchar(25)
-- );
-- Create table News
-- (
-- 	id serial primary key,
-- 	newcontent varchar(255),
-- 	note varchar(255),
-- 	date_create varchar(50) not null,
-- 	image varchar(25)	
-- );
-- TAI KHOAN
-- create table Logins
-- (
-- 	username varchar(50) not null primary key,
-- 	password varchar(50) not null
-- );





-- Create table Account_images
-- (
-- 	id serial primary key,
-- 	image varchar(25),	
-- 	id_account int ,	
-- 	FOREIGN KEY (id_account) REFERENCES Accounts(id) ON DELETE CASCADE
-- );
use thecoffeeson
go

 --create table Tokens
 --(
	--id int primary key identity,
 --	user_name varchar(50) ,
 --	create_date date not null, 	
	--token nvarchar(max),
	--roles nvarchar (30),
 --	);

 Create table Stores
 (
 	id int primary key identity,
 	name nvarchar(max),
 	phone nvarchar(11),
 	email varchar(80),
 	address nvarchar(max) 	
 );

--create table Customers
--(
--	id int primary key identity,
--	username varchar(50) not null UNIQUE,
--	password varchar(60) not null,
--	status bit DEFAULT(1) ,	
--	level int default(1)	
--);


--create table Employees
--(
--	id int primary key identity,
--	username varchar(50) not null UNIQUE,
--	password varchar(60) not null,
--	status bit DEFAULT(1) ,
--	start_date date DEFAULT getdate()		
--);

create table Roles
(
	id int primary key identity,	
	name nvarchar(50) UNIQUE not null,
);

Create table Accounts_info
(
	id int primary key identity,
	name nvarchar(100) null,
	phone varchar(11) null,
	email varchar(80) null,
	picture varchar(max) null,
	birthday date null,	
	gender bit null,
	username varchar(50) not null UNIQUE,
	password varchar(60) not null,
	create_date date DEFAULT getdate(),
	status bit DEFAULT(1),
	id_role int not null,	
	FOREIGN KEY (id_role) REFERENCES Roles(id) ON DELETE CASCADE 	
	
);
--CREATE UNIQUE INDEX indunique
--  ON Accounts_info(id_cus)
--  WHERE id_cus IS NOT NULL
--go

--CREATE UNIQUE INDEX indunique1
--  ON Accounts_info(id_emp)
--  WHERE id_emp IS NOT NULL
--go

-- create table Notifications
-- (
-- 	id serial primary key,
-- 	message varchar(255),
-- 	status boolean DEFAULT(true),
-- 	id_cus int,
-- 	FOREIGN KEY (id_cus) REFERENCES Customers(id) ON DELETE CASCADE
-- );
--  SAN PHAM
create table Categories
(
	id int primary key identity,	
	type nvarchar(50) not null,
	status bit DEFAULT(1)
);

create table Products
(
	id int primary key identity,
	name nvarchar(80) not null,	
	image varchar(max) not null,
	price float not null,
	status int DEFAULT(1) ,
	id_categories int not null,	
	FOREIGN KEY (id_categories) REFERENCES Categories(id) ON DELETE CASCADE
	
); 


create table Feedbacks
(
	id int primary key identity,
	note nvarchar(255),
	status bit DEFAULT(1),
	rate int,
	date_time date default getdate(),
	id_cus int not null,
	id_product int not null,
	FOREIGN KEY (id_cus) REFERENCES Accounts_info(id) ON DELETE CASCADE,
	FOREIGN KEY (id_product) REFERENCES Products(id) ON DELETE CASCADE
);
 create table Sizes
 (
	id int primary key identity,
	Product_size nvarchar(10),
	status bit DEFAULT(1),
	Product_price float
 );
--  THANH TOAN
create table Vouchers
(
	id int primary key identity,
	status bit DEFAULT(1),
	value float not null,
	start_time date DEFAULT getdate(),
	end_time date not null,
	quanity int
);
create table Orders
(
	id int primary key identity,
	date_created date DEFAULT(getdate()),
	payment int DEFAULT 1,
	id_cus int not null,	
	id_voucher int ,
	FOREIGN KEY (id_cus) REFERENCES Accounts_info(id) ON DELETE CASCADE,
	FOREIGN KEY (id_voucher) REFERENCES Vouchers(id) ON DELETE CASCADE

);

create table Orders_Detail
(
	id int primary key identity,
	amount int not null,
	id_product int not null,
	id_order int not null,
	id_size int DEFAULT 1,
	FOREIGN KEY (id_size) REFERENCES Sizes(id) ON DELETE CASCADE,
	FOREIGN KEY (id_product) REFERENCES Products(id) ON DELETE CASCADE,
	FOREIGN KEY (id_order) REFERENCES Orders(id) ON DELETE CASCADE
);

create table Ships_info
(
	id int primary key identity,
	name_cus nvarchar(255) not null,
	address nvarchar(max) not null,
	phone_cus varchar(11) not null,
	note nvarchar(100) not null,
	price float not null DEFAULT 15000,
	id_order int not null,
	FOREIGN KEY (id_order) REFERENCES Orders(id) ON DELETE CASCADE
);

create table Orders_status
(
	id int primary key identity,
	status int not null DEFAULT(1),
	comment nvarchar(80) not null,
	id_order int not null,
	FOREIGN KEY (id_order) REFERENCES Orders(id) ON DELETE CASCADE
);


create table Bills
(
	id int primary key identity,
	date_create date DEFAULT getdate(),	
	total float  ,
	id_order int not null,	
	FOREIGN KEY (id_order) REFERENCES Orders(id) ON DELETE CASCADE	
);


-- create table Record
-- (
-- 	id serial primary key,
-- 	status varchar(255) not null,
-- 	id_bill int not null,
-- 	FOREIGN KEY (id_bill) REFERENCES Bill(id) ON DELETE CASCADE
-- );


