use thecoffeeson
go

INSERT INTO Stores (name ,phone ,email ,address )
    VALUES ('TheCoffeeSon','0777888999','javiSon@gmail.com',N'duong TruongSon phuong 2 quan TanBinh TP.HCM'),
	('TheCoffeeLuan','0777888999','javi1Luan@gmail.com',N'duong BaoLuan phuong 1 quan Govap TP.HCM'),
	('TheCoffeeHau','0777888999','javiHau2@gmail.com',N'duong ThanhHau phuong 3 quan BinhTan TP.HCM'),
	('TheCoffeeMinh','0777888999','javiMinh2@gmail.com',N'duong NhatMinh phuong 4 quan ThuDuc TP.HCM'),
	('TheCoffeeDat','0777888999','javiDat3@gmail.com',N'duong Dat phuong 6 quan 4 TP.HCM');

INSERT INTO Roles (name)
    VALUES ('ROLE_ADMIN'),
	('ROLE_CUSTOMER');

	
	
INSERT INTO accounts_info (name, phone, email,picture, birthday, gender,username,password,id_role,create_date)
    VALUES
	(N'vo danh 1','0011222333','nguyenvan1@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','dat','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',1,'2020-12-01'),	
	(N'vo danh 3','0011222333','nguyenvan3@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','emp2-testday','123',1,'2021-01-02'),
	(N'vo danh 4','0011222333','nguyenvan1@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','emp3-testday','123',1,'2021-02-03'),	
	(N'vo danh 5','0011222333','nguyenvan3@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','emp4-testday','123',1,'2021-02-04'),
	(N'vo danh 6','0011222333','nguyenvan3@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','emp5','123',1,'2021-02-04');

INSERT INTO accounts_info (name, phone, email,picture, birthday, gender,username,password,id_role)
    VALUES
	(N'vo danh 0','0011222333','nguyenvan0@gail.com','cus1.png','2021-01-04','0','hau','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me',2),	
	(N'vo danh 0','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','cus0','123',2),	
	(N'vo danh 2','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','cus1','123',2),
	(N'vo danh 7','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','1','cus2','123',2),
	(N'vo danh 8','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','1','cus3','123',2),	
	(N'vo danh 9','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','0','cus4','123',2),
	(N'vo danh 10','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','0','cus5','123',2),
	(N'vo danh 0','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','1','cus6','123',2),	
	(N'vo danh 2','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','0','cus7','123',2),
	(N'vo danh 7','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','1','cus8','123',2),
	(N'vo danh 8','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','cus9','123',2),	
	(N'vo danh 9','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','0','cus10','123',2),
	(N'vo danh 10','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','cus11','123',2),	
	(N'vo danh 2','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','cus12','123',2),
	(N'vo danh 7','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','cus13','123',2),
	(N'vo danh 8','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','1','cus14','123',2),	
	(N'vo danh 9','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','0','cus15','123',2),
	(N'vo danh 10','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','cus16','123',2),
	(N'vo danh 0','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','cus18','123',2),	
	(N'vo danh 2','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','cus19','123',2),
	(N'vo danh 7','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','1','cus20','123',2),
	(N'vo danh 8','0011222333','nguyenvan0@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-01-04','0','cus21','123',2),	
	(N'vo danh 9','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','1','cus22','123',2),
	(N'vo danh 10','0011222333','nguyenvan2@gail.com','https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg','2021-02-04','0','cus23','123',2);
	
INSERT INTO Categories (type)
    VALUES
	(N'Cafe'),
	(N'Trà sữa'),
	(N'Yaourt'),
	(N'Sữa'),
	(N'Thức uống khác');
	
INSERT INTO Products(name, image, price, id_categories)
VALUES (N'Bạc Xỉu','https://res.cloudinary.com/minhchon/image/upload/v1618853092/Thecoffeeson/Bacxiu_hhi5qi.png',28000,'1'),
	(N'Cà Phê Sữa','https://res.cloudinary.com/minhchon/image/upload/v1618852896/Thecoffeeson/Cafesua_ssatwd.png',30000,'1'),
	(N'Capuchino','https://res.cloudinary.com/minhchon/image/upload/v1618852787/Thecoffeeson/Capuchino_jljyti.png',35000,'1'),
	(N'Soda','https://res.cloudinary.com/minhchon/image/upload/v1618852194/Thecoffeeson/Soda_h3mqd8.png',35000,'1'),
	(N'Yaourt Dâu','https://res.cloudinary.com/minhchon/image/upload/v1618851325/Thecoffeeson/yaourtdau_bzmo9o.png',35000,'1'),
	(N'Nước Ép Thơm','https://res.cloudinary.com/minhchon/image/upload/v1618851267/Thecoffeeson/Nuocepthom_g2zirz.png',38000,'1'),
	(N'Nước Dưa Hấu','https://res.cloudinary.com/minhchon/image/upload/v1618851246/Thecoffeeson/Nuocduahau_ht4aop.png',45000,'1'),
	(N'Nước Ép Cam','https://res.cloudinary.com/minhchon/image/upload/v1618851116/Thecoffeeson/Nuoccam_qstmmp.png',45000,'1'),
	(N'Sinh Tố Xoài','https://res.cloudinary.com/minhchon/image/upload/v1618851068/Thecoffeeson/Xoaidaxay_clmdfs.png',48000,'1'),
	(N'Sinh Tố Việt Quất','https://res.cloudinary.com/minhchon/image/upload/v1618851022/Thecoffeeson/Vietquat_fyvtrv.png',48000,'1'),
	(N'Trà Vải','https://res.cloudinary.com/minhchon/image/upload/v1618850953/Thecoffeeson/Travaihoahong_b0dyif.png',28000,'2'),
	(N'Trà OLong Sen Vàng','https://res.cloudinary.com/minhchon/image/upload/v1618850895/Thecoffeeson/Traolongsenvang_dk8r5w.png',32000,'2'),
	(N'Trà Đào Cam Sả','https://res.cloudinary.com/minhchon/image/upload/v1618850862/Thecoffeeson/Tradaocamsa_farecy.png',32000,'2'),
	(N'Nước Chanh Dây','https://res.cloudinary.com/minhchon/image/upload/v1618850826/Thecoffeeson/Sodachanhday_c4dmvf.png',32000,'2'),
	(N'Sinh Tố Phúc Bồn Tử','https://res.cloudinary.com/minhchon/image/upload/v1618850792/Thecoffeeson/Phucbontu_zmonsd.png',32000,'2'),
	(N'Khoai Môn Cream Cheese','https://res.cloudinary.com/minhchon/image/upload/v1618850703/Thecoffeeson/Khoaimonkemcheese_sgmvnq.png',22000,'3'),
	(N'MatCha Cream Cheese','https://res.cloudinary.com/minhchon/image/upload/v1618850659/Thecoffeeson/Matchadaxay_usud29.png',32000,'3'),
	(N'Sữa Tươi Cà Phê','https://res.cloudinary.com/minhchon/image/upload/v1618850605/Thecoffeeson/Suatuoicafe_me3k91.png',34000,'3'),
	(N'Chocolate Cream Cheese','https://res.cloudinary.com/minhchon/image/upload/v1618850554/Thecoffeeson/Chocoladaxayphomai_t1pflu.png',35000,'3'),

	(N'Chocolate Đá Xay','https://res.cloudinary.com/minhchon/image/upload/v1617619692/Thecoffeeson/Chocolatedaxay_rwkilm.png',32000,'3'),
	(N'yaourt ca cao','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',38000,'3'),
	(N'sửa tươi bạc hà','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',32000,'4'),
	(N'sửa nóng','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',34000,'4'),
	(N'sửa đá','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',34000,'4'),
	(N'bạch xỉu nóng','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',32000,'4'),
	(N'bạch xỉu đá','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',32000,'4'),
	(N'sửa huế','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',36000,'4'),
	(N'Dừa tười','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',36000,'5'),
	(N'Soda Chanh đường','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',36000,'5'),
	(N'Soda Chanh muối','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',38000,'5'),
	(N'Soda Xí muội','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',38000,'5'),
	(N'Soda Chanh dây','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',38000,'5'),
	(N'Soda Cam','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',38000,'5'),
	(N'Cam vắt','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',40000,'5'),
	(N'Cam vắt mật ong','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',40000,'5'),
	(N'Cam vắt xi muội','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',40000,'5'),
	(N'Cam sữa','https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png',40000,'5'); 


	
	
INSERT INTO feedbacks (note,rate,id_cus,id_product)
    VALUES
	(N'coffee nay ...',4,1,9),
	(N'coffee nay ...',4,3,1),
	(N'coffee nay ...',4,2,3),
	(N'coffee nay ...',4,4,4),
	(N'coffee nay ...',4,1,4),
	(N'coffee nay ...',4,1,1),	
	(N'coffee nay ...',4,1,12);
	
INSERT INTO Sizes (Product_size,Product_price )
    VALUES
	('S',1.0),
	('M',1.2),
	('L',1.25),
	('XL',1.3);
	
INSERT INTO Vouchers ( value ,start_time ,end_time ,quanity )
    VALUES
	(50000,'2020-12-04','2021-01-03',30),
	(75000,'2020-11-04','2021-02-03',30),
	(60000,'2020-12-04','2021-03-03',30),
	(30000,'2020-09-04','2021-01-03',30),
	(20000,'2020-10-04','2021-01-03',30),
	(10000,'2020-02-04','2021-01-03',30);

INSERT INTO orders (date_created,id_cus)
    VALUES
	('2020-01-04',1),
	('2020-02-04',2),
	('2020-03-04',3),
	('2020-04-04',4),
	('2020-05-04',5),
	('2020-06-04',6),
	('2020-07-04',7),
	('2020-08-04',8),
	('2020-09-04',9),
	('2020-10-04',10),
	('2020-11-04',13),
	('2020-12-04',11),
	('2021-01-04',12),
	('2021-02-04',13),
	('2021-03-04',14),
	('2021-04-04',15);
	
INSERT INTO orders_detail (amount,id_product,id_order)
    VALUES
	(3,2,1),
	(5,1,1),
	(1,3,1),
	(2,4,1),
	(5,9,2),
	(1,6,2),
	(2,7,2),
	(5,11,3),
	(1,6,3),
	(2,7,3),
	(5,11,4),
	(1,26,4),	
	(2,17,4),
	(5,1,5),
	(1,22,5),	
	(2,3,6),
	(1,4,6),
	(2,6,7),
	(3,7,7),	
	(6,5,8),
	(1,8,8),
	(7,9,9),	
	(2,10,9),
	(5,11,10),
	(1,12,10),	
	(2,13,10),
	(1,14,11),
	(1,15,11),	
	(4,16,11),
	(2,17,12),
	(3,18,12),	
	(4,19,12),
	(3,20,13),
	(3,21,13),	
	(1,22,13),
	(1,23,14),
	(2,24,14),	
	(2,25,14),
	(6,1,15),
	(2,24,15),	
	(2,3,15),
	(1,10,16),
	(1,24,16),	
	(1,13,16)
	;

INSERT INTO ships_info (name_cus ,address ,	phone_cus ,	note ,price ,id_order )
    VALUES
	(N'nguyen thi nhan hang 0','000 duong1 quan1','0111222333','tran nhan hang 1',15000,1),
	(N'nguyen thi nhan hang 1','001 duong1 quan1','0111222322','tran nhan hang 1',15000,2),
	(N'nguyen thi nhan hang 2','002 duong1 quan2','0111222311','tran nhan hang 1',15000,3),
	(N'nguyen thi nhan hang 3','003 duong1 quan3','0111222344','tran nhan hang 1',15000,4),
	(N'nguyen thi nhan hang 4','004 duong1 quan4','0111222355','tran nhan hang 1',15000,5),
	(N'nguyen thi nhan hang 0','000 duong1 quan1','0111222333','tran nhan hang 1',15000,6),
	(N'nguyen thi nhan hang 1','001 duong1 quan1','0111222322','tran nhan hang 1',15000,7),
	(N'nguyen thi nhan hang 2','002 duong1 quan2','0111222311','tran nhan hang 1',15000,8),
	(N'nguyen thi nhan hang 3','003 duong1 quan3','0111222344','tran nhan hang 1',15000,9),
	(N'nguyen thi nhan hang 4','004 duong1 quan4','0111222355','tran nhan hang 1',15000,10),
	(N'nguyen thi nhan hang 0','000 duong1 quan1','0111222333','tran nhan hang 1',15000,11),
	(N'nguyen thi nhan hang 1','001 duong1 quan1','0111222322','tran nhan hang 1',15000,12),
	(N'nguyen thi nhan hang 2','002 duong1 quan2','0111222311','tran nhan hang 1',15000,13),
	(N'nguyen thi nhan hang 3','003 duong1 quan3','0111222344','tran nhan hang 1',15000,14),
	(N'nguyen thi nhan hang 3','003 duong1 quan3','0111222344','tran nhan hang 1',15000,15),
	(N'nguyen thi nhan hang 4','004 duong1 quan4','0111222355','tran nhan hang 1',15000,16);
	
	
INSERT INTO Orders_status (status,comment,id_order)
    VALUES
	('succsec',N'Chờ vận chuyển',1),
	('onboard',N'Chờ vận chuyển',2),
	('succsec',N'Chờ vận chuyển',3),
	('fail',N'Chờ vận chuyển',4),
	('succsec',N'Chờ vận chuyển',5),
	('succsec',N'Chờ vận chuyển',6),
	('succsec',N'Chờ vận chuyển',7),
	('succsec',N'Chờ vận chuyển',8),
	('fail',N'Chờ vận chuyển',9),
	('refund',N'Chờ vận chuyển',10),
	('succsec',N'Chờ vận chuyển',11),
	('onboard',N'Chờ vận chuyển',12),
	('succsec',N'Chờ vận chuyển',13),
	('succsec',N'Chờ vận chuyển',14),
	('succsec',N'Chờ vận chuyển',15),	
	('refund',N'Chờ vận chuyển',16);

	
INSERT INTO bills (	date_create ,id_order)
    VALUES
	('2020-01-04',1),
	('2020-02-04',2),
	('2020-03-04',3),
	('2020-04-04',4),
	('2020-05-04',5),
	('2020-06-04',6),
	('2020-07-04',7),
	('2020-08-04',8),
	('2020-09-04',9),
	('2020-10-04',10),
	('2020-11-04',11),
	('2020-12-04',12),
	('2021-01-04',13),
	('2021-02-03',14),
	('2021-03-04',15),
	('2021-04-04',16);


	
	
-- 	select* , concat(key, id) as new  from customers 
-- 	select * from customers;
-- 	select * from accounts_info
-- 	inner join customers on accounts_info.id_cus = customers.id;