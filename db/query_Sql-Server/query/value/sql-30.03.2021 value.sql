use thecoffeeson
go

INSERT INTO Stores (name ,phone ,email ,address )
    VALUES ('TheCoffeeSon','0777888999','javiSon@gmail.com','duong TruongSon phuong 2 quan TanBinh TP.HCM'),
	('TheCoffeeLuan','0777888999','javi1Luan@gmail.com','duong BaoLuan phuong 1 quan Govap TP.HCM'),
	('TheCoffeeHau','0777888999','javiHau2@gmail.com','duong ThanhHau phuong 3 quan BinhTan TP.HCM'),
	('TheCoffeeMinh','0777888999','javiMinh2@gmail.com','duong NhatMinh phuong 4 quan ThuDuc TP.HCM'),
	('TheCoffeeDat','0777888999','javiDat3@gmail.com','duong Dat phuong 6 quan 4 TP.HCM');


INSERT INTO employees (username,password)
    VALUES('dat','$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me'),
	('emp1','123'),
	('emp2','123'),
	('emp3','123'),
	('emp4','123');
go
INSERT INTO employees (username,password,start_date)
    VALUES('emp0-testday','123','2020-12-01'),
	('emp1-testday','123','2021-01-01'),
	('emp2-testday','123','2021-01-02'),
	('emp3-testday','123','2021-02-03'),
	('emp4-testday','123','2021-02-04');
go

INSERT INTO customers (username,password)
    VALUES('cus0','123'),
	('cus1','123'),
	('cus2','123'),
	('cus3','123'),
	('cus4','123'),
	('cus5','123');
	
	
INSERT INTO accounts_info (name, phone, email,picture, birthday, gender,id_emp)
    VALUES
	('vo danh 1','0011222333','nguyenvan1@gail.com','emp1.png','2021-01-04','0','1'),	
	('vo danh 3','0011222333','nguyenvan3@gail.com','emp2.png','2021-02-04','0','2'),
	('vo danh 4','0011222333','nguyenvan1@gail.com','emp3.png','2021-01-04','0','3'),	
	('vo danh 5','0011222333','nguyenvan3@gail.com','emp4.png','2021-02-04','0','4'),
	('vo danh 6','0011222333','nguyenvan3@gail.com','emp5.png','2021-02-04','0','5');

INSERT INTO accounts_info (name, phone, email,picture, birthday, gender,id_cus)
    VALUES
	('vo danh 0','0011222333','nguyenvan0@gail.com','cus1.png','2021-01-04','0','1'),	
	('vo danh 2','0011222333','nguyenvan2@gail.com','cus2.png','2021-02-04','0','2'),
	('vo danh 7','0011222333','nguyenvan0@gail.com','cus1.png','2021-01-04','0','3'),
	('vo danh 8','0011222333','nguyenvan0@gail.com','cus1.png','2021-01-04','0','4'),	
	('vo danh 9','0011222333','nguyenvan2@gail.com','cus2.png','2021-02-04','0','5');
	
INSERT INTO Categories (type)
    VALUES
	(N'Cafe'),
	(N'Trà sữa'),
	(N'yaourt'),
	(N'sữa'),
	(N'thức uống khác');
	
INSERT INTO Products(name, image, price, id_categories)
VALUES (N'Cà phê đá','ca-phe-da.jpeg',28000,'1'),
	(N'Cà Phê Nóng','ca-phe-nong.jpeg',30000,'1'),
	(N'Cà Phê Sữa Đá','ca-phe-sua-da.jpeg',35000,'1'),
	(N'Cà Phê Sữa Nóng','ca-phe-sua-nong.jpeg',35000,'1'),
	(N'Cà Phê Rhum','ca-phe-Rhum.jpeg',35000,'1'),
	(N'Cà Phê Sữa Rhum','ca-phe-sua-Rhum.jpeg',38000,'1'),
	(N'Cà Phê Bailey','ca-phe-Bailey.jpeg',45000,'1'),
	(N'Cà Phê Đá kem','ca-phe-da-kem.jpeg',45000,'1'),
	(N'Cà Phê Sữa Baileys','ca-phe-sua-bailey.jpeg',48000,'1'),
	(N'Cà Phê Sữa kem','ca-phe-sua-kem.jpeg',48000,'1'),
	(N'Trà Sữa kem','tra-sua.jpeg',28000,'2'),
	(N'Trà Sữa dâu','tra-sua-dau.jpeg',32000,'2'),
	(N'Trà Sữa bạc hà','tra-sua-bac-ha.jpeg',32000,'2'),
	(N'Trà Sữa sâm dứa','tra-sua-sam-dua.jpeg',32000,'2'),
	(N'Trà Sữa cam','tra-sua-cam.jpeg',32000,'2'),
	(N'yaourt nguyên chất','yaourt-nguyen-chat.jpeg',22000,'3'),
	(N'yaourt đá','yaourt-nguyen-chat.jpeg',32000,'3'),
	(N'yaourt cà phê','yaourt-ca-phet.jpeg',34000,'3'),
	(N'yaourt bạc hà','yaourt-bac-ha.jpeg',35000,'3'),
	(N'yaourt thạch','yaourt-thach.jpeg',32000,'3'),
	(N'yaourt ca cao','yaourt-ca-cao.jpeg',38000,'3'),
	(N'sửa tươi bạc hà','sua-tuoi-bac-ha.jpeg',32000,'4'),
	(N'sửa nóng','sua-tuoi-nong.jpeg',34000,'4'),
	(N'sửa đá','sua-tuoi-nong.jpeg',34000,'4'),
	(N'bạch xỉu nóng','sua-tuoi-nong.jpeg',32000,'4'),
	(N'bạch xỉu đá','sua-tuoi-nong.jpeg',32000,'4'),
	(N'sửa huế','sua-tuoi-nong.jpeg',36000,'4'),
	(N'Dừa tười','dua-tuoi.jpeg',36000,'5'),
	(N'Soda Chanh đường','soda-chanh-duong.jpeg',36000,'5'),
	(N'Soda Chanh muối','soda-chanh-muoi.jpeg',38000,'5'),
	(N'Soda Xí muội','soda-xi-muoi.jpeg',38000,'5'),
	(N'Soda Chanh dây','soda-chanh.jpeg',38000,'5'),
	(N'Soda Cam','soda-cam.jpeg',38000,'5'),
	(N'Cam vắt','cam-vat.jpeg',40000,'5'),
	(N'Cam vắt mật ong','cam-vat-mat-ong.jpeg',40000,'5'),
	(N'Cam vắt xi muội','cam-vat-xi-muoi.jpeg',40000,'5'),
	(N'Cam sữa','cam-sua.jpeg',40000,'5'); 

	
	
INSERT INTO feedbacks (note,rate,id_cus,id_product)
    VALUES('coffee nay ...',4,1,9),
	('coffee nay ...',4,3,1),
	('coffee nay ...',4,2,3),
	('coffee nay ...',4,4,4),
	('coffee nay ...',4,1,4),
	('coffee nay ...',4,1,1),	
	('coffee nay ...',4,1,12);
	
INSERT INTO Sizes (Product_size,Product_price )
    VALUES('Size_S',1.0),
	('Size_M',1.2),
	('Size_L',1.25),
	('Size_XL',1.3);
	
INSERT INTO Vouchers ( value ,start_time ,end_time ,quanity )
    VALUES (50000,'2020-12-04','2021-01-03',30),
	(75000,'2020-11-04','2021-01-03',30),
	(60000,'2020-12-04','2021-01-03',30),
	(30000,'2020-09-04','2021-01-03',30),
	(20000,'2020-10-04','2021-01-03',30),
	(10000,'2020-02-04','2021-01-03',30);

INSERT INTO orders (date_created,id_cus,id_emp)
    VALUES('11/11/2021',1,1),
	('11/11/2021',2,1),
	('11/11/2021',3,1),
	('11/11/2021',3,1),
	('11/11/2021',4,1);
	
INSERT INTO orders_detail (amount,id_product,id_order)
    VALUES(3,2,1),
	(5,1,1),
	(1,3,1),
	(2,4,1),
	(5,9,2),
	(1,6,2),
	(2,7,2),
	(5,11,3),
	(1,6,3),
	(2,7,3),
	(5,11,4),
	(1,26,4),
	(2,17,4);

INSERT INTO ships_info (name_cus ,address ,	phone_cus ,	note ,price ,id_order )
    VALUES('nguyen thi nhan hang 0','000 duong1 quan1','0111222333','tran nhan hang 1',15000,1),
	('nguyen thi nhan hang 1','001 duong1 quan1','0111222322','tran nhan hang 1',15000,2),
	('nguyen thi nhan hang 2','002 duong1 quan2','0111222311','tran nhan hang 1',15000,3),
	('nguyen thi nhan hang 3','003 duong1 quan3','0111222344','tran nhan hang 1',15000,4),
	('nguyen thi nhan hang 4','004 duong1 quan4','0111222355','tran nhan hang 1',15000,5);
	
	
INSERT INTO Orders_status (status,comment,id_order)
    VALUES('succsec','cho van chuyn',1),
	('onboard','cho van chuyn',2),
	('succsec','cho van chuyn',3),
	('fail','cho van chuyn',4),
	('refund','cho van chuyn',5);

	
INSERT INTO bills (	date_create ,id_order)
    VALUES('10/09/2021',1),
	('11/09/2021',2),
	('12/09/2021',3),
	('11/09/2021',4),
	('03/09/2021',5);
	
	
-- 	select* , concat(key, id) as new  from customers 
-- 	select * from customers;
-- 	select * from accounts_info
-- 	inner join customers on accounts_info.id_cus = customers.id;