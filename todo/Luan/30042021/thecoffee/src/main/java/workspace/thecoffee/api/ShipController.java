package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Ships_Info;
import workspace.thecoffee.service.ShipService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api")
@RestController
public class ShipController {
    private final ShipService shipService;

    @Autowired
    public ShipController(ShipService shipService){
        this.shipService= shipService; 
    }

    // lấy toàn bộ thông tin ship trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/admin/ship
    OutPut Data:
     [ship_info {
        "id": "value",
        "name_cus": "value",
        "address": "value",
        "phone_cus": "value",
        "note": "value",
        "price": "value",
        "orders": "value"
    },
    ship_info{},..
    ship_info{}
    ]
    */
    @GetMapping("/admin/ship")
    public List<Ships_Info> getAllShip(){
       return shipService.getAllShip();
    }

    
// tìm ship_info theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/ship/{id} 
    OutPut Data:
     ship_info{
        "id": "[id]",
        "name_cus": "value",
        "address": "value",
        "phone_cus": "value",
        "note": "value",
        "price": "value",
        "orders": "value"
    }
    */
    @GetMapping("/admin/ship/{id}")
    public Ships_Info  getShipById(@PathVariable("id") Integer id){
        return shipService.getShipById(id).orElse(null);
    }

// thêm một ship_info vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/ship/{body} 
    Input Data:
     body{        
        "name_cus": "newvalues",
        "address": "newvalues",
        "phone_cus": "newvalues",
        "note": "newvalues",
        "price": "newvalues",
        "orders": "values"
    }
    */
    // @PostMapping ("/user/ship")
    // public void addShip(@RequestBody Ships_Info shipinf){
    //     shipService.addShip(shipinf);
    // }

 //xóa một ship_info khỏi hệ thống Input Delete URL= http://localhost:8081/api/ship/{id} 
     
    @DeleteMapping("/admin/ship/{id}")
    public void deleteShip(@PathVariable("id") Integer id){
        shipService.deleteShip(id);
    }

// chính sửa thông tin ship_info theo id 
    /*
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng 
    Input Put URL= http://localhost:8081/api/ship/{id} 
    Input Data:
         ship_info{
        "id": "[id]",
        "name_cus": "repvalue",
        "address": "repvalue",
        "phone_cus": "repvalue",
        "note": "repvalue",
        "price": "repvalue",
        "orders": "value"
    }
    OutPut data 
             ship_info{
        "id": "[id]",
        "name_cus": "repvalue",
        "address": "repvalue",
        "phone_cus": "repvalue",
        "note": "repvalue",
        "price": "repvalue",
        "orders": "value"
    } 
    */
    @PutMapping("/admin/ship/{id}")
    public void updateShip(@PathVariable("id") Integer id,@RequestBody Ships_Info newship){
        shipService.updateShip(id,newship);
    }
}
