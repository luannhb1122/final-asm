package workspace.thecoffee.service;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.OrderDao;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Orders_Status;

@Service
public class OrderService {
    private final OrderDao orderDao;
    private final BillService billService;
    private final Order_DetailService order_DetailService;
    private final Order_StatusService order_StatusService;
    private final VoucherService voucherService;
    private final ShipService shipService;

    

    @Autowired
    public OrderService(ShipService shipService,OrderDao orderDao,Order_StatusService order_StatusService, BillService billService,Order_DetailService order_DetailService,VoucherService voucherService) {
        this.orderDao = orderDao;
        this.billService= billService;
        this.order_DetailService= order_DetailService;
        this.order_StatusService= order_StatusService; 
        this.voucherService= voucherService  ;
        this.shipService= shipService;
        
    }

    int id =0;
    List<Orders> newList = new ArrayList<Orders>();
    List<Orders> allList = new ArrayList<Orders>();
    public List<Orders> getAllOrder() {
        return orderDao.findAll();
    } 

    public List<Orders> get20Order() {       
       //  odList.add(orderDao.searchOrders(id));
        // boolean check = false;
        // allList.addAll(orderDao.findAll()) ;
       
            newList.addAll(orderDao.searchOrders(id));            
            // check =true;
            id+=50;
            return newList;
      
            
        
       
    } 

    public List<Orders> findOrderByCustomer(int id) {
        return orderDao.findOrderByCustomer(id);
    } 

    public Optional<Orders> findMaxId() {
        return orderDao.findMaxId();
    }
    public void addOrder(Orders od){
        Date date = new Date();//get lay thoi gian hien tai
        od.setDate_created(date);
        orderDao.save(od);
        Optional<Orders> typeOrdersData = findMaxId();
        Orders oldOd = typeOrdersData.get();
        // Orders_Status data =  od.getOrder_Status();
        if(od.getOrders_Detail()!=null ){ order_DetailService.addOrder_Detailnull(oldOd, od.getOrders_Detail());}       
        if(od.getShip_Info()!=null ){ shipService.addShip(oldOd,od.getShip_Info());}       
        order_StatusService.addOrders_Status(oldOd);            
        if(od.getVouchers()!=null){ voucherService.checkVourcher(od.getVouchers().getId());}
          //  billService.addBill();
    }

    // public void addOrder(Orders od){
    //     orderDao.save(new Orders(
    //                             od.getDate_created(),
    //                             od.getPayment(),
    //                             od.getCustomers(),
    //                             od.getEmployees(),
    //                             od.getVouchers()                               
    //                             )
    //                 );
                    
    // }
    public Optional<Orders> getOrderById(Integer id) {
        return orderDao.findById(id);
    }

    public void deleteOrder(Integer id) {
        orderDao.deleteById(id);
    }

    public void updateOrder(Integer id, Orders newod){
        Optional<Orders> typeOrdersData = getOrderById(id);
        Orders oldOd = typeOrdersData.get();
        oldOd.setDate_created(newod.getDate_created());
        oldOd.setPayment(newod.getPayment());
        oldOd.setCustomers(newod.getCustomers());        
        oldOd.setVouchers(newod.getVouchers());
        oldOd.setOrders_Detail(newod.getOrders_Detail());       
        orderDao.save(oldOd);
    }



}
