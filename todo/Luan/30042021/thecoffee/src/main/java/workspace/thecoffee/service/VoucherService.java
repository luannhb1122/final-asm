package workspace.thecoffee.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.VoucherDao;
import workspace.thecoffee.model.Vouchers;

@Service
public class VoucherService {
    private final VoucherDao voucherDao;
    @Autowired
    public VoucherService(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }
// lấy tất cả voucher
    public List<Vouchers> getAllVoucher() {
        return voucherDao.findAll();
    }
// lấy voucher còn sử dụng được
    public List<Vouchers> VoucherUser() {
        return voucherDao.VoucherUser();
    }
// lấy voucher theo id
    public Optional<Vouchers> getVoucherById(Integer id) {
        return voucherDao.findById(id);
    }

    public void addVoucher(Vouchers vou) {
        Date date = new Date();
        /* voucherDao.save(new Vouchers(           
        //     vou.getStatus(),
        //     vou.getValue(),          
        //     vou.getStart_time(),
        //     vou.getEnd_time(),
        //     vou.getQuanity()
           ));
          */ 
        System.out.println( vou.getStart_time());
       
        voucherDao.save(vou);
   
    }

    public void updatVoucher(Integer id, Vouchers newVou){
        Optional<Vouchers> VoucherData = getVoucherById(id);
        Vouchers oldVou = VoucherData.get();
        oldVou.setStatus(newVou.getStatus());
        oldVou.setValue(newVou.getValue());
        oldVou.setStart_time(newVou.getStart_time());
        oldVou.setEnd_time(newVou.getEnd_time());
        oldVou.setQuanity(newVou.getQuanity());
        voucherDao.save(oldVou);
    }
    public void checkVourcher (int id){
        Optional<Vouchers> VoucherData = getVoucherById(id);
        Vouchers oldVou = VoucherData.get();
        int oldQuanlity =oldVou.getQuanity() ;
        
        Date dateVou = oldVou.getEnd_time();
        Date today =new Date();
        if (dateVou.compareTo(today) > 0){falseStatusVoucher(id);} //dateVou < today thì dateVou.compareTo(today)=-1
        if (oldQuanlity == 0){falseStatusVoucher(id);}
        else{
            
            oldVou.setQuanity(oldQuanlity);
            voucherDao.save(oldVou);}
       
        
    }
    // public void checkTrueVourcher (int id){
    //     Optional<Vouchers> VoucherData = getVoucherById(id);
    //     Vouchers oldVou = VoucherData.get();
    //     int oldQuanlity =oldVou.getQuanity() ;
      
        
    // }
    

    public void falseStatusVoucher(Integer id){
        Optional<Vouchers> VoucherData = getVoucherById(id);
        Vouchers oldVou = VoucherData.get();
        oldVou.setStatus(false);
    }
    public void deleteVoucher(Integer id) {
        voucherDao.deleteById(id);
    }

}
