package workspace.thecoffee.service;

import workspace.thecoffee.dao.SizeDao;
import workspace.thecoffee.model.Sizes;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SizeService {
    private final SizeDao sizeDao;
    @Autowired
    public SizeService(SizeDao sizeDao) {
        this.sizeDao = sizeDao;
    }

    public List<Sizes> getAllSize() {
        return sizeDao.findAll();
    }

    public Optional<Sizes> getSizeById(Integer id) {
        return sizeDao.findById(id);
    }

    public void addSize(Sizes size) {
        sizeDao.save(new Sizes(           
            size.getType(),
            size.getPrice(),
            size.getStatus()
            ));
    }

    public void updatSize(Integer id, Sizes newSize){
        Optional<Sizes> sizeData = getSizeById(id);
        Sizes oldSizes = sizeData.get();
        oldSizes.setType(newSize.getType());
        oldSizes.setPrice(newSize.getPrice());
        oldSizes.setStatus(newSize.getStatus());
        sizeDao.save(oldSizes);
    }

    public void deleteSize(Integer id) {
        Optional<Sizes> sizeData = getSizeById(id);
        Sizes oldSizes = sizeData.get();
        oldSizes.setStatus(false);
       // sizeDao.deleteById(id);
    }

}
