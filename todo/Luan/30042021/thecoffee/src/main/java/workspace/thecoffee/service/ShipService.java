package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.ShipDao;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Ships_Info;

@Service
public class ShipService {
    private final ShipDao shipDao;
    @Autowired
    public ShipService(ShipDao shipDao) {
        this.shipDao = shipDao;
    }

    public List<Ships_Info> getAllShip() {
        return shipDao.findAll();
    }

    public Optional<Ships_Info> getShipById(Integer id) {
        return shipDao.findById(id);
    }

    public Optional<Ships_Info> findShipByOrder(Integer id) {
        return shipDao.findShipByOrder(id);
    }


    public void addShip(Orders shipInf,Ships_Info ship) {
        Ships_Info newship = ship;   
         newship.setOrders(shipInf);
        shipDao.save(newship)
            ;
    }

    public void updateShip(Integer id, Ships_Info newShipInf){
        Optional<Ships_Info> shipsData = getShipById(id);
        Ships_Info oldship = shipsData.get();
        oldship.setName_cus(newShipInf.getName_cus());
        oldship.setAddress(newShipInf.getAddress());
        oldship.setPhone_cus(newShipInf.getPhone_cus());
        oldship.setNote(newShipInf.getNote());
        oldship.setPrice(newShipInf.getPrice());
        oldship.setOrders(newShipInf.getOrders());
        shipDao.save(oldship);
    }

    public void deleteShip(Integer id) {        
        shipDao.deleteById(id);
    }

}
