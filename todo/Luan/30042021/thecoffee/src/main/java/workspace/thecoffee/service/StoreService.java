package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.StoreDao;
import workspace.thecoffee.model.Stores;

@Service
public class StoreService {
    private final StoreDao storeDao;
    @Autowired
    public StoreService(StoreDao storeDao) {
        this.storeDao = storeDao;
    }

    public List<Stores> getAllStore() {
        return storeDao.findAll();
    }

    public Optional<Stores> getStoreById(Integer id) {
        return storeDao.findById(id);
    }

    public void addStore(Stores stores) {
        storeDao.save(new Stores(           
            stores.getName(),
            stores.getPhone(),
            stores.getEmail(),
            stores.getAddress()
            ));
    }

    public void updateStore(Integer id, Stores newStores){
        Optional<Stores> storesData = getStoreById(id);
        Stores oldstores = storesData.get();
        oldstores.setName(newStores.getName());
        oldstores.setPhone(newStores.getPhone());
        oldstores.setEmail(newStores.getEmail());
        oldstores.setAddress(newStores.getAddress());
        storeDao.save(oldstores);
    }

    public void deleteStore(Integer id) {
        storeDao.deleteById(id);
    }

}
