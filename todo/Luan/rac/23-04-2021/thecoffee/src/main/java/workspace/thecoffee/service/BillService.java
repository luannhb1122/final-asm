package workspace.thecoffee.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.util.ArrayBuilders.IntBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.BillDao;
import workspace.thecoffee.dao.OrderDao;
import workspace.thecoffee.model.Bills;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Orders_Detail;
import workspace.thecoffee.model.Ships_Info;

@Service
public class BillService {
    private final BillDao billDao;
    private final OrderDao orderDao;
    private final ShipService shipService;
    
    public Date getCurrentTimeUsingDate(Date date) {
       
        String strDateFormat = "dd-mm-yyyy";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        System.out.println("Current time of the day using Date - 12 hour format: " + date+"  "+formattedDate);
        return date;
    }
    @Autowired
    public BillService(BillDao billDao,OrderDao orderDao,ShipService shipService ) {
        this.billDao = billDao;
        this.orderDao = orderDao;
        this.shipService= shipService;
    }
 
    public List<Bills> getAllBill() {        
        return billDao.findAllBill();

    }

    public Optional<Bills> getBillById(Integer id) {
        return billDao.findById(id);
    }

    public float getCostShip(int id){
        Optional<Ships_Info> shipsData = shipService.findShipByOrder(id);
        Ships_Info oldship = shipsData.get();
       return oldship.getPrice();
    }
    
    public Float sumBill(Orders od){
      Orders odDB =  od;      
     
      Float sum = (float) 0 ;
      for (Orders_Detail orders_Detail:  odDB.getOrders_Detail()) {
        int amount= orders_Detail.getAmount();
        Float price = (Float)orders_Detail.getProducts().getPrice();
        Float sum1 = (price*amount);
        sum += sum1;
      }
      float discount =0;
      if(od.getVouchers()!=null){
         discount =  od.getVouchers().getValue();
      }
      System.out.println(discount);
      float shipCost =  getCostShip(od.getId());
     // float costShip = od.
   //   return sum-discount+shipCost;
   System.out.println(sum-discount+shipCost);
       return sum-discount+shipCost;
    }

    // public void addBill() {
    //     Bills bill = new Bills();
    //     bill.setDate_create(getCurrentTimeUsingDate());
    //     bill.setTotal(sumBill());
    //     bill.setOrders(orderDao.findMaxId().get());
    //     billDao.save(bill); 
    //     // billDao.save(new Bills(           
    //     //     bill.getDate_create(),
    //     //     bill.getTotal(),
    //     //     bill.getOrders()
    //     //     ));
    // }

    public void addNewBill(Orders od){
        Bills bill = new Bills();
        Date date = new Date();
        bill.setDate_create(date);
        bill.setTotal(sumBill(od));
        bill.setOrders(od);
        billDao.save(bill); 
    }

    public void addBillTest(Bills bill) {
        Date date = new Date();
        bill.setDate_create(date);
        bill.setOrders(orderDao.findMaxId().get());
        billDao.save(bill); 
        // billDao.save(new Bills(           
        //     bill.getDate_create(),
        //     bill.getTotal(),
        //     bill.getOrders()
        //     ));
    }
    public void updatBill(Integer id, Bills newBills){
        Optional<Bills> billsData = getBillById(id);
        Bills oldBills = billsData.get();
        oldBills.setDate_create(newBills.getDate_create());
        oldBills.setTotal(sumBill(oldBills.getOrders()));
        oldBills.setOrders(newBills.getOrders());
        billDao.save(oldBills);
    }

    public void deleteBill(Integer id) {
        billDao.deleteById(id);
    }


}
