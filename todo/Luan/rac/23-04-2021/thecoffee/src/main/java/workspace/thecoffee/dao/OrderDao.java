package workspace.thecoffee.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Orders;

public interface OrderDao extends JpaRepository<Orders, Integer> {
    @Query(value = "SELECT TOP 1 * from Orders Order by id desc", nativeQuery = true)
    public java.util.Optional<Orders> findMaxId();

    @Query(value = "exec findOrderByCustomer @id_cus= :id_cus", nativeQuery = true)
    public java.util.List<Orders> findOrderByCustomer(@Param("id_cus") int id_cus);
}
