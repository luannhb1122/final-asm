package workspace.thecoffee.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Feedbacks;

public interface FeedbackDao extends JpaRepository<Feedbacks,Integer> {
    @Query(value = "exec findFeedbacksbyProduct @id=:id_pr", nativeQuery = true)
    public List<Feedbacks> findFeedbacksbyProduct(@Param("id_pr") int id_pr);
}
