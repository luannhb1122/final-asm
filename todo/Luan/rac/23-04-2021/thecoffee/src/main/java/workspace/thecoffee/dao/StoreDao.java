package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Stores;

public interface StoreDao extends JpaRepository<Stores,Integer>{
    
}
