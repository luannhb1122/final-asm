package workspace.thecoffee.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "Categories")
public class Categories {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "Type")
    private String type;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy="categories")
	List<Products> products;

    public Categories() {

	}
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Boolean getStatus() {
        return status;
    }
    public void setStatus(Boolean status) {
        this.status = status;
    }
    public Categories(String type, Boolean status) {
        this.type = type;
        this.status = status;
    }
}

