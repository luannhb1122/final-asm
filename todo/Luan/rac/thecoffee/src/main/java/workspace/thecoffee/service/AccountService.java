package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.AccountDao;
import workspace.thecoffee.model.Accounts_Info;


@Service
public class AccountService {
    private final AccountDao accountDao;
    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public List<Accounts_Info> getAllAccount() {
        return accountDao.findAll();
    }

    public List<Accounts_Info> getAllEmp() {
        return accountDao.searchAllEmp();
    }

    public List<Accounts_Info> getAllCus() {
        return accountDao.searchAllCus();
    }
    // public void addAccount(Accounts_Info account) {
    //     accountDao.save(new Accounts_Info
    //             (   account.getName(),
    //                 account.getPhone(), 
    //                 account.getEmail(),
    //                 account.getBirthday(), 
    //                 account.getGender(),
    //                 account.getPicture()          
                    
    //             )
    //         );
    // }


    public void addAccountNull(){
         accountDao.save(new Accounts_Info(null,null,null,null,null,null));
    }

    public void addAccount(Accounts_Info account) {
            accountDao.save(account );
        }
    public Optional<Accounts_Info> getAccountById(Integer id) {
        return accountDao.findById(id);
    }

    public  Optional<Accounts_Info> findMaxId() {
        return accountDao.findMaxId();
    }

    public void deleteAccount(Integer id) {
        accountDao.deleteById(id);
    }
     
    // public void CusaddAccount(Accounts_Info cus){
    //     Optional<Accounts_Info> typeAccountData = findMaxId();
    //     Accounts_Info oldAccount = typeAccountData.get();
    //     System.out.println(oldAccount);          
    //     oldAccount.setCustomers(cus);       
    //     accountDao.save(oldAccount);
    // }

    // public void EmpaddAccount(Employees emp){
    //     addAccountNull();
    //     Optional<Accounts_Info> typeAccountData = findMaxId();
    //     Accounts_Info oldAccount = typeAccountData.get();
    //     System.out.println(oldAccount);          
    //     oldAccount.setEmployees(emp);       
    //     accountDao.save(oldAccount);
    // }
    
    public void updateAccount(Integer id, Accounts_Info newAccount){
        Optional<Accounts_Info> typeAccountData = getAccountById(id);
        Accounts_Info oldAccount = typeAccountData.get();
        System.out.println(oldAccount);  
        oldAccount.setName(newAccount.getName());
        oldAccount.setPhone(newAccount.getPhone());
        oldAccount.setEmail(newAccount.getEmail());
        oldAccount.setBirthday(newAccount.getBirthday());
        oldAccount.setGender(newAccount.getGender());
        oldAccount.setPicture(newAccount.getPicture());       
        accountDao.save(oldAccount);
    }

}
