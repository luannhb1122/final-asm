package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders_Detail;
import workspace.thecoffee.service.Order_DetailService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/order_detail")
@RestController
public class Order_DetailController {
    private final Order_DetailService order_DetailService;

    @Autowired
    public Order_DetailController(Order_DetailService order_DetailService){
        this.order_DetailService= order_DetailService; 
    }

    @GetMapping
    public List<Orders_Detail> getAllOrder_Detail(){
       return order_DetailService.getAllOrder_Detail();
    }
    
    // @GetMapping(path ="{id}" )
    // public Orders_Detail getOrderDetailById (@PathVariable("id") Integer id){
    //     return order_DetailService.getOrder_DetailById(id).orElse(null);
    // }

    @GetMapping(path ="{id}" )
    public List<Orders_Detail> findByIdOrder (@PathVariable("id") Integer id){
        return order_DetailService.findByIdOrder(id);
    }

    @PostMapping
    public void addOrderlD (@RequestBody Orders_Detail odt){
        order_DetailService.addOrder_Detail(odt);
    }

    @DeleteMapping(path ="{id}" )
    public void deleteOrderD(@PathVariable("id") Integer id){
        order_DetailService.deleteOrder_Detail(id);
    }

    @PutMapping(path="{id}")
    public void updateOrderD(@PathVariable("id") Integer id,@RequestBody Orders_Detail newodt){
        order_DetailService.updateOrder_Detail(id, newodt);
    }
}
