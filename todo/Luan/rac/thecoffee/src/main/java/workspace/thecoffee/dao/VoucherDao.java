package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Vouchers;

public interface VoucherDao extends JpaRepository<Vouchers,Integer>{
    
}
