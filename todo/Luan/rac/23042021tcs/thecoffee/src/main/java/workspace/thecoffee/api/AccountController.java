package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Accounts_Info;

import workspace.thecoffee.service.AccountService;

@CrossOrigin
@RequestMapping("api/account")
@RestController
public class AccountController {
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService){
        this.accountService= accountService; 
    }   
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")// 	"username":"dat" ,"password":"123"  
// lấy toàn bộ thông tin tài khoản trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/account
    OutPut Data:
     [account{ 
        "id":"value",       
        "name": "values",
        "phone": "values",
        "email": "values",
        "picture": "values",
        "birthday": "values",
        "gender": values,
        "username": "values",
        "password": "values",
        "create_date": "values",
        "status": true,
        "roles": {
            "id": values,
            "name": "values"
        }      
    },
    account{},..
    account{}
    ]
    */
    @GetMapping
    public List<Accounts_Info> getAllAccount(){
       return accountService.getAllAccount();
    }
// lấy toàn bộ thông tin tài khoản Nhân viên trong hệ thống       
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/account/emp
    OutPut Data:
     [account{        
        "name": "values",
        "phone": "values",
        "email": "values",
        "picture": "values",
        "birthday": "values",
        "gender": values,
        "username": "values",
        "password": "values",
        "create_date": "values",
        "status": true,
        "roles": {
            "id": values,
            "name": "values"
        }      
    },
    account{},..
    account{}
    ]
    */
    @GetMapping("emp")
    public List<Accounts_Info> getAllEmp(){
       return accountService.getAllEmp();
    }
// lấy toàn bộ thông tin tài khoản Khách Hàng trong hệ thống
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/account/cus
    OutPut Data:
     [account{        
        "name": "values",
        "phone": "values",
        "email": "values",
        "picture": "values",
        "birthday": "values",
        "gender": values,
        "username": "values",
        "password": "values",
        "create_date": "values",
        "status": true,
        "roles": {
            "id": values,
            "name": "values"
        }      
    },
    account{},..
    account{}
    ]
    */
    @GetMapping("cus")
    public List<Accounts_Info> getAllCus(){
       return accountService.getAllCus();
    }
    
// tìm tài khoản theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/account/{id} 
    OutPut Data:
     account[id]{        
        "name": "values",
        "phone": "values",
        "email": "values",
        "picture": "values",
        "birthday": "values",
        "gender": values,
        "username": "values",
        "password": "values",
        "create_date": "values",
        "status": true,
        "roles": {
            "id": values,
            "name": "values"
        }      
    }
    */
    @GetMapping(path ="{id}" )
    public Accounts_Info  getAccountById(@PathVariable("id") Integer id){
        return accountService.getAccountById(id).orElse(null);
    } 


// thêm một tài khoản vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/account/{body} 
    Input Data:
     body{        
        "name": "newvalues",
        "phone": "newvalues",
        "email": "newvalues",
        "picture": "newvalues",
        "birthday": "newvalues",
        "gender": newvalues,
        "username": "newvalues",
        "password": "newvalues"      
    }
    */
    @PostMapping
    public void addAccount(@RequestBody Accounts_Info acc){
        accountService.addAccount(acc);
    }
//xóa một tài khoản khỏi hệ thống Input Delete URL= http://localhost:8081/api/account/{id} 
    @DeleteMapping(path ="{id}")
    public void deleteAccountById(@PathVariable("id") Integer id){
        accountService.deleteAccount(id);
    }
// chính sửa thông tin tài khoản theo id 
    /*
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng 
    Input Put URL= http://localhost:8081/api/account/{id} 
    Input Data:
     {        
        "name": "repvalues",
        "phone": "repvalues",
        "email": "repvalues",
        "picture": "repvalues",
        "birthday": "repvalues",
        "gender": repvalues,
        "username": "dat",
        "password": "$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me",
        "create_date": "repvalues",
        "status": true,
        "roles": {
            "id": 2,
            "name": "ROLE_ADMIN"
        }
    OutPut data 
         {        
        "name": "repvalues",
        "phone": "repvalues",
        "email": "repvalues",
        "picture": "repvalues",
        "birthday": "repvalues",
        "gender": repvalues,
        "username": "dat",
        "password": "$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me",
        "create_date": "repvalues",
        "status": true,
        "roles": {
            "id": 2,
            "name": "ROLE_ADMIN"
        }    
    */
    @PutMapping(path ="{id}")
    public void updateAccount(@PathVariable("id") Integer id,@RequestBody Accounts_Info acc){
        accountService.updateAccount(id,acc);
    }

    // @GetMapping("max")
    // public Accounts_Info findMaxId(){
    //     return accountService.findMaxId().orElse(null);
    // }
    // @PostMapping("cus")
    // public void CusaddAccount(@RequestBody Customers acc){
    //     accountService.CusaddAccount(acc);
    // }
}
