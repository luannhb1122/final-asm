package workspace.thecoffee.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "Orders_Detail")
public class Orders_Detail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "amount")
    private Integer amount;

    @ManyToOne 
    @JoinColumn(name="id_size")   
	Sizes sizes;

    @ManyToOne 
    @JoinColumn(name="id_product")    
	Products products;

    @ManyToOne 
    @JoinColumn(name="id_order", referencedColumnName = "id")
    @JsonBackReference(value="Order_Detail")
	Orders orders;
    // private Integer orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Sizes getSizes() {
        return sizes;
    }

    public void setSizes(Sizes sizes) {
        this.sizes = sizes;
    }

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Orders_Detail(Integer amount, Sizes sizes, Products products, Orders orders) {
        this.amount = amount;
        this.sizes = sizes;
        this.products = products;
        this.orders = orders;
    }

    public Orders_Detail(Integer amount, Sizes sizes, Products products, Integer orders) {
        this.amount = amount;
        this.sizes = sizes;
        this.products = products;
        this.orders.setId(orders)  ;
    }

    public Orders_Detail() {
    }
    // public Orders getOrders() {
    //     return orders;
    // }
    // public Orders getOrders() {
    //     return orders;
    // }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    // public void getOrders() {
    //     orders.getId();
    // }

    // public void setOrdersID(Integer orders) {
    //     this.orders.setId(orders);
    // }
    

    // public Orders getOrders() {
    //     return orders;
    // }

    // public void setOrders(Orders orders) {
    //     this.orders = orders;
    // }
}
