package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders_Detail;
import workspace.thecoffee.service.Order_DetailService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/order_detail")
@RestController
public class Order_DetailController {
    private final Order_DetailService order_DetailService;

    @Autowired
    public Order_DetailController(Order_DetailService order_DetailService){
        this.order_DetailService= order_DetailService; 
    }
// lấy toàn bộ thông tin order_detail trong hệ thống
     /*
    Input Get URL= http://localhost:8081/api/order_detail
    OutPut Data:
     [order_detail{
        "id": value,
        "amount": value,
        "sizes": value,
        "products": value,
        "id_order":value
    },
    order_detail{},..
    order_detail{}
    ]
    */
    @GetMapping
    public List<Orders_Detail> getAllOrder_Detail(){
       return order_DetailService.getAllOrder_Detail();
    } 

// lấy toàn bộ thông tin order_detail trong hệ thống theo Id_Order
     /*
    Input Get URL= http://localhost:8081/api/order_detail/{id}
    OutPut Data:
     [order_detail{
        "id": value,
        "amount": value,
        "sizes": value,
        "products": value,
        "id_order":{id}
    },
    order_detail{},..
    order_detail{}
    ]
    */    
    @GetMapping(path ="{id}" )
    public List<Orders_Detail> findByIdOrder (@PathVariable("id") Integer id){
        return order_DetailService.findByIdOrder(id);
    }

// thêm một order_detail vào hệ thống
     /*
    Input Post URL= http://localhost:8081/api/order_detail/{body} 
    Input Data:
     body  
     [order_detail{       
        "amount": newvalue,
        "sizes": newvalue,
        "products": newvalue,
        "id_order":newvalue
    },
    order_detail{},..
    order_detail{}
    ]
    */
    @PostMapping
    public void addOrderlD (@RequestBody Orders_Detail odt){
        order_DetailService.addOrder_Detail(odt);
    }

    
//xóa một order_detail khỏi hệ thống Input Delete URL= http://localhost:8081/api/order_detail/{id} 
  
    @DeleteMapping(path ="{id}" )
    public void deleteOrderD(@PathVariable("id") Integer id){
        order_DetailService.deleteOrder_Detail(id);
    }


// chính sửa thông tin order_detail theo id 
    /*
    Input Put URL= http://localhost:8081/api/order_detail/{id} 
    Input Data:
    {   
        "amount": repvalue,
        "sizes": repvalue,
        "products": repvalue,
        "id_order":{id} 
    },
    OutPut data 
    {   
        "amount": repvalue,
        "sizes": repvalue,
        "products": repvalue,
        "id_order":{id} 
    }   
    */
    @PutMapping(path="{id}")
    public void updateOrderD(@PathVariable("id") Integer id,@RequestBody Orders_Detail newodt){
        order_DetailService.updateOrder_Detail(id, newodt);
    }
}
