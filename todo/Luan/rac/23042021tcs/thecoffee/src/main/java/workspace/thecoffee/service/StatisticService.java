package workspace.thecoffee.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

// import workspace.thecoffee.dao.OrderDao;
import workspace.thecoffee.dao.StatisticDao;
// import workspace.thecoffee.model.Product_Statistic;
// import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Statistic;

@Service
public class StatisticService {
    private final StatisticDao statisticDao;    
    @Autowired
    public StatisticService(StatisticDao statisticDao) {
        this.statisticDao = statisticDao;
       
    }    
    public List<Object> callYear( ) {
        return statisticDao.callYear();
	}  

    
// thong ke doanh thu
    public List<Object> doanhThuToday( ) {
        return statisticDao.doanhThuToday();
	}  

    public List<Object> doanhThuAll( ) {
        return statisticDao.doanhThuAll();
	} 

    public List<Object> doanhthunam( Integer year) {
        return statisticDao.doanhthunam(year);
	}  

    public List<Object> doanhthunhieunam( ) {
        return statisticDao.doanhthunhieunam();
	} 
    public List<Object> sumdoanhthu( ) {
        return statisticDao.sumdoanhthu();
	} 



 //Thong ke theo hoa don
    public List<Statistic> getAllStatic() {
        return statisticDao.findAllOrder();
    } 

    public List<Statistic> findOrderId(Integer id) {
        return statisticDao.findOrderId(id);
    }  
    
    
//thong ke theo san pham
    public List<Object> allProductSales(){
        return statisticDao.allProductSales();
    }  

    public List<Object> findProduct_statisticMon(String pr,Integer mon, Integer year) {
        return statisticDao.findProduct_statisticMon(pr,mon,year);
	} 

	public List<Object> product_statisticMon(Integer mon, Integer year) {
        return statisticDao.product_statisticMon(mon,year);
	}  

    public List<Object> product_statisticYear( Integer year) {
        return statisticDao.product_statisticYear(year);
	}  
    // public List<Object> billsInMon(Integer mon){
    //     return statisticDao.billsInMon(mon);
    // }   

    // public List<Object> billsAll(){
    //     return statisticDao.billsAll();
    // }

}
