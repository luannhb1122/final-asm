package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import workspace.thecoffee.model.Orders;


public interface OrderDao extends JpaRepository<Orders,Integer>{
    @Query(value = "SELECT TOP 1 * from Orders Order by id desc", nativeQuery = true)
    public java.util.Optional<Orders> findMaxId();
}
