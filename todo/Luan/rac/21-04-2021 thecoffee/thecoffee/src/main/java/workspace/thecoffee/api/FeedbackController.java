package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Feedbacks;
import workspace.thecoffee.service.FeedbackService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/feedback")
@RestController
public class FeedbackController {
    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService){
        this.feedbackService= feedbackService; 
    }

    @GetMapping
    public List<Feedbacks> getAllFeedback(){
       return feedbackService.getAllFeedback();
    }

    @GetMapping(path="{id}")
    public Feedbacks getFeedbackById(@PathVariable("id") Integer id){
        return feedbackService.getFeedbackById(id).orElse(null);

    }

    @PostMapping
    public void addFeedBack(@RequestBody Feedbacks newfb) {
        feedbackService.addFeedBack(newfb);       
    }

    @DeleteMapping(path="{id}")
    public void deleteFeedback(@PathVariable("id") Integer id) {
        feedbackService.deleteFeedback(id);        
    }

    @PutMapping(path="{id}")
    public void updateFeedBack(@PathVariable("id") Integer id,@RequestBody Feedbacks newfb) {
        feedbackService.updateFeedBack(id, newfb);
    }
}
