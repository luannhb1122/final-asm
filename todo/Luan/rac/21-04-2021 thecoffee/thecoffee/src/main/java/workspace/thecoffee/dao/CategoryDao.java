package workspace.thecoffee.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Categories;

public interface CategoryDao extends JpaRepository<Categories,Integer> {
    
}
