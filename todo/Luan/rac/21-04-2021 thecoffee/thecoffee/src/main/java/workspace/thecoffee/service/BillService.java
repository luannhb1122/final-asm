package workspace.thecoffee.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Optional;

//import com.fasterxml.jackson.databind.util.ArrayBuilders.IntBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.BillDao;
import workspace.thecoffee.dao.OrderDao;
import workspace.thecoffee.model.Bills;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Orders_Detail;

@Service
public class BillService {
    private final BillDao billDao;
    private final OrderDao orderDao;
    
    public  Date getCurrentTimeUsingDate() {
        Date date = new Date();
        String strDateFormat = "dd-mm-yyy";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat);
        String formattedDate= dateFormat.format(date);
        System.out.println("Current time of the day using Date - 12 hour format: " + date+"  "+formattedDate);
        return date;
    }
    @Autowired
    public BillService(BillDao billDao,OrderDao orderDao ) {
        this.billDao = billDao;
        this.orderDao = orderDao;
    }
 
    public List<Bills> getAllBill() {
        getCurrentTimeUsingDate();
        return billDao.findAllBill();

    }

    public Optional<Bills> getBillById(Integer id) {
        return billDao.findById(id);
    }

    public Float sumBill(){
      Orders odDB =  orderDao.findMaxId().get();      
     
      Float sum = (float) 0 ;
      for (Orders_Detail orders_Detail:  odDB.getOrders_Detail()) {
        int amount= orders_Detail.getAmount();
        Float price = (Float)orders_Detail.getProducts().getPrice();
        Float sum1 = (price*amount);
        sum += sum1;
      }
       return sum;
    }

    public void addBill() {
        Bills bill = new Bills();
        bill.setDate_create(getCurrentTimeUsingDate());
        bill.setTotal(sumBill());
        bill.setOrders(orderDao.findMaxId().get());
        billDao.save(bill); 
        // billDao.save(new Bills(           
        //     bill.getDate_create(),
        //     bill.getTotal(),
        //     bill.getOrders()
        //     ));
    }

    public void addBillTest(Bills bill) {
        bill.setDate_create(getCurrentTimeUsingDate());
        bill.setOrders(orderDao.findMaxId().get());
        billDao.save(bill); 
        // billDao.save(new Bills(           
        //     bill.getDate_create(),
        //     bill.getTotal(),
        //     bill.getOrders()
        //     ));
    }
    public void updatBill(Integer id, Bills newBills){
        Optional<Bills> billsData = getBillById(id);
        Bills oldBills = billsData.get();
        oldBills.setDate_create(newBills.getDate_create());
        oldBills.setTotal(newBills.getTotal());
        oldBills.setOrders(newBills.getOrders());
        billDao.save(oldBills);
    }

    public void deleteBill(Integer id) {
        billDao.deleteById(id);
    }


}
