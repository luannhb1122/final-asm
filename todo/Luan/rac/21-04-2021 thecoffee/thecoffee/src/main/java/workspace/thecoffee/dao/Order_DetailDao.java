package workspace.thecoffee.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
 import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Orders_Detail;

public interface Order_DetailDao extends JpaRepository<Orders_Detail,Integer> {
    


@Query(value = "SELECT * FROM Orders_Detail WHERE id_order = :id_order", nativeQuery = true)
public List <Orders_Detail> findByIdOrder(@Param("id_order") Integer id_order);

// @Query(value = "SELECT * FROM Orders_Detail WHERE id_order = :id_order", nativeQuery = true)
// public List <Orders_Detail> findByIdOrder(@Param("id_order") Integer id_order);
}

