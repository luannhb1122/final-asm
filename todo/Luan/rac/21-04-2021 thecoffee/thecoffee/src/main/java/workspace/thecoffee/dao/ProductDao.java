package workspace.thecoffee.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
// import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Products;

public interface ProductDao extends JpaRepository<Products, Integer> {
    
   @Query(value = "select * from product_view", nativeQuery = true)
    public List<Products> findAllProduct();

    @Query(value =" exec findProduct @pr = :pr", nativeQuery = true)
    public List<Products> findProduct(@Param("pr") String pr);
}
