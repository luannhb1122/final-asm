package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Feedbacks;

public interface FeedbackDao extends JpaRepository<Feedbacks,Integer> {
    
}
