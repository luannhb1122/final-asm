package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders;
import workspace.thecoffee.service.OrderService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/order")
@RestController
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService){
        this.orderService= orderService; 
    }

    @GetMapping
    public List<Orders> getAllOrder(){
       return orderService.getAllOrder();
    }

    @GetMapping(path ="{id}" )
    public Orders getOrderById (@PathVariable("id") Integer id){
        return orderService.getOrderById(id).orElse(null);
    }

    @PostMapping
    public void addOrder (@RequestBody Orders od){
        orderService.addOrder(od);
    }

    @DeleteMapping(path ="{id}" )
    public void deleteOrder(@PathVariable("id") Integer id){
        orderService.deleteOrder(id);
    }

    @PutMapping(path ="{id}" )
    public void updateOrder(@PathVariable("id") Integer id,@RequestBody Orders newod){
        orderService.updateOrder(id, newod);
    }
    // @GetMapping("max")
    // public Orders findMaxId(){
    //     return orderService.findMaxId().orElse(null);
    // }
}
