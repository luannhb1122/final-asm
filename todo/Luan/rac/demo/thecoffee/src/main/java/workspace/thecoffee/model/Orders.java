package workspace.thecoffee.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Orders")
public class Orders {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_created;

    @Column(name = "payment")
    private Integer payment;


  

    @ManyToOne 
    @JoinColumn(name="id_cus")
    // @JsonBackReference(value="cus-id")
	Customers customers;

    @ManyToOne
    @JoinColumn(name="id_emp")
    // @JsonBackReference(value="emp-id")
	Employees employees;

    @ManyToOne 
    @JoinColumn(name="id_voucher")
    // @JsonBackReference(value="Vou-id")
	Vouchers vouchers;

    @OneToMany(mappedBy="orders")
    @JsonManagedReference(value="Order_Detail")
	List<Orders_Detail> orders_Detail;

    @OneToOne(mappedBy = "orders")
    // @JsonManagedReference(value="bill-id")
    private Bills bills;

    @OneToOne(mappedBy = "orders")
    // @JsonManagedReference(value="ship-id")
    private Ships_Info ship_Info;

    @OneToOne(mappedBy = "orders")
    private Orders_Status order_Status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }

    public Employees getEmployees() {
        return employees;
    }

    public void setEmployees(Employees employees) {
        this.employees = employees;
    }

    public Vouchers getVouchers() {
        return vouchers;
    }

    public void setVouchers(Vouchers vouchers) {
        this.vouchers = vouchers;
    }

    public Integer getPayment() {
        return payment;
    }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public List<Orders_Detail> getOrders_Detail() {
        return orders_Detail;
    }

    public void setOrders_Detail(List<Orders_Detail> orders_Detail) {
        this.orders_Detail = orders_Detail;
    }

    public Orders(Date date_created, Integer payment, Customers customers, Employees employees, Vouchers vouchers,
            List<Orders_Detail> orders_Detail) {
        this.date_created = date_created;
        this.payment = payment;
        this.customers = customers;
        this.employees = employees;
        this.vouchers = vouchers;
        this.orders_Detail = orders_Detail;
    }

    

    public Orders() {
    }

    public Orders(Date date_created, Integer payment, Customers customers, Employees employees, Vouchers vouchers) {
        this.date_created = date_created;
        this.payment = payment;
        this.customers = customers;
        this.employees = employees;
        this.vouchers = vouchers;
    }

    public Orders(Date date_created, Integer payment, Customers customers, Employees employees) {
        this.date_created = date_created;
        this.payment = payment;
        this.customers = customers;
        this.employees = employees;
    }
    
    
}

    // @OneToMany(mappedBy="orders")
	//  List<Orders_Detail> orders_Detail;
    //  private Orders_Detail orders_Detail;
    
 


