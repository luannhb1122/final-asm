package workspace.thecoffee.service;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import workspace.thecoffee.dao.EmployeeDao;
import workspace.thecoffee.model.Employees;

@Service
public class EmployeeService {
    private final EmployeeDao employeeDao;
    private final AccountService accountService;
    @Autowired
    public EmployeeService(EmployeeDao employeeDao, AccountService accountService) {
        this.employeeDao = employeeDao;
        this.accountService = accountService;
    }

    public List<Employees> getAllEmployee() {
        return employeeDao.findAll();
    }

    public void addEmployees(Employees emp) {
        // accountService.addAccountNull();
        Date start_date = new Date();
        emp.setStart_date(start_date);
        emp.setStatus(true);  
        employeeDao.save(emp);
        accountService.EmpaddAccount(emp);
        
    }
    public Optional<Employees> getEmployeeById(Integer id) {
        return employeeDao.findById(id);
    }  

    public void deleteEmployees(Integer id) {
        employeeDao.deleteById(id);
    }


    public void updatEmployees(Integer id, Employees newEmp){
        Optional<Employees> typeEmpData = getEmployeeById(id);
        Employees oldEmp = typeEmpData.get();        
        oldEmp.setUsername(newEmp.getUsername());
        oldEmp.setPassword(newEmp.getPassword());
        oldEmp.setStatus(newEmp.getStatus());
        oldEmp.setStart_date(newEmp.getStart_date());
        // oldEmp.setAccount_Info(newEmp.getAccount_Info());   
        accountService.updateAccount(newEmp.getAccount_Info().getId(),newEmp.getAccount_Info());       
        employeeDao.save(oldEmp);
    }

}
