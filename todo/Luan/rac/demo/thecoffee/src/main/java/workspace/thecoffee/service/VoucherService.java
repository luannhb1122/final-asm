package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.VoucherDao;
import workspace.thecoffee.model.Vouchers;

@Service
public class VoucherService {
    private final VoucherDao voucherDao;
    @Autowired
    public VoucherService(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }

    public List<Vouchers> getAllVoucher() {
        return voucherDao.findAll();
    }
    public Optional<Vouchers> getVoucherById(Integer id) {
        return voucherDao.findById(id);
    }

    public void addVoucher(Vouchers vou) {
        voucherDao.save(new Vouchers(           
            vou.getStatus(),
            vou.getValue(),          
            vou.getStart_time(),
            vou.getEnd_time(),
            vou.getQuanity()
            ));
    }

    public void updatVoucher(Integer id, Vouchers newVou){
        Optional<Vouchers> VoucherData = getVoucherById(id);
        Vouchers oldVou = VoucherData.get();
        oldVou.setStatus(newVou.getStatus());
        oldVou.setValue(newVou.getValue());
        oldVou.setStart_time(newVou.getStart_time());
        oldVou.setEnd_time(newVou.getEnd_time());
        oldVou.setQuanity(newVou.getQuanity());
        voucherDao.save(oldVou);
    }

    public void deleteVoucher(Integer id) {
        voucherDao.deleteById(id);
    }

}
