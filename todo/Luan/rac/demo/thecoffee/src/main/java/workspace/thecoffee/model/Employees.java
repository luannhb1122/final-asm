package workspace.thecoffee.model;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Employees")
public class Employees {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date start_date;

    @OneToOne(mappedBy = "employees")
    @JsonManagedReference(value="emp-id")
    private Accounts_Info account_Info;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getStart_date() {
        return start_date;
    }

    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    public Employees() {
    }

    public Employees(String username, String password, Boolean status, Date start_date) {
        this.username = username;
        this.password = password;
        this.status = status;
        this.start_date = start_date;
    }

    public Accounts_Info getAccount_Info() {
        return account_Info;
    }

    public void setAccount_Info(Accounts_Info account_Info) {
        this.account_Info = account_Info;
    }
}
