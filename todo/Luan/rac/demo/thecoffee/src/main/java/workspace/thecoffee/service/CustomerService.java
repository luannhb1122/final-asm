package workspace.thecoffee.service;

import workspace.thecoffee.dao.AccountDao;
import workspace.thecoffee.dao.CustomerDao;
import workspace.thecoffee.model.Accounts_Info;
import workspace.thecoffee.model.Customers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    private final AccountService accountService;
    private final CustomerDao customerDao;
    @Autowired
    public CustomerService(CustomerDao customerDao,AccountService accountService ) {
        this.customerDao = customerDao;
        this.accountService = accountService;
    }
   

    public List<Customers> getAllCustomer() {
        return customerDao.findAll();
    }
    public void addCustomer(Customers cus) {
        accountService.addAccountNull();
        cus.setLevel(1);
        cus.setStatus(true);  
        customerDao.save(cus);
        accountService.CusaddAccount(cus);
        
    }
    public Optional<Customers> getCustomerById(Integer id) {
        return customerDao.findById(id);
    }  

    public void deleteCustomer(Integer id) {
        customerDao.deleteById(id);
    }


    public void updatCustomer(Integer id, Customers newCus){
        Optional<Customers> typeCustomersData = getCustomerById(id);
        Customers oldCus = typeCustomersData.get();
        System.out.println(oldCus);
        oldCus.setUsername(newCus.getUsername());
        oldCus.setPassword(newCus.getPassword());
        oldCus.setStatus(newCus.getStatus());
        oldCus.setLevel(newCus.getLevel());
        accountService.updateAccount(newCus.getAccount_Info().getId(),newCus.getAccount_Info());
        // oldCus.setAccount_Info(newCus.getAccount_Info());          
        customerDao.save(oldCus);
    }
}
