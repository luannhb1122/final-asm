package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.Order_DetailDao;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Orders_Detail;

@Service
public class Order_DetailService {
    private final Order_DetailDao order_DetailDao;
    @Autowired
    public Order_DetailService(Order_DetailDao order_DetailDao) {
        this.order_DetailDao = order_DetailDao;
    }

    public List<Orders_Detail> getAllOrder_Detail() {
        return order_DetailDao.findAll();
    }

    public List<Orders_Detail> findByIdOrder(Integer id_order) {
        return order_DetailDao.findByIdOrder(id_order);
    }
    
    public Optional<Orders_Detail> getOrder_DetailById(Integer id) {
        return order_DetailDao.findById(id);
    }

    public void addOrder_Detailnull( Orders norderD,List<Orders_Detail> dt) {
        for (Orders_Detail orders_Detail: dt) { 
        Orders_Detail orderD = new  Orders_Detail();
        orderD.setOrders(norderD);
        orderD.setAmount(orders_Detail.getAmount());
        orderD.setProducts(orders_Detail.getProducts());
        orderD.setSizes(orders_Detail.getSizes());
        order_DetailDao.save(orderD);
         }
    }

    public void addOrder_Detail(Orders_Detail orderD) {
        order_DetailDao.save(new Orders_Detail
                (   orderD.getAmount(),
                    orderD.getSizes(), 
                    orderD.getProducts(),
                    orderD.getOrders()                         
                    
                )
            );
    }

    public void deleteOrder_Detail(Integer id) {
        order_DetailDao.deleteById(id);
    }

    public void updateOrder_Detail(Integer id, Orders_Detail newODT){
        Optional<Orders_Detail> typeOrder_DetailData = getOrder_DetailById(id);
        Orders_Detail oldODT = typeOrder_DetailData.get();
        oldODT.setAmount(newODT.getAmount());
        oldODT.setSizes(newODT.getSizes());
        oldODT.setProducts(newODT.getProducts());
        oldODT.setOrders(newODT.getOrders());          
        order_DetailDao.save(oldODT);
    }
}
