package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Employees;
import workspace.thecoffee.service.EmployeeService;

/* @CrossOrigin(origins = "http://localhost:8082") */
@CrossOrigin
@RequestMapping("api/employee")
@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService){
        this.employeeService= employeeService; 
    }

    @GetMapping
    public List<Employees> getAllEmployee(){
       return employeeService.getAllEmployee();
    }

    @GetMapping(path ="{id}" )
    public Employees  getEmployeeById(@PathVariable("id") Integer id){
        return employeeService.getEmployeeById(id).orElse(null);
    } 

    @PostMapping
    public void addEmployees(@RequestBody Employees emp){
        employeeService.addEmployees(emp);
    }

    @DeleteMapping(path ="{id}")
    public void deleteEmployees(@PathVariable("id") Integer id){
        employeeService.deleteEmployees(id);
    }

    @PutMapping(path ="{id}")
    public void updatEmployees(@PathVariable("id") Integer id,@RequestBody Employees nEmp){
        employeeService.updatEmployees(id,nEmp);
    }
}
