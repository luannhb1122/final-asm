package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import workspace.thecoffee.model.Product_Statistic;
// import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Statistic;
// import workspace.thecoffee.service.OrderService;
import workspace.thecoffee.service.StatisticService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/check")
@RestController
public class StatisticController {
    private final StatisticService statisticService;

    @Autowired
    public StatisticController(StatisticService statisticService){
        this.statisticService= statisticService; 
    }
// API call year
    @GetMapping("/year" )
    public  List<Object> callYear (){
        return statisticService.callYear();
    }
// API call order
    @GetMapping
    public List<Statistic> getAllStatic(){
       return  statisticService.getAllStatic();
    }

    @GetMapping(path ="{id}" )
    public List<Statistic> findOrderId (@PathVariable("id") Integer id){
        return statisticService.findOrderId(id);
    }
// API Call Product
    @GetMapping("/product" ) // call tat ca san pham da ban
    public  List<Object> allProductSales (){
        return statisticService.allProductSales();
    }
    @GetMapping("/product/find" ) // call 1 san pham ban duoc trong thang
    public  List<Object> findProduct_statisticMon (@RequestParam("pr") String pr,@RequestParam("mon") Integer mon,@RequestParam("year") Integer year){
        return statisticService.findProduct_statisticMon(pr, mon, year);
    }
    @GetMapping("/product/mon" ) //call tat ca san pham ban duoc trong thang
    public  List<Object> product_statisticMon (@RequestParam("mon") Integer mon,@RequestParam("year") Integer year){
        return statisticService.product_statisticMon(mon, year);
    }
    @GetMapping("/product/year" ) // call tat ca san pham ban duoc trong nam
    public  List<Object> product_statisticYear (@RequestParam("year") Integer year){
        return statisticService.product_statisticYear( year);
    }
//API call doanh thu
    @GetMapping("/doanhthu/today" )
    public  List<Object> doanhThuToday(){
        return statisticService.doanhThuToday();
    }

    @GetMapping("/doanhthu/All" )
    public  List<Object> doanhThuAll (){
        return statisticService.doanhThuAll();
    }

    @GetMapping("/doanhthu/year" )
    public  List<Object> doanhthunam (@RequestParam("year") Integer year){
        return statisticService.doanhthunam( year);
    }
  

    // @GetMapping("/bill" )
    // public  List<Object> billsInMon (@RequestParam("mon") Integer id){
    //     return statisticService.billsInMon(id);
    // }
    // @GetMapping("/billall" )
    // public  List<Object> billsAll (){
    //     return statisticService.billsAll();
    // }

   
}
