package workspace.thecoffee.api;

import workspace.thecoffee.model.Customers;
import workspace.thecoffee.service.CustomerService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/customer")
@RestController
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService){
        this.customerService= customerService; 
    }

    @GetMapping
    public List<Customers> getAllCustomer(){
       return customerService.getAllCustomer();
    }
    @GetMapping(path ="{id}" )
    public Customers  getCustomerById(@PathVariable("id") Integer id){
        return customerService.getCustomerById(id).orElse(null);
    }

    @PostMapping
    public void addCustomer(@RequestBody Customers cus){
        customerService.addCustomer(cus);
    }

    @DeleteMapping(path ="{id}")
    public void deleteCustomer(@PathVariable("id") Integer id){
        customerService.deleteCustomer(id);
    }

    @PutMapping(path ="{id}")
    public void updatCustomer(@PathVariable("id") Integer id,@RequestBody Customers ncus){
        customerService.updatCustomer(id,ncus);
    }

}
