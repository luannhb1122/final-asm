package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Ships_Info;
import workspace.thecoffee.service.ShipService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/ship")
@RestController
public class ShipController {
    private final ShipService shipService;

    @Autowired
    public ShipController(ShipService shipService){
        this.shipService= shipService; 
    }

    @GetMapping
    public List<Ships_Info> getAllShip(){
       return shipService.getAllShip();
    }

    @GetMapping(path ="{id}" )
    public Ships_Info  getShipById(@PathVariable("id") Integer id){
        return shipService.getShipById(id).orElse(null);
    }

    @PostMapping
    public void addShip(@RequestBody Ships_Info shipinf){
        shipService.addShip(shipinf);
    }

    @DeleteMapping(path ="{id}")
    public void deleteShip(@PathVariable("id") Integer id){
        shipService.deleteShip(id);
    }

    @PutMapping(path ="{id}")
    public void updateShip(@PathVariable("id") Integer id,@RequestBody Ships_Info newship){
        shipService.updateShip(id,newship);
    }
}
