package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Sizes;
import workspace.thecoffee.service.SizeService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/size")
@RestController
public class SizeController {
    private final SizeService sizeService;

    @Autowired
    public SizeController(SizeService sizeService){
        this.sizeService= sizeService; 
    }

    @GetMapping
    public List<Sizes> getAllSize(){
       return sizeService.getAllSize();
    }

    @GetMapping(path ="{id}" )
    public Sizes  getSizeById(@PathVariable("id") Integer id){
        return sizeService.getSizeById(id).orElse(null);
    }

    @PostMapping
    public void addSize(@RequestBody Sizes size){
        sizeService.addSize(size);
    }

    @DeleteMapping(path ="{id}")
    public void deleteSize(@PathVariable("id") Integer id){
        sizeService.deleteSize(id);
    }

    @PutMapping(path ="{id}")
    public void updatSize(@PathVariable("id") Integer id,@RequestBody Sizes newSize){
        sizeService.updatSize(id,newSize);
    }
}
