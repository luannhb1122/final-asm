package workspace.thecoffee.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import workspace.thecoffee.model.Accounts_Info;

public interface AccountDao extends JpaRepository<Accounts_Info,Integer> {
@Query(value = "SELECT TOP 1 * from Accounts_info Order by id desc", nativeQuery = true)
public java.util.Optional<Accounts_Info> findMaxId();


}
