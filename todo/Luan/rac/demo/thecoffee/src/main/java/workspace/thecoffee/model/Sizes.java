package workspace.thecoffee.model;

import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Sizes")
public class Sizes {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_size")
    private String type;

    @Column(name = "product_price")
    private Double price;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy="sizes")    
	List<Orders_Detail> orders_Detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Sizes(String type, Double price, Boolean status) {
        this.type = type;
        this.price = price;
        this.status = status;     
    }   

    public Sizes(String type, Double price) {
        this.type = type;
        this.price = price;
    }

    public Sizes() {
    }

    // public List<Orders_Detail> getOrders_Detail() {
    //     return orders_Detail;
    // }

    // public void setOrders_Detail(List<Orders_Detail> orders_Detail) {
    //     this.orders_Detail = orders_Detail;
    // }
    
    
}
