package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Ships_Info;

public interface ShipDao extends JpaRepository<Ships_Info,Integer> {
    
}
