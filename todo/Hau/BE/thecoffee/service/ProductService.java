package workspace.thecoffee.service;

import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.ProductDao;
import workspace.thecoffee.model.Products;

@Service
public class ProductService {
    private final ProductDao productDao;
    @Autowired
    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public void addProduct(Products products) {
      //  productDao.save(new Products(products.getName(),products.getPrice(), products.getImage(),products.getStatus(), products.getCategories()));
      if(products.getImage()==null){
        products.setImage("https://product.hstatic.net/1000075078/product/espresso-nong_4b32833e9a5f48768ea5d5d2a4df0303_master.jpg");
      }    
        productDao.save(products);
    }   
//xuất sản phẩm đang được bán trong hệ thống
    public List<Products> getAllProduct() {
        return productDao.findAllProduct();
    }  
//xuất tất cả sản phẩm của hệ thống
    public List<Products> AllProduct() {
        return productDao.AllProduct();
    }  
//tìm sản phẩm theo tên
    public List<Products> findProduct(String pr) {
        return productDao.findProduct(pr);       
    }
//tìm sản phẩm theo ID
    public Optional<Products> getProductById(Integer id) {
        return productDao.findById(id);
    }
//Xóa sản phẩm khỏi bản hiển thị
    public void deleteProduct(Integer id) {
        Optional<Products> typeProductData = getProductById(id);
        Products oldProduct = typeProductData.get();
        oldProduct.setStatus(0);
        //productDao.deleteById(id);
    }

//chỉnh sửa sản phẩm
    public void updateProduct(Integer id, Products newProduct){
        Optional<Products> typeProductData = getProductById(id);
        Products oldProduct = typeProductData.get();
        oldProduct.setName(newProduct.getName());
        oldProduct.setPrice(newProduct.getPrice());
        oldProduct.setImage(newProduct.getImage());
        oldProduct.setStatus(newProduct.getStatus());
        oldProduct.setCategories(newProduct.getCategories());
        productDao.save(oldProduct);
    }
}

