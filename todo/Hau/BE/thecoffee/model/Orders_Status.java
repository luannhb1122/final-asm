package workspace.thecoffee.model;

// import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;




@Entity
@Table(name = "Orders_Status")
public class Orders_Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    private int status =1;

    @Column(name = "comment")
    private String comment;

    @OneToOne    
   
    @JsonBackReference(value="order_Status-id")
    @JoinColumn(name="id_order", referencedColumnName = "id")
    private Orders orders;

    public Integer getId() {
        return id;
    }

    public int getStatus() {
        return status;
    }

    public void setId(Integer id) {
        this.id = id;
    }   

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Orders_Status() {
    }

    public Integer isStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Orders_Status(int status, String comment, Orders orders) {
        this.status = status;
        this.comment = comment;
        this.orders = orders;
    }

    
}
