package workspace.thecoffee.model;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Orders")
public class Orders {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date_created")
    @Temporal(TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy",locale = "vi-VN", timezone = "Asia/Ho_Chi_Minh")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date date_created;

    @Column(name = "payment")
    private Integer payment;
  

    @ManyToOne 
    @JoinColumn(name="id_cus")
    // @JsonBackReference(value="cus-id")
	Accounts_Info customers;

    // @ManyToOne
    // @JoinColumn(name="id_emp")
    // // @JsonBackReference(value="emp-id")
	// Employees employees;

    @ManyToOne 
    @JoinColumn(name="id_voucher")
    // @JsonBackReference(value="Vou-id")
	Vouchers vouchers;

    @OneToMany(mappedBy="orders")
    @JsonManagedReference(value="Order_Detail")
	List<Orders_Detail> orders_Detail;

    @OneToOne(mappedBy = "orders")
    // @JsonManagedReference(value="bill-id")
    private Bills bills;

    public Ships_Info getShip_Info() {
        return ship_Info;
    }

    public void setShip_Info(Ships_Info ship_Info) {
        this.ship_Info = ship_Info;
    }

    public Orders_Status getOrder_Status() {
        return order_Status;
    }

    public void setOrder_Status(Orders_Status order_Status) {
        this.order_Status = order_Status;
    }

    @OneToOne(mappedBy = "orders",cascade = {CascadeType.ALL})
    @JsonManagedReference(value="ship-id")
    private Ships_Info ship_Info;

    @OneToOne(mappedBy = "orders",cascade = {CascadeType.ALL})
    @JsonManagedReference(value="order_Status-id")
    
    private Orders_Status order_Status;

    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate_created() {
        return date_created;
    }

    public void setDate_created(Date date_created) {
        this.date_created = date_created;
    }
 

    public Vouchers getVouchers() {
        return vouchers;
    }

    public void setVouchers(Vouchers vouchers) {
        this.vouchers = vouchers;
    }

    public Integer getPayment() {
        return payment;
    }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public List<Orders_Detail> getOrders_Detail() {
        return orders_Detail;
    }

    public void setOrders_Detail(List<Orders_Detail> orders_Detail) {
        this.orders_Detail = orders_Detail;
    }   

    public Orders() {
    }

  

    public Accounts_Info getCustomers() {
        return customers;
    }

    public void setCustomers(Accounts_Info customers) {
        this.customers = customers;
    }

    public Orders(Date date_created, Integer payment, Accounts_Info customers, Vouchers vouchers,
            List<Orders_Detail> orders_Detail, Bills bills, Ships_Info ship_Info, Orders_Status order_Status) {
        this.date_created = date_created;
        this.payment = payment;
        this.customers = customers;
        this.vouchers = vouchers;
        this.orders_Detail = orders_Detail;
        this.bills = bills;
        this.ship_Info = ship_Info;
        this.order_Status = order_Status;
    }

    public Orders(Integer payment, Accounts_Info customers, Vouchers vouchers, List<Orders_Detail> orders_Detail,
            Ships_Info ship_Info) {
        this.payment = payment;
        this.customers = customers;
        this.vouchers = vouchers;
        this.orders_Detail = orders_Detail;
        this.ship_Info = ship_Info;
    }

    public Orders(Integer payment, Accounts_Info customers, List<Orders_Detail> orders_Detail, Ships_Info ship_Info) {
        this.payment = payment;
        this.customers = customers;
        this.orders_Detail = orders_Detail;
        this.ship_Info = ship_Info;
    }
    
    
}

    // @OneToMany(mappedBy="orders")
	//  List<Orders_Detail> orders_Detail;
    //  private Orders_Detail orders_Detail;
    
 


