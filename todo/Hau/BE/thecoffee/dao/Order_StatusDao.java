package workspace.thecoffee.dao;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Orders_Status;

public interface Order_StatusDao extends JpaRepository<Orders_Status,Integer> {
    @Query(value ="exec findOrder_statusUser @status =:st ,@id_cus =:id_cus ", nativeQuery = true)
    public List<Orders_Status> findOrder_statusUser(@Param("st")int st,@Param("id_cus")int id_cus);


    @Query(value ="select* from Orders_status where id_order =:id_order ", nativeQuery = true)
    public Optional<Orders_Status> findOrder_status(@Param("id_order")int id_order);
    
}
