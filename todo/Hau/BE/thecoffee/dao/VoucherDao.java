package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import workspace.thecoffee.model.Vouchers;

public interface VoucherDao extends JpaRepository<Vouchers,Integer>{
    @Query(value = "select * from VoucherUser_View", nativeQuery = true)
        public java.util.List<Vouchers> VoucherUser();
        
}
