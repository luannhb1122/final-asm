package workspace.thecoffee.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Orders;

public interface OrderDao extends JpaRepository<Orders, Integer> {
    @Query(value = "SELECT TOP 1 * from Orders Order by id desc", nativeQuery = true)
    public java.util.Optional<Orders> findMaxId();

    @Query(value = "exec findOrderByCustomer @id_cus= :id_cus", nativeQuery = true)
    public java.util.List<Orders> findOrderByCustomer(@Param("id_cus") int id_cus);

    @Query(value = "exec searchOrders @id= :id", nativeQuery = true)
    public java.util.List<Orders> searchOrders(@Param("id") int id);

    @Query(value = "exec findOrder_status @status =:st", nativeQuery = true)
    public java.util.List<Orders> findOrder_status(@Param("st") int st);   
    

    @Query(value = "select* from Orders order by id desc", nativeQuery = true)
    public java.util.List<Orders> findAllOrderDesc();
   
}
