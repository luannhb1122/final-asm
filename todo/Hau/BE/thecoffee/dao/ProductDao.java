package workspace.thecoffee.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

// import org.springframework.data.jpa.repository.Query;
import java.util.List;
import workspace.thecoffee.model.Products;

public interface ProductDao extends JpaRepository<Products, Integer> {
    
   @Query(value = "select * from productTrue_view", nativeQuery = true)
   public List<Products> findAllProduct();

   @Query(value = "exec softProduct", nativeQuery = true)
   public List<Products> AllProduct();

   @Query(value =" exec findProduct @pr = :pr", nativeQuery = true)
   public List<Products> findProduct(@Param("pr") String pr);
}
