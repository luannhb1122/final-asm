package workspace.thecoffee.api;

import java.util.List;

import javax.validation.*;
import javax.validation.constraints.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Roles;
import workspace.thecoffee.service.RoleService;

@CrossOrigin
@RequestMapping("api/superadmin/role")
@RestController
public class RoleController {

    private final RoleService roleService;

    @Autowired
    public RoleController(RoleService roleService){
        this.roleService= roleService; 
    }

 // lấy toàn bộ thông tin role trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/role
    OutPut Data:
     role[{
        "id": 2,
        "name": "ROLE_ADMIN"
    },
    {
        "id": 1,
        "name": "ROLE_CUSTOMER"
    }
    ]
    
    */  

    @GetMapping
    public List<Roles> getAllRole(){
       return roleService.getAllRole();
    }

    // tìm role theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/role/{id} 
    OutPut Data:
     role[id]{ 
            "id": values,
            "name": "values"
    }
    */
    @GetMapping(path = "{id}")
    public Roles getRoleById(@PathVariable ("id") Integer id){
        return roleService.getRoleById(id)
                .orElse(null);
    }

    
}



// @PostMapping
    // public void addRole(@RequestBody Roles roles){
    //     roleService.addRole(roles);
    // }

    //  @DeleteMapping(path = "{id}")
    //  public void deleteTRoleById(@PathVariable ("id") Integer id){
    //     roleService.deleteRole(id);
    //  }

    // @PutMapping(path = "{id}")
    // public void updateRoleById(@PathVariable ("id") Integer id,@Valid @NotNull @RequestBody Roles roleToUpdate){
    //     roleService.updateRole(id, roleToUpdate);
    // }