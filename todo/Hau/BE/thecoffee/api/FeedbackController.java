package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Feedbacks;
import workspace.thecoffee.service.FeedbackService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api")
@RestController
public class FeedbackController {
    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService){
        this.feedbackService= feedbackService; 
    }
// lấy toàn bộ thông tin feedback trong hệ thống
     /*
    Input Get URL= http://localhost:8081/api/feedback
    OutPut Data:
     [feedback{
        "id": value,
        "note": "value",
        "status": value,
        "rate": value,
        "date_time": "value",
        "customers": "value",
        "products": "value"
    },
    feedback{},..
    feedback{}
    ]
    */
    @GetMapping("admin/feedback")
    public List<Feedbacks> getAllFeedback(){
       return feedbackService.getAllFeedback();
    }

    @GetMapping("user/feedback")
    public List<Feedbacks> findFeedbacksbyProduct(@RequestParam("id_pr")int id){
       return feedbackService.findFeedbacksbyProduct(id);
    }
// tìm feedback theo id
    /*
    Input Get URL= http://localhost:8081/api/admin/feedback/{id} 
    OutPut Data:
     feedback[id]{
        "id": value,
        "note": "value",
        "status": value,
        "rate": value,
        "date_time": "value",
        "customers": "value",
        "products": "value"
    }
    */
    @GetMapping("/admin/feedback/{id}")
    public Feedbacks getFeedbackById(@PathVariable("id") Integer id){
        return feedbackService.getFeedbackById(id).orElse(null);
    }
// thêm một tài khoản vào hệ thống
     /*
    Input Post URL= http://localhost:8081/api/admin/feedback/{body} 
    Input Data:
     body{
        "id": "newvalue",
        "note": "newvalue",
        "status": "newvalue",
        "rate": "newvalue",
        "date_time": "newvalue",
        "customers": "newvalue",
        "products": "newvalue"
    }
    */ 

    @PostMapping("/admin/feedback")
    public void addFeedBack(@RequestBody Feedbacks newfb) {
        feedbackService.addFeedBack(newfb);       
    }

//xóa một feedback khỏi hệ thống Input Delete URL= http://localhost:8081/api/admin/feedback/{id} 
  
    @DeleteMapping("/admin/feedback/{id}")
    public void deleteFeedback(@PathVariable("id") Integer id) {
        feedbackService.deleteFeedback(id);        
    }
// chính sửa thông tin feedback theo id 
    /*
    Input Put URL= http://localhost:8081/api/admin/feedback/{id} 
    Input Data:
     {
        "id": "{id}",
        "note": "repvalue",
        "status": "repvalue",
        "rate": "repvalue",          
        "products": "repvalue"
    }
    OutPut data 
         {
        "id": "{id}",
        "note": "repvalue",
        "status": "repvalue",
        "rate": "repvalue",              
        "products": "repvalue"
    } 
    */
    @PutMapping("/admin/feedback/{id}")
    public void updateFeedBack(@PathVariable("id") Integer id,@RequestBody Feedbacks newfb) {
        feedbackService.updateFeedBack(id, newfb);
    }
}
