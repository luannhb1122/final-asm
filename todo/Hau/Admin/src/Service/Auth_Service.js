import axios from "axios";
const user =JSON.parse(sessionStorage.getItem('tokens'))
const API_URL = "http://localhost:8081/api/";
// const register = (username, email) => {
//   return axios.post(API_URL + "admin/account/registerAdmin", {
//     username,
//     email,
//   })
// };
const forgot = (username, email) => {
  return axios.get(API_URL + `all/account/forgotPassword/?username=${username}&email=${email}`, {
    username,
    email,
  });
};
const otp = (username, password, otp) => {
  return axios.get(API_URL + `all/account/forgotPasswordConfirm?username=${username}&password=${password}&otp=${otp}`, {
    username,
    password,
    otp
  });
};
const changepassword = (username, oldpassword, newpassword) => {
  return axios.get(API_URL + `user/account/changePassword?username=${username}&oldpassword=${oldpassword}&newpassword=${newpassword}`, /* {
    username,
    oldpassword,
    newpassword
   */   { headers: {
		'Content-Type': 'application/json',
    Authorization: `Bearer `+ user
}});
};
const login = (username, password) => {
  return axios
    .post(API_URL + "login", {
      username,
      password,
    })
    .then((response) => {
      if (response.data.token && response.data.roles[0]==="ROLE_ADMIN"|| response.data.roles[0]==="ROLE_SUPERADMIN") {
        sessionStorage.setItem("tokens", JSON.stringify(response.data.token));
        // localStorage.setItem("user", JSON.stringify(response.data));
        axios
        .get(API_URL + `user/account/searchAccountByUsername/${username}`, {
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ` + response.data.token
          }
        })
        .then((res) => {
          sessionStorage.setItem('profile', JSON.stringify(res.data));
        });
    
      }else{
        alert("lỗi")
      }
      return response.data;
     
    });
};
const logout = () => {
  sessionStorage.removeItem("tokens");
};

 const getCurrentUser = () => {
  return JSON.parse(sessionStorage.getItem("tokens"));
};

// const getCurrentUserName = () => {
//   return JSON.parse(localStorage.getItem("user"));
// };
const getProfile = () => {
	return JSON.parse(sessionStorage.getItem('profile'));
};


export default {
  // register,
  login,
  forgot,
  logout,
  otp,
   getCurrentUser, 
   changepassword,
  //  getCurrentUserName,
   getProfile
};
