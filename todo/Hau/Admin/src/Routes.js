import React, { useState } from 'react';
import Main from './Main/Main';
import Login from './pages/Login';
import Register from './pages/Register';
import Forgot from './pages/ForgotPassword';
import OTP from './pages/OTP'
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

const authGuard = (Component) => () => {
	return sessionStorage.getItem("tokens") ? (
	  <Component />
	) : (
	  <Redirect to="/login" />
	);
  };
  const Routes = (props) => (
	<Router {...props}>
	  <Switch>
		<Route path="/login">
		  <Login />
		</Route>
		{/* <Route path="/register">
		  <Register />
		</Route> */}
		<Route path="/forgot">
		  <Forgot />
		</Route>
		<Route path="/otp">
		  <OTP />
		</Route>
		<Route path="/" render={authGuard(Main)}></Route>
		<Route exact path="/">
		  <Redirect to="/" />
		</Route>
		{/* <Route path="*">
		  <NotFound />
		</Route> */}
	  </Switch>
	</Router>
  );
export default Routes;
