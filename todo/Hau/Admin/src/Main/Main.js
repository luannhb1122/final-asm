import React, { useEffect, useState } from 'react';
import Product from '../pages/Product';
import Home from '../pages/Home';
import AllOrder from '../pages/Bill';
import Category from '../pages/Category';
import Customer from '../pages/Customer';
import Employee from '../pages/Employee';
import BillView from '../pages/Bill_view';
import Store from '../pages/Store';
import Order from '../pages/Order';
import Voucher from '../pages/Voucher';
import Statistical_Product from '../pages/Statistical_Product';
import { Route, Link } from 'react-router-dom';
import AuthService from '../Service/Auth_Service';
import Profile from '../pages/Profile';
import ChangePassword from '../pages/ChangePassword';
import OrderFail from '../pages/OrderFail'
import Size from '../pages/Size';
import Register from '../pages/Register';
import Data from "../components/Axios/AxiosServicer";
import './main.css';
export default function Main() {

	const [ currentUser, setCurrentUser ] = useState(undefined);

	useEffect(() => {
		const user = AuthService.getProfile();

		if (user) {
			setCurrentUser(user);
			console.log(currentUser);
		}
	}, []);

	const logout = () => {
		sessionStorage.clear();

	};
	return (
		<div>
			<nav className="navbar navbar-expand-lg navbar-light header-navbar navbar-static">
				<div className="container-fluid navbar-wrapper">
					<div className="navbar-header d-flex">
						<div
							className="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center"
							data-toggle="collapse"
						>
							<i className="ft-menu font-medium-3" />
						</div>
					</div>
					<div className="navbar-container">
						<div className="collapse navbar-collapse d-block" id="navbarSupportedContent">
							<ul className="navbar-nav">
								<li className="dropdown nav-item mr-1">
									{currentUser ? (
										<div className="navbar-nav mr-2">
											<li className="nav-item mt-1">
												<Link to={'/profile'} className="nav-link bold profile" style={{color:"black"}}>
										{/* <img className="avatar" src={currentUser.picture} alt={currentUser.user} height="35" width="35">{currentUser.picture}</img> {currentUser.username} */}
										<img className="avatar" src={currentUser.picture} alt={currentUser.user} height="40" width="40" style={{marginRight:"10	px"}}/> 
										<a style={{fontSize:'20px', fontFamily:'-moz-initial'}}> {currentUser.name.slice(currentUser.name.lastIndexOf(' ')===-1?"0":currentUser.name.lastIndexOf(' '),currentUser.name.length)} </a>
												</Link>
											</li>
											<li className="nav-item">
												<a href="/login" className="nav-link ft-log-out profile" style={{color:"red", fontSize:"16px"}} onClick={logout}>
													Đăng Xuất
												</a>
											</li>
											{/* <li className="nav-item">
												<a href="/register" className="nav-link" style={{color:"blue"}}>
												<i className="ft-users" style={{color:"blue"}}/>	Đăng Kí
												</a>
											</li> */}
										</div>
									) : (
										<div className="navbar-nav ml-auto">
											<li className="nav-item">
												<Link to={'/login'} className="nav-link">
													Login
												</Link>
											</li>
										</div>
									)}
								</li>
							
								
							</ul>
						</div>
					</div>
				</div>
			</nav>
			{/* Side Bar*/}
			<div className="wrapper">
				{/*         <!-- main menu--> */}
				<div
					className="app-sidebar menu-fixed"
					data-background-color="man-of-steel"
					data-image="../../../app-assets/img/sidebar-bg/01.jpg"
					data-scroll-to-active="true"
				>
					<div className="sidebar-header">
						<div className="logo clearfix active">
							<Link className="logo-text float-left" to="/" data-toggle="pill" role="tab">
								<div className="logo-img">
									<img src="../../../app-assets/img/Asset 2.png" alt="Logo" style={{ width: 40 }} />
									<span class="text">COFFEE SON</span>
								</div>
							</Link>
							<a
								className="nav-toggle d-none d-lg-none d-xl-block"
								id="sidebarToggle"
								href="javascript:;"
							>
								<i className="toggle-icon ft-toggle-right" data-toggle="expanded" />
							</a>
							<a className="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;">
								<i className="ft-x" />
							</a>
						</div>
					</div>
					{/*            <!-- Sidebar Header Ends-->
            <!-- / main menu header-->
            <!-- main menu content--> */}
					<div className="sidebar-content main-menu-content">
						<div className="nav-container">
							<ul
								className="navigation navigation-main"
								id="v-pills-tab"
								data-menu="menu-navigation"
								role="tablist"
								aria-orientation="vertical"
							>
								<li>
									<Link to="/" data-toggle="pill" role="tab">
										<i className="ft-home" />
										<span className="menu-title" data-i18n="Home">
											Trang Chủ
										</span>
									</Link>
								</li>
								<li className="has-sub nav-item">
									<a href="javascript:;">
										<i className="ft-aperture" />
										<span className="menu-title" data-i18n="Product">
											Sản Phẩm
										</span>
										{/* <span className="tag badge badge-pill badge-success float-right mr-1 mt-1">6</span> */}
									</a>
									<ul className="menu-content">
										<li>
											<Link to="/Product" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="ProductAll">
													Tất cả sản phẩm
												</span>
											</Link>
										</li>
										<li>
											<Link to="/Size" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="ProductAll">
													Size
												</span>
											</Link>
										</li>
										<li>
											<Link to="/Category" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="ProductAll">
													Loại sản phẩm
												</span>
											</Link>
										</li>
									</ul>
								</li>
								<li className="has-sub nav-item">
									<a href="javascript:;">
										<i className="ft-box" />
										<span className="menu-title" data-i18n="Order">
											Tất cả đơn hàng
										</span>
									</a>
									<ul className="menu-content">
										<li>
											<Link to="/order" data-toggle="pill" role="tab" /*  href="javascript:;" */>
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="allbill">
													Đơn hàng đang xử lý
												</span>
											</Link>
										</li>
										<li>
											<Link
												to="/allbill"
												data-toggle="pill"
												role="tab" /*  href="javascript:;" */
											>
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="OrderAll">
													Đơn hàng đã giao
												</span>
											</Link>
										</li>
										<li>
											<Link to="/orderfail" data-toggle="pill" role="tab" /*  href="javascript:;" */>
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="allbill">
													Đơn hàng bị hủy
												</span>
											</Link>
										</li>
									</ul>
								</li>
								<li className="has-sub nav-item">
									<a href="javascript:;">
										<i className="ft-edit" />
										<span className="menu-title" data-i18n="Staff">
											Tài Khoản
										</span>
									</a>
									<ul className="menu-content">
										{	
											currentUser!=undefined && currentUser.roles.id==3?
											<li>
											<Link to="/employee" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="StaffAll">
													Nhân Viên
												</span>
											</Link>
										</li>:''
										}

										<li>
											<Link to="/customer" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="Customer">
													Khách Hàng
												</span>
											</Link>
										</li>
									</ul>
								</li>
								<li className="has-sub nav-item">
									<a href="javascript:;">
										<i className="ft-grid" />
										<span className="menu-title" data-i18n="Tables">
											Cửa hàng & Khuyến mãi
										</span>
									</a>
									<ul className="menu-content">
										<li>
											<Link to="/store" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="StaffAll">
													Thông Tin Cửa Hàng
												</span>
											</Link>
										</li>

									</ul>
									<ul className="menu-content">
										<li>
											<Link to="/voucher" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="voucher">
												Khuyến mãi
												</span>
											</Link>
										</li>

									</ul>
								</li>
								<li className="has-sub nav-item">
									<a href="javascript:;">
										<i className="ft-layout" />
										<span className="menu-title" data-i18n="Staticial">
											Thống Kê
										</span>
									</a>
									<ul className="menu-content">
										<li>
											<Link to="/billview" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="Basic">
													Doanh Thu
												</span>
											</Link>
										</li>
										<li>
											<Link to="/statistical_product" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="Advanced">
													Sản Phẩm
												</span>
											</Link>
										</li>
									</ul>
								</li>
								<li className="has-sub nav-item">
									<a href="javascript:;">
										<i className="ft-layers" />
										<span className="menu-title" data-i18n="Cards">
											Hỗ Trợ
										</span>
									</a>
									<ul className="menu-content">
										<li>
								
											<Link to="/changepassword" data-toggle="pill" role="tab">
												<i className="ft-arrow-right submenu-icon" />
												<span className="menu-item" data-i18n="Basic Cards">
												Đổi Mật Khẩu
												</span>
												</Link>
												
										
										</li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
					{/*      <!-- main menu content--> */}
					<div className="sidebar-background" />
				</div>

				<div className="main-panel">
					{/*       <!-- BEGIN : Main Content--> */}
					<div className="main-content">
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path={'/'} component={Home} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path={'/Product'} component={Product} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/Size">
								<Size />
							</Route>
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/Category">
								<Category />
							</Route>
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/allbill">
								<AllOrder />
							</Route>
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/customer">
								<Customer />
							</Route>
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/employee">
								<Employee />
							</Route>
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/billview" component={BillView} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/statistical_product" component={Statistical_Product} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/store" component={Store} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/order" component={Order} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/voucher" component={Voucher} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/profile" component={Profile} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/changepassword" component={ChangePassword} />
						</div>
						<div className="tab-content" id="v-pills-tabContent">
							<Route path="/orderfail" component={OrderFail} />
						</div>
						{/* <div className="tab-content" id="v-pills-tabContent">
							<Route path="/register" component={Register} />
						</div> */}
					</div>

					{/*      <!-- END : End Main Content--> */}
					{/* 
            <!-- BEGIN : Footer--> */}
					<footer className="footer undefined undefined">
						<p className="clearfix text-muted m-0">
							<span>Copyright &copy; 2021 &nbsp;</span>
							<a href="#" id="pixinventLink" target="_blank">
								Team Coffee Sơn
							</a>
							<span className="d-none d-sm-inline-block">, All rights reserved.</span>
						</p>
					</footer>
					{/*        <!-- End : Footer--> */}
					{/*            <!-- Scroll to top button --> */}
					<button className="btn btn-primary scroll-top" type="button">
						<i className="ft-arrow-up" />
					</button>
				</div>
			</div>
			{/* 
    <!-- START Notification Sidebar--> */}
			<aside className="notification-sidebar d-none d-sm-none d-md-block" id="notification-sidebar">
				<a className="notification-sidebar-close">
					<i className="ft-x font-medium-3 grey darken-1" />
				</a>
				<div className="side-nav notification-sidebar-content">
					<div className="row">
						<div className="col-12 notification-nav-tabs">
							<ul className="nav nav-tabs">
								<li className="nav-item">
									<a
										className="nav-link active"
										id="base-tab1"
										data-toggle="tab"
										aria-controls="activity-tab"
										href="#activity-tab"
										aria-expanded="true"
									>
										Activity
									</a>
								</li>
								<li className="nav-item">
									<a
										className="nav-link"
										id="base-tab2"
										data-toggle="tab"
										aria-controls="settings-tab"
										href="#settings-tab"
										aria-expanded="false"
									>
										Settings
									</a>
								</li>
							</ul>
						</div>
						<div className="col-12 notification-tab-content">
							<div className="tab-content">
								<div
									className="row tab-pane active"
									id="activity-tab"
									role="tabpanel"
									aria-expanded="true"
									aria-labelledby="base-tab1"
								>
									<div className="col-12" id="activity">
										<h5 className="my-2 text-bold-500">System Logs</h5>
										<div className="timeline-left timeline-wrapper mb-3" id="timeline-1">
											<ul className="timeline">
												<li className="timeline-line mt-4" />
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<i className="ft-download primary" />
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>New Update Available</span>
															<span className="float-right grey font-italic font-small-2">
																1 min ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Android Pie 9.0.0_r52v availabe (658MB).
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<span className="text-bold-500">Download Now!</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<img
																className="avatar"
																src="../../../app-assets/img/portrait/small/avatar-s-15.png"
																alt="avatar"
																width="40"
															/>
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Reminder!</span>
															<span className="float-right grey font-italic font-small-2">
																52 min ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Your meeting is scheduled with Mr. Derrick Walters at 16:00.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<span className="text-bold-500">Snooze</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<img
																className="avatar"
																src="../../../app-assets/img/portrait/small/avatar-s-16.png"
																alt="avatar"
																width="40"
															/>
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Recieved a File</span>
															<span className="float-right grey font-italic font-small-2">
																4 hours ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Christina Rogers sent you a file for the next conference.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<img
																	src="./app-assets/img/icons/sketch-mac-icon.png"
																	alt="icon"
																	width="20"
																/>
																<span className="text-bold-500 ml-2">
																	Diamond.sketch
																</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<i className="ft-mic primary" />
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Voice Message</span>
															<span className="float-right grey font-italic font-small-2">
																10 hours ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Natalya Parker sent you a voice message.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<span className="text-bold-500">Listen</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<i className="ft-cloud-drizzle primary" />
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Weather Update</span>
															<span className="float-right grey font-italic font-small-2">
																Yesterday
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Hi John! It is a rainy day with 16&deg;C.
														</p>
													</div>
												</li>
											</ul>
										</div>
										<h5 className="my-2 text-bold-500">Applications Logs</h5>
										<div className="timeline-left timeline-wrapper" id="timeline-2">
											<ul className="timeline">
												<li className="timeline-line mt-4" />
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<img
																className="avatar"
																src="../../../app-assets/img/portrait/small/avatar-s-26.png"
																alt="avatar"
																width="40"
															/>
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Gmail</span>
															<span className="float-right grey font-italic font-small-2">
																Just now
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Victoria Hampton sent you a mail and has a file attachment
															with it.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<img
																	src="./app-assets/img/icons/pdf.png"
																	alt="pdf icon"
																	width="20"
																/>
																<span className="text-bold-500 ml-2">Register.pdf</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<i className="ft-droplet primary" />
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>MakeMyTrip</span>
															<span className="float-right grey font-italic font-small-2">
																7 hours ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															Your next flight for San Francisco will be on 24th March.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<span className="text-bold-500">Important</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<img
																className="avatar"
																src="../../../app-assets/img/portrait/small/avatar-s-23.png"
																alt="avatar"
																width="40"
															/>
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>CNN</span>
															<span className="float-right grey font-italic font-small-2">
																16 hours ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															U.S. investigating report says email account linked to CIA
															Director was hacked.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<span className="text-bold-500">Read full article</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<i className="ft-map primary" />
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Maps</span>
															<span className="float-right grey font-italic font-small-2">
																Yesterday
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">
															You visited Walmart Supercenter in Chicago.
														</p>
														<div className="notification-note">
															<div className="p-1 pl-2">
																<span className="text-bold-500">Write a Review!</span>
															</div>
														</div>
													</div>
												</li>
												<li className="timeline-item">
													<div className="timeline-badge">
														<span
															className="bg-primary bg-lighten-4"
															data-toggle="tooltip"
															data-placement="right"
															title="Portfolio project work"
														>
															<i className="ft-package primary" />
														</span>
													</div>
													<div className="activity-list-text">
														<h6 className="mb-1">
															<span>Updates Available</span>
															<span className="float-right grey font-italic font-small-2">
																2 days ago
															</span>
														</h6>
														<p className="mt-0 mb-2 font-small-3">19 app updates found.</p>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div className="row tab-pane" id="settings-tab" aria-labelledby="base-tab2">
									<div className="col-12" id="settings">
										<h5 className="mt-2 mb-3">General Settings</h5>
										<ul className="list-unstyled mb-0 mx-2">
											<li className="mb-3">
												<div className="mb-1">
													<span className="text-bold-500">Notifications</span>
													<div className="float-right">
														<div className="custom-switch">
															<input
																className="custom-control-input"
																id="noti-s-switch-1"
																type="checkbox"
															/>
															<label
																className="custom-control-label"
																for="noti-s-switch-1"
															/>
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">
													Use switches when looking for yes or no answers.
												</p>
											</li>
											<li className="mb-3">
												<div className="mb-1">
													<span className="text-bold-500">Show recent activity</span>
													<div className="float-right">
														<div className="checkbox">
															<input id="noti-s-checkbox-1" type="checkbox" checked />
															<label for="noti-s-checkbox-1" />
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">
													The "for" attribute is necessary to bind checkbox with the input.
												</p>
											</li>
											<li className="mb-3">
												<div className="mb-1">
													<span className="text-bold-500">Product Update</span>
													<div className="float-right">
														<div className="custom-switch">
															<input
																className="custom-control-input"
																id="noti-s-switch-4"
																type="checkbox"
																checked
															/>
															<label
																className="custom-control-label"
																for="noti-s-switch-4"
															/>
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">
													Message and mail me on weekly product updates.
												</p>
											</li>
											<li className="mb-3">
												<div className="mb-1">
													<span className="text-bold-500">Email on Follow</span>
													<div className="float-right">
														<div className="custom-switch">
															<input
																className="custom-control-input"
																id="noti-s-switch-3"
																type="checkbox"
															/>
															<label
																className="custom-control-label"
																for="noti-s-switch-3"
															/>
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">Mail me when someone follows me.</p>
											</li>
											<li className="mb-3">
												<div className="mb-1">
													<span className="text-bold-500">Announcements</span>
													<div className="float-right">
														<div className="checkbox">
															<input id="noti-s-checkbox-2" type="checkbox" checked />
															<label for="noti-s-checkbox-2" />
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">
													Receive all the news and announcements from my clients.
												</p>
											</li>
											<li className="mb-3">
												<div className="mb-1">
													<span className="text-bold-500">Date and Time</span>
													<div className="float-right">
														<div className="checkbox">
															<input id="noti-s-checkbox-3" type="checkbox" />
															<label for="noti-s-checkbox-3" />
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">
													Show date and time on top of every page.
												</p>
											</li>
											<li>
												<div className="mb-1">
													<span className="text-bold-500">Email on Comments</span>
													<div className="float-right">
														<div className="custom-switch">
															<input
																className="custom-control-input"
																id="noti-s-switch-2"
																type="checkbox"
																checked
															/>
															<label
																className="custom-control-label"
																for="noti-s-switch-2"
															/>
														</div>
													</div>
												</div>
												<p className="font-small-3 m-0">
													Mail me when someone comments on my article.
												</p>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</aside>
			{/*     <!-- END Notification Sidebar--> */}
			<div className="sidenav-overlay" />
			<div className="drag-target" />
		</div>
	);
}
