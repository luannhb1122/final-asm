import url from './AxiosConfig';
// -------------------------------------------PRODUCT-------------------------------------
//Lấy dữ liệu toàn sản phẩm

const getAllproduct = () => {
	return url.get('/admin/product');
};
//Tìm sản phẩm theo tên sản phẩm
const getproduct = (id, data) => {
	return url.get(`/admin/product/${id}`, data);
};

//Thêm Sản Phẩm
const createproduct = (data) => {
	return url.post('/admin/product', data);
};

//Cập Nhật Sản Phẩm Theo ID
const updateproduct = (id, data) => {
	return url.put(`/admin/product/${id}`, data);
};

//Xóa Sản phẩm theo ID
const removeproduct = (id) => {
	return url.delete(`/admin/product/${id}`);
};
// -------------------------------------------CATEGORY-------------------------------------
//Lấy dữ liệu toàn sản phẩm
const getAllcategory = () => {
	return url.get('/all/category');
};
//Tìm sản phẩm theo tên sản phẩm
const getcategory = (id, data) => {
	return url.get(`/admin/category/${id}`, data);
};

//Thêm Sản Phẩm
const createcategory = (data) => {
	return url.post('/admin/category', data);
};

//Cập Nhật Sản Phẩm Theo ID
const updatecategory = (id, data) => {
	return url.put(`/admin/category/${id}`, data);
};

//Xóa Sản phẩm theo ID
const removecategory = (id) => {
	return url.delete(`/admin/category/${id}`);
};
//Tìm Loại theo Loại
const getNamecategory = (type) => {
	return url.get(`/admin/category/${type}`);
};

// -------------------------------------------EMPLOYEE-------------------------------------
const getEmployee = () => {
	return url.get(`admin/account/emp`);
};
//Xóa Loại theo ID
const removeEmployee = (id) => {
	return url.delete(`admin/account/emp${id}`);
};
//Cập Nhật Loại Sản Phẩm Theo ID
const updateEmployee = (id, data) => {
	return url.put(`admin/account/emp${id}`, data);
};
//------------ACCOUNT--------------
const getProfile = (username) => {
	return url.get(`user/account/searchAccountByUsername/${username}`);
};

// -------------------------------------------CUSTOMER-------------------------------------
//Lấy dữ liệu Customer
const getAllCustomer = () => {
	return url.get('admin/account/cus');
};
//Cập Nhật dữ liệu Customer
const updateCustomer = (id, data) => {
	return url.put(`admin/account/cus/${id}`, data);
};
//Xóa dữ liệu Customer
const removeCustomer = (id) => {
	return url.delete(`admin/account/cus/${id}`);
};

// -------------------------------------------FEEBACK-------------------------------------
//Lấy dữ liệu toàn sản phẩm theo Feeback
const getAllfeedback = () => {
	return url.get('/feedback');
};
//Cập Nhật Feeback Theo ID
const updatefeedback = (id, data) => {
	return url.put(`admin/feedback/${id}`, data);
};
//Xóa  Feeback theo ID
const removefeedback = (id) => {
	return url.delete(`admin/feedback/${id}`);
};
// -------------------------------------------BILL-------------------------------------
//Lấy dữ liệu Bill
const getAllallbill = () => {
	return url.get('admin/bill');
};
// -------------------------------------------THỐNG KÊ BILL-------------------------------------
//Lấy dữ liệu toàn sản phẩm
const getbillyear2020 = () => {
	return url.get('admin/check/doanhthu/year?year=2020');
};
const getbillfindyear = (year) => {
	return url.get(`admin/check/doanhthu/year?year=${year}`);
};

const getAllBill = () => {
	return url.get('admin/check/doanhthu/All');
};
const getbillyear = () => {
	return url.get('admin/check/year');
};
// -------------------------------------------THỐNG KÊ PRODUCT-------------------------------------
//Lấy dữ liệu toàn sản phẩm
const getStatisticProduct = () => {
	return url.get('admin/check/product');
};
const getStatisticProductyear = () => {
	return url.get('admin/check/year');
};

const getproductfindyear = (year) => {
	return url.get(`admin/check/product/year?year=${year}`);
};
//------------STORE--------------
const getpStore = () => {
	return url.get('all/store');
};
const createStore = (data) => {
	return url.post('admin/store', data);
};

//Cập Nhật Sản Phẩm Theo ID
const updateStore = (id, data) => {
	return url.put(`admin/store/${id}`, data);
};

//Xóa Sản phẩm theo ID
const removeStore = (id) => {
	return url.delete(`admin/store/${id}`);
};

//------------order--------------
const getAllorder = () => {
	return url.get('admin/order');
};
const getorder1 = () => {
	return url.get('admin/order/status1');
};
const getorder0 = () => {
	return url.get('admin/order/status0');
};
const getOrderStatusXuLy = (st) => {
	return url.get(`admin/order/status?st=${st}`);
};
const getAllorderstatus = (id) => {
	return url.get(`/admin/order_status/success?id=${id}`);
};


//--------Voucher-----------------
const getpVoucher = () => {
	return url.get('admin/voucher');
};
const createVoucher = (data) => {
	return url.post('admin/voucher', data);
};

//Cập Nhật Sản Phẩm Theo ID
const updateVoucher= (id, data) => {
	return url.put(`admin/voucher/${id}`, data);
};

//Xóa Sản phẩm theo ID
const removeVoucher = (id) => {
	return url.delete(`admin/voucher/${id}`);
};
//dk tài khoản
const register = (data) => {
	return url.post("superadmin/account/registerAdmin",data)
};
//dk pass
// const changePassword = (data) => {
// 	return url.get(`user/account/changePassword`,data)
// };

// tổng doanh thu
const Gettotal = () => {
	return url.get('admin/check/doanhthu/total');
};
const Getbill = () => {
	return url.get('admin/check/countbill');
};
// UpdateProfile 
const UpdateProfile = (id, data) => {
	return url.put(`user/account/${id}`, data);
};

// Size
const getAllsize = () => {
	return url.get('/all/size');
};

//Thêm size
const createsize = (data) => {
	return url.post('/admin/size', data);
};

//Cập Nhật size
const updatesize = (id, data) => {
	return url.put(`/admin/size/${id}`, data);
};

//Xóa size
const removesize = (id) => {
	return url.delete(`/admin/size/${id}`);
};

/// block

const getblock = (id) => {
	return url.get(`/admin/account/block${id}`);
};

const getonblock = (id) => {
	return url.get(`admin/account/unblock${id}`);
};

const getblockSA = (id) => {
	return url.get(`/superadmin/account/block${id}`);
};

const getonblockSA = (id) => {
	return url.get(`superadmin/account/unblock${id}`);
};

export default {
	Getbill,
	//-----getblock------
	getblock,
	getonblock,
	getblockSA,
	getonblockSA,
	//----size-------
	getAllsize,
	createsize,
	updatesize,
	removesize,
	//----------UpdateProfile----
	UpdateProfile,
	//--------------total
	Gettotal,
	//-----------đk tài khoản---------
	register,
	// changePassword,
	// -----------Product---------------
	getAllproduct,
	getproduct,
	createproduct,
	updateproduct,
	removeproduct,
	// -----------Category---------------
	getAllcategory,
	getcategory,
	createcategory,
	updatecategory,
	removecategory,
	getNamecategory,
	// -----------Employee---------------
	getEmployee,
	updateEmployee,
	removeEmployee,
	getProfile,
	// -----------CUSTOMER---------------
	getAllCustomer,
	updateCustomer,
	removeCustomer,
	// -----------FEEBACK---------------
	getAllfeedback,
	updatefeedback,
	removefeedback,
	// -----------BILL---------------
	getAllallbill,
	// ----------- THỐNG KÊ BILL---------------
	getbillyear2020,
	getbillfindyear,
	getAllBill,
	getbillyear,
	// ----------- THỐNG KÊ PRODUCT---------------
	getStatisticProduct,
	getStatisticProductyear,
	getproductfindyear,
	//-----------STORE------------
	getpStore,
	createStore,
	updateStore,
	removeStore,
	//----------ORDER-----------------
	getAllorder,
	//---------Voucher--------
	getpVoucher,
	createVoucher,
	updateVoucher,
	removeVoucher,
	//---------------
	getAllorderstatus,
	getorder1,
	getorder0,
	getOrderStatusXuLy

};
