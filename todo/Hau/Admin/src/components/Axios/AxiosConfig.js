import axios from 'axios';
const user =JSON.parse(sessionStorage.getItem('tokens'))
console.log("Bearer "+user)
const axiosInstance = axios.create({
    baseURL: 'http://localhost:8081/api/',
    headers: {
		'Content-Type': 'application/json',
         Authorization: `Bearer `+ user
	}
});
// axiosInstance.interceptors.request.use(request => {
//     // code to be executed
//     return request;
// }, error => {
    
// });

export default axiosInstance;