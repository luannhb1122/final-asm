import React, { useEffect, Fragment, useState } from 'react';
import Data from '../components/Axios/AxiosServicer';
import Category from '../components/Axios/AxiosServicer';
import DataTable from 'react-data-table-component';
import { Modal, Row } from 'react-bootstrap';
import AddProduct from './AddProduct';
import './Style.css';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

export default function Customer(props) {
	// Khai báo giá trị
	const initialproducttate = {
		id: '',
		name: '',
		image: '',
		price: '',
		status: '',
		categories: {
			id: ''
		}
	};

	const initialproducttate1 = {
		id: '',
		name: '',
		image: '',
		price: '',
		status: '',
		id_category: ''
	};

	const statusItem = [
		{ id: 0, name: 'Ngưng bán' },
		{ id: 1, name: 'Đang bán' },
		{ id: 2, name: 'Nổi bật' }
	]
	//Khai báo set và get mảng
	const [product, setproduct] = useState([]);
	const [productItem, setproductItem] = useState(initialproducttate);
	const [productItemNew, setproductItemNew] = useState(initialproducttate1);
	const [search, setsearch] = useState(initialproducttate);
	const [index, setindex] = useState(0);
	// const [ message, setMessage ] = useState('');
	const [image, setImage] = useState('');
	const [loading, setLoading] = useState(false);
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetAllProduct();
		getAlltype();
	}, []);
	const GetAllProduct = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Data.getAllproduct()
			.then((response) => {
				setproduct(response.data);
				setsearch(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const setActiveTutorial = (product, index) => {
		setproductItem(product);
		setindex(index);
		handleShowModal();
	};
	// Sự kiện nhập giá trị vào form
	function handleInputChange(evt) {
		const value = evt.target.value;
		setproductItem({
			...productItem,
			[evt.target.name]: value
		});
		setproductItemNew({
			...productItemNew,
			[evt.target.name]: value
		});
	}
	const [list, setList] = React.useState([]);
	const getAlltype = () => {
		Category.getAllcategory()
			.then((response) => {
				setList(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Set selected default value
	const optionCategory = list.map((item) => {
		if (item.id === productItem.categories.id) {
			return (
				<option value={item.id} selected>
					{item.type}
				</option>
			);
		} else {
			return <option value={item.id}>{item.type}</option>;
		}
	});
	const selectCategory = () => {
		return (
			<select className="form-control" id="id_category" name="id_category" onChange={handleInputChange}>
				{optionCategory}
			</select>
		);
	};
	// uploadt image
	const uploadImage = async (e) => {
		const files = e.target.files;
		const data = new FormData();
		data.append('file', files[0]);
		data.append('upload_preset', 'Thecoffeeson');
		setLoading(true);
		const res = await fetch('https://api.cloudinary.com/v1_1/minhchon/image/upload', {
			method: 'POST',
			body: data
		});
		const file = await res.json();
		setImage(file.secure_url);
		console.log(file.secure_url);
		setLoading(false);
	};

	// Cập nhật dữ liệu sản phẩm
	
	const UpdateProduct = () => {
		var data = {
			name: productItem.name,
			price: parseFloat(productItem.price),
			status: parseInt(productItem.status),
			image: image != '' ? image : productItem.image,
			categories: {
				id: productItemNew.id_category != '' ? productItemNew.id_category : productItem.categories.id
			}
		};

		if (validation() === true) {
			Data.updateproduct(productItem.id, data)
				.then((response) => {
					console.log(response.data);
					handleCloseModal();
					handleMessageShow()

				})
				.catch((e) => {
					console.log(e);
				});
		} else {
			// handleErrorFieldShow()
		}
	};
	//duyệt vòng lặp loại


	// Set selected default value
	// const optionproduct = pr.map((item) => {
	// 		return (
	// 			<>
	// 				<option value={0} selected>Ngưng bán</option>
	// 				<option value={1}>Đang bán</option>
	// 				<option value={2}>Nổi bật</option>
	// 			</>
	// 		);

	// });
	// const selectproduct = () => {
	// 	return (
	// 		<select
	// 			className="form-control"
	// 			id="statusproduct"
	// 			name="statusproduct"
	// 			onChange={handleInputChange}
	// 		>
	// 			<>
	// 				<option value={0}>Ngưng bán</option>
	// 				<option value={1}>Đang bán</option>
	// 				<option value={2}>Nổi bật</option>
	// 			</>
	// 		</select>
	// 	);
	// };

	//Set selected default value
	const optionStatus = statusItem.map((item) => {
		if (item.id === productItem.status) {
			return (
				<option value={item.id} selected>
					{item.name}
				</option>
			);
		} else {
			return <option value={item.id}>{item.name}</option>;
		}
	});
	const selectStatus = () => {
		return (
			<select className="form-control" id="status" name="status" onChange={handleInputChange}>
				{optionStatus}
			</select>
		);
	};

	//Xóa
	const deleteTutorial = () => {
		Data.removeproduct(productItem.id)
			.then((response) => {
				console.log(response.data);
				handleCloseModal();
				handleDeleteShow()
			})
			.catch((e) => {
				console.log(e);
			});
	};

	//Hiển thị Modal
	const [show, setShow] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'ID',
			selector: 'id',
			sortable: true
		},
		{
			name: 'Hình',
			sortable: true,
			cell: (row) => <img height="190px" width="256px" alt="" src={`${row.image}`} />
		},
		{
			name: 'Tên Sản Phẩm',
			selector: 'name',
			sortable: true
		},

		{
			name: 'Giá',
			selector: 'price',
			sortable: true,
			cell: (row) => (
				<NumberFormat style={{ background: 'none', border: 'none' }} value={row.price} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />
			)
		},
		{
			name: 'Trạng Thái',
			selector: 'status',
			sortable: true,
			cell: (row) => (row.status === 1 ? <p style={{ color: 'orange' }}>Đang bán</p> : row.status === 2 ? <p style={{ color: 'blue' }}>Nổi bật</p> : <p style={{ color: 'red' }}>Ngưng bán</p>),
		},
		{
			name: 'Loại',
			selector: 'categories.type',
			sortable: true
		},
		{
			name: 'Chức Năng',
			key: 'action',
			text: 'Action',
			cell: (product, index) => {
				return (
					<Fragment>
						<button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(product, index)}>
							<i className="ft-edit-1">Cập Nhật</i>
						</button>
					</Fragment>
				);
			}
		}
	];
	//CSS DATATABLE
	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900',

			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center',

			}
		},

		headCells: {
			style: {
				fontSize: '15px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},
		cells: {
			style: {
				fontSize: '15px',
				paddingLeft: '0 8px',
				backgroundColor: 'white',
				justifyContent: 'center',
				alignitems: 'center',
				borderbottom: '#EEEEEE solid 10px'
			}
		}
	};
	// Checkbox tất cả table
	const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
		<div className="custom-control custom-checkbox">
			<input type="checkbox" className="custom-control-input" ref={ref} {...rest} />
			<label className="custom-control-label" onClick={onClick} />
		</div>
	));

	const ReloadError = () => {
		handleErrorClose();
		handleShow();
	};
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//Modal Update
	const [showmes, setShowmes] = useState(false);
	const handleMessageClose = () => setShowmes(false);
	const handleMessageShow = () => setShowmes(true);
	//Modal Delete
	const [showDelete, setShowDelete] = useState(false);
	const handleDeleteClose = () => setShowDelete(false);
	const handleDeleteShow = () => setShowDelete(true);
	////erorr
	const [showError, setShowError] = useState(false);
	const handleErrorClose = () => setShowError(false);
	const handleErrorShow = () => setShowError(true);
	////erorr field
	const [showErrorField, setShowErrorField] = useState(false);
	const handleErrorFieldClose = () => setShowErrorField(false);
	const handleErrorFieldShow = () => setShowErrorField(true);
	const ReloadPage = () => {
		handleMessageClose();
		refreshTable();
	};
	const Reload = () => {
		handleDeleteClose();
		refreshTable();
	};

	//Reset lại Table

	const refreshTable = () => {
		GetAllProduct();
	};
	//tiem kiếm
	const handleSearch = (event) => {
		let value = event.target.value.toLowerCase();
		let result = [];
		console.log(value);
		if (value === '') {
			setsearch(product);
		} else {
			result = product.filter((data) => {
				return data.name.toLowerCase().includes(value);
			});

			setsearch(result);
		}
	};

	function validation() {
		if (productItem.name === "" || productItem.price === "") {
			return handleErrorShow();
		} else if (productItem.price < 0) {
			return handleErrorFieldShow();
		} else {
			return true;
		}
	}

	return (
		<div>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">
								Trang Chủ
							</Link>
							<Link className="breadcrumb-item" to="/Product">
								Tất cả sản phẩm
							</Link>
						</li>
					</ul>
				</div>
			</div>

			<div className="container-fluid">
				<div className="row">
					<div className="col-md-10 float-left">
						<label>Tìm Kiếm: </label>
						<input type="text" className="form-control" placeholder="nhập tên sản phẩm" style={{ backgroundColor: 'white' }} onChange={(event) => handleSearch(event)} />
					</div>
					<div className="col-md-2 pt-3">
						<AddProduct />
					</div>
				</div>
			</div>

			{/*  Hiện thi Table */}
			<div className="card mt-2">
				<DataTable
					customStyles={customStyles}
					title="Tất Cả Sản Phẩm" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={search} // Dữ liệu đổ vào bảng

					pagination // Hiển thị phân trang hay không
					selectableRowsComponent={BootyCheckbox} // checkbox all

				/>
			</div>
			{/* Modal Thông báo*/}
			<Modal dialogClassName="messmodal" show={showmes} onHide={handleMessageClose}>
				<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4 text-modal">Cập Nhật Thành Công</p>
				</Modal.Body>

				<button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			<Modal dialogClassName="messmodal" show={showDelete} onHide={handleDeleteClose}>
				<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">Xóa Thành Công</p>
				</Modal.Body>

				<button className="btn btn-primary" type="button" onClick={Reload}>OK</button>
			</Modal>
			{/* Modal Error*/}
			<Modal dialogClassName="messmodal" show={showError} onHide={handleErrorClose}>
				<div className="icon-boxx">
					<i className="ft-x text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">không được để trống</p>
				</Modal.Body>

				<button className="btn btn-danger btn-sm btn-block" type="button" onClick={ReloadError}>OK</button>
			</Modal>
			{/* Lỗi Field */}
			<Modal dialogClassName="messmodal" show={showErrorField} onHide={handleErrorFieldClose}>
				<div className="icon-boxx">
					<i className="ft-x text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">Giá Không được âm</p>
				</Modal.Body>

				<button className="btn btn-danger btn-sm btn-block" type="button" onClick={handleErrorFieldClose} >OK</button>
			</Modal>
			{productItem ? (
				<Modal
					dialogClassName="dialogModal"
					show={show}
					onHide={handleCloseModal}
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title className="tille_Modal">Cập Nhật Sản Phẩm</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="container" >
							<section className="panel panel-default">
								<div className="form-group row">
									<div className="form-group col-sm-5">
										<label className="control-label">Hình Ảnh:</label>
										<input
											className="file-upload-input"
											type="file"
											name="image"
											onChange={uploadImage}
										/>
										<div class="drag-text" />
										{loading ? (
											<h6 className="spinner-border text-primary">
												{' '}
												<span class="sr-only">Loading...</span>
											</h6>
										) : (
											<img
												src={image !== '' ? image : productItem.image}
												alt={productItem.name}
												style={{ width: '100%', height: '300px' }}
											/>
										)}
									</div>
									{/*nhập tên sản phẩm */}
									<div className="form-group col-sm-7">
										<label className="control-label">Tên Sản Phẩm:</label>
										<input
											type="text"
											className="form-control"
											id="name"
											name="name"
											value={productItem.name}
											onChange={handleInputChange}
											placeholder="Tên Sản Phẩm"
										/>
										{/*    Giá */}
										<label className="control-label mt-3">Giá:</label>
										<input
											type="text"
											className="form-control"
											id="price"
											value={productItem.price}
											onChange={handleInputChange}
											name="price"
											placeholder="Giá"
										/>
										{/* id-type */}
										<label for="name" className="control-label mt-4">
											Loại:
									</label>
										<div className="">{selectCategory()}</div>

										{/* Trạng Thái */}
										<label for="name" className="control-label mt-4">
											Trạng Thái:
									</label>
										<div className="">{selectStatus()}</div>
									</div>

								</div>
							</section>
							{/*  Nút button Update */}
							<div className="form-group row pt-2 div-button">
								<div className="col-sm-12">
									<button type="submit" className="btn btn-sm btn-success " onClick={UpdateProduct} style={{ marginRight: '20px' }}>
										Cập Nhật
									</button>
									{/* <button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
										Xóa
									</button> */}
								</div>
							</div>
						</div>
					</Modal.Body>
				</Modal>
			) : (
				<div>
					<br />
					<p>Chưa có dữ Liệu</p>
				</div>
			)}
		</div>
	);
}
