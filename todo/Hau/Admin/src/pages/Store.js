
import Store from '../components/Axios/AxiosServicer';
import './Style.css'
import React, { useEffect, Fragment, useState } from 'react';
import Addstore from '../pages/AddStore';
import DataTable from 'react-data-table-component';
/* import AddType from '../pages/AddCategory'; */
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
/* import { Nav, Col, Tab } from 'react-bootstrap';
import Tabs from 'react-bootstrap/Tabs' */


export default function Stores(props) {

	const initialTypeProduct = {
		id: '',
		name: '',
		phone: '',
        email: '',
        address: '',
	};
    // Khai báo giá trị
    //Khai báo set và get mảng
    //mãng tất cả sản phẩm
    const [store, setstore] = useState([]);
    const [currentTutorial, setCurrentTutorial] = useState(initialTypeProduct);
    const [Search, setSearch] = useState([]);
    const [message, setMessage] = useState('');
    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        Getstore();

    }, []);
    // tất cả sản phẩm bán
    const Getstore = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        Store.getpStore()
            .then((response) => {
                setstore(response.data);
                setCurrentTutorial(response.data);
                setSearch(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };
    const setActiveTutorial = (StoreNew, index) => {
		setCurrentTutorial(StoreNew);
		handleShowModal();
	};

    function handleInputChange(evt) {
		const value = evt.target.value;
		setCurrentTutorial({
			...currentTutorial,
			[evt.target.name]: value
		});
	}

    const updateTutorial = () => {
		var data = {
			name: currentTutorial.name,
			phone: currentTutorial.phone,
            email: currentTutorial.email,
            address: currentTutorial.address,
		};
		Store.updateStore(currentTutorial.id, data)
			.then((response) => {
				console.log(response.data);
				handleCloseModal();
				handleMessageShow()
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Xóa
	const deleteTutorial = () => {
		Store.removeStore(currentTutorial.id)
			.then((response) => {
				console.log(response.data);
				handleCloseModal();
				handleDeleteShow()
			})
			.catch((e) => {
				console.log(e);
			});
	};

	//Hiện thị Modal
	const [show, setShow] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
		//Modal Update
		const [showmes, setShowmes] = useState(false);
		const handleMessageClose = () => setShowmes(false);
		const handleMessageShow = () => setShowmes(true);
		//Modal Delete
		const [showDelete, setShowDelete] = useState(false);
		const handleDeleteClose = () => setShowDelete(false);
		const handleDeleteShow = () => setShowDelete(true);
		const ReloadPage = () => {
			handleMessageClose();
			refreshTable();
		};
		const Reload = () => {
			handleDeleteClose();
			refreshTable();
		};

        const refreshTable = () => {
            Getstore();
        };

    // tất cả sản phẩm 
	const columns = [
		{
			name: 'ID',
			selector: 'id',
			sortable: true
		},
		{
			name: 'Tên cửa hàng',
			selector: 'name',
			sortable: true
		},
		{
			name: 'Số điện thoại',
			selector: 'phone',
			sortable: true,
		},
        {
			name: 'email',
			selector: 'email',
			sortable: true,
		},
        {
			name: 'địa chỉ',
			selector: 'address',
			sortable: true,
		},
		{
			name: 'Chức Năng',
			key: 'action',
			text: 'Action',
			cell: (TypeProduct, index) => {
				return (
					<Fragment>
						<button
							className="btn btn-primary btn-sm"
							onClick={() => setActiveTutorial(TypeProduct, index)}
						>
							<i className="ft-edit-1">Cập Nhật</i>
						</button>
					</Fragment>
				);
			}
		}
	];

    const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900'
			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},

		headCells: {
			style: {
				fontSize: '15px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},
		cells: {
			style: {
				fontSize: '15px',
				paddingLeft: '0 8px',
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center',
				borderbottom: '#EEEEEE solid 10px'
			}
		}
	};

    const handleSearch = (event) => {
		let value = event.target.value.toLowerCase();
		let result = [];
		console.log(value);
		if (value === "") {
			setSearch(setstore);
		} else {
			result = store.filter((data) => {
				return data.name.toLowerCase().includes(value);
			});

			setSearch(result);
		}
	}

    return (
        <div style={{marginTop:'-100px'}}>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">
								Trang Chủ
							</Link>
							<Link className="breadcrumb-item" to="/Category">
								Tất cả cửa hàng
							</Link>
						</li>
					</ul>
				</div>
			</div>
			{/* Tìm Kiếm Loại */}
			<div className="container-fluid">
				<div className="row mb-2">
					<div className="col-md-10 float-left">
					<label>Tìm Kiếm:</label>
					<input type="text" placeholder="nhập tên" className="form-control" onChange={(event) => handleSearch(event)} style={{backgroundColor:'white'}} />
					</div>
			        <div className="col-md-2 pt-3">
					<Addstore />
					</div>
				</div>
			</div>
			{/*  Hiện thi Table */}

				<DataTable
					customStyles={customStyles}
					title="Cửa hàng" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={Search} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
				/>
							{/* Modal Thông báo*/}
			<Modal dialogClassName="messmodal" show={showmes} onHide={handleMessageClose}>
			<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>
				<Modal.Body>
				<p className="text-center mt-4">Cập Nhật Thành Công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			<Modal dialogClassName="messmodal" show={showDelete} onHide={handleDeleteClose}>
			<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>
				<Modal.Body>
				<p className="text-center mt-4">Xóa Thành Công</p>
			</Modal.Body>

            <button className="btn btn-danger btn-block" type="button" onClick={Reload}>OK</button>
			</Modal>
			{currentTutorial ? (
				<Modal
					dialogClassName="dialogModal"
					show={show}
					onHide={handleCloseModal}
					backdrop="static"
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title className="tille_Modal">Cập nhật Cửa hàng</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="container">
							<section className="panel panel-default">
								{/* <div className="form-group row pt-3">
									<label className="col-sm-3 control-label">ID:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="id"
											name="id"
											required
											value={currentTutorial.id}
											placeholder="ID"
										/>
									</div>
								</div> */}
								{/* id-type */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">Tên cửa hàng:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="name"
											required
											name="name"
											value={currentTutorial.name}
											onChange={handleInputChange}
											placeholder="name"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Số điện thoại:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="phone"
											required
											name="phone"
											value={currentTutorial.phone}
											onChange={handleInputChange}
											placeholder="phone"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Email:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="email"
											required
											name="email"
											value={currentTutorial.email}
											onChange={handleInputChange}
											placeholder="emial"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Địa chỉ:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="address"
											required
											name="address"
											value={currentTutorial.address}
											onChange={handleInputChange}
											placeholder="address"
										/>
									</div>
								</div>
							</section>

							{/*  Nút button Update */}
							<div className="form-group row pt-2 div-button">
								<div className="col-sm-12">
									<button type="submit" className="btn btn-sm btn-success" onClick={updateTutorial} style={{ marginRight: '20px' }}>
										Cập Nhật
									</button>
									<button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
										Xóa
									</button>
								</div>
							</div>
							<p>{message}</p>
						</div>
					</Modal.Body>
				</Modal>
			) : (
				<div>
					<br />
					<p>Please click on a Tutorial...</p>
				</div>
			)}
		</div>

    );
}
