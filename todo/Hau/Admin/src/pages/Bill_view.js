import React, { useEffect, useState, Fragment } from 'react';
import BillView from '../components/Axios/AxiosServicer';
import './Style.css'
import { Nav, Col, Tab } from 'react-bootstrap';
import Tabs from 'react-bootstrap/Tabs'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { ExportToExcel } from './ExportToExcel'
import { Chart } from "react-google-charts";
import NumberFormat from 'react-number-format';
import DataTable from 'react-data-table-component';

export default function Bill(props) {
	// Khai báo giá trị
	//Khai báo set và get mảng
	const [billyear2020, setbillyear2020] = useState([]);
	const [billfindyear, setbillfindyear] = useState([]);
	const [billfindyeartotal, setbillfindyeartotal] = useState([]);
	const [allbill, setallbill] = useState([]);
	const [billyear, setbillyear] = useState([]);
	const [billa, setbilla] = useState([]);
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		Getbillyear2020();
		Getbillfindyear();
		GetAllallbill();
		Getbillyear();

	}, []);


	const Getbillyear2020 = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getbillyear2020()
			.then((response) => {
			
				setbillyear2020(response.data);
				console.log(response.data)
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const Getbillfindyear = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getbillfindyear(2021)
			.then((response) => {
				const datachart = [
					['Tháng', "Doanh thu 2021"]
				];
				setbillfindyeartotal(response.data)
				setbillfindyear(datachart.concat(response.data))
				console.log(response.data)
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const GetAllallbill = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getAllBill()
			.then((response) => {
				setallbill(response.data);
				setbilla(response.data);
				console.log(response.data)
			})
			.catch((e) => {
				console.log(e);
			});
	};

	const Getbillyear = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getbillyear()
			.then((response) => {
				setbillyear(response.data);
				console.log(response.data)
			})
			.catch((e) => {
				console.log(e);
			});
	};

	function handleInputChange(evt) {
		const value = evt.target.value;
		const test = ([]);
		if (value == "all") {
			setbilla(allbill);
		} else {
			for (let i = 0; i < allbill.length; i++) {
				if (allbill[i][1] == value) {

					test.push(allbill[i]);

				}

			}
			setbilla(test)
		}

	}

	function handleInputChangechart(evt) {
		const value = evt.target.value;
		BillView.getbillfindyear(value)
			.then((response) => {
				const datachart = [
					['Tháng', "Doanh thu "+value]
				];
				setbillfindyeartotal(response.data)
				setbillfindyear(datachart.concat(response.data))
				console.log(billfindyear)
			})
			.catch((e) => {
				console.log(e);
			});

	}

	const datatest=[]
    // tất cả sản phẩm 
    const list = billa.map((item, index) => {
        const test={
            id:(index+1),
            monthofyear:item[0]+'/'+item[1],
            total:item[2]
        }
        datatest.push(test);
    });

	// const list = billa.map((item, index) => {
	// 	return (
	// 		<tr key={index}>
	// 			<td>{index +1}</td>
	// 			<td>{item[0]}/{item[1]}</td>
	// 			<td><NumberFormat style={{background:'none',border:'none'}} value={item[2]} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></td>
	// 		</tr>
	// 	)
	// });

	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900',

			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center',

			}
		},

		headCells: {
			style: {
				fontSize: '15px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},
		cells: {
			style: {
				fontSize: '15px',
				paddingLeft: '0 8px',
				backgroundColor: 'white',
				justifyContent: 'center',
				alignitems: 'center',
				borderbottom: '#EEEEEE solid 10px'
			}
		}
	};
    const columns = [
		{
			name: 'STT',
			selector: 'id',
			sortable: true
		},
        {
			name: 'Tháng năm',
			selector: 'monthofyear',
			sortable: true
		},
        {
			name: 'Tổng tiền',
			selector: 'total',
			sortable: true,
            cell:(row) =>(
                <NumberFormat style={{background:'none',border:'none'}} value={row.total} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />
                )
		}
	
	];
    const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
		<div className="custom-control custom-checkbox">
			<input type="checkbox" className="custom-control-input" ref={ref} {...rest} />
			<label className="custom-control-label" onClick={onClick} />
		</div>
	));

	//Set selected default value
	let year;
	const optionYear = allbill.map((item) => {
		if (item[1] !== year) {
			year = item[1];
			return (
				<option value={item[1]}>
					{"Năm " + item[1]}
				</option>
			)
		}
	});
	const selectCategory = () => {
		return (
			<select
				className="form-control"
				id="date"
				name="date"
				onChange={handleInputChange}
				style={{backgroundColor:'white'}}
			>
				<option value="all">
					Hiển thị tất cả
				</option>
				{optionYear}
			</select>
		);
	};

	const selectchartyear = () => {
		return (
			<select
				className="form-control"
				id="date"
				name="date"
				onChange={handleInputChangechart} style={{ backgroundColor: 'white' }}
			>
				{optionYear}
			</select>
		);
	};

	// const NumberFormat = require('react-number-format');
	
	const totalTable = () => {
		let sumtotal=0;
		for(let i =0; i<billa.length;i++){
			sumtotal += parseInt(billa[i][2]);
		}
		return (
			<NumberFormat className="form-control col-sm-4 float-right text-right"  style={{background:'none',border:'none',color:'red',fontWeight:'bold',fontSize:'18px' }} value={sumtotal} displayType={'text'} thousandSeparator={true} prefix={'Tổng doanh thu: '} suffix={' VNĐ'} />
		
		);
	};

	const totalChart = () => {
		let sumtotal=0;
		for(let i =0; i<billfindyeartotal.length;i++){
			sumtotal += parseInt(billfindyeartotal[i][1]);
		}
		return (
			<NumberFormat className="form-control col-sm-4 float-right text-right"  style={{background:'none',border:'none',color:'red',fontWeight:'bold',fontSize:'18px' }} value={sumtotal} displayType={'text'} thousandSeparator={true} prefix={'Tổng doanh thu: '} suffix={' VNĐ'} />
		
		);
	};

	return (
		<div  style={{marginTop:'-100px'}}>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
							<Link className="breadcrumb-item" to="/Product">Tổng Số Doanh Thu</Link>
						</li>
					</ul>
				</div>
			</div>
			<Tabs defaultActiveKey="home" transition={false} id="noanim-tab-example">
				<Tab eventKey="home" title="Biểu Đồ Thể hiện Doanh Thu">
				<div className="form-group">
						<div className="col-sm-2" style={{ float: 'right', marginBottom: 15 }} >
							{selectchartyear()}
						</div>
						
					</div>
					<h1>Biểu Đồ Thể Hiện Doanh thu</h1>
					<div>
						<Chart
							width={'1250px'}
							height={'500px'}
							chartType="ColumnChart"
							loader={<div>Loading Chart</div>}
							data={billfindyear}
							options={{
								// Material design options
								chart: {
									subtitle: 'Biểu Đồ Doanh thu',
									colors:['#004411']

								},
							
							}}
							// For tests
							/* rootProps={{ 'data-testid': '1' }} */
						/>

					</div>
							{totalChart()}
				</Tab>
				<Tab eventKey="contact" title="Năm" >
				<div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-md-10 float-left">
                       
                    </div>
                    <div className="col-md-2 pt-3">
                        {selectCategory()}
                    </div>
                </div>
            </div>
					{/* <table  className="table table-hover table-bordered table-striped text-center" >
						<thead className="table bg-warning" style={{color:'white'}}>
							<tr>
								<th scope="col">STT </th>
								<th scope="col">Tháng năm</th>
								<th scope="col">Tổng tiền</th>
								
							</tr>
						</thead>
						<tbody>
							{list}
						</tbody>
					</table> */}
					<div className="card mt-2">
				<DataTable
					customStyles={customStyles}
					title="Tất Cả Sản Phẩm" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={datatest} // Dữ liệu đổ vào bảng
					defaultSortField="status"
					pagination // Hiển thị phân trang hay không
					selectableRowsComponent={BootyCheckbox} // checkbox all
					paginationPerPage={12}
					paginationRowsPerPageOptions={[12, 24, 36, 48, 60]}
				/>	
			</div>
					{totalTable()}
				</Tab>
			</Tabs>
			{/* 	xuất extends */}
			{/* 			<div className="App">
     			 <ExportToExcel apiData={data} fileName={fileName} />
    		</div> */}
			{/*  Hiện thi Table */}

		</div>
	);
}
