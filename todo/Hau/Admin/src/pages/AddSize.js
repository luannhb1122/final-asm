import React, { useState,useEffect } from 'react';
import Size from '../components/Axios/AxiosServicer';
import { Modal, Button } from 'react-bootstrap';
import './Style.css'
export default function Addstore() {

	const initialTypeProduct = {
		id: '',
		type: '',
		price: ''
	};;
	const [size, setsize] = useState(initialTypeProduct);

	function handleInputChange(evt) {
		const value = evt.target.value;
		setsize({
			...size,
			[evt.target.id]: value
		});
	}

	const saveTutorial = (data) => {
        var data = {
			type: size.type,
            price: size.price,
            status:true
		};
		if(validation() === true){
		Size.createsize(data)
			.then(response => {
				// console.log(response.data)
				handleClose();
				handleNotificationShow();
			})
			.catch(e => {
				handleNofErrorShow()
			});
		} else {

		}
	};

	function validation() {
		if (size.type === "" || size.price) {
			return handleNofErrorShow();
		} else if (size.price < 0 ) {
			return handleErrorFieldShow() ;
		} else {
			return true;
		}
	}

		////erorr field
		const [showErrorField, setShowErrorField] = useState(false);
		const handleErrorFieldClose = () => setShowErrorField(false);
		const handleErrorFieldShow = () => setShowErrorField(true);

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//Thông Báo thành công
	const [Notification, setNotification] = useState(false);
	const handleNotificationClose = () => setNotification(false);
	const handleNotificationShow = () => setNotification(true);
	const ReloadPage = () => {
		handleNotificationClose();
	window.location.reload()
	};
		//Thông Báo thất bại
		const [NofError, setNofError] = useState(false);
		const handleNofErrorClose = () => setNofError(false);
		const handleNofErrorShow = () => setNofError(true);
		const ReloadNofError= () => {
			handleNofErrorClose();
			handleShow();
		};
	return (
		<>
			<Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
			<i className="ft-file-plus">&nbsp;Thêm Size</i>
  </Button>
       {/* Modal Thông báo thành công */}
      <Modal dialogClassName="messmodal" show={Notification} onHide={handleNotificationClose}>
             <div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Thêm Thành Công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			       {/* Modal Thông báo thất bại */}
				   <Modal dialogClassName="messmodal" show={NofError} onHide={handleNofErrorClose}>
             <div className="icon-box">
					<i className="ft-x text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Không được để trống</p>
			</Modal.Body>

            <button className="btn btn-danger btn-block" type="button" onClick={ReloadNofError}>OK</button>
			</Modal>
			{/* Lỗi Field */}
			<Modal dialogClassName="messmodal" show={showErrorField} onHide={handleErrorFieldClose}>
				<div className="icon-boxx">
					<i className="ft-x text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">Giá Không được âm</p>
				</Modal.Body>

				<button className="btn btn-danger btn-sm btn-block" type="button" onClick={handleErrorFieldClose} >OK</button>
			</Modal>
			{/* Modal Loại */}
			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm Size</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
						<div className="submit-form">
								<div>
									<section className="panel panel-default">
										{/*Form Type */}
										
                                        <div className="form-group row">
									<label className="col-sm-3 control-label">Tên Size:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="type"
											required
											name="type"
											value={size.type}
											onChange={handleInputChange}
											placeholder="Size"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Giá:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="price"
											required
											name="price"
											value={size.price}
											onChange={handleInputChange}
											placeholder="price"
										/>
									</div>
								</div>
											<button className="btn btn-outline-success" style={{marginLeft:'150px'}} onClick={saveTutorial} >Thêm</button>
										
									</section>
								</div>
						
						</div>
					</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
