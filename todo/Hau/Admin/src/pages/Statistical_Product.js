import React, { useEffect, useState } from 'react';
import BillView from '../components/Axios/AxiosServicer'
import './Style.css'
import DataTable from 'react-data-table-component';
/* import { Nav, Col, Tab } from 'react-bootstrap';
import Tabs from 'react-bootstrap/Tabs' */
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';


export default function Bill(props) {


    // Khai báo giá trị
    //Khai báo set và get mảng
    //mãng tất cả sản phẩm
    const [Staticproduct, setStaticproduct] = useState([]);
    const [Staticproductmain, setStaticproductmain] = useState([]);
    const [allbill, setallbill] = useState([]);
    const [Search, setSearch] = useState([]);
    const [StaticproductYear, setStaticproductYear] = useState([]);

    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        GetStaticproduct();
        GetAllallbill();
    }, []);
    // tất cả sản phẩm bán
    const GetStaticproduct = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getStatisticProduct()
            .then((response) => {
                setStaticproduct(response.data);
                setSearch(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const GetAllallbill = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getAllBill()
            .then((response) => {
                setallbill(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const Getbillyear = (value) => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getproductfindyear(value)
            .then((response) => {
                setStaticproduct(response.data);
                setSearch(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    let year;
    const optionYear = allbill.map((item) => {
        if (item[1] !== year) {
            year = item[1];
            return (
                <option value={year}>
                    {"Năm " + year}
                </option>
            )
        }
    });
    const selectCategory = () => {
        return (
            <select
                className="form-control"
                id="date"
                name="date"
                onChange={handleInputChange} style={{ backgroundColor: 'white' }}
            >
                <option value="all">
                    Hiển thị tất cả
				</option>
                {optionYear}
            </select>
        );
    };

    function handleInputChange(evt) {
        const valueyear = evt.target.value;
        const test = ([]);
        if (valueyear == "all") {
            GetStaticproduct();
            console.log(valueyear)
        } else {
            Getbillyear(valueyear);
            console.log(valueyear)
        }
    }
    const datatest=[]
    // tất cả sản phẩm 
    const list = Search.map((item, index) => {
        const test={
            id:(index+1),
            name:item[0],
            quantity:item[1],
            total:item[2]
        }
        datatest.push(test);
    });
    // const list = Search.map((item, index) => {
    //     const test={
    //         a:(index+1),
    //         field1:item[0],
    //         field2:item[1],
    //     }
    //     datatest.push(test);
    //     console.log(columns)
    //     return (
    //         <tr key={index}>
    //             <td>{index+1}</td>
    //             <td>{item[0]}</td>
    //             <td>{item[1]}</td>
    //             <td><NumberFormat style={{background:'none',border:'none'}} value={item[2]} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></td>
    //         </tr>
    //     )
    // });
    const handleSearch = (event) => {
        let value = event.target.value.toLowerCase();
        let result = [];
        console.log(value);
        if (value === "") {
            setSearch(Staticproduct);
        } else {
            result = Staticproduct.filter((data) => {
                return data[0].toLowerCase().includes(value);
            });

            setSearch(result);
        }
    }
	let sumtotal=0;
	const total = () => {
		let sumtotal=0;
		for(let i =0; i<Search.length;i++){
			sumtotal += parseInt(Search[i][2]);
		}
		return (
			<NumberFormat className="form-control col-sm-4 float-right text-right"  style={{background:'none',border:'none',color:'red',fontWeight:'bold',fontSize:'18px',marginTop:'-10px' }} value={sumtotal} displayType={'text'} thousandSeparator={true} prefix={'Tổng doanh thu: '} suffix={' VNĐ'} />
		
		);
	};
    const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900',

			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center',

			}
		},

		headCells: {
			style: {
				fontSize: '15px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},
		cells: {
			style: {
				fontSize: '15px',
				paddingLeft: '0 8px',
				backgroundColor: 'white',
				justifyContent: 'center',
				alignitems: 'center',
				borderbottom: '#EEEEEE solid 10px'
			}
		}
	};
    const columns = [
		{
			name: 'STT',
			selector: 'id',
			sortable: true
		},
        {
			name: 'Tên sản phẩm',
			selector: 'name',
			sortable: true
		},
        {
			name: 'Số lượng bán',
			selector: 'quantity',
			sortable: true
		},
        {
			name: 'Tổng tiền',
			selector: 'total',
			sortable: true,
            cell:(row) =>(
                <NumberFormat style={{background:'none',border:'none'}} value={row.total} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />
                )
		}
	
	];
    const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
		<div className="custom-control custom-checkbox">
			<input type="checkbox" className="custom-control-input" ref={ref} {...rest} />
			<label className="custom-control-label" onClick={onClick} />
		</div>
	));
    return (
        <div>
            {/*     <!--Tiêu Đề--> */}
            <div class="input-group">
                <div className="breadcrumbs-area clearfix">
                    <ul className="breadcrumbs float-left list-unstyled">
                        <li class="breadcrumb-item" aria-current="page">
                            <Link className="breadcrumb-item" to="/">Trang Chủ</Link>
                            <Link className="breadcrumb-item" to="/Product">Tổng Số Doanh Thu</Link>
                        </li>
                    </ul>
                </div>
            </div>
            <div className="container-fluid">
                <div className="row mb-2">
                    <div className="col-md-10 float-left">
                        <label>Tìm Kiếm:</label>
                        <input type="text" className="form-control" placeholder="nhập tên sản phẩm" style={{ backgroundColor: 'white' }} onChange={(event) => handleSearch(event)} />
                    </div>
                    <div className="col-md-2 pt-3">
                        {selectCategory()}
                    </div>
                </div>
            </div>
            <br></br>
            {/* <table id="example" className="table  table-hover table-bordered table-striped  text-center" >
                <thead className="table bg-warning" style={{ color: 'white' }}>
                    <tr>
                    <th scope="col">STT</th>
                        <th scope="col">Tên Sản Phẩm </th>
                        <th scope="col">Số Lượng Bán</th>
                        <th scope="col">Tổng Tiền</th>
                    </tr>
                </thead>
                <tbody>
                    {list}
                </tbody>
            </table> */}
            {/*  Hiện thi Table */}
			<div className="card mt-2">
				<DataTable
					customStyles={customStyles}
					title="Tất Cả Sản Phẩm" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={datatest} // Dữ liệu đổ vào bảng
					defaultSortField="status"
					pagination // Hiển thị phân trang hay không
					selectableRowsComponent={BootyCheckbox} // checkbox all
                   
				/>
			</div>
            {total()}
        </div>

    );
}
