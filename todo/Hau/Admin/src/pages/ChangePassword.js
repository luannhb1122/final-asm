import React, { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import {Link,useHistory } from 'react-router-dom';
import { Modal} from 'react-bootstrap';

import AuthService from "../Service/Auth_Service";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const vusername = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

const vnewpassword  = (value) => {
    if (value.length <6 ){
      return (
        <div className="alert alert-danger" role="alert">
               The password must be between 6 and 40 characters.
        </div>
      );
    }
  };
  const vpassword = (value) => {
    if (value.length < 6 || value.length > 40) {
      return (
        <div className="alert alert-danger" role="alert">
          The password must be between 6 and 40 characters.
        </div>
      );
    }
  };
const Register = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const [username, setUsername] = useState("");
  const [oldpassword, setPassword] = useState("");
  const [newpassword, setNewpassword] = useState("");
  const [successful, setSuccessful] = useState(false);
  const [message, setMessage] = useState("");
  const history = useHistory();
  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const oldpassword = e.target.value;
    setPassword(oldpassword);
  };

  const onChangeNewPassword= (e) => {
    const newpassword = e.target.value;
    setNewpassword(newpassword);
  };



  const handleRegister = (e) => {
    e.preventDefault();

    setMessage("");
    setSuccessful(false);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      AuthService.changepassword(username,oldpassword,newpassword).then(
        (response) => {
          setMessage(response.data.message);
          setSuccessful(true);
          handleShow();
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setMessage(resMessage);
          setSuccessful(false);
        }
      );
    }
  };
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
  const ReloadPage = () => {
		handleClose();
    history.push("/");
	};
  return (
    <div className="purple-square container-fluid" style={{marginTop:'-130px',marginBottom:'-100px'}}> 
    <div className="col-md-3">
        <div className="card card-signin my-">
          <div className="card-body">
            <h5 className="card-title text-center text-uppercase">Đổi Mật Khẩu</h5>
        <Form onSubmit={handleRegister} ref={form}>
          {!successful && (
            <div>
              <div className="form-group">
                <label htmlFor="username">Tài Khoản</label>
                <Input
                  type="text"
                  className="form-control"
                  name="username"
                  value={username}
                  onChange={onChangeUsername}
                  validations={[required, vusername]}
                  placeholder="Tên Đăng Nhập"
                />
              </div>

              <div className="form-group">
                <label htmlFor="password">Mật Khẩu Cũ</label>
                <Input
                  type="password"
                  className="form-control"
                  name="oldpassword"
                  value={oldpassword}
                  onChange={onChangePassword}
                  validations={[required, vpassword]}
                  placeholder="Mật Khẩu"
                />
              </div>
              <div className="form-group">
                <label htmlFor="email">Mật Khẩu Mới</label>
                <Input
                  type="password"
                  className="form-control"
                  name="newpassword"
                  value={newpassword}
                  onChange={onChangeNewPassword}
                  validations={[required, vnewpassword]}
                  placeholder="Mật Khẩu Mới"
                />
              </div>
              <div className="form-group row mt-3">
                <button className="btn btn-primary ml-3">Đổi Mật Khẩu</button>
              </div>
            </div>
          )}

          {message && (
            <div className="form-group">
              <div
                className={
                  successful ? "alert alert-success" : "alert alert-danger"
                }
                role="alert"
              >
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
    </div>
    </div>
    		<Modal dialogClassName="messmodal" show={show} onHide={handleClose}>
			<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>	
				<Modal.Body>
				<p className="text-center mt-4">Đổi mật khẩu thành công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button"  onClick={ReloadPage}>OK</button>
			</Modal>
    </div>

  );
};

export default Register;

// import React, { useState, useEffect } from 'react';
// import Data from '../components/Axios/AxiosServicer';
// import { Modal, Button } from 'react-bootstrap';
// import './Style.css'
// export default function Account() {
//   const initialTutorialState = {
//     username: "",
//     oldpassword: "",
//     newpassword:""
//   };
//   const [register, setregister] = useState(initialTutorialState);

//   function handleInputChange(evt) {
//     const value = evt.target.value;
//     setregister({
//       ...register,
//       [evt.target.id]: value
//     });
//   }

//   const saveTutorial = (data) => {
//     var data = {
//       username: (register.username),
//       oldpassword: register.oldpassword,
//       newpassword:  register.newpassword,
//     };
//     Data.changePassword(data)
//       .then(response => {
//         handleClose();
//         handleNotificationShow();
//       })
//       .catch(e => {
//         handleNofErrorShow()
//       });
//   };


//   const [show, setShow] = useState(false);
//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);
//   //Thông Báo thành công
//   const [Notification, setNotification] = useState(false);
//   const handleNotificationClose = () => setNotification(false);
//   const handleNotificationShow = () => setNotification(true);
//   const ReloadPage = () => {
//     handleNotificationClose();
//     window.location.reload()
//   };
//   //Thông Báo thất bại
//   const [NofError, setNofError] = useState(false);
//   const handleNofErrorClose = () => setNofError(false);
//   const handleNofErrorShow = () => setNofError(true);
//   const ReloadNofError = () => {
//     handleNofErrorClose();
//     handleShow();
//   };
//   return (
//     <>
//       <Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
//         <i className="ft-file-plus">&nbsp;Thêm Admin</i>
//       </Button>
//       {/* Modal Thông báo thành công */}
//       <Modal dialogClassName="messmodal" show={Notification} onHide={handleNotificationClose}>
//         <div className="icon-box">
//           <i className="ft-check text-center"></i>
//         </div>
//         <Modal.Body>
//           <p className="text-center mt-4">Thêm Thành Công</p>
//         </Modal.Body>

//         <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
//       </Modal>
//       {/* Modal Thông báo thất bại */}
//       <Modal dialogClassName="messmodal" show={NofError} onHide={handleNofErrorClose}>
//         <div className="icon-box">
//           <i className="ft-x text-center"></i>
//         </div>
//         <Modal.Body>
//           <p className="text-center mt-4">Thêm Thất bại</p>
//         </Modal.Body>

//         <button className="btn btn-danger btn-block" type="button" onClick={ReloadNofError}>OK</button>
//       </Modal>
//       {/* Modal Loại */}
//       <Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
//         <Modal.Header closeButton>
//           <Modal.Title >Thêm Adim</Modal.Title>
//         </Modal.Header>
//         <Modal.Body>
//           <div className='container'>
//             <div className="submit-form">
//               <div>
//                 <section className="panel panel-default">
//                   {/*Form Type */}
//                   <div className='form-group row'>
//                     <label for="name" className="col-sm-3 control-label">username:</label>
//                     <div className="col-sm-8">
//                       <input type="text"
//                         className="form-control"
//                         id="username"
//                         required
//                         value={register.username}
//                         onChange={handleInputChange}
//                         name="username" placeholder="username" />
//                     </div>
//                   </div>
//                   <div className='form-group row'>
//                     <label for="name" className="col-sm-3 control-label">oldpassword:</label>
//                     <div className="col-sm-8">
//                       <input type="text"
//                         className="form-control"
//                         id="oldpassword"
//                         required
//                         value={register.oldpassword}
//                         onChange={handleInputChange}
//                         name="oldpassword" placeholder="oldpassword" />
//                     </div>
//                   </div>
//                   <div className='form-group row'>
//                     <label for="name" className="col-sm-3 control-label">oldpassword:</label>
//                     <div className="col-sm-8">
//                       <input type="text"
//                         className="form-control"
//                         id="newpassword"
//                         required
//                         value={register.newpassword}
//                         onChange={handleInputChange}
//                         name="newpassword" placeholder="newpassword" />
//                     </div>
//                   </div>
//                   {/* <button type="submit" className="btn btn-sm btn-success"  onClick={saveTutorial}>Thêm</button> */}
//                   <div className='mt-4 div-button'>
//                     <button className="btn btn-outline-success mr-2 " onClick={saveTutorial} >Thêm</button>
//                     {/* <button className="btn btn-outline-primary " onClick={newProduct} >Clear</button> */}
//                   </div>
//                 </section>
//               </div>

//             </div>
//           </div>
//         </Modal.Body>
//       </Modal>
//     </>

//   );
// }
