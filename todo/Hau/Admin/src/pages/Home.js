import React, { useEffect, Fragment, useState } from 'react';
/* import Feedback from '../Axios/AxiosServicer/feedback'; */
import Category from '../components/Axios/AxiosServicer';
import './Style.css'
import Data from '../components/Axios/AxiosServicer';
import NumberFormat from 'react-number-format';
import BillView from '../components/Axios/AxiosServicer';

import { Nav, Col, Tab } from 'react-bootstrap';
import Tabs from 'react-bootstrap/Tabs'
import { Chart } from "react-google-charts";
export default function Home() {
    const [product, setproduct] = useState([]);
    const [total, settotal] = useState([]);
    const [Staticproduct, setStaticproduct] = useState([]);
    const [bill, setbill] = useState([]);
    const [ Cus, setCus ] = useState([]);
    // const [QuantityProduct, setQuantityProduct] = useState('');
    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        GetAllProduct();
        Gettotal();
        GetStaticproduct();
        GetALlbill();
        GetALlemp();
    }, [])
    const GetAllProduct = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        Data.getAllproduct()
            .then((response) => {
                setproduct(response.data);
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };
    // sản phẩm nổi bật
    const ProductList = product.map((item, index) => {
        if (item.status === 2) {
            return (
                <tr key={index}>
                    <td><img height="150px" width="200px" alt="" src={`${item.image}`} /></td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>{item.categories.type}</td>
                </tr>
            )
        }
    });
    // total
    const Gettotal = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        Data.Gettotal()
            .then((response) => {
                settotal(response.data);
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const GetStaticproduct = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getStatisticProduct()
            .then((response) => {
                setStaticproduct(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const GetALlbill = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.Getbill()
			.then((response) => {
				setbill(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};

    const GetALlemp = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getAllCustomer()
			.then((response) => {
				setCus(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};

    const quantityOrderProduct = () => {
        let sumtotal = 0;
        for (let i = 0; i < Staticproduct.length; i++) {
            sumtotal += parseInt(Staticproduct[i][1]);
        }
        return sumtotal;
    }

    // biểu đồ
    const [billyear2020, setbillyear2020] = useState([]);
    const [billfindyear, setbillfindyear] = useState([]);
    const [billfindyeartotal, setbillfindyeartotal] = useState([]);
    const [allbill, setallbill] = useState([]);
    const [billyear, setbillyear] = useState([]);
    const [billa, setbilla] = useState([]);

    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        Getbillyear2020();
        Getbillfindyear();
        GetAllallbill();
        Getbillyear();

    }, []);


    const Getbillyear2020 = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getbillyear2020()
            .then((response) => {

                setbillyear2020(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };
    const Getbillfindyear = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getbillfindyear(2021)
            .then((response) => {
                const datachart = [
                    ['Tháng', "Doanh thu 2021"]
                ];
                setbillfindyeartotal(response.data)
                setbillfindyear(datachart.concat(response.data))
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };
    const GetAllallbill = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getAllBill()
            .then((response) => {
                setallbill(response.data);
                setbilla(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    const Getbillyear = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        BillView.getbillyear()
            .then((response) => {
                setbillyear(response.data);
                console.log(response.data)
            })
            .catch((e) => {
                console.log(e);
            });
    };

    function handleInputChangechart(evt) {
        const value = evt.target.value;
        BillView.getbillfindyear(value)
            .then((response) => {
                const datachart = [
                    ['Tháng', "Doanh thu " + value]
                ];
                setbillfindyeartotal(response.data)
                setbillfindyear(datachart.concat(response.data))
                console.log(billfindyear)
            })
            .catch((e) => {
                console.log(e);
            });

    }

    const list = billa.map((item, index) => {
        return (
            <tr key={index}>
                <td>{index + 1}</td>
                <td>{item[0]}/{item[1]}</td>
                <td><NumberFormat style={{ background: 'none', border: 'none' }} value={item[2]} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></td>
            </tr>
        )
    });

    //Set selected default value
    let year;
    const optionYear = allbill.map((item) => {
        if (item[1] !== year) {
            year = item[1];
            return (
                <option value={item[1]}>
                    {"Năm " + item[1]}
                </option>
            )
        }
    });

    const selectchartyear = () => {
        return (
            <select
                className="form-control"
                id="date"
                name="date"
                onChange={handleInputChangechart} style={{ backgroundColor: 'white' }}
            >
                {optionYear}
            </select>
        );
    };

    // const NumberFormat = require('react-number-format');

    const totalChart = () => {
        let sumtotal = 0;
        for (let i = 0; i < billfindyeartotal.length; i++) {
            sumtotal += parseInt(billfindyeartotal[i][1]);
        }
        return (
            <NumberFormat className="form-control col-sm-4 float-right text-right" style={{ background: 'none', border: 'none', color: 'red', fontWeight: 'bold', fontSize: '18px' }} value={sumtotal} displayType={'text'} thousandSeparator={true} prefix={'Tổng doanh thu: '} suffix={' VNĐ'} />

        );
    };

    return (
        <div>
            <div className="content-overlay"></div>
            <div className="content-wrapper">
                {/*               <!--Statistics cards Starts--> */}
                <div className="row">
                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-purple-love">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0"> <NumberFormat style={{ background: 'none', border: 'none', fontWeight: 'bold', fontSize: '18px' }} value={parseInt(total)} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></h3>
                                            <span>Tổng doanh thu</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-activity font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-ibiza-sunset">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0"><NumberFormat style={{ background: 'none', border: 'none', fontWeight: 'bold', fontSize: '18px' }} value={parseInt(quantityOrderProduct())} displayType={'text'} thousandSeparator={true} suffix={' sản phẩm'} /></h3>
                                            <span>Tổng số sản phẩm đã bán</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-percent font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart1" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-mint">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0"><NumberFormat style={{ background: 'none', border: 'none', fontWeight: 'bold', fontSize: '18px' }} value={parseInt(Cus.length)} displayType={'text'} thousandSeparator={true} suffix={' Khách hàng'} /></h3>
                                            <span>Tổng số khách hàng</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-trending-up font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart2" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-king-yna">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0"><NumberFormat style={{ background: 'none', border: 'none', fontWeight: 'bold', fontSize: '18px' }} value={parseInt(bill)} displayType={'text'} thousandSeparator={true} suffix={' Hóa đơn'} /></h3>
                                            <span>Tổng số hóa đơn bán</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-credit-card font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart3" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*                     <!--Statistics cards Ends-->

                    <!--Line with Area Chart 1 Starts--> */}
                <div className="row">
                    <div className="col-12">
                        <div className="form-group">
                            <div className="" style={{ float: 'right', marginBottom: 15 }} >
                                {selectchartyear()}
                            </div>
                        </div>
                        <h3 style={{ color: 'blue' }}>Biểu Đồ Thể Hiện Doanh thu</h3>
                        <div className='pt-2'>
                            <Chart
                                width={'1250px'}
                                height={'500px'}
                                colors={'#004411'}
                                chartType="AreaChart"
                                loader={<div>Loading Chart</div>}
                                data={billfindyear}
                                options={{
                                    // Material design options
                                    chart: {


                                    },

                                }}
                            />
                        </div>
                        {totalChart()}

                    </div>
                </div>
                {/*       <!--Line with Area Chart 1 Ends--> */}
                {/* biểu đò */}
                {/* end biểu đồ */}
                <div>
                    <div className="pt-2">
                        <h4 className="card-title pt-2" style={{ color: 'blue' }}>Mặt Hàng Nổi Bật</h4>
                    </div>
                    <table className="table table-hover table-bordered table-striped text-center" >
                        <thead className="table bg-warning" style={{ color: 'white' }}>
                            <tr>
                                <th>Hình ảnh</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá</th>
                                <th>Loại sản phẩm</th>
                            </tr>
                        </thead>
                        <tbody>
                            {ProductList}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    )
}
