import React, { useState,useEffect } from 'react';
import Store from '../components/Axios/AxiosServicer';
import { Modal, Button } from 'react-bootstrap';
import './Style.css'
export default function Stores() {

	const initialTypeProduct = {
		id: '',
		name: '',
		phone: '',
        email: '',
        address: '',
	};
	const [store, setstore] = useState(initialTypeProduct);

	function handleInputChange(evt) {
		const value = evt.target.value;
		setstore({
			...store,
			[evt.target.id]: value
		});
	}

	const saveTutorial = (data) => {
		var data = {
			name: store.name,
			phone: store.phone,
            email: store.email,
            address: store.address,
		
		};
		Store.createStore(data)
			.then(response => {
				// console.log(response.data)
				handleClose();
				handleNotificationShow();
			})
			.catch(e => {
				handleNofErrorShow()
			});
	};


	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//Thông Báo thành công
	const [Notification, setNotification] = useState(false);
	const handleNotificationClose = () => setNotification(false);
	const handleNotificationShow = () => setNotification(true);
	const ReloadPage = () => {
		handleNotificationClose();
	window.location.reload()
	};
		//Thông Báo thất bại
		const [NofError, setNofError] = useState(false);
		const handleNofErrorClose = () => setNofError(false);
		const handleNofErrorShow = () => setNofError(true);
		const ReloadNofError= () => {
			handleNofErrorClose();
			handleShow();
		};
	return (
		<>
			<Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
			<i className="ft-file-plus">&nbsp;Thêm Cửa Hàng</i>
  </Button>
       {/* Modal Thông báo thành công */}
      <Modal dialogClassName="messmodal" show={Notification} onHide={handleNotificationClose}>
             <div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Thêm Thành Công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			       {/* Modal Thông báo thất bại */}
				   <Modal dialogClassName="messmodal" show={NofError} onHide={handleNofErrorClose}>
             <div className="icon-box">
					<i className="ft-x text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Thêm Thất bại</p>
			</Modal.Body>

            <button className="btn btn-danger btn-block" type="button" onClick={ReloadNofError}>OK</button>
			</Modal>
			{/* Modal Loại */}
			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm cửa hàng</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
						<div className="submit-form">
								<div>
									<section className="panel panel-default">
										{/*Form Type */}
										
                                        <div className="form-group row">
									<label className="col-sm-3 control-label">name:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="name"
											required
											name="name"
											value={store.name}
											onChange={handleInputChange}
											placeholder="name"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Phone:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="phone"
											required
											name="phone"
											value={store.phone}
											onChange={handleInputChange}
											placeholder="phone"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Email:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="email"
											required
											name="email"
											value={store.email}
											onChange={handleInputChange}
											placeholder="emial"
										/>
									</div>
								</div>
                                <div className="form-group row">
									<label className="col-sm-3 control-label">Đại Chỉ:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="address"
											required
											name="address"
											value={store.address}
											onChange={handleInputChange}
											placeholder="address"
										/>
									</div>
								</div>
											<button className="btn btn-outline-success col-sm-2 " onClick={saveTutorial} >Thêm</button>
										
									</section>
								</div>
						
						</div>
					</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
