import React, { useEffect, Fragment, useState } from 'react';
import Data from '../components/Axios/AxiosServicer';
import DataTable from 'react-data-table-component';
import AddVoucher from './AddVoucher';
import { Modal, Row } from 'react-bootstrap';
import AddProduct from './AddProduct';
import './Style.css';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';

export default function Voucher(props) {
    // Khai báo giá trị
    const initialproducttate = {
        id: '',
        status: '',
        value: '',
        start_time: '',
        end_time: '',
        quanity: ''
    };


    //Khai báo set và get mảng
    const [voucher, setvoucher] = useState([]);
    const [voucheritem, setvoucheritem] = useState(initialproducttate);
    // const [search, setsearch] = useState('');
    const [searchID, setSearchID] = useState('');

    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        GetAllVoucher();
    }, []);
    const GetAllVoucher = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        Data.getpVoucher()
            .then((response) => {
                setvoucher(response.data);
                setSearchID(response.data)
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };
    const setActiveTutorial = (voucher, index) => {
        setvoucheritem(voucher);
        handleShowModal();
    };
    // Sự kiện nhập giá trị vào form
    function handleInputChange(evt) {
        const value = evt.target.value;
        setvoucheritem({
            ...voucheritem,
            [evt.target.name]: value
        });

    }


    // Cập nhật dữ liệu sản phẩm
    const UpdateProduct = () => {
        var data = {
            status: voucheritem.status,
            value: parseInt(voucheritem.value),
            start_time: voucheritem.start_time,
            end_time: voucheritem.end_time,
            quanity: voucheritem.quanity,

        };
        Data.updateVoucher(voucheritem.id, data)
            .then((response) => {
                console.log(response.data);
                handleCloseModal();
                handleMessageShow()

            })
            .catch((e) => {
                console.log(e);
            });
    };

    //Set selected default value
    //Xóa
    const deleteTutorial = () => {
        Data.removeVoucher(voucheritem.id)
            .then((response) => {
                console.log(response.data);
                handleCloseModal();
                handleDeleteShow()
            })
            .catch((e) => {
                console.log(e);
            });
    };

    //Hiển thị Modal
    const [show, setShow] = useState(false);
    const handleCloseModal = () => setShow(false);
    const handleShowModal = () => setShow(true);
    // Hiển thị title theo cột trong table
    const columns = [
        {
            name: 'ID',
            selector: 'id',
            sortable: true
        },
        {
            name: 'Trạng Thái',
            selector: 'status',
            sortable: true,
            cell: (row) => (row.status === true ? 'Hiện' : 'Ẩn')
        },
        {
            name: 'Giá trị',
            selector: 'value',
            sortable: true,
            cell:(row) =>(<NumberFormat style={{ background: 'none', border: 'none' }} value={row.value} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />)
        },
        {
            name: 'Ngày bắt đầu',
            selector: 'start_time',
            sortable: true
        },
        {
            name: 'ngày kết thúc',
            selector: 'end_time',
            sortable: true
        },
        {
            name: 'Số lượng',
            selector: 'quanity',
            sortable: true
        },

        {
            name: 'Chức Năng',
            key: 'action',
            text: 'Action',
            cell: (voucher, index) => {
                return (
                    <Fragment>
                        <button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(voucher, index)}>
                            <i className="ft-edit-1">Cập Nhật</i>
                        </button>
                    </Fragment>
                );
            }
        }
    ];
    //CSS DATATABLE
    const customStyles = {
        title: {
            style: {
                fontColor: 'red',
                fontWeight: '900',

            }
        },
        rows: {
            style: {
                minHeight: '72px', // override the row height
                backgroundColor: '#F7F7F8',
                justifyContent: 'center',
                alignitems: 'center',

            }
        },

        headCells: {
            style: {
                fontSize: '15px',
                fontWeight: '500',
                textTransform: 'uppercase',
                paddingLeft: '0 8px',
                backgroundColor: '#fff',
                fontColor: 'black',
                justifyContent: 'center',
                alignitems: 'center'
            }
        },
        cells: {
            style: {
                fontSize: '15px',
                paddingLeft: '0 8px',
                backgroundColor: 'white',
                justifyContent: 'center',
                alignitems: 'center',
                borderbottom: '#EEEEEE solid 10px'
            }
        }
    };
    // Checkbox tất cả table
    //Modal Update
    const [showmes, setShowmes] = useState(false);
    const handleMessageClose = () => setShowmes(false);
    const handleMessageShow = () => setShowmes(true);
    //Modal Delete
    const [showDelete, setShowDelete] = useState(false);
    const handleDeleteClose = () => setShowDelete(false);
    const handleDeleteShow = () => setShowDelete(true);
    const ReloadPage = () => {
        handleMessageClose();
        refreshTable();
    };
    const Reload = () => {
        handleDeleteClose();
        refreshTable();
    };

    //Reset lại Table

    const refreshTable = () => {
        GetAllVoucher();
    };
    //tiem kiếm
    const handleSearch = (event) => {
		let value = event.target.value.toLowerCase();
		let result = [];
		console.log(value);
		if (value === "") {
			setSearchID(voucher);
			console.log(searchID);
		} else {
			result = voucher.filter((data) => {
				return data.start_time.toLowerCase().includes(value);
			});

			setSearchID(result);
		}
	}

    return (
        <div style={{marginTop:'-100px'}}>
            {/*     <!--Tiêu Đề--> */}
            <div class="input-group">
                <div className="breadcrumbs-area clearfix">
                    <ul className="breadcrumbs float-left list-unstyled">
                        <li class="breadcrumb-item" aria-current="page">
                            <Link className="breadcrumb-item" to="/">
                                Trang Chủ
							</Link>
                            <Link className="breadcrumb-item" to="/Product">
                                Tất cả sản phẩm
							</Link>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-10 float-left">
                        <label>Tìm Kiếm: </label>
                        <input type="text" className="form-control" placeholder="nhập tên sản phẩm" style={{ backgroundColor: 'white' }} onChange={(event) => handleSearch(event)} />
                    </div>
                    <div className="col-md-2 pt-3">
                        <AddVoucher />
                    </div>
                </div>
            </div>

            {/*  Hiện thi Table */}
            <div className="card mt-2">
                <DataTable
                    customStyles={customStyles}
                    title="Tất Cả voucher" //Tiêu đề bảng
                    columns={columns} // Hiển thị cột
                    data={searchID} // Dữ liệu đổ vào bảng
                    defaultSortField="id"
                    pagination // Hiển thị phân trang hay không
                // checkbox all
                />
            </div>
            {/* Modal Thông báo*/}
            <Modal dialogClassName="messmodal" show={showmes} onHide={handleMessageClose}>
                <div className="icon-box">
                    <i className="ft-check text-center"></i>
                </div>
                <Modal.Body>
                    <p className="text-center mt-4 text-modal">Cập Nhật Thành Công</p>
                </Modal.Body>

                <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
            </Modal>
            <Modal dialogClassName="messmodal" show={showDelete} onHide={handleDeleteClose}>
                <div className="icon-box">
                    <i className="ft-check text-center"></i>
                </div>
                <Modal.Body>
                    <p className="text-center mt-4">Xóa Thành Công</p>
                </Modal.Body>

                <button className="btn btn-primary" type="button" onClick={Reload}>OK</button>
            </Modal>
            {voucheritem ? (
                <Modal
                    dialogClassName="dialogModal"
                    show={show}
                    onHide={handleCloseModal}
                    keyboard={false}
                >
                    <Modal.Header closeButton>
                        <Modal.Title className="tille_Modal">Cập Nhật Sản Phẩm</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <section className="panel panel-default">
                                <div className="form-group row">
                                    {/*nhập tên sản phẩm */}
                                    <div className="form-group col-sm-12">
                                        <label className="control-label">Trạng thái:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="status"
                                            name="status"
                                            value={(voucheritem.status === true) ? "Hiện" : "Ẩn"}
                                            onChange={handleInputChange}
                                            placeholder="Tên Sản Phẩm"
                                        />
                                        {/*    Giá */}
                                        <label className="control-label mt-3">Giá:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="value"
                                            value={voucheritem.value}
                                            onChange={handleInputChange}
                                            name="value"
                                            placeholder="Giá"
                                        />
                                        {/* start_time */}
                                        <label className="control-label mt-3">Ngày Bắt Đầu:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="start_time"
                                            value={voucheritem.start_time}
                                            onChange={handleInputChange}
                                            name="start_time"
                                            placeholder="Ngày bắt đầu"
                                        />
                                        {/* end_time */}
                                        <label className="control-label mt-3">Ngày Kết Thúc:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="end_time"
                                            value={voucheritem.end_time}
                                            onChange={handleInputChange}
                                            name="end_time"
                                            placeholder="ngày kết thúc"
                                        />
                                        {/* id-type */}
                                        <label className="control-label mt-3">Số lượng:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="quanity"
                                            value={voucheritem.quanity}
                                            onChange={handleInputChange}
                                            name="quanity"
                                            placeholder="Số lượng"
                                        />
                                       
                                    </div>
                
                                </div>
                                {/*  Nút button Update */}

                                <div className="float-right">
                                <button type="submit" className="btn btn-sm btn-success " onClick={UpdateProduct} style={{ marginRight: '20px' }}>
                                        Cập Nhật
									</button>
                                    <button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
                                        Xóa
									</button>
                                </div>

                            </section>

                        </div>
                    </Modal.Body>
                </Modal>
            ) : (
                <div>
                    <br />
                    <p>Chưa có dữ Liệu</p>
                </div>
            )}
        </div>
    );
}
