import React, { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";
import {Link,useHistory } from 'react-router-dom';
import { Modal} from 'react-bootstrap';

import AuthService from "../Service/Auth_Service";

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const validEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const vusername = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};


const Forgot = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [successful, setSuccessful] = useState(false);
  const [message, setMessage] = useState("");
  const history = useHistory();
  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };


  const onChangeEmail = (e) => {
    const email = e.target.value;
    setEmail(email);
  };


  const handleRegister = (e) => {
    e.preventDefault();

    setMessage("");
    setSuccessful(false);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      AuthService.forgot(username,email).then(
        (response) => {
          setMessage(response.data.message);
          setSuccessful(true);
          handleShow();
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();
          setMessage(resMessage);
          setSuccessful(false);
        }
      );
    }
  };
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
  const ReloadPage = () => {
		handleClose();
    history.push("/otp");
	};
  return (
    <div className="purple-square container-fluid"> 
    <div className="col-md-3">
        <div className="card card-signin my-">
          <div className="card-body">
            <h5 className="card-title text-center text-uppercase">Quên Mật Khẩu</h5>

        <Form onSubmit={handleRegister} ref={form}>
          {!successful && (
            <div>
              <div className="form-group">
                <label htmlFor="username">Tài Khoản</label>
                <Input
                  type="text"
                  className="form-control"
                  name="username"
                  value={username}
                  onChange={onChangeUsername}
                  validations={[required, vusername]}
                  placeholder="Tên Đăng Nhập"
                />
              </div>

              <div className="form-group">
                <label htmlFor="email">Email</label>
                <Input
                  type="text"
                  className="form-control"
                  name="email"
                  value={email}
                  onChange={onChangeEmail}
                  validations={[required, validEmail]}
                  placeholder="Email"
                />
              </div>
              <div className="form-group row mt-3">
             <button className="btn btn-primary ml-4 ">Lấy Mã OTP</button>
                <Link to='/login'><button className="btn btn-primary ml-4">Quay Lại</button></Link>
              </div>
            </div>
          )}

          {message && (
            <div className="form-group">
              <div
                className={
                  successful ? "alert alert-success" : "alert alert-danger"
                }
                role="alert"
              >
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
    </div>
    </div>
    		<Modal dialogClassName="messmodal" show={show} onHide={handleClose}>
			<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>	
				<Modal.Body>
				<p className="text-center mt-4">Lấy Mã OTP thành công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
    </div>
    
  );
};

export default Forgot;