import React, { useState,useEffect } from 'react';
import Data from '../components/Axios/AxiosServicer';
import { Modal, Button } from 'react-bootstrap';
import './Style.css'
export default function Account() {
	const initialTutorialState = {
		type: "",
		status:""
	};
	const [tutorial, setTutorial] = useState(initialTutorialState);

	function handleInputChange(evt) {
		const value = evt.target.value;
		setTutorial({
			...tutorial,
			[evt.target.id]: value
		});
	}
	


	const saveTutorial = (data) => {
		var data = {
			type: (tutorial.type),
			status: true,
		};
		if(validation() === true){
			Data.createcategory(data)
			.then(response => {
				// console.log(response.data)
				handleClose();
				handleNotificationShow();
			})
			.catch(e => {
				handleNofErrorShow()
			});
		} else {

		}
	};

	function validation() {
		if (tutorial.type === "") {
			return handleNofErrorShow();
		}else {
			return true;
		}
	}


	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//Thông Báo thành công
	const [Notification, setNotification] = useState(false);
	const handleNotificationClose = () => setNotification(false);
	const handleNotificationShow = () => setNotification(true);
	const ReloadPage = () => {
		handleNotificationClose();
	window.location.reload()
	};
		//Thông Báo thất bại
		const [NofError, setNofError] = useState(false);
		const handleNofErrorClose = () => setNofError(false);
		const handleNofErrorShow = () => setNofError(true);
		const ReloadNofError= () => {
			handleNofErrorClose();
			handleShow();
		};
	return (
		<>
			<Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
			<i className="ft-file-plus">&nbsp;Thêm Loại</i>
  </Button>
       {/* Modal Thông báo thành công */}
      <Modal dialogClassName="messmodal" show={Notification} onHide={handleNotificationClose}>
             <div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Thêm Thành Công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			       {/* Modal Thông báo thất bại */}
				   <Modal dialogClassName="messmodal" show={NofError} onHide={handleNofErrorClose}>
             <div className="icon-box">
					<i className="ft-x text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Không được để trống</p>
			</Modal.Body>

            <button className="btn btn-danger btn-block" type="button" onClick={ReloadNofError}>OK</button>
			</Modal>
			{/* Modal Loại */}
			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm Loại</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
						<div className="submit-form">
								<div>
									<section className="panel panel-default">
										{/*Form Type */}
										<div className='form-group row'>
											<label for="name" className="col-sm-3 control-label">Loại:</label>
											<div className="col-sm-7">
												<input type="text"
													className="form-control"
													id="type"
													required
													value={tutorial.type}
													onChange={handleInputChange}
													name="type" placeholder="Loại" />
											</div>
										
											<button className="btn btn-outline-success col-sm-2 " onClick={saveTutorial} >Thêm</button>
										</div>
									</section>
								</div>
						
						</div>
					</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
