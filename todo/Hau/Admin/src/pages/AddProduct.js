import React, { useState, useEffect } from 'react';
import ProductDataService from '../components/Axios/AxiosServicer';
import Category from '../components/Axios/AxiosServicer';
import axios from 'axios';
import { Modal, Button } from 'react-bootstrap';

import './Style.css'
export default function Account(props) {
	const initialProductState = {
		name: '',
		price: '',
		image: '',
		categories: {
			id: ''
		}
	};
	const [Product, setProduct] = useState(initialProductState);
	const [image, setImage] = useState('')
	const [loading, setLoading] = useState(false)

	function handleInputChange(evt) {
		const value = evt.target.value;
		setProduct({
			...Product,
			[evt.target.name]: value
		});
	}
	const saveProduct = () => {
		var data = {
			name: Product.name,
			price: parseFloat(Product.price),
			image: image,
			status: 1,
			categories: {
				id: (parseInt(Product.categories) >= 0) ? parseInt(Product.categories) : list[0].id
			}
		};
		console.log(data)
		if (validation() === true) {
			ProductDataService.createproduct(data)
				.then((response) => {
					handleClose();
					handleMessageShow()

				})
				.catch((e) => {
					handleClose();
					handleErrorShow()
				});
		} else {
			// handleErrorFieldShow()
		}


	};

	const [list, setList] = React.useState([]);
	useEffect(() => {
		GetType();
		setImage("https://res.cloudinary.com/minhchon/image/upload/v1619504668/Thecoffeeson/noimage_m1zogb.jpg?fbclid=IwAR06Tlla_8IxL0OF8W1Q7Zt9RUPAWBUp7zY0hbsg0Kaeg6OidiCKC6jD1Vs")
	}, []);
	const GetType = () => {
		Category.getAllcategory()
			.then((response) => {
				setList(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const optionCategory = list.map((item) => {
		return (
			<option value={item.id}>{item.type}</option>
		)
	});
	const selectCategory = () => {
		return (
			<select className='form-control' id="categories" name="categories" onChange={handleInputChange}>
				{optionCategory}
			</select>
		)
	};

	const newProduct = () => {
		setProduct(initialProductState);

	};
	const ReloadPage = () => {
		handleMessageClose();
		window.location.reload()

	};
	const ReloadError = () => {
		handleErrorClose();
		handleShow();
	};
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//success
	const [showmes, setShowmes] = useState(false);
	const handleMessageClose = () => setShowmes(false);
	const handleMessageShow = () => setShowmes(true);
	////erorr
	const [showError, setShowError] = useState(false);
	const handleErrorClose = () => setShowError(false);
	const handleErrorShow = () => setShowError(true);
	////erorr field
	const [showErrorField, setShowErrorField] = useState(false);
	const handleErrorFieldClose = () => setShowErrorField(false);
	const handleErrorFieldShow = () => setShowErrorField(true);
	//Upload Hình ảnh
	const uploadImage = async e => {
		const files = e.target.files
		const data = new FormData()
		data.append('file', files[0])
		data.append('upload_preset', 'Thecoffeeson')
		setLoading(true)
		const res = await fetch(
			'	https://api.cloudinary.com/v1_1/minhchon/image/upload',
			{
				method: 'POST',
				body: data
			}
		)
		const file = await res.json()
		setImage(file.secure_url)
		console.log(image)
		setLoading(false)
	}
	function validation() {
		if (Product.name === "" || Product.price === "") {
			return handleErrorShow();
		} else if (Product.price < 0 ) {
			return handleErrorFieldShow() ;
		} else {
			return true;
		}
	}

	return (
		<>
			{/* Modal Thông báo*/}
			<Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
				<i className="ft-file-plus">&nbsp;Thêm Sản Phẩm</i>
			</Button>
			<Modal dialogClassName="messmodal" show={showmes} onHide={handleMessageClose}>
				<div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">Thêm Thành Công</p>
				</Modal.Body>

				<button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			{/* Modal Error*/}
			<Modal dialogClassName="messmodal" show={showError} onHide={handleErrorClose}>
				<div className="icon-boxx">
					<i className="ft-x text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">không được để trống</p>
				</Modal.Body>

				<button className="btn btn-danger btn-sm btn-block" type="button" onClick={ReloadError}>OK</button>
			</Modal>
			{/* Lỗi Field */}
			<Modal dialogClassName="messmodal" show={showErrorField} onHide={handleErrorFieldClose}>
				<div className="icon-boxx">
					<i className="ft-x text-center"></i>
				</div>
				<Modal.Body>
					<p className="text-center mt-4">Giá Không được âm</p>
				</Modal.Body>

				<button className="btn btn-danger btn-sm btn-block" type="button" onClick={handleErrorFieldClose} >OK</button>
			</Modal>
			{/* Modal Thêm */}
			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm Sản phẩm</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
								<section className="panel panel-default">
									<div className="form-group row">
										<div className="form-group col-sm-5">
											<label className="control-label">Hình Ảnh:</label>
											<input
												className="file-upload-input "
												type="file"
												name="image"
												onChange={uploadImage}
											/>
											<div class="drag-text" />
											{loading ? (
												<h6 className="spinner-border text-primary"> <span class="sr-only">Loading...</span></h6>
											) : (
												<img
													src={image}
													alt={Product.name}
													style={{ width: '100%', height: "300px" }}
												/>
											)}
										</div>
										{/* // nhập tên sản phẩm */}
										<div className='form-group col-sm-7'>
											<label for="name" className="control-label">Tên Sản Phẩm:</label>
											<input type="text"
												className="form-control"
												id="name"
												required
												value={Product.name}
												onChange={handleInputChange}
												name="name" placeholder="Tên Sản Phẩm" />
											{/*    Giá */}
											<label for="name" className="control-label mt-4">Giá:</label>
											<input type="text"
												className="form-control"
												id="price"
												required
												value={Product.price}
												onChange={handleInputChange}
												name="price" placeholder="Giá" />


											{/*  Loại */}

											<label for="name" className="control-label mt-4">Loại:</label>
											<div className="">
												{selectCategory()}
											</div>
											<div className=' mt-4 div-button'>
											<button className="btn btn-outline-success mr-2 " onClick={saveProduct} >Thêm</button>
											<button className="btn btn-outline-primary " onClick={newProduct} >Clear</button>
										</div>
										</div>
									
									</div>
								</section>
							</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
