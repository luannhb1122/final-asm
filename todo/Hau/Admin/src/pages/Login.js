import React, { useState, useRef } from "react";
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { useHistory,Link } from 'react-router-dom';
import AuthService from "../Service/Auth_Service";
import './Login.css'
const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const Login = (props) => {
  const form = useRef();
  const checkBtn = useRef();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const history = useHistory();
  const onChangeUsername = (e) => {
    const username = e.target.value;
    setUsername(username);
  };

  const onChangePassword = (e) => {
    const password = e.target.value;
    setPassword(password);
  };

  const handleLogin = (e) => {
    e.preventDefault();
    setMessage("");
    setLoading(true);
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      AuthService.login(username, password).then(
        () => {
        //  history.push("/profile/"+username);
          // window.location.reload();
          history.push("/");
          window.location.reload();
        },
        (error) => {
          const resMessage =
            (error.response &&
              error.response.data &&
              error.response.data.message) ||
            error.message ||
            error.toString();

          setLoading(false);
          setMessage(resMessage);
        }
      );
    } else {
      setLoading(false);
    }
  };

  return (

	<div className="purple-square container-fluid"> 
	<div className="col-md-3">
			<div className="card card-signin my-">
				<div className="card-body">
					<h5 className="card-title text-center text-uppercase">Đăng Nhập</h5>
        <Form className="form-signin" onSubmit={handleLogin} ref={form}>
          <div className="form-group">
            <label htmlFor="username">Tài Khoản</label>
            <Input
              type="text"
              className="form-control"
              name="username"
              value={username}
              placeholder="Tên đăng nhập"
              onChange={onChangeUsername}
              validations={[required]}
              
            />
          </div>

          <div className="form-group">
            <label htmlFor="password">Mật Khẩu</label>
            <Input
              type="password"
              className="form-control"
              name="password"
              value={password}
              onChange={onChangePassword}
              validations={[required]}
              placeholder="Mật Khẩu"
            />
          </div>
          <p className="text-center text-p ml-4 mt-2 pl-4">
									<Link to='/forgot' className="Forgot ml-4 pl-4" >
										&nbsp;Quên mật khẩu
									</Link>
								</p>
          <div className="form-group">
            <button className="btn btn-primary btn-block" disabled={loading} >
              {loading && (
                <span className="spinner-border spinner-border-sm text-danger"></span>
              )}
              <span>Đăng Nhập</span>
            </button>
         
          </div>
          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: "none" }} ref={checkBtn} />
        </Form>
      </div>
    </div>

    </div>
    {/* <div id="myModal" class="modal fade">
	<div class="modal-dialog modal-confirm">
		<div class="modal-content">
			<div class="modal-header">
				<div class="icon-box">
					<i class="material-icons">&#xE876;</i>
				</div>				
				<h4 class="modal-title w-100">Awesome!</h4>	
			</div>
			<div class="modal-body">
				<p class="text-center">Your booking has been confirmed. Check your email for detials.</p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-success btn-block" data-dismiss="modal">OK</button>
			</div>
		</div>
	</div>
</div>  */}
	</div>
 


  );
};

export default Login;