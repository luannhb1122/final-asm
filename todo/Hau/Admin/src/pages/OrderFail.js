import React, { useEffect, useState, Fragment } from 'react';
import Order from '../components/Axios/AxiosServicer';
import DataTable from 'react-data-table-component';
import './Style.css'
import { Modal } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';
/* import {ExportToExcel} from './ExportToExcel' */
import axios from 'axios';

export default function AllOders(props) {

	const initialProductState = {
		id: ""
	};
	// Khai báo giá trị
	//Khai báo set và get mảng
	const [orederstatus, setorederstatus] = useState(initialProductState);
	const [order, setorder] = useState([]);
	const [id, setid] = useState(0);
	const [selectfind, setselectfind] = useState('');
    const [totalOrder_Detail, settotalOrder_Detail] = useState('');
	let orderByindex = -1;
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALloder();
	}, []);
	const GetALloder = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Order.getorder0()
			.then((response) => {
				setorder(response.data);
				setSearch(response.data);
				setselectfind('phone');
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};

	function handleInputChange(evt) {
		const value = evt.target.value;
		setselectfind(value);
		console.log(value)
	}
	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'id',
			selector: 'id',
			sortable: true
		},
		{
			name: 'Ngày Tạo',
			selector: 'date_created',
			sortable: true
		},
		/* 		{
					name: 'Tổng Tiền',
					selector: 'total',
					sortable: true
				}, */
		{
			name: 'Tên Khách Hàng',
			selector: 'customers.name',
			sortable: true
		},
		{
			name: 'Số Điện Thoại',
			selector: 'customers.phone',
			sortable: true
		},
		{
			name: 'Thanh toán',
			selector: 'payment',
			sortable: true,
			cell:(row) =>(row.payment===1)?"Tiền mặt":"khác"
		},
		{
			name: 'Trạng Thái',
			selector: 'order_Status.comment',
			sortable: true,
		},
		{
			name: 'Chi Tiết Đơn Hàng',
			key: 'action',
			text: 'Action',
			cell: (bill, index) => {
				return (
					<Fragment>
						{/*  	<UpdateProduct/> */}
						<button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(bill.id)}>
							<i className="ft-edit-1" >Chi Tiết đơn Hàng</i>
						</button>
					</Fragment>
				);
			}
		},


	];
	const setActiveTutorial = (id) => {
		setid(id);
		handleShowModal();
	};
	//CSS DATATABLE
	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900',

			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},

		headCells: {
			style: {
				fontSize: '15px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'

			},
		},
		cells: {
			style: {
				fontSize: '15px',
				paddingLeft: '0 8px',
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center',
				borderbottom: '#EEEEEE solid 10px'
			},
		},
	};




	const [show, setShow] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
    let sum =0;
	const menuItems = order.map((item, i) => {

		const subMenuItems = item.orders_Detail.map((subItem, index) => {
			if (item.id === id) {
                sum +=subItem.products.price * subItem.amount;
				orderByindex = i;
				console.log(sum)
				return (
					<>
						<tr key={index}>
							<td>{subItem.products.name}</td>
							<td>{subItem.sizes.type}</td>
							<td>{subItem.amount}</td>
							<td><NumberFormat style={{ background: 'none', border: 'none' }} value={subItem.products.price} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></td>
							<td>{subItem.products.categories.type}</td>
							<td><NumberFormat style={{ background: 'none', border: 'none' }} value={subItem.products.price * subItem.amount} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></td>
						</tr>
                        
					</>

				);
			} else
				return (<></>);
		});
		return (
			<>{subMenuItems}</>
		);
	});
	// xuất ra ex

	// const detailOrder=() => {
	// 	return(

	// 	)

	// }

	/* 	const [data, setData] = React.useState([])
		const fileName = "myfile"; // here enter filename for your excel file
	  
		React.useEffect(() => {
		  const fetchData = () =>{
		   axios.get('http://localhost:8081/api/order_detail').then(r => setData(r.data) )
		  }
		  fetchData()
		}, [])
	 */

	const [Search, setSearch] = useState([]);
	const handleSearch = (event) => {
		let value = event.target.value;
		let result = [];
		if (value === "") {
			setSearch(order);
			console.log(Search)
			console.log("all")
		} else if (selectfind === "phone") {
			result = order.filter((data) => {
				return data.orders.customers.phone.includes(value);
			});
			setSearch(result);
			console.log("phone")
		} else if (selectfind === "name") {
			result = order.filter((data) => {
				return data.orders.customers.name.toLowerCase().includes(value);
			});
			setSearch(result);
			console.log("name")
		} else if (selectfind === "date") {
			result = order.filter((data) => {
				return data.date_create.includes(value);
			});
			setSearch(result);
			console.log("date")
		}
	}

	const selectSearch = () => {
		return (
			<select className="form-control" onChange={handleInputChange} style={{ backgroundColor: 'white' }}>
				<option value="phone" selected>Số điện thoại KH</option>
				<option value="name">Tên khách hàng</option>
				<option value="date">Ngày</option>
			</select>
		);
	};
	const detailOrder = () => {
		if (orderByindex > -1) {
			return(
			<>
				<h2 className="text-center">Hóa Đơn</h2>
				<div className="row mt-3 mb-2" >
					<div className="col-sm-4">
						<strong>ID hóa đơn:</strong> {id}
					</div>
					<div className="col-sm-4">
						<strong>ID khách hàng:</strong> {order[orderByindex].customers.id}
					</div>
					<div className="col-sm-4">
						<strong>Ngày hóa đơn:</strong> {order[orderByindex].date_created}
					</div>
				</div>
				<div className="row  mt-2 mb-3" >
					<div className="col-sm-4">
						<strong>Tên khách hàng:</strong> {order[orderByindex].ship_Info.name_cus}
					</div>
					<div className="col-sm-4">
						<strong>SĐT khách hàng:</strong> {order[orderByindex].ship_Info.phone_cus}
					</div>
					<div className="col-sm-4">
						<strong>Địa chỉ KH:</strong> {order[orderByindex].ship_Info.address}
					</div>
                    <div className="col-sm-4">
						<strong>Voucher:</strong> {order[orderByindex].vouchers!==null ?<NumberFormat style={{background:'none',border:'none'}} value={order[orderByindex].vouchers.value} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />:"Không có"}
					</div>
                    <div className="col-sm-4">
						<strong>Phí vận chuyển: </strong><NumberFormat style={{background:'none',border:'none'}} value={15000} displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} />
					</div>
                    <div className='col-sm-4'>
                                        <strong>Tổng tiền: <NumberFormat style={{background:'none',border:'none'}} value={
                                        order[orderByindex].vouchers!==null? sum +15000 - Number(order[orderByindex].vouchers.value):sum +15000
                                    } displayType={'text'} thousandSeparator={true} suffix={' VNĐ'} /></strong>
                                        </div>  
				</div>
			</>);
		};
	};
	return (
		<>
			{
				order.length > 0 ? (

					<div style={{ marginTop: '-100px' }}>
						{/*     <!--Tiêu Đề--> */}
						<div class="input-group">
							<div className="breadcrumbs-area clearfix">
								<ul className="breadcrumbs float-left list-unstyled">
									<li class="breadcrumb-item" aria-current="page">
										<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
										<Link className="breadcrumb-item" to="/Product">Tất cả Đơn hàng</Link>
									</li>
								</ul>
							</div>
						</div>
						<div className="container-fluid">
							<div className="row mb-2">
								<div className="col-md-10 float-left">
									<label>Tìm Kiếm:</label>
									<input type="text" className="form-control" style={{ backgroundColor: 'white' }} onChange={(event) => handleSearch(event)} />
								</div>
								<div className="col-md-2 pt-3">
									{selectSearch}
								</div>
							</div>
						</div>

						<div className="card mt-2" >
							<DataTable
								customStyles={customStyles}
								title="Tất cả Đơn hàng" //Tiêu đề bảng
								columns={columns} // Hiển thị cột
								data={Search} // Dữ liệu đổ vào bảng
								// defaultSortField="id"
								pagination // Hiển thị phân trang hay không
							/* 	selectableRows */
							/>
						</div>

						<Modal
							dialogClassName="dialogModalBill"
							show={show}
							onHide={handleCloseModal}
							keyboard={false}
						>
							<Modal.Body>
								<div className="container">
									{detailOrder()}
									<table className="table table-hover text-center">
										<thead>
											<tr >
												<th>Sản Phẩm</th>
												<th>Size</th>
												<th>Số Lượng</th>
												<th>Giá</th>
												<th>Loại</th>
												<th>Thành Tiền</th>
											</tr>
										</thead>
										<tbody>
											{menuItems}
										</tbody>

									</table>
                                   
									{/* <hr></hr>
                                    <p className="text-bold-400 text-right"> <strong>Tạm Tính:</strong> {listItem.total}VNĐ</p>
                                       <p className="text-bold-400  text-right"> <strong>Thuế GTGT:</strong> {1000}VNĐ</p>
                                <hr></hr>
                                <p className="text-right"> <strong>Tổng cộng:</strong> {listItem.total-1000}VNĐ</p> */}
		
								</div>



							</Modal.Body>
						</Modal>

					</div>
				) : (
					<div className="text-center">
						<span className="spinner-border spinner-border-xs text-danger text-center"></span>
						<h3>Đang tải vui lòng chờ...</h3>
					</div>
				)
			}
		</>

	);
}
