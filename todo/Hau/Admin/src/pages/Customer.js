import React, { useEffect, useState, Fragment } from 'react';
import Customer from '../components/Axios/AxiosServicer';
import DataTable from 'react-data-table-component';
import './Style.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Modal, Form } from 'react-bootstrap';
import DataTableExtensions from 'react-data-table-component-extensions';
import 'react-data-table-component-extensions/dist/index.css';
import Switch from 'react-switch';
import axios from 'axios';

/* import ExportToExcel  from './ExportToExcel' */

export default function Cus(props) {
	// Khai báo giá trị
	const initialproducttate = {
		id: '',
		name: '',
		phone: '',
		email: '',
		picture: '',
		birthday: '',
		gender: '',
		username: '',
		password: '',
		role: 1,
		create_date: '',
		status: true
	};

	const initialproducttate1 = {
		id: '',
		name: '',
		phone: '',
		email: '',
		birthday: '',
		gender: '',
		picture: ''
	};

	//Khai báo set và get mảng
	const [ Cus, setCus ] = useState([]);
	//Khai báo set và get mảng
	// const [ Cuss, setCuss ] = useState([]);
	const [ CusItem, setCusItem ] = useState(initialproducttate);
	const [ CusItema, setCusItema ] = useState(initialproducttate1);
	const [ index, setindex ] = useState(0);
	const [ Search, setSearch ] = useState([]);
	const [ message, setMessage ] = useState('');

	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALlemp();
	}, []);
	const GetALlemp = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Customer.getAllCustomer()
			.then((response) => {
				setCus(response.data);
				setSearch(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	// const setActiveTutorial = (Cus, index) => {
	// 	setCusItem(Cus);
	// 	setCusItema(Cus.account_Info);
	// 	setindex(index);
	// 	handleShowModal();
	// 	console.log(Cus);
	// 	console.log(Cus.account_Info);
	// 	console.log(CusItema);
	// };

	// const UpdateProduct = () => {
	// 	var data = {
	// 		id: parseInt(CusItem.id),
	// 		username: CusItem.username,
	// 		password: CusItem.password,
	// 		status: true,
	// 		level: parseInt(CusItem.level),
	// 		account_Info: {
	// 			id: CusItema.id,
	// 			name: CusItema.name,
	// 			phone: CusItema.phone,
	// 			email: CusItema.email,
	// 			birthday: CusItema.birthday,
	// 			gender: CusItema.gender === 'Nam' ? true : false,
	// 			picture: CusItema.picture
	// 		}
	// 	};
	// 	console.log(CusItem);
	// 	console.log(data);
	// 	Customer.updateCustomer(CusItem.id, data)
	// 		.then((response) => {
	// 			console.log(response.data);
	// 			console.log(data);
	// 			/* 	setMessage('Thành Công!'); */
	// 			handleCloseModal();
	// 			refreshTable();
	// 		})
	// 		.catch((e) => {
	// 			console.log(e);
	// 		});
	// };

	//Xóa
	// const deleteTutorial = () => {
	// 	Customer.removeCustomer(Cus.id)
	// 		.then((response) => {
	// 			console.log(response.data);
	// 			setMessage('Xóa Thành Công!');
	// 			handleCloseModal();
	// 			refreshTable();
	// 		})
	// 		.catch((e) => {
	// 			console.log(e);
	// 		});
	// };

	// const block = () => {
	// 	Customer.getblock(Cus.id)
	// 		.then((response) => {
	// 			console.log(response.data);
	// 			setMessage('Block Thành Công!');
	// 		})
	// 		.catch((e) => {
	// 			console.log(e);
	// 		});
	// };

	function HandleputChangeBlock(id) {
		console.log(id);
		for (let i = 0; i < Search.length; i++) {
			if (Search[i].id === id && Search[i].status === true) {
				Customer.getblock(id)
					.then((response) => {
						setMessage('Khóa tài khoản Thành Công!');
						window.location.reload();
					})
					.catch((e) => {
						console.log(e);
					});
			}
		}
	}

	function HandleputChangeUnBlock(id) {
		console.log(id);
		for (let i = 0; i < Search.length; i++) {
			if (Search[i].id === id && Search[i].status === false) {
				Customer.getonblock(id)
					.then((response) => {
						setMessage('Mở khóa tài khoản Thành Công!');
						window.location.reload();
					})
					.catch((e) => {
						console.log(e);
					});
			}
		}
	}
	const [ show, setShow ] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);

	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'Hình',
			sortable: true,
			cell: (row) => <img height="190px" width="256px" alt="" src={`${row.picture}`} />
		},
		{
			name: 'Tài khoản',
			selector: 'username',
			sortable: true
		},
		{
			name: 'Tên',
			selector: 'name',
			sortable: true
		},
		{
			name: 'Số điện thoại',
			selector: 'phone',
			sortable: true
		},
		{
			name: 'email',
			selector: 'email',
			sortable: true
		},
		{
			name: 'Ngày sinh',
			selector: 'birthday',
			sortable: true
		},
		{
			name: 'Giới tính',
			selector: 'gender',
			sortable: true,
			cell: (row) => (row.gender === true ? 'Nam' : 'Nữ')
		},
		{
			name: 'Trạng thái',
			selector: 'status',
			sortable: true,
			cell: (row) =>
				row.status ? (
					<button className="btn btn-sm btn-danger" onClick={() => HandleputChangeBlock(row.id)}>
						Khóa tài khoản
					</button>
				) : (
					<button className="btn btn-sm btn-success" onClick={() => HandleputChangeUnBlock(row.id)}>
						Mở khóa
					</button>
				)
		}
	];

	// xuất ra file ex
	/* 	 const [data, setData] = React.useState([])
	  const fileName = "myfile"; // here enter filename for your excel file
	
	  React.useEffect(() => {
		const fetchData = () =>{
		 axios.get('http://localhost:8081/api/customer').then(r => setData(r.data) )
		}
		fetchData()
	  }, [])  */

	const refreshList = () => {
		GetALlemp();
	};
	const refreshTable = () => {
		GetALlemp();
	};

	//CSS DATATABLE
	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900'
			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},

		headCells: {
			style: {
				fontSize: '12px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},
		cells: {
			style: {
				fontSize: '12px',
				paddingLeft: '0 8px',
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		}
	};

	const handleSearch = (event) => {
		let value = event.target.value.toLowerCase();
		let result = [];
		console.log(value);
		if (value === '') {
			setSearch(Cus);
		} else {
			result = Cus.filter((data) => {
				return data.name.toLowerCase().includes(value);
			});

			setSearch(result);
		}
	};

	return (
		<div style={{ marginTop: '-100px' }}>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">
								Trang Chủ
							</Link>
							<Link className="breadcrumb-item" to="/Product">
								Tất cả Khách Hàng
							</Link>
						</li>
					</ul>
				</div>
				<div className="col-md-10 float-left">
					<label>Tìm Kiếm:</label>
					<input
						type="text"
						className="form-control"
						placeholder="nhập tên khách hàng"
						style={{ backgroundColor: 'white' }}
						onChange={(event) => handleSearch(event)}
					/>
				</div>
			</div>

			<div className="card mt-2">
				<DataTable
					customStyles={customStyles}
					title="Tất Cả Khách Hàng" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={Search} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
					/* 	selectableRows */
				/>
			</div>

			<div>
				<br />
				<p>Please click on a Tutorial...</p>
			</div>
		</div>
	);
}
