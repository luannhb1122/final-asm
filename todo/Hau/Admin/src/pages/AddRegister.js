import React, { useState, useEffect } from 'react';
import Data from '../components/Axios/AxiosServicer';
import { Modal, Button } from 'react-bootstrap';
import './Style.css'
export default function Account() {
    const initialTutorialState = {
        username: "",
        email: ""
    };
    const [register, setregister] = useState(initialTutorialState);

    function handleInputChange(evt) {
        const value = evt.target.value;
        setregister({
            ...register,
            [evt.target.id]: value
        });
    }

    const saveTutorial = (data) => {
        var data = {
            username: (register.username),
            email: register.email
        };
        Data.register(data)
            .then(response => {
                handleClose();
                handleNotificationShow();
            })
            .catch(e => {
                handleNofErrorShow()
            });
    };


    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    //Thông Báo thành công
    const [Notification, setNotification] = useState(false);
    const handleNotificationClose = () => setNotification(false);
    const handleNotificationShow = () => setNotification(true);
    const ReloadPage = () => {
        handleNotificationClose();
        window.location.reload()
    };
    //Thông Báo thất bại
    const [NofError, setNofError] = useState(false);
    const handleNofErrorClose = () => setNofError(false);
    const handleNofErrorShow = () => setNofError(true);
    const ReloadNofError = () => {
        handleNofErrorClose();
        handleShow();
    };
    return (
        <>
            <Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
                <i className="ft-file-plus">&nbsp;Tạo tài khoản</i>
            </Button>
            {/* Modal Thông báo thành công */}
            <Modal dialogClassName="messmodal" show={Notification} onHide={handleNotificationClose}>
                <div className="icon-box">
                    <i className="ft-check text-center"></i>
                </div>
                <Modal.Body>
                    <p className="text-center mt-4">Tạo Thành Công</p>
                </Modal.Body>

                <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
            </Modal>
            {/* Modal Thông báo thất bại */}
            <Modal dialogClassName="messmodal" show={NofError} onHide={handleNofErrorClose}>
                <div className="icon-box">
                    <i className="ft-x text-center"></i>
                </div>
                <Modal.Body>
                    <p className="text-center mt-4">Thêm Thất bại</p>
                </Modal.Body>

                <button className="btn btn-danger btn-block" type="button" onClick={ReloadNofError}>OK</button>
            </Modal>
            {/* Modal Loại */}
            <Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title >Tạo Tài Khoản</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <div className='container'>
                        <div className="submit-form">
                            <div>
                                <section className="panel panel-default">
                                    {/*Form Type */}
                                    <div className='form-group row'>
                                        <label for="name" className="col-sm-3 control-label">Tên Tài khoản:</label>
                                        <div className="col-sm-8">
                                            <input type="text"
                                                className="form-control"
                                                id="username"
                                                required
                                                value={register.username}
                                                onChange={handleInputChange}
                                                name="username" placeholder="Tên Tài khoản" />
                                        </div>
                                    </div>
                                    <div className='form-group row'>
                                        <label for="name" className="col-sm-3 control-label">Email:</label>
                                        <div className="col-sm-8">
                                            <input type="text"
                                                className="form-control"
                                                id="email"
                                                required
                                                value={register.email}
                                                onChange={handleInputChange}
                                                name="email" placeholder="Email" />
                                        </div>
                                    </div>
                                    {/* <button type="submit" className="btn btn-sm btn-success"  onClick={saveTutorial}>Thêm</button> */}
                                    <div className='mt-4 div-button'>
                                        <button className="btn btn-outline-success mr-2 " onClick={saveTutorial} >Tạo Tài Khoản</button>
                                        {/* <button className="btn btn-outline-primary " onClick={newProduct} >Clear</button> */}
                                    </div>
                                </section>
                            </div>

                        </div>
                    </div>
                </Modal.Body>
            </Modal>
        </>

    );
}
