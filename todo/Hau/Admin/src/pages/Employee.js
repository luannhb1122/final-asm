import React, { useEffect, useState, Fragment } from 'react';
import Employee from '../components/Axios/AxiosServicer';
import DataTable from 'react-data-table-component';
import './Style.css';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';
import AddRegister from './AddRegister';
import AuthSevice from '../Service/Auth_Service';
import swal from 'sweetalert'
export default function Emp(props) {
	// Khai báo giá trị
	const initialproducttate = {
		id: '',
		name: '',
		phone: '',
		email: '',
		picture: '',
		birthday: '',
		gender: '',
		username: '',
		password: '',
		role: 1,
		create_date: '',
		status: true
	};

	const initialproducttate1 = {
		id: '',
		name: '',
		phone: '',
		email: '',
		birthday: '',
		gender: '',
		picture: ''
	};

	//Khai báo set và get mảng
	const [ Emp, setEmp ] = useState([]);
	//Khai báo set và get mảng
	const [ Emps, setEmps ] = useState([]);
	const [ EmpItem, setEmpItem ] = useState(initialproducttate);
	const [ EmpItema, setEmpItema ] = useState(initialproducttate1);
	const [ index, setindex ] = useState(0);
	const [ Search, setSearch ] = useState([]);
	const [ message, setMessage ] = useState('');
	const profile = AuthSevice.getProfile();
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALlemp();
	}, []);
	const GetALlemp = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Employee.getEmployee()
			.then((response) => {
				setEmp(response.data);
				setSearch(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const setActiveTutorial = (Emp, index) => {
		setEmpItem(Emp);
		setEmpItema(Emp.account_Info);
		setindex(index);
		handleShowModal();
		console.log(Emp);
		console.log(Emp.account_Info);
		console.log(EmpItema);
	};

	const UpdateProduct = () => {
		var data = {
			id: parseInt(EmpItem.id),
			username: EmpItem.username,
			password: EmpItem.password,
			status: true,
			start_date: EmpItem.start_date,
			account_Info: {
				id: EmpItema.id,
				name: EmpItema.name,
				phone: EmpItema.phone,
				email: EmpItema.email,
				birthday: EmpItema.birthday,
				gender: EmpItema.gender === 'Nam' ? true : false,
				picture: EmpItema.picture
			}
		};
		console.log(EmpItem);
		console.log(data);
		Employee.updateEmployee(EmpItem.id, data)
			.then((response) => {
				console.log(response.data);
				console.log(data);
				/* 	setMessage('Thành Công!'); */
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};

	//Xóa
	const deleteTutorial = () => {
		Employee.removeEmployee(EmpItem.id)
			.then((response) => {
				console.log(response.data);
				setMessage('Xóa Thành Công!');
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};

	function handleInputChange(evt) {
		const value = evt.target.value;
		setEmpItem({
			...EmpItem,
			[evt.target.name]: value
		});
		setEmpItema({
			...EmpItema,
			[evt.target.name]: value
		});
	}

	function HandleputChangeBlock(id) {
		for (let i = 0; i < Search.length; i++) {
			if (profile.roles.id === 3) {
				if (Search[i].id === id && Search[i].status === true) {
					Employee.getblockSA(id)
						.then((response) => {
							window.location.reload();
						})
						.catch((e) => {
							console.log(e);
						});
				}
			}
		}
	}

	function HandleputChangeUnBlock(id) {
		for (let i = 0; i < Search.length; i++) {
			if (profile.roles.id === 3) {
				if (Search[i].id === id && Search[i].status === false) {
					Employee.getonblockSA(id)
						.then((response) => {
							window.location.reload();
						})
						.catch((e) => {
							console.log(e);
						});
				}
			}
		}
	}

	const [ show, setShow ] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'Hình',
			sortable: true,
			cell: (row) => <img height="190px" width="256px" alt="" src={`${row.picture}`} />
		},
		{
			name: 'Tên',
			selector: 'name',
			sortable: true
		},
		{
			name: 'Ngày tạo',
			selector: 'create_date',
			sortable: true
		},
		{
			name: 'Số điện thoại',
			selector: 'phone',
			sortable: true
		},
		{
			name: 'email',
			selector: 'email',
			sortable: true
		},
		{
			name: 'Ngày sinh',
			selector: 'birthday',
			sortable: true
		},
		{
			name: 'Giới tính',
			selector: 'gender',
			sortable: true,
			cell: (row) => (row.gender === true ? 'Nam' : 'Nữ')
		},
		{
			name: 'Trạng thái',
			selector: 'status',
			sortable: true,
			cell: (row) =>
				row.status ? (
					<button className="btn btn-sm btn-danger" onClick={() => HandleputChangeBlock(row.id)}>
						Khóa tài khoản
					</button>
				) : (
					<button className="btn btn-sm btn-success" onClick={() => HandleputChangeUnBlock(row.id)}>
						Mở khóa
					</button>
				)
			// <input
			// 	className="form-check-input"
			// 	type="radio"
			// 	name={`status${row.id}`}

			// 	id='status'
			// 	onclick={ChangeStatus}
			// />
			// <Switch
			// onClick={ChangeStatus}
			// id='status'
			// name={`status${row.id}`}
			// 	className="react-switch"
			// />
		}
	];

	const refreshList = () => {
		GetALlemp();
	};
	const refreshTable = () => {
		GetALlemp();
	};

	//CSS DATATABLE
	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900'
			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},

		headCells: {
			style: {
				fontSize: '15px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},
		cells: {
			style: {
				fontSize: '15px',
				paddingLeft: '0 8px',
				backgroundColor: 'white',
				justifyContent: 'center',
				alignitems: 'center',
				borderbottom: '#EEEEEE solid 10px'
			}
		}
	};

	const handleSearch = (event) => {
		let value = event.target.value.toLowerCase();
		let result = [];
		console.log(value);
		if (value === '') {
			setSearch(Emp);
		} else {
			result = Emp.filter((data) => {
				return data.name.toLowerCase().includes(value);
			});

			setSearch(result);
		}
	};

	return (
		<div style={{ marginTop: '-100px' }}>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">
								Trang Chủ
							</Link>
							<Link className="breadcrumb-item" to="/Product">
								Tất cả Khách Hàng
							</Link>
						</li>
					</ul>
				</div>
			</div>
			<div className="container-fluid">
				<div className="row mb-2">
					<div className="col-md-10 float-left">
						<label>Tìm Kiếm:</label>
						<input
							type="text"
							className="form-control"
							placeholder="nhập tên nhân viên"
							style={{ backgroundColor: 'white' }}
							onChange={(event) => handleSearch(event)}
						/>
					</div>
					<div className="col-md-2 pt-3">
						<AddRegister />
					</div>
				</div>
			</div>
			{/*  Hiện thi Table */}
			<div className="card mt-2">
				<DataTable
					customStyles={customStyles}
					title="Tất Cả Nhân viên" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={Search} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
					/* 	selectableRows */
				/>
			</div>
			{EmpItem ? (
				<Modal
					dialogClassName="dialogModal"
					show={show}
					onHide={handleCloseModal}
					backdrop="static"
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title className="tille_Modal">Cập Nhật Thông tin</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="container">
							<section className="panel panel-default">
								{/* // nhập tên sản phẩm */}
								<div className="form-group row ">
									<label className="col-sm-3 control-label">username:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="username"
											name="username"
											value={EmpItem.username}
											onChange={handleInputChange}
											placeholder="username"
										/>
									</div>
								</div>
								{/*    Giá */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">password:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="password"
											value={EmpItem.password}
											onChange={handleInputChange}
											name="password"
											placeholder="password"
										/>
									</div>
								</div>
								<div className="form-group row">
									<label className="col-sm-3 control-label">start_date:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="start_date"
											value={EmpItem.start_date}
											onChange={handleInputChange}
											name="start_date"
											placeholder="start_date"
										/>
									</div>
								</div>
								{/*   load hình anh */}
								{/* 		account_Info */}

								{/* name */}
								{/* 								<div className="form-group row">
									<label className="col-sm-3 control-label">id:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="id"
											value={EmpItema.id}
											onChange={handleInputChange}
											name="id"
											placeholder="id"
										/>
									</div>
								</div> */}

								{/* name */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">name:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="name"
											value={EmpItema.name}
											onChange={handleInputChange}
											name="name"
											placeholder="name"
										/>
									</div>
								</div>
								{/* phone */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">phone:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="phone"
											value={EmpItema.phone}
											onChange={handleInputChange}
											name="phone"
											placeholder="phone"
										/>
									</div>
								</div>
								{/* email */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">email:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="email"
											value={EmpItema.email}
											onChange={handleInputChange}
											name="email"
											placeholder="email"
										/>
									</div>
								</div>

								{/* email */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">birthday:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="birthday"
											value={EmpItema.birthday}
											onChange={handleInputChange}
											name="birthday"
											placeholder="birthday"
										/>
									</div>
								</div>
								{/* gender */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">gender:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="gender"
											value={EmpItema.gender === true ? 'Nam' : 'Nữ'}
											onChange={handleInputChange}
											name="gender"
											placeholder="gender"
										/>
									</div>
								</div>

								{/* gender */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">picture:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="picture"
											value={EmpItema.picture}
											onChange={handleInputChange}
											name="picture"
											placeholder="picture"
										/>
									</div>
								</div>
							</section>
							{/*  Nút button Update */}
							<div className="form-group row pt-2 div-button">
								<div className="col-sm-12">
									<button type="submit" className="btn btn-sm btn-success" onClick={UpdateProduct}>
										Cập Nhật
									</button>
									<button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
										Xóa
									</button>
								</div>
							</div>
							<p>{message}</p>
						</div>
					</Modal.Body>
				</Modal>
			) : (
				<div>
					<br />
					<p>Please click on a Tutorial...</p>
				</div>
			)}
		</div>
	);
}
