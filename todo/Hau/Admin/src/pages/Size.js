import React, { useEffect, Fragment, useState } from 'react';
import DataSize from '../components/Axios/AxiosServicer';
import DataTable from 'react-data-table-component';
import AddSize from '../pages/AddSize';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import NumberFormat from 'react-number-format';
export default function Size() {
    // Khai báo giá trị
    const initialTypeProduct = {
        id: '',
        type: '',
        price: '',
        status:''
    };
    //Khai báo set và get mảng
    const [size, setsize] = useState([]);
    const [sizeitem, setsizeitem] = useState(initialTypeProduct);
    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        GetSize();
    }, []);
    const GetSize = () => {
        DataSize.getAllsize()
            .then((response) => {
                setsize(response.data);
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };
    const setActiveTutorial = (size, index) => {
        setsizeitem(size);
        handleShowModal();
    };
    // Sự kiện nhập giá trị vào form
    function handleInputChange(evt) {
        const value = evt.target.value;
        setsizeitem({
            ...sizeitem,
            [evt.target.name]: value
        });
    }

    // Cập nhật dữ liệu Type
    const updateTutorial = () => {
        var data = {
            type: sizeitem.type,
            price: sizeitem.price,
            status: sizeitem.status
        };
        DataSize.updatesize(sizeitem.id, data)
            .then((response) => {
                console.log(response.data);
                handleCloseModal();
                handleMessageShow()
            })
            .catch((e) => {
                console.log(e);
            });
    };
    //Xóa
    const deleteTutorial = () => {
        DataSize.removesize(sizeitem.id)
            .then((response) => {
                console.log(response.data);
                handleCloseModal();
                handleDeleteShow()
            })
            .catch((e) => {
                console.log(e);
            });
    };

    // Set selected default value
    const selectCategory = () => {
        if (sizeitem.status) {
            return (
                <select
                    className="form-control"
                    id="status"
                    name="status"
                    onChange={handleInputChange}
                >
                    <option value="true" selected>
                        Hiện
				</option>
                    <option value="false">
                        Ẩn
            </option>
                </select>
            );
        } else {
            return (
                <select
                    className="form-control"
                    id="status"
                    name="status"
                    onChange={handleInputChange}
                >
                    <option value="true" >
                        Hiện
            </option>
                    <option value="false" selected>
                        Ẩn
        </option>
                </select>
            );
        }
    }

        //Hiện thị Modal
        const [show, setShow] = useState(false);
        const handleCloseModal = () => setShow(false);
        const handleShowModal = () => setShow(true);
        //Modal Update
        const [showmes, setShowmes] = useState(false);
        const handleMessageClose = () => setShowmes(false);
        const handleMessageShow = () => setShowmes(true);
        //Modal Delete
        const [showDelete, setShowDelete] = useState(false);
        const handleDeleteClose = () => setShowDelete(false);
        const handleDeleteShow = () => setShowDelete(true);
        const ReloadPage = () => {
            handleMessageClose();
            refreshTable();
        };
        const Reload = () => {
            handleDeleteClose();
            refreshTable();
        };
        // Hiển thị title theo cột trong table
        const columns = [
            {
                name: 'ID',
                selector: 'id',
                sortable: true
            },
            {
                name: 'Size',
                selector: 'type',
                sortable: true
            },
            {
                name: 'Giá trị tương ứng',
                selector: 'price',
                sortable: true,
            },
            {
                name: 'Trạng Thái',
                selector: 'status',
                sortable: true,
                cell: (row) => (row.status === true ? 'Hiện' : 'Ẩn')
            },
            {
                name: 'Chức Năng',
                key: 'action',
                text: 'Action',
                cell: (TypeProduct, index) => {
                    return (
                        <Fragment>
                            <button
                                className="btn btn-primary btn-sm"
                                onClick={() => setActiveTutorial(TypeProduct, index)}
                            >
                                <i className="ft-edit-1">Cập Nhật</i>
                            </button>
                        </Fragment>
                    );
                }
            }
        ];


        //CSS Datatable
        const customStyles = {
            title: {
                style: {
                    fontColor: 'red',
                    fontWeight: '900'
                }
            },
            rows: {
                style: {
                    minHeight: '72px', // override the row height
                    backgroundColor: '#F7F7F8',
                    justifyContent: 'center',
                    alignitems: 'center'
                }
            },

            headCells: {
                style: {
                    fontSize: '15px',
                    fontWeight: '500',
                    textTransform: 'uppercase',
                    paddingLeft: '0 8px',
                    backgroundColor: '#fff',
                    fontColor: 'black',
                    justifyContent: 'center',
                    alignitems: 'center'
                }
            },
            cells: {
                style: {
                    fontSize: '15px',
                    paddingLeft: '0 8px',
                    backgroundColor: '#F7F7F8',
                    justifyContent: 'center',
                    alignitems: 'center',
                    borderbottom: '#EEEEEE solid 10px'
                }
            }
        };
        //Reset Table
        const refreshTable = () => {
            GetSize();
        };

        return (
            <div>
                {/*     <!--Tiêu Đề--> */}
                <div class="input-group">
                    <div className="breadcrumbs-area clearfix">
                        <ul className="breadcrumbs float-left list-unstyled">
                            <li class="breadcrumb-item" aria-current="page">
                                <Link className="breadcrumb-item" to="/">
                                    Trang Chủ
							</Link>
                                <Link className="breadcrumb-item" to="/Category">
                                    Tất cả loại sản phẩm
							</Link>
                            </li>
                        </ul>
                    </div>
                </div>
                {/* Tìm Kiếm Loại */}
                <div className="container-fluid">
                    <div className="row mb-2">
                        <div className="col-md-10 float-left">

                        </div>
                        <div className="col-md-2 pt-3">
                            <AddSize />
                        </div>
                    </div>
                </div>
                {/*  Hiện thi Table */}

                <DataTable
                    customStyles={customStyles}
                    title="Size" //Tiêu đề bảng
                    columns={columns} // Hiển thị cột
                    data={size} // Dữ liệu đổ vào bảng
                    defaultSortField="id"
                    pagination // Hiển thị phân trang hay không
                />
                {/* Modal Thông báo*/}
                <Modal dialogClassName="messmodal" show={showmes} onHide={handleMessageClose}>
                    <div className="icon-box">
                        <i className="ft-check text-center"></i>
                    </div>
                    <Modal.Body>
                        <p className="text-center mt-4">Cập Nhật Thành Công</p>
                    </Modal.Body>

                    <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
                </Modal>
                <Modal dialogClassName="messmodal" show={showDelete} onHide={handleDeleteClose}>
                    <div className="icon-box">
                        <i className="ft-check text-center"></i>
                    </div>
                    <Modal.Body>
                        <p className="text-center mt-4">Xóa Thành Công</p>
                    </Modal.Body>

                    <button className="btn btn-danger btn-block" type="button" onClick={Reload}>OK</button>
                </Modal>
                {sizeitem ? (
                    <Modal
                        dialogClassName="dialogModal"
                        show={show}
                        onHide={handleCloseModal}
                        keyboard={false}
                    >
                        <Modal.Header>
                            <Modal.Title className="tille_Modal">Cập Nhật Size</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="container">
                                <section className="panel panel-default">
                                    <div className="form-group row pt-3">
                                        <label className="col-sm-4 control-label">ID:</label>
                                        <div className="col-sm-8">
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="id"
                                                name="id"
                                                required
                                                value={sizeitem.id}
                                                placeholder="ID"
                                            />
                                        </div>
                                    </div>
                                    {/* id-type */}
                                    <div className="form-group row">
                                        <label className="col-sm-4 control-label">Size:</label>
                                        <div className="col-sm-8">
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="type"
                                                required
                                                name="type"
                                                value={sizeitem.type}
                                                onChange={handleInputChange}
                                                placeholder="Loại"
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label className="col-sm-4 control-label">Giá trị tương ứng:</label>
                                        <div className="col-sm-8">
                                            <input
                                                type="text"
                                                className="form-control"
                                                id="price"
                                                required
                                                name="price"
                                                value={sizeitem.price}
                                                onChange={handleInputChange}
                                                placeholder="Giá"
                                            />
                                        </div>
                                    </div>
                                    <div className="form-group row">
                                        <label for="name" className="col-sm-4 control-label">
                                            Trạng Thái:
									</label>
                                        <div className="col-sm-8">{selectCategory()}</div>
                                    </div>
                                </section>

                                {/*  Nút button Update */}
                                <div className="form-group row pt-2 div-button">
                                    <div className="col-sm-12">
                                        <button type="submit" className="btn btn-sm btn-success" onClick={updateTutorial} style={{ marginRight: '20px' }}>
                                            Cập Nhật
									</button>
                                        {/* <button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
                                            Xóa
									</button> */}
                                    </div>
                                </div>
                                {/* <p>{message}</p> */}
                            </div>
                        </Modal.Body>
                    </Modal>
                ) : (
                    <div>
                        <br />
                        <p>Please click on a Tutorial...</p>
                    </div>
                )}
            </div>
        );
    }
