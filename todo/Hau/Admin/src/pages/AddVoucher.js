import React, { useState,useEffect } from 'react';
import Voucher from '../components/Axios/AxiosServicer';

import { Modal, Button } from 'react-bootstrap';
import './Style.css'
export default function AddVoucher() {

	const initialproducttate = {
        id: '',
        status: '',
        value: '',
        start_time: '',
        end_time: '',
        quanity: ''
    };

    const [voucher, setvoucher] = useState(initialproducttate);

	function handleInputChange(evt) {
		const value = evt.target.value;
		setvoucher({
			...voucher,
			[evt.target.id]: value
		});
	}

	const saveTutorial = (data) => {
		var data = {
			status: true,
			value: voucher.value,
            start_time: voucher.start_time,
            end_time: voucher.end_time,
            quanity: voucher.quanity,
		
		};
		Voucher.createVoucher(data)
			.then(response => {
				// console.log(response.data)
				handleClose();
				handleNotificationShow();
			})
			.catch(e => {
				handleNofErrorShow()
			});
	};


	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);
	//Thông Báo thành công
	const [Notification, setNotification] = useState(false);
	const handleNotificationClose = () => setNotification(false);
	const handleNotificationShow = () => setNotification(true);
	const ReloadPage = () => {
		handleNotificationClose();
	window.location.reload()
	};
		//Thông Báo thất bại
		const [NofError, setNofError] = useState(false);
		const handleNofErrorClose = () => setNofError(false);
		const handleNofErrorShow = () => setNofError(true);
		const ReloadNofError= () => {
			handleNofErrorClose();
			handleShow();
		};
	return (
		<>
			<Button variant="primary" className="btn  btn-outline-danger" onClick={handleShow}>
			<i className="ft-file-plus">&nbsp;Thêm Voucher</i>
  </Button>
       {/* Modal Thông báo thành công */}
      <Modal dialogClassName="messmodal" show={Notification} onHide={handleNotificationClose}>
             <div className="icon-box">
					<i className="ft-check text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Thêm Thành Công</p>
			</Modal.Body>

            <button className="btn btn-success btn-block" type="button" onClick={ReloadPage}>OK</button>
			</Modal>
			       {/* Modal Thông báo thất bại */}
				   <Modal dialogClassName="messmodal" show={NofError} onHide={handleNofErrorClose}>
             <div className="icon-box">
					<i className="ft-x text-center"></i>
				</div>				
				<Modal.Body>
				<p className="text-center mt-4">Thêm Thất bại</p>
			</Modal.Body>

            <button className="btn btn-danger btn-block" type="button" onClick={ReloadNofError}>OK</button>
			</Modal>
			{/* Modal Loại */}
			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm Loại</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
						<div className="submit-form">
								<div>
									<section className="panel panel-default">
										{/*Form Type */}
										
                                        <label className="control-label mt-3">Giá:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="value"
                                            value={voucher.value}
                                            onChange={handleInputChange}
                                            name="value"
                                            placeholder="Giá"
                                        />
                                        {/* start_time */}
                                        <label className="control-label mt-3">Ngày Bắt Đầu:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="start_time"
                                            value={voucher.start_time}
                                            onChange={handleInputChange}
                                            name="start_time"
                                            placeholder="Ngày bắt đầu"
                                        />
                                        {/* end_time */}
                                        <label className="control-label mt-3">Ngày Kết Thúc:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="end_time"
                                            value={voucher.end_time}
                                            onChange={handleInputChange}
                                            name="end_time"
                                            placeholder="ngày kết thúc"
                                        />
                                        {/* id-type */}
                                        <label className="control-label mt-3">Số lượng:</label>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="quanity"
                                            value={voucher.quanity}
                                            onChange={handleInputChange}
                                            name="quanity"
                                            placeholder="Số lượng"
                                        />
											<button className="btn btn-outline-success col-sm-2 " onClick={saveTutorial} >Thêm</button>
										
									</section>
								</div>
						
						</div>
					</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
