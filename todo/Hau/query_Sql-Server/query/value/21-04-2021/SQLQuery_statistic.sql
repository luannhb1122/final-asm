/****** Script for SelectTopNRows command from SSMS  ******/
use thecoffeeson
go

-- SELECT * from Accounts_info where role =0

--view tong thong ke
create view orders_view as
--select *
select odt.id ,
		od.id as id_order,		
		b.date_create as date_sale,
		pr.name as product,
		pr.image,
		pr.price,
		od.payment,
		ost.status,
		odt.amount,( odt.amount*pr.price*sz.Product_price) as total 
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 
 go
  use thecoffeeson
  go
 select * from orders_view 


 select MONTH(date_sale),sum(total)
 from orders_view
 where year(date_sale)= 2020
 group by MONTH(date_sale)
 go

  select YEAR(date_sale) from orders_view 
group by YEAR(date_sale)
 go
 --xuat year
  --drop PROCEDURE yearCall 
  --go
 CREATE PROCEDURE yearCall 
AS
  select YEAR(date_sale) from orders_view 
group by YEAR(date_sale)
 go

 exec yearCall;
 go

 --thong ke doanh thu
 --Doanh thu hom nay
 --drop view Today_view
 create view Today_view as
 select product,amount, date_sale ,sum(total) as total
 from orders_view
 where day(date_sale)= day(getdate()) and MONTH(date_sale)= month(getdate()) and year(date_sale) =year(getdate())
 group by product,date_sale,amount
 go


 --drop PROCEDURE doanhThuToday 
 CREATE PROCEDURE doanhThuToday
 as 
 select* from Today_view
 --select product, MONTH(date_sale)as month, year(date_sale)as year ,sum(total) as total
 --from orders_view
 --where day(date_sale)= day(getdate()) and MONTH(date_sale)= month(getdate()) and year(date_sale) =year(getdate())
 --group by product,MONTH(date_sale),year(date_sale)
 go
 exec doanhThuToday
 go

 --drop PROCEDURE doanhThuAll 
 --go
 CREATE PROCEDURE doanhThuAll 
AS
 select MONTH(date_sale)as month, year(date_sale)as year ,sum(total) as total
 from orders_view
 group by MONTH(date_sale),year(date_sale)
 go
 exec doanhThuAll
 go

 --drop PROCEDURE doanhthunam
 CREATE PROCEDURE doanhthunam @year int
AS
select MONTH(date_sale)as month ,sum(total) as total
 from orders_view
 where year(date_sale)= @year
 group by MONTH(date_sale)
 go
go
exec doanhthunam @year= 2020
go
--drop PROCEDURE doanhthunhieunam
 CREATE PROCEDURE doanhthunhieunam 
AS
select year(date_sale)as year ,sum(total) as total
 from orders_view
 --where year(date_sale)= @year
 group by year(date_sale)
 go

exec doanhthunhieunam
go

 CREATE PROCEDURE sumdoanhthu 
AS
select sum(total) as total
 from orders_view
 --where year(date_sale)= @year
-- group by total
 go
 exec sumdoanhthu
 go

-- ban tong
--drop PROCEDURE findOrder
CREATE PROCEDURE findOrder 
AS
select * from orders_view 
go
exec findOrder
go
-- thong ke san pham------------------------------------------
--drop PROCEDURE searchProduct
--go
 CREATE PROCEDURE searchProduct @pr nvarchar(50),@mon int ,@yr int
 as
 select Product,month(date_sale) as month ,sum(amount)as amout,sum(total)as total
 from orders_view 
 where Product like '%'+@pr+'%' and month(date_sale) = @mon and year(date_sale) = @yr
 group by Product , month(date_sale)
 go

 exec searchProduct @pr = a, @mon =1 ,@yr=2021
 go
  
  CREATE PROCEDURE allProduct
 as
 select Product,sum(amount)as amout,sum(total)as total
 from orders_view 
 group by Product
 go
 
  exec allProduct
  go

-- thong ke san pham theo thang
 select Product,sum(amount)as amout,sum(total)as total
 from orders_view
  where Month(date_sale) =1 and year(date_sale) =2020
 group by Product,Month(date_sale)
 go
  
  --drop PROCEDURE product_statisticMon
  --go

   CREATE PROCEDURE product_statisticMon @month  int ,@year int
AS

 select Product,sum(amount)as amout,sum(total)as total
 from orders_view
 where Month(date_sale) =@month and year(date_sale) =@year
 group by Product,Month(date_sale)

go


exec product_statisticMon @month= 1,@year=2020

--thong ke san pham theo nam
 select Product,sum(amount)as amout,sum(total)as total
 from orders_view
  where  year(date_sale) =2020
 group by Product
 go

   CREATE PROCEDURE product_statisticYear @year int
AS
 select Product,sum(amount)as amout,sum(total)as total
 from orders_view
  where  year(date_sale) = @year
 group by Product

go

exec product_statisticYear @year=2021
go


CREATE PROCEDURE findOrderbyid @idod int
AS
select * from orders_view where id_order = @idod
go

exec findOrderbyid @idod = 1;

exec findOrderbyid @idod = 1;



go

use thecoffeeson
--SELECT * from Accounts_info where id_role =1

----view
----select *
----drop view bills_view
--create view bills_view as
----select *
--select pr.name as product, b.date_create,odt.amount ,( odt.amount*pr.price*sz.Product_price) as total  
--from Orders od
--inner join Orders_Detail odt on od.id = odt.id_order
--inner join Products pr on odt.id_product = pr.id
--inner join Sizes sz on odt.id_size = sz.id
--inner join bills b on b.id_order = od.id

-- go

-- select * from bills_view


-- --drop view Product_viewStatistic


-- --all product
-- --drop procedure allProduct
-- CREATE PROCEDURE allProduct
-- as
-- select * from Product_viewStatistic
-- go
--  exec allProduct

-- --  CREATE PROCEDURE findProduct @pr nvarchar(50)
-- --as
-- --select * from Product_viewStatistic where product = @pr
-- --go
-- -- exec findProduct @pr = 'cafe'

-- select MONTH(date_create),sum(amount),sum(total)
-- from bills_view
-- where year(date_create)= 2020
-- group by MONTH(date_create)
-- go

--  select MONTH(date_create)as month, sum(amount) as amount,sum(total) as total
-- from bills_view
---- where MONTH(date_create)= @month
-- group by MONTH(date_create)

-- ------------------------------------------------------------------------------------------------------------
-- --function


-- --thong ke product theo thang

-- --drop PROCEDURE product_statistic;
-- CREATE PROCEDURE product_statistic @month  int ,@year int
--AS
-- select *
-- from Product_viewStatistic
-- where month = @month and year =@year

--go

--exec product_statistic @month=10 ,@year= 2021



------------------------------------------
-- -- func thong ke theo day



--  --drop PROCEDURE billsbydate
-- CREATE PROCEDURE billsbydate @day int, @mon int ,@year int
 
--AS
-- select day(date_create) as date,date_create, sum(amount) as amount,sum(total) as total
-- from bills_view
-- where day(date_create)= @day and month(date_create)= @mon and year(date_create)= @year
-- group by date_create
--go
--exec billsbydate @day =9, @mon=11 ,@year =2021

----drop PROCEDURE billsbydayMon
--CREATE PROCEDURE billsbydayMon @mon int 
 
--AS
-- select day(date_create) as date,date_create, sum(amount) as amount,sum(total) as total
-- from bills_view
-- where   month(date_create)= @mon
-- group by date_create
--go
--exec billsbydayMon  @mon=11 

----day
----drop PROCEDURE billsbyday
--CREATE PROCEDURE billsbyday @day int
 
--AS
-- select day(date_create) as date,date_create, sum(amount) as amount,sum(total) as total
-- from bills_view
-- where day(date_create)= @day 
-- group by date_create
--go
--exec billsbyday @day =9

-------------------------------------------
-- -- func thong ke theo thang
-- --drop PROCEDURE billsAll
--  CREATE PROCEDURE billsAll
 
--AS
-- select date_create , sum(amount) as amount,sum(total) as total
-- from bills_view
-- group by date_create
--go

--exec billsAll



-- --drop PROCEDURE billsbymonth
-- CREATE PROCEDURE billsbymonth @month int
 
--AS
-- select date_create , sum(amount) as amount,sum(total) as total
-- from bills_view
-- where MONTH(date_create)= @month
-- group by date_create
--go
--exec billsbymonth @month =4

-------------------------------------------

-- -- func thong ke theo nam
----drop PROCEDURE billsbyYear
-- CREATE PROCEDURE billsbyYear @year int 
--AS
-- select year(date_create)as month, sum(amount) as amount,sum(total) as total
-- from bills_view
-- where year(date_create)= @year
-- group by year(date_create)
--go

--exec billsbyYear @year =2021

--go




-- select * from bills_view