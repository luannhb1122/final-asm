use thecoffeeson
go

--drop database thecoffeeson

--drop TABLE Bills;
--drop TABLE Orders_status;
--drop TABLE Ships_info;
--drop TABLE Orders_Detail;
--drop TABLE Orders;
--drop TABLE Vouchers;
--drop TABLE Sizes;
--drop TABLE Feedbacks;
--drop TABLE Products;
--drop TABLE Categories;
--drop TABLE Accounts_info;

go


--view product theo status va loai
create view product_view as
--select *
select pr.id, pr.name, pr.image, pr.price, pr.status,pr.id_categories
from Products pr
inner join Categories ctl on pr.id_categories = ctl.id
where ctl.status =1 and pr.status =1
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 
 go


 --find san pham theo ten
 CREATE PROCEDURE findProduct @pr nvarchar(50)
 as
 select *
 from product_view 
 where name like '%'+@pr+'%' 
 go

 exec findProduct @pr = ca
go

 -- view bill
--view tong thong ke
--drop view bills_view
create view bills_view as
select b.id, b.date_create,sum( odt.amount*pr.price*sz.Product_price) as total,b.id_order 
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
group by b.id , b.date_create ,b.id_order 
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 
 go

 select * from bills_view
--=======
--drop database thecoffeeson

--drop TABLE Bills;
--drop TABLE Orders_status;
--drop TABLE Ships_info;
--drop TABLE Orders_Detail;
--drop TABLE Orders;
--drop TABLE Vouchers;
--drop TABLE Sizes;
--drop TABLE Feedbacks;
--drop TABLE Products;
--drop TABLE Categories;
--drop TABLE Accounts_info;
--drop TABLE Employees;
--drop TABLE Customers;
--drop TABLE Roles;
--create view showCart 
-->>>>>>> 0572402d6ed90bdbcb4f3614d65d5feb8069fd27
