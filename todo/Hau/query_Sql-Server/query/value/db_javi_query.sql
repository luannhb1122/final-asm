use master
go
drop database thecoffeeson

drop TABLE Bills;
drop TABLE Orders_status;
drop TABLE Ships_info;
drop TABLE Orders_Detail;
drop TABLE Orders;
drop TABLE Vouchers;
drop TABLE Sizes;
drop TABLE Feedbacks;
drop TABLE Products;
drop TABLE Categories;
drop TABLE Accounts_info;
drop TABLE Employees;
drop TABLE Customers;
drop TABLE Roles;
create view showCart 