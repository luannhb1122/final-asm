-- create database Coffee;

-- HOME PAGE INFO
-- Create table Stores
-- (
-- 	id serial primary key,
-- 	name varchar(50),
-- 	phone varchar(11),
-- 	email varchar(50),
-- 	address varchar(50),
-- 	website varchar(25)
-- );
-- Create table News
-- (
-- 	id serial primary key,
-- 	newcontent varchar(255),
-- 	note varchar(255),
-- 	date_create varchar(50) not null,
-- 	image varchar(25)	
-- );
-- TAI KHOAN
-- create table Logins
-- (
-- 	username varchar(50) not null primary key,
-- 	password varchar(50) not null
-- );

-- create table Tokens
-- (
	
-- 	token varchar(50)  primary key,
-- 	status bit not null,
-- 	username_login varchar(50) not null,
-- 	FOREIGN KEY (username_login) REFERENCES Logins(username) ON DELETE CASCADE
-- );



-- Create table Account_images
-- (
-- 	id serial primary key,
-- 	image varchar(25),	
-- 	id_account int ,	
-- 	FOREIGN KEY (id_account) REFERENCES Accounts(id) ON DELETE CASCADE
-- );

create table Customers
(
	id serial primary key,
	username varchar(50) not null,
	password varchar(50) not null,
	status boolean DEFAULT(true) ,
	level int not null	
);


create table Employees
(
	id serial primary key,
	username varchar(50) not null,
	password varchar(50) not null,
	status boolean DEFAULT(true) ,
	start_date varchar(25) not null	
);

Create table Accounts_info
(
	id serial primary key,
	name varchar(50),
	phone varchar(20),
	email varchar(50),
	birthday varchar(15),
	picture varchar(15),
	gender int,
	id_role int not null,
	FOREIGN KEY (id_role) REFERENCES Employees(id) ON DELETE CASCADE,
	FOREIGN KEY (id_role) REFERENCES Customers(id) ON DELETE CASCADE
);

-- create table Notifications
-- (
-- 	id serial primary key,
-- 	message varchar(255),
-- 	status boolean DEFAULT(true),
-- 	id_cus int,
-- 	FOREIGN KEY (id_cus) REFERENCES Customers(id) ON DELETE CASCADE
-- );
--  SAN PHAM
create table Categories
(
	id serial primary key,	
	type varchar(50) not null,
	status boolean DEFAULT(true)
);

create table Products
(
	id serial primary key,
	name varchar(50) not null,	
	image varchar(25) not null,
	price float not null,
	status boolean DEFAULT(true) ,
	id_categories int not null,	
	FOREIGN KEY (id_categories) REFERENCES Categories(id) ON DELETE CASCADE
	
); 

create table Feedbacks
(
	id serial primary key,
	note varchar(255),
	status boolean DEFAULT(true),
	rate int,
	id_cus int not null,
	id_product int not null,
	FOREIGN KEY (id_cus) REFERENCES Customers(id) ON DELETE CASCADE,
	FOREIGN KEY (id_product) REFERENCES Products(id) ON DELETE CASCADE
);
 create table Sizes
 (
	id serial primary key,
	Producte_size varchar(10),
	 status boolean DEFAULT(true),
	Producte_price float
 );
--  THANH TOAN
create table Vouchers
(
	id serial primary key,
	status int not null,
	values float not null,
	start_time varchar(50) not null,
	end_time varchar(50) not null,
	quanity int
);
create table Orders
(
	id serial primary key,
	date_created varchar(50) not null,
	payment varchar(50) DEFAULT 'cash',
	id_cus int not null,
	id_emp int not null,
	id_voucher int ,
	FOREIGN KEY (id_cus) REFERENCES Customers(id) ON DELETE CASCADE,
	FOREIGN KEY (id_voucher) REFERENCES Vouchers(id) ON DELETE CASCADE,
	FOREIGN KEY (id_emp) REFERENCES Employees(id) ON DELETE CASCADE
);

create table Order_Details
(
	id serial primary key,
	amount int not null,
	id_product int not null,
	id_orders int not null,
	id_size int not null DEFAULT 1,
	FOREIGN KEY (id_size) REFERENCES Sizes(id) ON DELETE CASCADE,
	FOREIGN KEY (id_product) REFERENCES Products(id) ON DELETE CASCADE,
	FOREIGN KEY (id_orders) REFERENCES Orders(id) ON DELETE CASCADE
);

create table Ship_infos
(
	id serial primary key,
	name_cus varchar(255) not null,
	address varchar(50) not null,
	phone_cus varchar(11) not null,
	note varchar(100) not null,
	price float not null DEFAULT 15000,
	id_orders int not null,
	FOREIGN KEY (id_orders) REFERENCES Orders(id) ON DELETE CASCADE
);

create table Order_status
(
	id serial primary key,
	status varchar(50) not null,
	coment varchar(50) not null,
	id_orders int not null,
	FOREIGN KEY (id_orders) REFERENCES Orders(id) ON DELETE CASCADE
);


create table Bills
(
	id serial primary key,
	date_create varchar(50) not null,	
	total float ,
	id_orders int not null,	
	FOREIGN KEY (id_orders) REFERENCES Orders(id) ON DELETE CASCADE	
);



-- create table Record
-- (
-- 	id serial primary key,
-- 	status varchar(255) not null,
-- 	id_bill int not null,
-- 	FOREIGN KEY (id_bill) REFERENCES Bill(id) ON DELETE CASCADE
-- );


