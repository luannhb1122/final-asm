USE [master]
GO
/****** Object:  Database [thecoffeeson]    Script Date: 5/3/2021 7:04:07 PM ******/
CREATE DATABASE [thecoffeeson]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'thecoffeeson', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\thecoffeeson.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'thecoffeeson_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\thecoffeeson_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [thecoffeeson] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [thecoffeeson].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [thecoffeeson] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [thecoffeeson] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [thecoffeeson] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [thecoffeeson] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [thecoffeeson] SET ARITHABORT OFF 
GO
ALTER DATABASE [thecoffeeson] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [thecoffeeson] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [thecoffeeson] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [thecoffeeson] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [thecoffeeson] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [thecoffeeson] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [thecoffeeson] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [thecoffeeson] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [thecoffeeson] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [thecoffeeson] SET  ENABLE_BROKER 
GO
ALTER DATABASE [thecoffeeson] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [thecoffeeson] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [thecoffeeson] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [thecoffeeson] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [thecoffeeson] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [thecoffeeson] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [thecoffeeson] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [thecoffeeson] SET RECOVERY FULL 
GO
ALTER DATABASE [thecoffeeson] SET  MULTI_USER 
GO
ALTER DATABASE [thecoffeeson] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [thecoffeeson] SET DB_CHAINING OFF 
GO
ALTER DATABASE [thecoffeeson] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [thecoffeeson] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [thecoffeeson] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'thecoffeeson', N'ON'
GO
USE [thecoffeeson]
GO
/****** Object:  Table [dbo].[Accounts_info]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Accounts_info](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[phone] [varchar](11) NULL,
	[email] [varchar](80) NULL,
	[picture] [varchar](max) NULL,
	[birthday] [date] NULL,
	[gender] [bit] NULL,
	[username] [varchar](50) NOT NULL,
	[password] [varchar](60) NOT NULL,
	[create_date] [date] NULL DEFAULT (getdate()),
	[status] [bit] NULL DEFAULT ((1)),
	[id_role] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Bills]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bills](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_create] [date] NULL DEFAULT (getdate()),
	[total] [float] NULL,
	[id_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Categories]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](50) NOT NULL,
	[status] [bit] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Feedbacks]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Feedbacks](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[note] [nvarchar](255) NULL,
	[status] [bit] NULL DEFAULT ((1)),
	[rate] [int] NULL,
	[date_time] [date] NULL DEFAULT (getdate()),
	[id_cus] [int] NOT NULL,
	[id_product] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[date_created] [date] NULL DEFAULT (getdate()),
	[payment] [int] NULL DEFAULT ((1)),
	[id_cus] [int] NOT NULL,
	[id_voucher] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders_Detail]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders_Detail](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[amount] [int] NOT NULL,
	[id_product] [int] NOT NULL,
	[id_order] [int] NOT NULL,
	[id_size] [int] NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Orders_status]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders_status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[status] [int] NOT NULL DEFAULT ((1)),
	[comment] [nvarchar](80) NOT NULL,
	[id_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Products]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Products](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](80) NOT NULL,
	[image] [varchar](max) NOT NULL,
	[price] [float] NOT NULL,
	[status] [int] NULL DEFAULT ((1)),
	[id_categories] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ships_info]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ships_info](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name_cus] [nvarchar](255) NOT NULL,
	[address] [nvarchar](max) NOT NULL,
	[phone_cus] [varchar](11) NOT NULL,
	[note] [nvarchar](100) NOT NULL,
	[price] [float] NOT NULL DEFAULT ((15000)),
	[id_order] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sizes]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sizes](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Product_size] [nvarchar](10) NULL,
	[status] [bit] NULL DEFAULT ((1)),
	[Product_price] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Stores]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stores](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[phone] [nvarchar](11) NULL,
	[email] [varchar](80) NULL,
	[address] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vouchers]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vouchers](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[status] [bit] NULL DEFAULT ((1)),
	[value] [float] NOT NULL,
	[start_time] [date] NULL DEFAULT (getdate()),
	[end_time] [date] NOT NULL,
	[quanity] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[orders_view]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- SELECT * from Accounts_info where role =0

--view tong thong ke
create view [dbo].[orders_view] as
--select *
select odt.id ,
		od.id as id_order,		
		b.date_create as date_sale,
		pr.name as product,
		pr.image,
		pr.price,
		od.payment,
		ost.status,
		odt.amount,( odt.amount*pr.price*sz.Product_price) as total 
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 


GO
/****** Object:  View [dbo].[Today_view]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 --thong ke doanh thu
 --Doanh thu hom nay

 create view [dbo].[Today_view] as
 select product, MONTH(date_sale)as month, year(date_sale)as year ,sum(total) as total
 from orders_view
 where day(date_sale)= day(getdate()) and MONTH(date_sale)= month(getdate()) and year(date_sale) =year(getdate())
 group by product,MONTH(date_sale),year(date_sale)
 


GO
/****** Object:  View [dbo].[bills_view]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bills_view] as
select b.id, b.date_create,sum( odt.amount*pr.price*sz.Product_price) as total,b.id_order
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
where ost.status = 2
group by b.id , b.date_create ,b.id_order ,ost.comment

--where year(b.date_create)= 2020 

GO
/****** Object:  View [dbo].[orders_viewsussecc]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 create view [dbo].[orders_viewsussecc] as
 select 
		od.id as id_order,		
		b.date_create as date_sale,	
		od.payment,
		ost.status		 
from Orders od
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
where ost.status =2

GO
/****** Object:  View [dbo].[product_view]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--drop view product_view
create view [dbo].[product_view] as
select pr.id, pr.name, pr.image, pr.price, pr.status,pr.id_categories
from Products pr
inner join Categories ctl on pr.id_categories = ctl.id


GO
/****** Object:  View [dbo].[productTrue_view]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--drop view productTrue_view
create view [dbo].[productTrue_view] as
--select *
select pr.id, pr.name, pr.image, pr.price, pr.status,pr.id_categories
from Products pr
inner join Categories ctl on pr.id_categories = ctl.id
where pr.status =1 or pr.status =2 and ctl.status =1
 
--where ost.status like 'succsec'
--where year(b.date_create)= 2020 


GO
/****** Object:  View [dbo].[VoucherUser_View]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec findFeedbacksbyProduct @id=4
create view [dbo].[VoucherUser_View] as
select v.id, v.status, v.value, v.start_time, v.end_time, quanity 
from Orders o 
RIGHT JOIN Vouchers v 
on O.id_voucher = V.id  where v.status =1 and end_time> GETDATE()
Group by v.id, v.status, v.value, v.start_time, v.end_time, quanity
Having count(id_voucher) < (select quanity from Vouchers where id = v.id)


GO
SET IDENTITY_INSERT [dbo].[Accounts_info] ON 

INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (1, N'SUPERADMIN', N'0964138876', N'nguyentiedat03171999@gmail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1618809968/Thecoffeeson/nongdan_yu5fvu.jpg', CAST(N'2021-01-04' AS Date), 0, N'admin', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2020-12-01' AS Date), 1, 3)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (2, N'Nguyễn Tiến Đạt', N'0964138876', N'nguyentiedat03171999@gmail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619507468/Thecoffeeson/NguyenTienDat_iybjcp.png', CAST(N'2021-01-04' AS Date), 0, N'dat', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2020-12-01' AS Date), 1, 1)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (3, N'Ngô Hoàng Bảo Luân', N'0764265512', N'ghost451201@gmail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619507461/Thecoffeeson/NgoHoangBaoLuan_m2eqio.jpg', CAST(N'2021-02-04' AS Date), 1, N'luan', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-01-02' AS Date), 1, 1)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (4, N'Võ Trường Sơn', N'0989697995', N'votruongson264@gmail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619507451/Thecoffeeson/VoTruong_Son_eqlkrh.png', CAST(N'2021-01-04' AS Date), 0, N'son', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-02-03' AS Date), 1, 1)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (5, N'Nguyễn Thanh Hậu', N'0794751044', N'nguyenvan3@gmail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619711414/Thecoffeeson/NguyenThanhHau_iyzxe8.jpg', CAST(N'2021-02-04' AS Date), 1, N'hau', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-02-04' AS Date), 1, 1)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (6, N'Nguyễn Nhật Minh', N'0376571409', N'mdung2406@gmail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619715988/Thecoffeeson/NguyenNhatMinh_iqy04w.jpg', CAST(N'2021-02-04' AS Date), 1, N'minh', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-02-04' AS Date), 1, 1)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (7, N'Nguyễn Văn Cường', N'0761238764', N'Cuongnv0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-01-04' AS Date), 0, N'cuonglg', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (8, N'Linh Ngọc Vân', N'0257894456', N'vanln0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-01-04' AS Date), 0, N'cus0', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (9, N'Vũ Minh Tiến', N'0641238643', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-02-04' AS Date), 1, N'cus1', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (10, N'Đặng Ngọc Mai', N'0123257789', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-01-04' AS Date), 0, N'cus2', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (11, N'Huỳnh Quốc Đạt', N'0863452251', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-01-04' AS Date), 1, N'cus3', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (12, N'Nguyễn Mai Oanh', N'0642589954', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-02-04' AS Date), 0, N'cus4', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (13, N'Phan Minh Tú', N'0168947729', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-02-04' AS Date), 0, N'cus5', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (14, N'Hoàng Tú Lan', N'0753214456', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.png', CAST(N'2021-01-04' AS Date), 1, N'cus6', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (15, N'Đô Tuấn Nam', N'0985735561', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 0, N'cus7', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (16, N'Đổ Thị Ánh Thu', N'0123457654', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 1, N'cus8', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (17, N'Phan Ngọc Tuyến', N'0123562234', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 0, N'cus9', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (18, N'Lê Minh Nguyệt', N'098752568', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 0, N'cus10', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (19, N'Lê Thiết Vương', N'098758853', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 1, N'cus11', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (20, N'Nguyễn Vân Diểm Hương', N'097358841', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 1, N'cus12', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (21, N'Nguyễn Tuyên', N'0987865435', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 0, N'cus13', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (22, N'Trần Thi Phương Uyên', N'0834562234', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 1, N'cus14', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (23, N'Đăng Văn Trung', N'0978956432', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 0, N'cus15', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (24, N'Võ Ánh Hằng', N'0978905532', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 1, N'cus16', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (25, N'Đinh Quốc Hoàng', N'0870981234', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 0, N'cus18', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (26, N'Ngô Hoàng Kim Vân', N'0965790122', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 1, N'cus19', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (27, N'Nguyên Đông Quân', N'0998877886', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 1, N'cus20', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (28, N'Huỳnh Liên Nguyện', N'0789065312', N'nguyenvan0@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-01-04' AS Date), 0, N'cus21', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (29, N'Phan Văn Kiên', N'0098999333', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 1, N'cus22', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
INSERT [dbo].[Accounts_info] ([id], [name], [phone], [email], [picture], [birthday], [gender], [username], [password], [create_date], [status], [id_role]) VALUES (30, N'Ngô Hoàng Thanh Liên', N'090088642', N'nguyenvan2@gail.com', N'https://res.cloudinary.com/minhchon/image/upload/v1619717279/Thecoffeeson/default_vjq0i8.jpg', CAST(N'2021-02-04' AS Date), 0, N'cus23', N'$2a$10$o2kTQD7fBafkZpqDgiQ0ve1f3j9NYRUF62TptSS.f/YDk9lKFv5me', CAST(N'2021-05-02' AS Date), 1, 2)
SET IDENTITY_INSERT [dbo].[Accounts_info] OFF
SET IDENTITY_INSERT [dbo].[Bills] ON 

INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (1, CAST(N'2020-01-04' AS Date), NULL, 1)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (2, CAST(N'2020-02-04' AS Date), NULL, 2)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (3, CAST(N'2020-03-04' AS Date), NULL, 3)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (4, CAST(N'2020-04-04' AS Date), NULL, 4)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (5, CAST(N'2020-05-04' AS Date), NULL, 5)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (6, CAST(N'2020-06-04' AS Date), NULL, 6)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (7, CAST(N'2020-07-04' AS Date), NULL, 7)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (8, CAST(N'2020-08-04' AS Date), NULL, 8)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (9, CAST(N'2020-09-04' AS Date), NULL, 9)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (10, CAST(N'2020-10-04' AS Date), NULL, 10)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (11, CAST(N'2020-11-04' AS Date), NULL, 11)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (12, CAST(N'2020-12-04' AS Date), NULL, 12)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (13, CAST(N'2021-01-04' AS Date), NULL, 13)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (14, CAST(N'2021-02-03' AS Date), NULL, 14)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (15, CAST(N'2021-03-04' AS Date), NULL, 15)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (16, CAST(N'2021-04-04' AS Date), NULL, 16)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (17, CAST(N'2020-01-01' AS Date), NULL, 17)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (18, CAST(N'2020-01-01' AS Date), NULL, 18)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (19, CAST(N'2020-01-02' AS Date), NULL, 19)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (20, CAST(N'2020-01-03' AS Date), NULL, 20)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (21, CAST(N'2020-01-04' AS Date), NULL, 21)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (22, CAST(N'2020-01-05' AS Date), NULL, 22)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (23, CAST(N'2020-01-06' AS Date), NULL, 23)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (24, CAST(N'2020-01-07' AS Date), NULL, 24)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (25, CAST(N'2020-01-08' AS Date), NULL, 25)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (26, CAST(N'2020-01-09' AS Date), NULL, 26)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (27, CAST(N'2020-01-10' AS Date), NULL, 27)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (28, CAST(N'2020-01-11' AS Date), NULL, 28)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (29, CAST(N'2020-01-12' AS Date), NULL, 29)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (30, CAST(N'2020-01-13' AS Date), NULL, 30)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (31, CAST(N'2020-01-14' AS Date), NULL, 31)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (32, CAST(N'2020-01-15' AS Date), NULL, 32)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (33, CAST(N'2020-01-16' AS Date), NULL, 33)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (34, CAST(N'2020-01-17' AS Date), NULL, 34)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (35, CAST(N'2020-01-18' AS Date), NULL, 35)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (36, CAST(N'2020-01-19' AS Date), NULL, 36)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (37, CAST(N'2020-01-20' AS Date), NULL, 37)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (38, CAST(N'2020-01-21' AS Date), NULL, 38)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (39, CAST(N'2020-01-22' AS Date), NULL, 39)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (40, CAST(N'2020-01-23' AS Date), NULL, 40)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (41, CAST(N'2020-01-24' AS Date), NULL, 41)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (42, CAST(N'2020-01-25' AS Date), NULL, 42)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (43, CAST(N'2020-01-26' AS Date), NULL, 43)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (44, CAST(N'2020-01-27' AS Date), NULL, 44)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (45, CAST(N'2020-01-28' AS Date), NULL, 45)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (46, CAST(N'2020-01-29' AS Date), NULL, 46)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (47, CAST(N'2020-01-30' AS Date), NULL, 47)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (48, CAST(N'2020-01-31' AS Date), NULL, 48)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (49, CAST(N'2020-02-01' AS Date), NULL, 49)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (50, CAST(N'2020-02-02' AS Date), NULL, 50)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (51, CAST(N'2020-02-03' AS Date), NULL, 51)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (52, CAST(N'2020-02-04' AS Date), NULL, 52)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (53, CAST(N'2020-02-05' AS Date), NULL, 53)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (54, CAST(N'2020-02-06' AS Date), NULL, 54)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (55, CAST(N'2020-02-07' AS Date), NULL, 55)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (56, CAST(N'2020-02-08' AS Date), NULL, 56)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (57, CAST(N'2020-02-09' AS Date), NULL, 57)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (58, CAST(N'2020-02-10' AS Date), NULL, 58)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (59, CAST(N'2020-02-11' AS Date), NULL, 59)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (60, CAST(N'2020-02-12' AS Date), NULL, 60)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (61, CAST(N'2020-02-13' AS Date), NULL, 61)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (62, CAST(N'2020-02-14' AS Date), NULL, 62)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (63, CAST(N'2020-02-15' AS Date), NULL, 63)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (64, CAST(N'2020-02-16' AS Date), NULL, 64)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (65, CAST(N'2020-02-17' AS Date), NULL, 65)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (66, CAST(N'2020-02-18' AS Date), NULL, 66)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (67, CAST(N'2020-02-19' AS Date), NULL, 67)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (68, CAST(N'2020-02-20' AS Date), NULL, 68)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (69, CAST(N'2020-02-21' AS Date), NULL, 69)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (70, CAST(N'2020-02-22' AS Date), NULL, 70)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (71, CAST(N'2020-02-23' AS Date), NULL, 71)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (72, CAST(N'2020-02-24' AS Date), NULL, 72)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (73, CAST(N'2020-02-25' AS Date), NULL, 73)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (74, CAST(N'2020-02-26' AS Date), NULL, 74)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (75, CAST(N'2020-02-27' AS Date), NULL, 75)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (76, CAST(N'2020-02-28' AS Date), NULL, 76)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (77, CAST(N'2020-03-01' AS Date), NULL, 77)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (78, CAST(N'2020-03-02' AS Date), NULL, 78)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (79, CAST(N'2020-03-03' AS Date), NULL, 79)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (80, CAST(N'2020-03-04' AS Date), NULL, 80)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (81, CAST(N'2020-03-05' AS Date), NULL, 81)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (82, CAST(N'2020-03-06' AS Date), NULL, 82)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (83, CAST(N'2020-03-07' AS Date), NULL, 83)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (84, CAST(N'2020-03-08' AS Date), NULL, 84)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (85, CAST(N'2020-03-09' AS Date), NULL, 85)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (86, CAST(N'2020-03-10' AS Date), NULL, 86)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (87, CAST(N'2020-03-11' AS Date), NULL, 87)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (88, CAST(N'2020-03-12' AS Date), NULL, 88)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (89, CAST(N'2020-03-13' AS Date), NULL, 89)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (90, CAST(N'2020-03-14' AS Date), NULL, 90)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (91, CAST(N'2020-03-15' AS Date), NULL, 91)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (92, CAST(N'2020-03-16' AS Date), NULL, 92)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (93, CAST(N'2020-03-17' AS Date), NULL, 93)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (94, CAST(N'2020-03-18' AS Date), NULL, 94)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (95, CAST(N'2020-03-19' AS Date), NULL, 95)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (96, CAST(N'2020-03-20' AS Date), NULL, 96)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (97, CAST(N'2020-03-21' AS Date), NULL, 97)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (98, CAST(N'2020-03-22' AS Date), NULL, 98)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (99, CAST(N'2020-03-23' AS Date), NULL, 99)
GO
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (100, CAST(N'2020-03-24' AS Date), NULL, 100)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (101, CAST(N'2020-03-25' AS Date), NULL, 101)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (102, CAST(N'2020-03-26' AS Date), NULL, 102)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (103, CAST(N'2020-03-27' AS Date), NULL, 103)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (104, CAST(N'2020-03-28' AS Date), NULL, 104)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (105, CAST(N'2020-03-29' AS Date), NULL, 105)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (106, CAST(N'2020-03-30' AS Date), NULL, 106)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (107, CAST(N'2020-03-31' AS Date), NULL, 107)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (108, CAST(N'2020-04-01' AS Date), NULL, 108)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (109, CAST(N'2020-04-02' AS Date), NULL, 109)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (110, CAST(N'2020-04-03' AS Date), NULL, 110)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (111, CAST(N'2020-04-04' AS Date), NULL, 111)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (112, CAST(N'2020-04-05' AS Date), NULL, 112)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (113, CAST(N'2020-04-06' AS Date), NULL, 113)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (114, CAST(N'2020-04-07' AS Date), NULL, 114)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (115, CAST(N'2020-04-08' AS Date), NULL, 115)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (116, CAST(N'2020-04-09' AS Date), NULL, 116)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (117, CAST(N'2020-04-10' AS Date), NULL, 117)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (118, CAST(N'2020-04-11' AS Date), NULL, 118)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (119, CAST(N'2020-04-12' AS Date), NULL, 119)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (120, CAST(N'2020-04-13' AS Date), NULL, 120)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (121, CAST(N'2020-04-14' AS Date), NULL, 121)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (122, CAST(N'2020-04-15' AS Date), NULL, 122)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (123, CAST(N'2020-04-16' AS Date), NULL, 123)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (124, CAST(N'2020-04-17' AS Date), NULL, 124)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (125, CAST(N'2020-04-18' AS Date), NULL, 125)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (126, CAST(N'2020-04-19' AS Date), NULL, 126)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (127, CAST(N'2020-04-20' AS Date), NULL, 127)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (128, CAST(N'2020-04-21' AS Date), NULL, 128)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (129, CAST(N'2020-04-22' AS Date), NULL, 129)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (130, CAST(N'2020-04-23' AS Date), NULL, 130)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (131, CAST(N'2020-04-24' AS Date), NULL, 131)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (132, CAST(N'2020-04-25' AS Date), NULL, 132)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (133, CAST(N'2020-04-26' AS Date), NULL, 133)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (134, CAST(N'2020-04-27' AS Date), NULL, 134)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (135, CAST(N'2020-04-28' AS Date), NULL, 135)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (136, CAST(N'2020-04-29' AS Date), NULL, 136)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (137, CAST(N'2020-04-30' AS Date), NULL, 137)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (138, CAST(N'2020-05-01' AS Date), NULL, 138)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (139, CAST(N'2020-05-02' AS Date), NULL, 139)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (140, CAST(N'2020-05-03' AS Date), NULL, 140)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (141, CAST(N'2020-05-04' AS Date), NULL, 141)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (142, CAST(N'2020-05-05' AS Date), NULL, 142)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (143, CAST(N'2020-05-06' AS Date), NULL, 143)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (144, CAST(N'2020-05-07' AS Date), NULL, 144)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (145, CAST(N'2020-05-08' AS Date), NULL, 145)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (146, CAST(N'2020-05-09' AS Date), NULL, 146)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (147, CAST(N'2020-05-10' AS Date), NULL, 147)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (148, CAST(N'2020-05-11' AS Date), NULL, 148)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (149, CAST(N'2020-05-12' AS Date), NULL, 149)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (150, CAST(N'2020-05-13' AS Date), NULL, 150)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (151, CAST(N'2020-05-14' AS Date), NULL, 151)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (152, CAST(N'2020-05-15' AS Date), NULL, 152)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (153, CAST(N'2020-05-16' AS Date), NULL, 153)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (154, CAST(N'2020-05-17' AS Date), NULL, 154)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (155, CAST(N'2020-05-18' AS Date), NULL, 155)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (156, CAST(N'2020-05-19' AS Date), NULL, 156)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (157, CAST(N'2020-05-20' AS Date), NULL, 157)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (158, CAST(N'2020-05-21' AS Date), NULL, 158)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (159, CAST(N'2020-05-22' AS Date), NULL, 159)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (160, CAST(N'2020-05-23' AS Date), NULL, 160)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (161, CAST(N'2020-05-24' AS Date), NULL, 161)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (162, CAST(N'2020-05-25' AS Date), NULL, 162)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (163, CAST(N'2020-05-26' AS Date), NULL, 163)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (164, CAST(N'2020-05-27' AS Date), NULL, 164)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (165, CAST(N'2020-05-28' AS Date), NULL, 165)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (166, CAST(N'2020-05-29' AS Date), NULL, 166)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (167, CAST(N'2020-05-30' AS Date), NULL, 167)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (168, CAST(N'2020-05-30' AS Date), NULL, 168)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (169, CAST(N'2020-06-01' AS Date), NULL, 169)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (170, CAST(N'2020-06-02' AS Date), NULL, 170)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (171, CAST(N'2020-06-03' AS Date), NULL, 171)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (172, CAST(N'2020-06-04' AS Date), NULL, 172)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (173, CAST(N'2020-06-05' AS Date), NULL, 173)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (174, CAST(N'2020-06-06' AS Date), NULL, 174)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (175, CAST(N'2020-06-07' AS Date), NULL, 175)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (176, CAST(N'2020-06-08' AS Date), NULL, 176)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (177, CAST(N'2020-06-09' AS Date), NULL, 177)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (178, CAST(N'2020-06-10' AS Date), NULL, 178)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (179, CAST(N'2020-06-11' AS Date), NULL, 179)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (180, CAST(N'2020-06-12' AS Date), NULL, 180)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (181, CAST(N'2020-06-13' AS Date), NULL, 181)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (182, CAST(N'2020-06-14' AS Date), NULL, 182)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (183, CAST(N'2020-06-15' AS Date), NULL, 183)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (184, CAST(N'2020-06-16' AS Date), NULL, 184)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (185, CAST(N'2020-06-17' AS Date), NULL, 185)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (186, CAST(N'2020-06-18' AS Date), NULL, 186)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (187, CAST(N'2020-06-19' AS Date), NULL, 187)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (188, CAST(N'2020-06-20' AS Date), NULL, 188)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (189, CAST(N'2020-06-21' AS Date), NULL, 189)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (190, CAST(N'2020-06-22' AS Date), NULL, 190)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (191, CAST(N'2020-06-23' AS Date), NULL, 191)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (192, CAST(N'2020-06-24' AS Date), NULL, 192)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (193, CAST(N'2020-06-25' AS Date), NULL, 193)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (194, CAST(N'2020-06-26' AS Date), NULL, 194)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (195, CAST(N'2020-06-27' AS Date), NULL, 195)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (196, CAST(N'2020-06-28' AS Date), NULL, 196)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (197, CAST(N'2020-06-29' AS Date), NULL, 197)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (198, CAST(N'2020-06-30' AS Date), NULL, 198)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (199, CAST(N'2020-07-02' AS Date), NULL, 199)
GO
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (200, CAST(N'2020-07-03' AS Date), NULL, 200)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (201, CAST(N'2020-07-04' AS Date), NULL, 201)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (202, CAST(N'2020-07-05' AS Date), NULL, 202)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (203, CAST(N'2020-07-06' AS Date), NULL, 203)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (204, CAST(N'2020-07-07' AS Date), NULL, 204)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (205, CAST(N'2020-07-08' AS Date), NULL, 205)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (206, CAST(N'2020-07-09' AS Date), NULL, 206)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (207, CAST(N'2020-07-10' AS Date), NULL, 207)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (208, CAST(N'2020-07-11' AS Date), NULL, 208)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (209, CAST(N'2020-07-12' AS Date), NULL, 209)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (210, CAST(N'2020-07-13' AS Date), NULL, 210)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (211, CAST(N'2020-07-14' AS Date), NULL, 211)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (212, CAST(N'2020-07-15' AS Date), NULL, 212)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (213, CAST(N'2020-07-16' AS Date), NULL, 213)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (214, CAST(N'2020-07-17' AS Date), NULL, 214)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (215, CAST(N'2020-07-18' AS Date), NULL, 215)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (216, CAST(N'2020-07-19' AS Date), NULL, 216)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (217, CAST(N'2020-07-20' AS Date), NULL, 217)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (218, CAST(N'2020-07-21' AS Date), NULL, 218)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (219, CAST(N'2020-07-22' AS Date), NULL, 219)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (220, CAST(N'2020-07-23' AS Date), NULL, 220)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (221, CAST(N'2020-07-24' AS Date), NULL, 221)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (222, CAST(N'2020-07-25' AS Date), NULL, 222)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (223, CAST(N'2020-07-26' AS Date), NULL, 223)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (224, CAST(N'2020-07-27' AS Date), NULL, 224)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (225, CAST(N'2020-07-28' AS Date), NULL, 225)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (226, CAST(N'2020-07-29' AS Date), NULL, 226)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (227, CAST(N'2020-07-30' AS Date), NULL, 227)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (228, CAST(N'2020-07-01' AS Date), NULL, 228)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (229, CAST(N'2020-07-30' AS Date), NULL, 229)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (230, CAST(N'2020-08-01' AS Date), NULL, 230)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (231, CAST(N'2020-08-02' AS Date), NULL, 231)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (232, CAST(N'2020-08-03' AS Date), NULL, 232)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (233, CAST(N'2020-08-04' AS Date), NULL, 233)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (234, CAST(N'2020-08-05' AS Date), NULL, 234)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (235, CAST(N'2020-08-06' AS Date), NULL, 235)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (236, CAST(N'2020-08-07' AS Date), NULL, 236)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (237, CAST(N'2020-08-08' AS Date), NULL, 237)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (238, CAST(N'2020-08-09' AS Date), NULL, 238)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (239, CAST(N'2020-08-10' AS Date), NULL, 239)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (240, CAST(N'2020-08-11' AS Date), NULL, 240)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (241, CAST(N'2020-08-12' AS Date), NULL, 241)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (242, CAST(N'2020-08-13' AS Date), NULL, 242)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (243, CAST(N'2020-08-14' AS Date), NULL, 243)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (244, CAST(N'2020-08-15' AS Date), NULL, 244)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (245, CAST(N'2020-08-16' AS Date), NULL, 245)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (246, CAST(N'2020-08-17' AS Date), NULL, 246)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (247, CAST(N'2020-08-18' AS Date), NULL, 247)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (248, CAST(N'2020-08-19' AS Date), NULL, 248)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (249, CAST(N'2020-08-20' AS Date), NULL, 249)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (250, CAST(N'2020-08-21' AS Date), NULL, 250)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (251, CAST(N'2020-08-22' AS Date), NULL, 251)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (252, CAST(N'2020-08-23' AS Date), NULL, 252)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (253, CAST(N'2020-08-24' AS Date), NULL, 253)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (254, CAST(N'2020-08-25' AS Date), NULL, 254)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (255, CAST(N'2020-08-26' AS Date), NULL, 255)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (256, CAST(N'2020-08-27' AS Date), NULL, 256)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (257, CAST(N'2020-08-28' AS Date), NULL, 257)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (258, CAST(N'2020-08-29' AS Date), NULL, 258)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (259, CAST(N'2020-08-30' AS Date), NULL, 259)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (260, CAST(N'2020-08-31' AS Date), NULL, 260)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (261, CAST(N'2020-09-01' AS Date), NULL, 261)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (262, CAST(N'2020-09-02' AS Date), NULL, 262)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (263, CAST(N'2020-09-03' AS Date), NULL, 263)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (264, CAST(N'2020-09-04' AS Date), NULL, 264)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (265, CAST(N'2020-09-05' AS Date), NULL, 265)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (266, CAST(N'2020-09-06' AS Date), NULL, 266)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (267, CAST(N'2020-09-07' AS Date), NULL, 267)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (268, CAST(N'2020-09-08' AS Date), NULL, 268)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (269, CAST(N'2020-09-09' AS Date), NULL, 269)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (270, CAST(N'2020-09-10' AS Date), NULL, 270)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (271, CAST(N'2020-09-11' AS Date), NULL, 271)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (272, CAST(N'2020-09-12' AS Date), NULL, 272)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (273, CAST(N'2020-09-13' AS Date), NULL, 273)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (274, CAST(N'2020-09-14' AS Date), NULL, 274)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (275, CAST(N'2020-09-15' AS Date), NULL, 275)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (276, CAST(N'2020-09-16' AS Date), NULL, 276)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (277, CAST(N'2020-09-17' AS Date), NULL, 277)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (278, CAST(N'2020-09-18' AS Date), NULL, 278)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (279, CAST(N'2020-09-19' AS Date), NULL, 279)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (280, CAST(N'2020-09-20' AS Date), NULL, 280)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (281, CAST(N'2020-09-21' AS Date), NULL, 281)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (282, CAST(N'2020-09-22' AS Date), NULL, 282)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (283, CAST(N'2020-09-23' AS Date), NULL, 283)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (284, CAST(N'2020-09-24' AS Date), NULL, 284)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (285, CAST(N'2020-09-25' AS Date), NULL, 285)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (286, CAST(N'2020-09-26' AS Date), NULL, 286)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (287, CAST(N'2020-09-27' AS Date), NULL, 287)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (288, CAST(N'2020-09-28' AS Date), NULL, 288)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (289, CAST(N'2020-09-29' AS Date), NULL, 289)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (290, CAST(N'2020-09-30' AS Date), NULL, 290)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (291, CAST(N'2020-10-01' AS Date), NULL, 291)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (292, CAST(N'2020-10-02' AS Date), NULL, 292)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (293, CAST(N'2020-10-03' AS Date), NULL, 293)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (294, CAST(N'2020-10-04' AS Date), NULL, 294)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (295, CAST(N'2020-10-05' AS Date), NULL, 295)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (296, CAST(N'2020-10-06' AS Date), NULL, 296)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (297, CAST(N'2020-10-07' AS Date), NULL, 297)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (298, CAST(N'2020-10-08' AS Date), NULL, 298)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (299, CAST(N'2020-10-09' AS Date), NULL, 299)
GO
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (300, CAST(N'2020-10-10' AS Date), NULL, 300)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (301, CAST(N'2020-10-11' AS Date), NULL, 301)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (302, CAST(N'2020-10-12' AS Date), NULL, 302)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (303, CAST(N'2020-10-13' AS Date), NULL, 303)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (304, CAST(N'2020-10-14' AS Date), NULL, 304)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (305, CAST(N'2020-10-15' AS Date), NULL, 305)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (306, CAST(N'2020-10-16' AS Date), NULL, 306)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (307, CAST(N'2020-10-17' AS Date), NULL, 307)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (308, CAST(N'2020-10-18' AS Date), NULL, 308)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (309, CAST(N'2020-10-19' AS Date), NULL, 309)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (310, CAST(N'2020-10-20' AS Date), NULL, 310)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (311, CAST(N'2020-10-21' AS Date), NULL, 311)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (312, CAST(N'2020-10-22' AS Date), NULL, 312)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (313, CAST(N'2020-10-23' AS Date), NULL, 313)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (314, CAST(N'2020-10-24' AS Date), NULL, 314)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (315, CAST(N'2020-10-25' AS Date), NULL, 315)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (316, CAST(N'2020-10-26' AS Date), NULL, 316)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (317, CAST(N'2020-10-27' AS Date), NULL, 317)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (318, CAST(N'2020-10-28' AS Date), NULL, 318)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (319, CAST(N'2020-10-29' AS Date), NULL, 319)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (320, CAST(N'2020-10-30' AS Date), NULL, 320)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (321, CAST(N'2020-11-01' AS Date), NULL, 321)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (322, CAST(N'2020-11-02' AS Date), NULL, 322)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (323, CAST(N'2020-11-03' AS Date), NULL, 323)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (324, CAST(N'2020-11-04' AS Date), NULL, 324)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (325, CAST(N'2020-11-05' AS Date), NULL, 325)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (326, CAST(N'2020-11-06' AS Date), NULL, 326)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (327, CAST(N'2020-11-07' AS Date), NULL, 327)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (328, CAST(N'2020-11-08' AS Date), NULL, 328)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (329, CAST(N'2020-11-09' AS Date), NULL, 329)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (330, CAST(N'2020-11-10' AS Date), NULL, 330)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (331, CAST(N'2020-11-11' AS Date), NULL, 331)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (332, CAST(N'2020-11-12' AS Date), NULL, 332)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (333, CAST(N'2020-11-13' AS Date), NULL, 333)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (334, CAST(N'2020-11-14' AS Date), NULL, 334)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (335, CAST(N'2020-11-15' AS Date), NULL, 335)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (336, CAST(N'2020-11-16' AS Date), NULL, 336)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (337, CAST(N'2020-11-17' AS Date), NULL, 337)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (338, CAST(N'2020-11-18' AS Date), NULL, 338)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (339, CAST(N'2020-11-19' AS Date), NULL, 339)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (340, CAST(N'2020-11-20' AS Date), NULL, 340)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (341, CAST(N'2020-11-21' AS Date), NULL, 341)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (342, CAST(N'2020-11-22' AS Date), NULL, 342)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (343, CAST(N'2020-11-23' AS Date), NULL, 343)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (344, CAST(N'2020-11-24' AS Date), NULL, 344)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (345, CAST(N'2020-11-25' AS Date), NULL, 345)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (346, CAST(N'2020-11-26' AS Date), NULL, 346)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (347, CAST(N'2020-11-27' AS Date), NULL, 347)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (348, CAST(N'2020-11-28' AS Date), NULL, 348)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (349, CAST(N'2020-11-29' AS Date), NULL, 349)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (350, CAST(N'2020-11-30' AS Date), NULL, 350)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (351, CAST(N'2020-12-01' AS Date), NULL, 351)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (352, CAST(N'2020-12-02' AS Date), NULL, 352)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (353, CAST(N'2020-12-03' AS Date), NULL, 353)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (354, CAST(N'2020-12-04' AS Date), NULL, 354)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (355, CAST(N'2020-12-05' AS Date), NULL, 355)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (356, CAST(N'2020-12-06' AS Date), NULL, 356)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (357, CAST(N'2020-12-07' AS Date), NULL, 357)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (358, CAST(N'2020-12-08' AS Date), NULL, 358)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (359, CAST(N'2020-12-09' AS Date), NULL, 359)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (360, CAST(N'2020-12-10' AS Date), NULL, 360)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (361, CAST(N'2020-12-11' AS Date), NULL, 361)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (362, CAST(N'2020-12-12' AS Date), NULL, 362)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (363, CAST(N'2020-12-13' AS Date), NULL, 363)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (364, CAST(N'2020-12-14' AS Date), NULL, 364)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (365, CAST(N'2020-12-15' AS Date), NULL, 365)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (366, CAST(N'2020-12-16' AS Date), NULL, 366)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (367, CAST(N'2020-12-17' AS Date), NULL, 367)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (368, CAST(N'2020-12-18' AS Date), NULL, 368)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (369, CAST(N'2020-12-19' AS Date), NULL, 369)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (370, CAST(N'2020-12-20' AS Date), NULL, 370)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (371, CAST(N'2020-12-21' AS Date), NULL, 371)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (372, CAST(N'2020-12-22' AS Date), NULL, 372)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (373, CAST(N'2020-12-23' AS Date), NULL, 373)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (374, CAST(N'2020-12-24' AS Date), NULL, 374)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (375, CAST(N'2020-12-25' AS Date), NULL, 375)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (376, CAST(N'2020-12-26' AS Date), NULL, 376)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (377, CAST(N'2020-12-27' AS Date), NULL, 377)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (378, CAST(N'2020-12-28' AS Date), NULL, 378)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (379, CAST(N'2020-12-29' AS Date), NULL, 379)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (380, CAST(N'2020-12-30' AS Date), NULL, 380)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (381, CAST(N'2021-01-01' AS Date), NULL, 381)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (382, CAST(N'2021-01-02' AS Date), NULL, 382)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (383, CAST(N'2021-01-03' AS Date), NULL, 383)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (384, CAST(N'2021-01-04' AS Date), NULL, 384)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (385, CAST(N'2021-01-05' AS Date), NULL, 385)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (386, CAST(N'2021-01-06' AS Date), NULL, 386)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (387, CAST(N'2021-01-07' AS Date), NULL, 387)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (388, CAST(N'2021-01-08' AS Date), NULL, 388)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (389, CAST(N'2021-01-09' AS Date), NULL, 389)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (390, CAST(N'2021-01-10' AS Date), NULL, 390)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (391, CAST(N'2021-01-11' AS Date), NULL, 391)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (392, CAST(N'2021-01-12' AS Date), NULL, 392)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (393, CAST(N'2021-01-13' AS Date), NULL, 393)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (394, CAST(N'2021-01-14' AS Date), NULL, 394)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (395, CAST(N'2021-01-15' AS Date), NULL, 395)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (396, CAST(N'2021-01-16' AS Date), NULL, 396)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (397, CAST(N'2021-01-17' AS Date), NULL, 397)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (398, CAST(N'2021-01-18' AS Date), NULL, 398)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (399, CAST(N'2021-01-19' AS Date), NULL, 399)
GO
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (400, CAST(N'2021-01-20' AS Date), NULL, 400)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (401, CAST(N'2021-01-21' AS Date), NULL, 401)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (402, CAST(N'2021-01-22' AS Date), NULL, 402)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (403, CAST(N'2021-01-23' AS Date), NULL, 403)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (404, CAST(N'2021-01-24' AS Date), NULL, 404)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (405, CAST(N'2021-01-25' AS Date), NULL, 405)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (406, CAST(N'2021-01-26' AS Date), NULL, 406)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (407, CAST(N'2021-01-27' AS Date), NULL, 407)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (408, CAST(N'2021-01-28' AS Date), NULL, 408)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (409, CAST(N'2021-01-29' AS Date), NULL, 409)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (410, CAST(N'2021-01-30' AS Date), NULL, 410)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (411, CAST(N'2021-02-01' AS Date), NULL, 411)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (412, CAST(N'2021-02-02' AS Date), NULL, 412)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (413, CAST(N'2021-02-03' AS Date), NULL, 413)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (414, CAST(N'2021-02-04' AS Date), NULL, 414)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (415, CAST(N'2021-02-05' AS Date), NULL, 415)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (416, CAST(N'2021-02-06' AS Date), NULL, 416)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (417, CAST(N'2021-02-07' AS Date), NULL, 417)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (418, CAST(N'2021-02-08' AS Date), NULL, 418)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (419, CAST(N'2021-02-09' AS Date), NULL, 419)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (420, CAST(N'2021-02-10' AS Date), NULL, 420)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (421, CAST(N'2021-02-11' AS Date), NULL, 421)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (422, CAST(N'2021-02-12' AS Date), NULL, 422)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (423, CAST(N'2021-02-13' AS Date), NULL, 423)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (424, CAST(N'2021-02-14' AS Date), NULL, 424)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (425, CAST(N'2021-02-15' AS Date), NULL, 425)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (426, CAST(N'2021-02-16' AS Date), NULL, 426)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (427, CAST(N'2021-02-17' AS Date), NULL, 427)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (428, CAST(N'2021-02-18' AS Date), NULL, 428)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (429, CAST(N'2021-02-19' AS Date), NULL, 429)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (430, CAST(N'2021-02-20' AS Date), NULL, 430)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (431, CAST(N'2021-02-21' AS Date), NULL, 431)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (432, CAST(N'2021-02-22' AS Date), NULL, 432)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (433, CAST(N'2021-02-23' AS Date), NULL, 433)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (434, CAST(N'2021-02-24' AS Date), NULL, 434)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (435, CAST(N'2021-02-25' AS Date), NULL, 435)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (436, CAST(N'2021-02-26' AS Date), NULL, 436)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (437, CAST(N'2021-02-27' AS Date), NULL, 437)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (438, CAST(N'2021-03-01' AS Date), NULL, 438)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (439, CAST(N'2021-03-02' AS Date), NULL, 439)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (440, CAST(N'2021-03-03' AS Date), NULL, 440)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (441, CAST(N'2021-03-04' AS Date), NULL, 441)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (442, CAST(N'2021-03-05' AS Date), NULL, 442)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (443, CAST(N'2021-03-06' AS Date), NULL, 443)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (444, CAST(N'2021-03-07' AS Date), NULL, 444)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (445, CAST(N'2021-03-08' AS Date), NULL, 445)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (446, CAST(N'2021-03-09' AS Date), NULL, 446)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (447, CAST(N'2021-03-10' AS Date), NULL, 447)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (448, CAST(N'2021-03-11' AS Date), NULL, 448)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (449, CAST(N'2021-03-12' AS Date), NULL, 449)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (450, CAST(N'2021-03-13' AS Date), NULL, 450)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (451, CAST(N'2021-03-14' AS Date), NULL, 451)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (452, CAST(N'2021-03-15' AS Date), NULL, 452)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (453, CAST(N'2021-03-16' AS Date), NULL, 453)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (454, CAST(N'2021-03-17' AS Date), NULL, 454)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (455, CAST(N'2021-03-18' AS Date), NULL, 455)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (456, CAST(N'2021-03-19' AS Date), NULL, 456)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (457, CAST(N'2021-03-20' AS Date), NULL, 457)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (458, CAST(N'2021-03-21' AS Date), NULL, 458)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (459, CAST(N'2021-03-22' AS Date), NULL, 459)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (460, CAST(N'2021-03-23' AS Date), NULL, 460)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (461, CAST(N'2021-03-24' AS Date), NULL, 461)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (462, CAST(N'2021-03-25' AS Date), NULL, 462)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (463, CAST(N'2021-03-26' AS Date), NULL, 463)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (464, CAST(N'2021-03-27' AS Date), NULL, 464)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (465, CAST(N'2021-03-28' AS Date), NULL, 465)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (466, CAST(N'2021-03-29' AS Date), NULL, 466)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (467, CAST(N'2021-03-30' AS Date), NULL, 467)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (468, CAST(N'2021-03-31' AS Date), NULL, 468)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (469, CAST(N'2021-04-01' AS Date), NULL, 469)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (470, CAST(N'2021-04-02' AS Date), NULL, 470)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (471, CAST(N'2021-04-03' AS Date), NULL, 471)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (472, CAST(N'2021-04-04' AS Date), NULL, 472)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (473, CAST(N'2021-04-05' AS Date), NULL, 473)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (474, CAST(N'2021-04-06' AS Date), NULL, 474)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (475, CAST(N'2021-04-07' AS Date), NULL, 475)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (476, CAST(N'2021-04-08' AS Date), NULL, 476)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (477, CAST(N'2021-04-09' AS Date), NULL, 477)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (478, CAST(N'2021-04-10' AS Date), NULL, 478)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (479, CAST(N'2021-04-11' AS Date), NULL, 479)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (480, CAST(N'2021-04-12' AS Date), NULL, 480)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (481, CAST(N'2021-04-13' AS Date), NULL, 481)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (482, CAST(N'2021-04-14' AS Date), NULL, 482)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (483, CAST(N'2021-04-15' AS Date), NULL, 483)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (484, CAST(N'2021-04-16' AS Date), NULL, 484)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (485, CAST(N'2021-04-17' AS Date), NULL, 485)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (486, CAST(N'2021-04-18' AS Date), NULL, 486)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (487, CAST(N'2021-04-19' AS Date), NULL, 487)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (488, CAST(N'2021-04-20' AS Date), NULL, 488)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (489, CAST(N'2021-04-21' AS Date), NULL, 489)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (490, CAST(N'2021-04-22' AS Date), NULL, 490)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (491, CAST(N'2021-04-23' AS Date), NULL, 491)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (492, CAST(N'2021-04-24' AS Date), NULL, 492)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (493, CAST(N'2021-04-25' AS Date), NULL, 493)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (494, CAST(N'2021-04-26' AS Date), NULL, 494)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (495, CAST(N'2021-04-27' AS Date), NULL, 495)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (496, CAST(N'2021-04-28' AS Date), NULL, 496)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (497, CAST(N'2021-04-29' AS Date), NULL, 497)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (498, CAST(N'2021-04-30' AS Date), NULL, 498)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (499, CAST(N'2021-05-01' AS Date), NULL, 499)
GO
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (500, CAST(N'2021-05-01' AS Date), NULL, 500)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (501, CAST(N'2021-05-01' AS Date), NULL, 501)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (502, CAST(N'2021-05-01' AS Date), NULL, 502)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (503, CAST(N'2021-05-01' AS Date), NULL, 503)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (504, CAST(N'2021-05-01' AS Date), NULL, 504)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (505, CAST(N'2021-05-01' AS Date), NULL, 505)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (506, CAST(N'2021-05-01' AS Date), NULL, 506)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (507, CAST(N'2021-05-01' AS Date), NULL, 507)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (508, CAST(N'2021-05-02' AS Date), NULL, 508)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (509, CAST(N'2021-05-02' AS Date), NULL, 509)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (510, CAST(N'2021-05-02' AS Date), NULL, 510)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (511, CAST(N'2021-05-02' AS Date), NULL, 511)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (512, CAST(N'2021-05-02' AS Date), NULL, 512)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (513, CAST(N'2021-05-02' AS Date), NULL, 513)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (514, CAST(N'2021-05-02' AS Date), NULL, 514)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (515, CAST(N'2021-05-02' AS Date), NULL, 515)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (516, CAST(N'2021-05-02' AS Date), NULL, 516)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (517, CAST(N'2021-05-02' AS Date), NULL, 517)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (518, CAST(N'2021-05-02' AS Date), NULL, 518)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (519, CAST(N'2021-05-03' AS Date), NULL, 519)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (520, CAST(N'2021-05-03' AS Date), NULL, 520)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (521, CAST(N'2021-05-03' AS Date), NULL, 521)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (522, CAST(N'2021-05-03' AS Date), NULL, 522)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (523, CAST(N'2021-05-03' AS Date), NULL, 523)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (524, CAST(N'2021-05-03' AS Date), NULL, 524)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (525, CAST(N'2021-05-03' AS Date), NULL, 525)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (526, CAST(N'2021-05-03' AS Date), NULL, 526)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (527, CAST(N'2021-05-03' AS Date), NULL, 527)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (528, CAST(N'2021-05-03' AS Date), NULL, 528)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (529, CAST(N'2021-05-04' AS Date), NULL, 529)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (530, CAST(N'2021-05-04' AS Date), NULL, 530)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (531, CAST(N'2021-05-04' AS Date), NULL, 531)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (532, CAST(N'2021-05-04' AS Date), NULL, 532)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (533, CAST(N'2021-05-03' AS Date), 38000, 533)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (534, CAST(N'2021-05-03' AS Date), 43000, 534)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (535, CAST(N'2021-05-03' AS Date), 43000, 535)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (536, CAST(N'2021-05-03' AS Date), 45000, 536)
INSERT [dbo].[Bills] ([id], [date_create], [total], [id_order]) VALUES (537, CAST(N'2021-05-03' AS Date), 46000, 537)
SET IDENTITY_INSERT [dbo].[Bills] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (1, N'Cafe', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (2, N'Trà', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (3, N'Yaourt', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (4, N'Sữa', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (5, N'Sinh tố', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (6, N'Nước ép', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (7, N'Thức uống khác', 1)
INSERT [dbo].[Categories] ([id], [type], [status]) VALUES (8, N'Thức uống Trái cây', 1)
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[Feedbacks] ON 

INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (1, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 1, 9)
INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (2, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 3, 1)
INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (3, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 2, 3)
INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (4, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 4, 4)
INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (5, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 1, 4)
INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (6, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 1, 1)
INSERT [dbo].[Feedbacks] ([id], [note], [status], [rate], [date_time], [id_cus], [id_product]) VALUES (7, N'coffee nay ...', 1, 4, CAST(N'2021-05-02' AS Date), 1, 12)
SET IDENTITY_INSERT [dbo].[Feedbacks] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (1, CAST(N'2020-01-04' AS Date), 1, 24, 1)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (2, CAST(N'2020-02-04' AS Date), 1, 2, 2)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (3, CAST(N'2020-03-04' AS Date), 1, 3, 3)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (4, CAST(N'2020-04-04' AS Date), 1, 4, 4)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (5, CAST(N'2020-05-04' AS Date), 1, 5, 5)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (6, CAST(N'2020-06-04' AS Date), 1, 6, 6)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (7, CAST(N'2020-07-04' AS Date), 1, 7, 1)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (8, CAST(N'2020-08-04' AS Date), 1, 8, 2)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (9, CAST(N'2020-09-04' AS Date), 1, 9, 3)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (10, CAST(N'2020-10-04' AS Date), 1, 10, 4)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (11, CAST(N'2020-11-04' AS Date), 1, 13, 5)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (12, CAST(N'2020-12-04' AS Date), 1, 11, 6)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (13, CAST(N'2021-01-04' AS Date), 1, 12, 1)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (14, CAST(N'2021-02-04' AS Date), 1, 13, 2)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (15, CAST(N'2021-03-04' AS Date), 1, 14, 3)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (16, CAST(N'2021-04-04' AS Date), 1, 15, 4)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (17, CAST(N'2021-04-05' AS Date), 1, 16, 5)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (18, CAST(N'2020-01-01' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (19, CAST(N'2020-01-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (20, CAST(N'2020-01-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (21, CAST(N'2020-01-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (22, CAST(N'2020-01-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (23, CAST(N'2020-01-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (24, CAST(N'2020-01-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (25, CAST(N'2020-01-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (26, CAST(N'2020-01-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (27, CAST(N'2020-01-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (28, CAST(N'2020-01-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (29, CAST(N'2020-01-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (30, CAST(N'2021-01-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (31, CAST(N'2021-01-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (32, CAST(N'2021-01-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (33, CAST(N'2021-01-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (34, CAST(N'2021-01-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (35, CAST(N'2020-01-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (36, CAST(N'2020-01-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (37, CAST(N'2020-01-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (38, CAST(N'2020-01-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (39, CAST(N'2020-01-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (40, CAST(N'2020-01-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (41, CAST(N'2020-01-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (42, CAST(N'2020-01-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (43, CAST(N'2020-01-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (44, CAST(N'2020-01-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (45, CAST(N'2020-01-28' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (46, CAST(N'2020-01-29' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (47, CAST(N'2021-01-30' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (48, CAST(N'2021-01-31' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (49, CAST(N'2020-02-01' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (50, CAST(N'2020-02-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (51, CAST(N'2020-02-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (52, CAST(N'2020-02-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (53, CAST(N'2020-02-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (54, CAST(N'2020-02-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (55, CAST(N'2020-02-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (56, CAST(N'2020-02-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (57, CAST(N'2020-02-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (58, CAST(N'2020-02-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (59, CAST(N'2020-02-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (60, CAST(N'2020-02-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (61, CAST(N'2020-02-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (62, CAST(N'2020-02-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (63, CAST(N'2020-02-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (64, CAST(N'2020-02-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (65, CAST(N'2020-02-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (66, CAST(N'2020-02-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (67, CAST(N'2020-02-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (68, CAST(N'2020-02-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (69, CAST(N'2020-02-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (70, CAST(N'2020-02-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (71, CAST(N'2020-02-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (72, CAST(N'2020-02-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (73, CAST(N'2020-02-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (74, CAST(N'2020-02-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (75, CAST(N'2020-02-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (76, CAST(N'2020-02-28' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (77, CAST(N'2020-03-01' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (78, CAST(N'2020-03-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (79, CAST(N'2020-03-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (80, CAST(N'2020-03-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (81, CAST(N'2020-03-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (82, CAST(N'2020-03-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (83, CAST(N'2020-03-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (84, CAST(N'2020-03-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (85, CAST(N'2020-03-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (86, CAST(N'2020-03-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (87, CAST(N'2020-03-11' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (88, CAST(N'2020-03-12' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (89, CAST(N'2020-03-13' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (90, CAST(N'2020-03-14' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (91, CAST(N'2020-03-15' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (92, CAST(N'2020-03-16' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (93, CAST(N'2020-03-17' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (94, CAST(N'2020-03-18' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (95, CAST(N'2020-03-19' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (96, CAST(N'2020-03-20' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (97, CAST(N'2020-03-21' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (98, CAST(N'2020-03-22' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (99, CAST(N'2020-03-23' AS Date), 1, 7, NULL)
GO
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (100, CAST(N'2020-03-24' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (101, CAST(N'2020-03-25' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (102, CAST(N'2020-03-26' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (103, CAST(N'2020-03-27' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (104, CAST(N'2020-03-28' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (105, CAST(N'2020-03-29' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (106, CAST(N'2020-03-30' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (107, CAST(N'2020-03-31' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (108, CAST(N'2020-04-01' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (109, CAST(N'2020-04-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (110, CAST(N'2020-04-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (111, CAST(N'2020-04-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (112, CAST(N'2020-04-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (113, CAST(N'2020-04-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (114, CAST(N'2020-04-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (115, CAST(N'2020-04-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (116, CAST(N'2020-04-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (117, CAST(N'2020-04-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (118, CAST(N'2020-04-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (119, CAST(N'2020-04-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (120, CAST(N'2020-04-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (121, CAST(N'2020-04-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (122, CAST(N'2020-04-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (123, CAST(N'2020-04-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (124, CAST(N'2020-04-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (125, CAST(N'2020-04-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (126, CAST(N'2020-04-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (127, CAST(N'2020-04-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (128, CAST(N'2020-04-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (129, CAST(N'2020-04-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (130, CAST(N'2020-04-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (131, CAST(N'2020-04-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (132, CAST(N'2020-04-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (133, CAST(N'2020-04-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (134, CAST(N'2020-04-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (135, CAST(N'2020-04-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (136, CAST(N'2020-04-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (137, CAST(N'2020-04-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (138, CAST(N'2020-05-01' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (139, CAST(N'2020-05-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (140, CAST(N'2020-05-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (141, CAST(N'2020-05-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (142, CAST(N'2020-05-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (143, CAST(N'2020-05-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (144, CAST(N'2020-05-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (145, CAST(N'2020-05-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (146, CAST(N'2020-05-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (147, CAST(N'2020-05-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (148, CAST(N'2020-05-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (149, CAST(N'2020-05-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (150, CAST(N'2020-05-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (151, CAST(N'2020-05-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (152, CAST(N'2020-05-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (153, CAST(N'2020-05-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (154, CAST(N'2020-05-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (155, CAST(N'2020-05-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (156, CAST(N'2020-05-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (157, CAST(N'2020-05-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (158, CAST(N'2020-05-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (159, CAST(N'2020-05-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (160, CAST(N'2020-05-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (161, CAST(N'2020-05-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (162, CAST(N'2020-05-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (163, CAST(N'2020-05-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (164, CAST(N'2020-05-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (165, CAST(N'2020-05-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (166, CAST(N'2020-05-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (167, CAST(N'2020-05-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (168, CAST(N'2020-05-31' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (169, CAST(N'2020-06-01' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (170, CAST(N'2020-06-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (171, CAST(N'2020-06-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (172, CAST(N'2020-06-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (173, CAST(N'2020-06-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (174, CAST(N'2020-06-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (175, CAST(N'2020-06-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (176, CAST(N'2020-06-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (177, CAST(N'2020-06-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (178, CAST(N'2020-06-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (179, CAST(N'2020-06-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (180, CAST(N'2020-06-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (181, CAST(N'2020-06-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (182, CAST(N'2020-06-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (183, CAST(N'2020-06-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (184, CAST(N'2020-06-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (185, CAST(N'2020-06-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (186, CAST(N'2020-06-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (187, CAST(N'2020-06-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (188, CAST(N'2020-06-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (189, CAST(N'2020-06-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (190, CAST(N'2020-06-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (191, CAST(N'2020-06-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (192, CAST(N'2020-06-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (193, CAST(N'2020-06-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (194, CAST(N'2020-06-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (195, CAST(N'2020-06-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (196, CAST(N'2020-06-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (197, CAST(N'2020-06-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (198, CAST(N'2020-06-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (199, CAST(N'2020-07-01' AS Date), 1, 17, NULL)
GO
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (200, CAST(N'2020-07-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (201, CAST(N'2020-07-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (202, CAST(N'2020-07-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (203, CAST(N'2020-07-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (204, CAST(N'2020-07-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (205, CAST(N'2020-07-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (206, CAST(N'2020-07-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (207, CAST(N'2020-07-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (208, CAST(N'2020-07-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (209, CAST(N'2020-07-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (210, CAST(N'2020-07-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (211, CAST(N'2020-07-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (212, CAST(N'2020-07-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (213, CAST(N'2020-07-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (214, CAST(N'2020-07-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (215, CAST(N'2020-07-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (216, CAST(N'2020-07-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (217, CAST(N'2020-07-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (218, CAST(N'2020-07-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (219, CAST(N'2020-07-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (220, CAST(N'2020-07-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (221, CAST(N'2020-07-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (222, CAST(N'2020-07-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (223, CAST(N'2020-07-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (224, CAST(N'2020-07-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (225, CAST(N'2020-07-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (226, CAST(N'2020-07-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (227, CAST(N'2020-07-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (228, CAST(N'2020-07-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (229, CAST(N'2020-07-31' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (230, CAST(N'2020-08-01' AS Date), 1, 18, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (231, CAST(N'2020-08-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (232, CAST(N'2020-08-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (233, CAST(N'2020-08-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (234, CAST(N'2020-08-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (235, CAST(N'2020-08-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (236, CAST(N'2020-08-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (237, CAST(N'2020-08-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (238, CAST(N'2020-08-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (239, CAST(N'2020-08-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (240, CAST(N'2020-08-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (241, CAST(N'2020-08-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (242, CAST(N'2020-08-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (243, CAST(N'2020-08-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (244, CAST(N'2020-08-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (245, CAST(N'2020-08-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (246, CAST(N'2020-08-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (247, CAST(N'2020-08-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (248, CAST(N'2020-08-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (249, CAST(N'2020-08-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (250, CAST(N'2020-08-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (251, CAST(N'2020-08-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (252, CAST(N'2020-08-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (253, CAST(N'2020-08-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (254, CAST(N'2020-08-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (255, CAST(N'2020-08-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (256, CAST(N'2020-08-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (257, CAST(N'2020-08-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (258, CAST(N'2020-08-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (259, CAST(N'2020-08-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (260, CAST(N'2020-08-31' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (261, CAST(N'2020-09-01' AS Date), 1, 19, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (262, CAST(N'2020-09-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (263, CAST(N'2020-09-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (264, CAST(N'2020-09-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (265, CAST(N'2020-09-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (266, CAST(N'2020-09-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (267, CAST(N'2020-09-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (268, CAST(N'2020-09-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (269, CAST(N'2020-09-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (270, CAST(N'2020-09-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (271, CAST(N'2020-09-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (272, CAST(N'2020-09-12' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (273, CAST(N'2020-09-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (274, CAST(N'2020-09-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (275, CAST(N'2020-09-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (276, CAST(N'2020-09-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (277, CAST(N'2020-09-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (278, CAST(N'2020-09-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (279, CAST(N'2020-09-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (280, CAST(N'2020-09-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (281, CAST(N'2020-09-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (282, CAST(N'2020-09-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (283, CAST(N'2020-09-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (284, CAST(N'2020-09-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (285, CAST(N'2020-09-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (286, CAST(N'2020-09-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (287, CAST(N'2020-09-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (288, CAST(N'2020-09-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (289, CAST(N'2020-09-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (290, CAST(N'2020-09-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (291, CAST(N'2020-10-01' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (292, CAST(N'2020-10-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (293, CAST(N'2020-10-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (294, CAST(N'2020-10-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (295, CAST(N'2020-10-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (296, CAST(N'2020-10-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (297, CAST(N'2020-10-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (298, CAST(N'2020-10-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (299, CAST(N'2020-10-09' AS Date), 1, 9, NULL)
GO
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (300, CAST(N'2020-10-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (301, CAST(N'2020-10-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (302, CAST(N'2020-10-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (303, CAST(N'2020-10-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (304, CAST(N'2020-10-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (305, CAST(N'2020-10-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (306, CAST(N'2020-10-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (307, CAST(N'2020-10-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (308, CAST(N'2020-10-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (309, CAST(N'2020-10-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (310, CAST(N'2020-10-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (311, CAST(N'2020-10-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (312, CAST(N'2020-10-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (313, CAST(N'2020-10-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (314, CAST(N'2020-10-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (315, CAST(N'2020-10-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (316, CAST(N'2020-10-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (317, CAST(N'2020-10-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (318, CAST(N'2020-10-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (319, CAST(N'2020-10-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (320, CAST(N'2020-10-31' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (321, CAST(N'2020-11-01' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (322, CAST(N'2020-11-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (323, CAST(N'2020-11-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (324, CAST(N'2020-11-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (325, CAST(N'2020-11-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (326, CAST(N'2020-11-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (327, CAST(N'2020-11-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (328, CAST(N'2020-11-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (329, CAST(N'2020-11-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (330, CAST(N'2020-11-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (331, CAST(N'2020-11-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (332, CAST(N'2020-11-12' AS Date), 1, 19, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (333, CAST(N'2020-11-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (334, CAST(N'2020-11-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (335, CAST(N'2020-11-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (336, CAST(N'2020-11-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (337, CAST(N'2020-11-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (338, CAST(N'2020-11-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (339, CAST(N'2020-11-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (340, CAST(N'2020-11-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (341, CAST(N'2020-11-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (342, CAST(N'2020-11-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (343, CAST(N'2020-11-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (344, CAST(N'2020-11-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (345, CAST(N'2020-11-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (346, CAST(N'2020-11-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (347, CAST(N'2020-11-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (348, CAST(N'2020-11-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (349, CAST(N'2020-11-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (350, CAST(N'2020-11-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (351, CAST(N'2020-12-01' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (352, CAST(N'2020-12-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (353, CAST(N'2020-12-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (354, CAST(N'2020-12-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (355, CAST(N'2020-12-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (356, CAST(N'2020-12-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (357, CAST(N'2020-12-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (358, CAST(N'2020-12-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (359, CAST(N'2020-12-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (360, CAST(N'2020-12-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (361, CAST(N'2020-12-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (362, CAST(N'2020-12-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (363, CAST(N'2020-12-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (364, CAST(N'2020-12-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (365, CAST(N'2020-12-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (366, CAST(N'2020-12-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (367, CAST(N'2020-12-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (368, CAST(N'2020-12-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (369, CAST(N'2020-12-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (370, CAST(N'2020-12-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (371, CAST(N'2020-12-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (372, CAST(N'2020-12-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (373, CAST(N'2020-12-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (374, CAST(N'2020-12-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (375, CAST(N'2020-12-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (376, CAST(N'2020-12-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (377, CAST(N'2020-12-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (378, CAST(N'2020-12-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (379, CAST(N'2020-12-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (380, CAST(N'2020-12-31' AS Date), 1, 17, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (381, CAST(N'2021-01-01' AS Date), 1, 21, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (382, CAST(N'2021-01-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (383, CAST(N'2021-01-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (384, CAST(N'2021-01-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (385, CAST(N'2021-01-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (386, CAST(N'2021-01-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (387, CAST(N'2021-01-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (388, CAST(N'2021-01-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (389, CAST(N'2021-01-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (390, CAST(N'2021-01-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (391, CAST(N'2021-01-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (392, CAST(N'2021-01-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (393, CAST(N'2021-01-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (394, CAST(N'2021-01-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (395, CAST(N'2021-01-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (396, CAST(N'2021-01-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (397, CAST(N'2021-01-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (398, CAST(N'2021-01-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (399, CAST(N'2021-01-20' AS Date), 1, 3, NULL)
GO
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (400, CAST(N'2021-01-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (401, CAST(N'2021-01-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (402, CAST(N'2021-01-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (403, CAST(N'2021-01-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (404, CAST(N'2021-01-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (405, CAST(N'2021-01-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (406, CAST(N'2021-01-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (407, CAST(N'2021-01-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (408, CAST(N'2021-01-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (409, CAST(N'2021-01-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (410, CAST(N'2021-01-31' AS Date), 1, 17, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (411, CAST(N'2021-02-01' AS Date), 1, 22, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (412, CAST(N'2021-02-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (413, CAST(N'2021-02-03' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (414, CAST(N'2021-02-04' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (415, CAST(N'2021-02-05' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (416, CAST(N'2021-02-06' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (417, CAST(N'2021-02-07' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (418, CAST(N'2021-02-08' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (419, CAST(N'2021-02-09' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (420, CAST(N'2021-02-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (421, CAST(N'2021-02-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (422, CAST(N'2021-02-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (423, CAST(N'2021-02-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (424, CAST(N'2021-02-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (425, CAST(N'2021-02-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (426, CAST(N'2021-02-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (427, CAST(N'2021-02-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (428, CAST(N'2021-02-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (429, CAST(N'2021-02-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (430, CAST(N'2021-02-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (431, CAST(N'2021-02-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (432, CAST(N'2021-02-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (433, CAST(N'2021-02-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (434, CAST(N'2021-02-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (435, CAST(N'2021-02-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (436, CAST(N'2021-02-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (437, CAST(N'2021-02-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (438, CAST(N'2021-03-01' AS Date), 1, 18, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (439, CAST(N'2021-03-02' AS Date), 1, 19, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (440, CAST(N'2021-03-03' AS Date), 1, 20, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (441, CAST(N'2021-03-04' AS Date), 1, 21, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (442, CAST(N'2021-03-05' AS Date), 1, 22, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (443, CAST(N'2021-03-06' AS Date), 1, 26, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (444, CAST(N'2021-03-07' AS Date), 1, 23, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (445, CAST(N'2021-03-08' AS Date), 1, 28, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (446, CAST(N'2021-03-09' AS Date), 1, 24, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (447, CAST(N'2021-03-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (448, CAST(N'2021-03-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (449, CAST(N'2021-03-12' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (450, CAST(N'2021-03-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (451, CAST(N'2021-03-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (452, CAST(N'2021-03-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (453, CAST(N'2021-03-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (454, CAST(N'2021-03-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (455, CAST(N'2021-03-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (456, CAST(N'2021-03-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (457, CAST(N'2021-03-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (458, CAST(N'2021-03-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (459, CAST(N'2021-03-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (460, CAST(N'2021-03-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (461, CAST(N'2021-03-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (462, CAST(N'2021-03-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (463, CAST(N'2021-03-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (464, CAST(N'2021-03-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (465, CAST(N'2021-03-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (466, CAST(N'2021-03-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (467, CAST(N'2021-03-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (468, CAST(N'2021-03-31' AS Date), 1, 17, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (469, CAST(N'2021-04-01' AS Date), 1, 23, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (470, CAST(N'2021-04-02' AS Date), 1, 19, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (471, CAST(N'2021-04-03' AS Date), 1, 20, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (472, CAST(N'2021-04-04' AS Date), 1, 21, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (473, CAST(N'2021-04-05' AS Date), 1, 22, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (474, CAST(N'2021-04-06' AS Date), 1, 26, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (475, CAST(N'2021-04-07' AS Date), 1, 23, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (476, CAST(N'2021-04-08' AS Date), 1, 28, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (477, CAST(N'2021-04-09' AS Date), 1, 24, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (478, CAST(N'2021-04-10' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (479, CAST(N'2021-04-11' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (480, CAST(N'2021-04-12' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (481, CAST(N'2021-04-13' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (482, CAST(N'2021-04-14' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (483, CAST(N'2021-04-15' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (484, CAST(N'2021-04-16' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (485, CAST(N'2021-04-17' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (486, CAST(N'2021-04-18' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (487, CAST(N'2021-04-19' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (488, CAST(N'2021-04-20' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (489, CAST(N'2021-04-21' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (490, CAST(N'2021-04-22' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (491, CAST(N'2021-04-23' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (492, CAST(N'2021-04-24' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (493, CAST(N'2021-04-25' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (494, CAST(N'2021-04-26' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (495, CAST(N'2021-04-27' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (496, CAST(N'2021-04-28' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (497, CAST(N'2021-04-29' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (498, CAST(N'2021-04-30' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (499, CAST(N'2021-05-01' AS Date), 1, 18, NULL)
GO
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (500, CAST(N'2021-05-01' AS Date), 1, 19, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (501, CAST(N'2021-05-01' AS Date), 1, 20, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (502, CAST(N'2021-05-01' AS Date), 1, 21, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (503, CAST(N'2021-05-01' AS Date), 1, 22, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (504, CAST(N'2021-05-01' AS Date), 1, 26, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (505, CAST(N'2021-05-01' AS Date), 1, 23, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (506, CAST(N'2021-05-01' AS Date), 1, 28, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (507, CAST(N'2021-05-01' AS Date), 1, 24, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (508, CAST(N'2021-05-02' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (509, CAST(N'2021-05-02' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (510, CAST(N'2021-05-02' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (511, CAST(N'2021-05-02' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (512, CAST(N'2021-05-02' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (513, CAST(N'2021-05-02' AS Date), 1, 14, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (514, CAST(N'2021-05-02' AS Date), 1, 15, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (515, CAST(N'2021-05-02' AS Date), 1, 16, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (516, CAST(N'2021-05-02' AS Date), 1, 1, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (517, CAST(N'2021-05-02' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (518, CAST(N'2021-05-02' AS Date), 1, 3, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (519, CAST(N'2021-05-03' AS Date), 1, 4, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (520, CAST(N'2021-05-03' AS Date), 1, 5, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (521, CAST(N'2021-05-03' AS Date), 1, 6, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (522, CAST(N'2021-05-03' AS Date), 1, 7, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (523, CAST(N'2021-05-03' AS Date), 1, 8, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (524, CAST(N'2021-05-03' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (525, CAST(N'2021-05-03' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (526, CAST(N'2021-05-03' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (527, CAST(N'2021-05-03' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (528, CAST(N'2021-05-03' AS Date), 1, 9, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (529, CAST(N'2021-05-03' AS Date), 1, 10, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (530, CAST(N'2021-05-04' AS Date), 1, 11, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (531, CAST(N'2021-05-04' AS Date), 1, 12, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (532, CAST(N'2021-05-04' AS Date), 1, 13, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (533, CAST(N'2021-05-03' AS Date), 1, 2, 1)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (534, CAST(N'2021-05-03' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (535, CAST(N'2021-05-03' AS Date), 1, 2, 1)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (536, CAST(N'2021-05-03' AS Date), 1, 2, NULL)
INSERT [dbo].[Orders] ([id], [date_created], [payment], [id_cus], [id_voucher]) VALUES (537, CAST(N'2021-05-03' AS Date), 1, 2, 5)
SET IDENTITY_INSERT [dbo].[Orders] OFF
SET IDENTITY_INSERT [dbo].[Orders_Detail] ON 

INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1, 3, 2, 1, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (2, 5, 1, 1, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (3, 1, 3, 1, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (4, 2, 4, 1, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (5, 5, 9, 2, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (6, 1, 6, 2, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (7, 2, 7, 2, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (8, 5, 11, 3, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (9, 1, 6, 3, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (10, 2, 7, 3, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (11, 5, 11, 4, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (12, 1, 24, 4, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (13, 2, 17, 4, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (14, 5, 1, 5, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (15, 1, 22, 5, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (16, 2, 3, 6, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (17, 1, 4, 6, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (18, 2, 6, 7, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (19, 3, 7, 7, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (20, 6, 5, 8, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (21, 1, 8, 8, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (22, 7, 9, 9, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (23, 2, 10, 9, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (24, 5, 11, 10, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (25, 1, 12, 10, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (26, 2, 13, 10, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (27, 1, 14, 11, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (28, 1, 15, 11, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (29, 4, 16, 11, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (30, 2, 17, 12, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (31, 3, 18, 12, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (32, 4, 19, 12, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (33, 3, 20, 13, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (34, 3, 21, 13, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (35, 1, 22, 13, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (36, 1, 23, 14, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (37, 2, 24, 14, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (38, 2, 21, 14, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (39, 6, 1, 15, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (40, 2, 24, 15, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (41, 2, 3, 15, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (42, 1, 10, 16, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (43, 1, 24, 16, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (44, 1, 10, 16, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (45, 2, 2, 17, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (46, 3, 1, 17, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (47, 6, 3, 17, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (48, 2, 2, 17, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (49, 3, 1, 18, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (50, 6, 3, 18, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (51, 1, 4, 18, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (52, 2, 9, 19, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (53, 1, 6, 19, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (54, 2, 7, 19, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (55, 4, 11, 20, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (56, 1, 6, 20, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (57, 2, 7, 20, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (58, 5, 11, 21, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (59, 2, 24, 21, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (60, 2, 17, 21, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (61, 4, 1, 22, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (62, 1, 22, 22, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (63, 2, 3, 23, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (64, 1, 4, 23, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (65, 2, 6, 24, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (66, 3, 7, 24, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (67, 6, 5, 25, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (68, 1, 8, 25, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (69, 7, 9, 26, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (70, 2, 10, 26, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (71, 5, 11, 27, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (72, 8, 12, 27, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (73, 2, 13, 28, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (74, 1, 14, 28, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (75, 1, 15, 28, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (76, 4, 16, 28, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (77, 2, 17, 29, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (78, 6, 18, 29, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (79, 4, 19, 30, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (80, 3, 20, 30, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (81, 7, 21, 31, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (82, 1, 22, 31, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (83, 4, 23, 31, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (84, 2, 24, 32, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (85, 2, 21, 32, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (86, 6, 1, 32, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (87, 2, 24, 33, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (88, 2, 3, 33, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (89, 1, 10, 34, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (90, 1, 24, 34, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (91, 1, 13, 34, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (92, 3, 2, 35, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (93, 5, 1, 35, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (94, 1, 3, 36, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (95, 2, 4, 36, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (96, 5, 9, 37, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (97, 1, 6, 37, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (98, 2, 7, 38, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (99, 5, 11, 38, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (100, 1, 6, 39, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (101, 2, 7, 39, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (102, 5, 11, 40, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (103, 1, 24, 40, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (104, 2, 17, 41, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (105, 5, 1, 41, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (106, 1, 22, 42, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (107, 2, 3, 42, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (108, 1, 4, 43, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (109, 2, 6, 43, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (110, 3, 7, 44, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (111, 6, 5, 44, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (112, 1, 8, 45, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (113, 5, 9, 45, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (114, 2, 10, 46, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (115, 5, 11, 46, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (116, 1, 12, 47, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (117, 2, 13, 47, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (118, 2, 14, 48, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (119, 1, 15, 48, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (120, 4, 16, 28, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (121, 5, 2, 49, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (122, 5, 1, 49, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (123, 7, 3, 49, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (124, 2, 4, 50, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (125, 5, 9, 50, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (126, 5, 6, 50, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (127, 2, 7, 51, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (128, 5, 11, 51, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (129, 3, 6, 52, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (130, 2, 7, 52, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (131, 5, 11, 53, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (132, 6, 24, 53, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (133, 2, 17, 54, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (134, 5, 1, 54, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (135, 1, 22, 55, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (136, 2, 3, 55, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (137, 2, 4, 57, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (138, 8, 6, 56, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (139, 3, 7, 57, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (140, 6, 5, 56, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (141, 1, 8, 58, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (142, 7, 9, 58, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (143, 2, 10, 59, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (144, 5, 11, 59, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (145, 1, 12, 60, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (146, 6, 13, 60, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (147, 1, 14, 61, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (148, 8, 15, 61, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (149, 6, 16, 62, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (150, 2, 17, 62, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (151, 3, 18, 63, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (152, 6, 19, 63, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (153, 3, 20, 64, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (154, 3, 21, 64, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (155, 6, 22, 65, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (156, 9, 23, 65, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (157, 2, 24, 66, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (158, 2, 21, 66, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (159, 6, 1, 67, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (160, 2, 24, 67, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (161, 2, 9, 68, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (162, 7, 10, 68, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (163, 1, 24, 69, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (164, 7, 13, 69, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (165, 3, 2, 70, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (166, 5, 1, 70, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (167, 7, 3, 71, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (168, 2, 4, 71, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (169, 5, 9, 72, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (170, 9, 6, 72, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (171, 2, 7, 73, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (172, 5, 11, 73, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (173, 2, 6, 74, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (174, 2, 7, 74, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (175, 5, 11, 75, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (176, 8, 24, 75, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (177, 2, 17, 76, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (178, 5, 1, 76, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (179, 3, 2, 77, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (180, 5, 1, 77, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (181, 9, 3, 77, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (182, 2, 4, 77, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (183, 5, 9, 78, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (184, 9, 6, 78, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (185, 2, 7, 78, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (186, 5, 11, 79, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (187, 1, 6, 79, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (188, 2, 7, 79, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (189, 5, 11, 80, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (190, 9, 24, 80, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (191, 2, 17, 80, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (192, 5, 1, 81, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (193, 1, 22, 81, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (194, 2, 3, 81, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (195, 1, 4, 82, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (196, 2, 6, 82, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (197, 3, 7, 82, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (198, 6, 5, 83, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (199, 1, 8, 83, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (200, 7, 9, 83, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (201, 6, 10, 84, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (202, 5, 11, 84, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (203, 9, 12, 84, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (204, 2, 13, 85, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (205, 1, 14, 85, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (206, 9, 15, 85, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (207, 4, 16, 86, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (208, 2, 17, 86, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (209, 3, 18, 87, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (210, 4, 19, 87, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (211, 3, 20, 88, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (212, 3, 21, 88, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (213, 4, 22, 88, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (214, 1, 23, 89, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (215, 2, 24, 89, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (216, 2, 21, 89, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (217, 6, 1, 90, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (218, 2, 24, 90, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (219, 2, 3, 91, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (220, 9, 10, 91, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (221, 1, 24, 92, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (222, 1, 13, 92, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (223, 3, 2, 93, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (224, 5, 1, 93, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (225, 1, 3, 94, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (226, 2, 4, 94, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (227, 5, 9, 95, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (228, 1, 6, 95, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (229, 2, 7, 96, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (230, 5, 11, 96, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (231, 1, 6, 97, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (232, 2, 7, 97, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (233, 5, 11, 98, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (234, 3, 24, 98, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (235, 2, 17, 99, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (236, 5, 1, 99, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (237, 1, 22, 100, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (238, 2, 3, 100, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (239, 1, 4, 101, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (240, 2, 6, 101, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (241, 3, 7, 102, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (242, 6, 5, 102, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (243, 4, 8, 103, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (244, 5, 9, 103, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (245, 2, 10, 104, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (246, 5, 11, 104, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (247, 4, 12, 105, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (248, 2, 13, 105, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (249, 1, 14, 106, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (250, 4, 15, 106, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (251, 4, 16, 107, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (252, 2, 17, 107, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (253, 6, 9, 108, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (254, 5, 1, 108, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (255, 6, 3, 108, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (256, 3, 2, 109, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (257, 5, 8, 109, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (258, 6, 3, 109, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (259, 2, 4, 110, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (260, 5, 9, 110, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (261, 6, 6, 110, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (262, 2, 7, 111, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (263, 5, 11, 111, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (264, 1, 6, 112, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (265, 2, 7, 112, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (266, 5, 11, 113, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (267, 1, 24, 113, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (268, 2, 17, 114, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (269, 5, 1, 114, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (270, 1, 22, 115, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (271, 2, 3, 115, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (272, 1, 4, 117, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (273, 2, 6, 116, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (274, 3, 7, 117, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (275, 6, 5, 116, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (276, 1, 8, 118, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (277, 7, 9, 118, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (278, 2, 10, 119, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (279, 5, 11, 119, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (280, 6, 12, 120, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (281, 5, 13, 120, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (282, 1, 14, 121, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (283, 1, 15, 121, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (284, 6, 16, 122, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (285, 2, 17, 122, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (286, 3, 18, 123, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (287, 6, 19, 123, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (288, 3, 20, 124, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (289, 3, 21, 124, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (290, 1, 22, 125, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (291, 6, 23, 125, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (292, 2, 24, 126, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (293, 2, 21, 126, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (294, 6, 1, 127, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (295, 5, 24, 127, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (296, 5, 3, 128, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (297, 6, 10, 128, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (298, 1, 24, 129, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (299, 6, 13, 129, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (300, 3, 2, 130, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (301, 5, 1, 130, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (302, 1, 3, 131, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (303, 5, 4, 131, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (304, 5, 9, 132, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (305, 6, 6, 132, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (306, 2, 7, 133, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (307, 5, 11, 133, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (308, 6, 6, 134, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (309, 2, 7, 134, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (310, 5, 11, 135, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (311, 6, 24, 135, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (312, 2, 17, 136, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (313, 5, 1, 136, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (314, 5, 17, 137, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (315, 5, 1, 137, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (316, 3, 2, 138, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (317, 5, 1, 138, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (318, 8, 3, 138, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (319, 3, 2, 139, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (320, 5, 1, 139, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (321, 8, 3, 139, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (322, 2, 4, 140, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (323, 5, 9, 140, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (324, 8, 6, 140, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (325, 2, 7, 141, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (326, 5, 11, 141, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (327, 1, 6, 142, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (328, 2, 7, 142, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (329, 5, 11, 143, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (330, 1, 24, 143, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (331, 2, 17, 144, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (332, 5, 1, 144, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (333, 8, 22, 145, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (334, 2, 3, 145, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (335, 3, 4, 147, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (336, 2, 6, 146, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (337, 6, 7, 147, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (338, 6, 5, 146, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (339, 1, 8, 148, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (340, 7, 9, 148, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (341, 2, 10, 149, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (342, 5, 11, 149, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (343, 3, 12, 150, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (344, 2, 13, 150, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (345, 2, 14, 151, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (346, 2, 15, 151, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (347, 4, 16, 152, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (348, 2, 17, 152, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (349, 3, 18, 153, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (350, 4, 19, 153, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (351, 3, 20, 154, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (352, 3, 21, 154, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (353, 1, 22, 155, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (354, 1, 23, 155, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (355, 2, 24, 156, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (356, 2, 21, 156, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (357, 6, 1, 157, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (358, 2, 24, 157, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (359, 2, 3, 158, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (360, 3, 10, 158, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (361, 3, 24, 159, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (362, 1, 13, 159, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (363, 3, 2, 160, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (364, 5, 1, 160, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (365, 1, 3, 161, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (366, 2, 4, 161, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (367, 5, 9, 162, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (368, 3, 6, 162, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (369, 2, 7, 163, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (370, 5, 11, 163, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (371, 1, 6, 164, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (372, 2, 7, 164, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (373, 5, 11, 165, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (374, 3, 24, 165, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (375, 2, 17, 166, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (376, 5, 1, 166, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (377, 2, 17, 167, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (378, 5, 1, 167, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (379, 2, 17, 168, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (380, 5, 1, 168, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (381, 3, 2, 169, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (382, 5, 1, 169, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (383, 1, 3, 169, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (384, 2, 4, 170, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (385, 5, 9, 170, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (386, 1, 6, 170, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (387, 2, 7, 171, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (388, 5, 11, 171, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (389, 7, 6, 172, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (390, 2, 7, 172, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (391, 5, 11, 173, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (392, 1, 24, 173, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (393, 2, 17, 174, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (394, 5, 1, 174, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (395, 7, 22, 175, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (396, 2, 3, 175, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (397, 7, 4, 177, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (398, 2, 6, 176, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (399, 3, 7, 177, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (400, 6, 5, 176, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (401, 7, 8, 178, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (402, 7, 9, 178, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (403, 2, 10, 179, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (404, 5, 11, 179, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (405, 7, 12, 180, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (406, 2, 13, 180, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (407, 1, 14, 181, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (408, 1, 15, 181, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (409, 4, 16, 182, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (410, 2, 17, 182, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (411, 3, 18, 183, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (412, 4, 19, 183, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (413, 3, 20, 184, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (414, 3, 21, 184, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (415, 1, 22, 185, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (416, 1, 23, 185, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (417, 2, 24, 186, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (418, 2, 21, 186, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (419, 6, 1, 187, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (420, 2, 24, 187, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (421, 2, 3, 188, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (422, 6, 10, 188, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (423, 6, 24, 189, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (424, 1, 13, 189, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (425, 3, 2, 190, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (426, 5, 1, 190, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (427, 1, 3, 191, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (428, 2, 4, 191, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (429, 5, 9, 192, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (430, 1, 6, 192, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (431, 2, 7, 193, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (432, 5, 11, 193, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (433, 1, 6, 194, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (434, 2, 7, 194, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (435, 5, 11, 195, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (436, 3, 24, 195, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (437, 2, 17, 196, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (438, 5, 1, 196, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (439, 2, 17, 197, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (440, 5, 1, 197, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (441, 2, 17, 198, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (442, 5, 1, 198, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (443, 3, 2, 199, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (444, 5, 1, 199, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (445, 2, 3, 199, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (446, 2, 4, 200, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (447, 5, 9, 200, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (448, 2, 6, 200, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (449, 2, 7, 201, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (450, 5, 11, 201, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (451, 2, 6, 202, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (452, 2, 7, 202, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (453, 5, 11, 203, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (454, 2, 24, 203, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (455, 2, 17, 204, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (456, 5, 1, 204, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (457, 3, 22, 205, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (458, 3, 3, 205, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (459, 3, 4, 207, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (460, 2, 6, 206, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (461, 3, 7, 207, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (462, 6, 5, 206, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (463, 1, 8, 208, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (464, 7, 9, 208, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (465, 2, 10, 209, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (466, 5, 11, 209, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (467, 1, 12, 210, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (468, 2, 13, 210, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (469, 1, 14, 211, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (470, 3, 15, 211, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (471, 4, 16, 212, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (472, 3, 17, 212, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (473, 3, 18, 213, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (474, 4, 19, 213, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (475, 3, 20, 214, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (476, 3, 21, 214, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (477, 3, 22, 215, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (478, 3, 23, 215, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (479, 2, 24, 216, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (480, 2, 21, 216, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (481, 6, 1, 217, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (482, 2, 24, 217, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (483, 2, 3, 218, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (484, 3, 10, 218, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (485, 3, 24, 219, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (486, 3, 13, 219, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (487, 3, 2, 220, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (488, 5, 1, 220, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (489, 1, 3, 221, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (490, 2, 4, 221, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (491, 5, 9, 222, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (492, 1, 6, 222, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (493, 2, 7, 223, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (494, 5, 11, 223, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (495, 3, 6, 224, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (496, 2, 7, 224, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (497, 5, 11, 225, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (498, 1, 24, 225, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (499, 2, 17, 226, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (500, 5, 1, 226, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (501, 2, 17, 227, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (502, 5, 1, 227, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (503, 2, 17, 228, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (504, 5, 1, 228, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (505, 3, 2, 228, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (506, 5, 1, 229, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (507, 7, 3, 229, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (508, 2, 4, 230, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (509, 5, 9, 230, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (510, 7, 6, 230, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (511, 2, 7, 231, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (512, 5, 11, 231, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (513, 1, 6, 232, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (514, 2, 7, 232, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (515, 5, 11, 233, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (516, 4, 24, 233, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (517, 2, 17, 234, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (518, 5, 1, 234, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (519, 7, 22, 235, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (520, 2, 3, 235, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (521, 1, 4, 237, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (522, 2, 6, 236, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (523, 3, 7, 237, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (524, 6, 5, 236, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (525, 7, 8, 238, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (526, 7, 9, 238, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (527, 2, 10, 239, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (528, 5, 11, 239, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (529, 1, 12, 240, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (530, 2, 13, 240, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (531, 1, 14, 241, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (532, 1, 15, 241, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (533, 4, 16, 242, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (534, 2, 17, 242, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (535, 6, 18, 243, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (536, 4, 19, 243, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (537, 3, 20, 244, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (538, 3, 21, 244, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (539, 1, 22, 245, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (540, 1, 23, 245, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (541, 2, 24, 246, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (542, 2, 21, 246, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (543, 6, 1, 247, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (544, 2, 24, 247, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (545, 2, 3, 248, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (546, 6, 10, 248, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (547, 1, 24, 249, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (548, 6, 13, 249, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (549, 3, 2, 250, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (550, 5, 1, 250, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (551, 6, 3, 251, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (552, 2, 4, 251, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (553, 5, 9, 252, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (554, 1, 6, 252, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (555, 2, 7, 253, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (556, 5, 11, 253, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (557, 6, 6, 254, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (558, 2, 7, 254, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (559, 5, 11, 255, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (560, 6, 24, 255, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (561, 2, 17, 256, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (562, 5, 1, 256, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (563, 2, 17, 257, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (564, 5, 1, 257, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (565, 2, 17, 258, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (566, 5, 1, 258, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (567, 3, 2, 258, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (568, 5, 1, 259, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (569, 3, 2, 259, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (570, 5, 1, 259, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (571, 6, 3, 260, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (572, 1, 3, 260, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (573, 2, 7, 261, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (574, 5, 11, 261, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (575, 8, 6, 262, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (576, 2, 7, 262, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (577, 5, 11, 263, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (578, 8, 24, 263, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (579, 2, 17, 264, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (580, 5, 1, 264, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (581, 1, 22, 265, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (582, 2, 3, 265, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (583, 1, 4, 267, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (584, 2, 6, 266, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (585, 3, 7, 267, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (586, 6, 5, 266, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (587, 7, 8, 268, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (588, 7, 9, 268, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (589, 2, 10, 269, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (590, 5, 11, 269, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (591, 7, 12, 270, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (592, 2, 13, 270, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (593, 1, 14, 271, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (594, 1, 15, 271, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (595, 4, 16, 272, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (596, 2, 17, 272, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (597, 3, 18, 273, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (598, 4, 19, 273, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (599, 3, 20, 274, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (600, 3, 21, 274, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (601, 1, 22, 275, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (602, 1, 23, 275, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (603, 2, 24, 276, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (604, 2, 21, 276, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (605, 6, 1, 277, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (606, 2, 24, 277, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (607, 2, 3, 278, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (608, 6, 10, 278, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (609, 6, 24, 279, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (610, 6, 13, 279, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (611, 3, 2, 280, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (612, 5, 1, 280, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (613, 1, 3, 281, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (614, 2, 4, 281, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (615, 5, 9, 282, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (616, 1, 6, 282, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (617, 2, 7, 283, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (618, 5, 11, 283, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (619, 1, 6, 284, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (620, 2, 7, 284, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (621, 5, 11, 285, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (622, 1, 24, 285, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (623, 2, 17, 286, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (624, 5, 1, 286, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (625, 2, 17, 287, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (626, 5, 1, 287, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (627, 4, 17, 288, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (628, 4, 1, 288, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (629, 4, 2, 288, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (630, 5, 1, 289, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (631, 3, 2, 289, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (632, 5, 1, 289, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (633, 1, 3, 283, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (634, 7, 4, 284, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (635, 5, 9, 290, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (636, 5, 6, 290, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (637, 5, 3, 290, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (638, 2, 7, 291, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (639, 5, 11, 291, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (640, 3, 6, 292, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (641, 2, 7, 292, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (642, 5, 11, 293, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (643, 3, 24, 293, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (644, 2, 17, 294, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (645, 5, 1, 294, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (646, 3, 22, 295, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (647, 2, 3, 295, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (648, 3, 4, 297, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (649, 2, 6, 296, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (650, 3, 7, 297, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (651, 6, 5, 296, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (652, 9, 8, 298, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (653, 7, 9, 298, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (654, 2, 10, 299, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (655, 5, 11, 299, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (656, 9, 12, 300, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (657, 2, 13, 300, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (658, 9, 14, 301, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (659, 9, 15, 301, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (660, 4, 16, 302, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (661, 2, 17, 302, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (662, 3, 18, 303, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (663, 4, 19, 303, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (664, 3, 20, 304, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (665, 3, 21, 304, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (666, 1, 22, 305, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (667, 5, 23, 305, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (668, 2, 24, 306, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (669, 2, 21, 306, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (670, 6, 1, 307, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (671, 2, 24, 307, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (672, 2, 3, 308, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (673, 5, 10, 308, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (674, 1, 24, 309, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (675, 1, 13, 309, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (676, 3, 2, 310, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (677, 5, 1, 310, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (678, 1, 3, 311, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (679, 2, 4, 311, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (680, 5, 9, 312, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (681, 1, 6, 312, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (682, 5, 7, 313, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (683, 5, 11, 313, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (684, 5, 6, 314, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (685, 2, 7, 314, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (686, 5, 11, 315, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (687, 1, 24, 315, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (688, 2, 17, 316, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (689, 5, 1, 316, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (690, 2, 17, 317, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (691, 5, 1, 317, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (692, 2, 17, 318, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (693, 5, 1, 318, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (694, 3, 2, 318, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (695, 5, 1, 319, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (696, 3, 2, 319, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (697, 5, 1, 319, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (698, 5, 3, 313, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (699, 2, 4, 314, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (700, 5, 9, 320, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (701, 1, 6, 320, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (702, 5, 3, 320, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (703, 2, 7, 321, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (704, 5, 11, 321, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (705, 9, 6, 322, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (706, 2, 7, 322, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (707, 9, 11, 323, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (708, 9, 24, 323, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (709, 2, 17, 324, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (710, 5, 1, 324, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (711, 9, 22, 325, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (712, 2, 3, 325, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (713, 9, 4, 327, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (714, 2, 6, 326, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (715, 9, 7, 327, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (716, 6, 5, 326, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (717, 1, 8, 328, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (718, 9, 9, 328, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (719, 2, 10, 329, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (720, 9, 11, 329, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (721, 1, 12, 330, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (722, 9, 13, 330, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (723, 1, 14, 331, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (724, 9, 15, 331, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (725, 4, 16, 332, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (726, 2, 17, 332, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (727, 3, 18, 333, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (728, 4, 19, 333, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (729, 3, 20, 334, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (730, 3, 21, 334, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (731, 1, 22, 335, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (732, 1, 23, 335, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (733, 9, 24, 336, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (734, 9, 21, 336, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (735, 6, 1, 337, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (736, 2, 24, 337, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (737, 2, 3, 338, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (738, 1, 10, 338, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (739, 1, 24, 339, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (740, 1, 13, 339, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (741, 3, 2, 340, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (742, 5, 1, 340, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (743, 1, 3, 341, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (744, 2, 4, 341, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (745, 5, 9, 342, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (746, 1, 6, 342, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (747, 2, 7, 343, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (748, 5, 11, 343, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (749, 1, 6, 344, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (750, 9, 7, 344, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (751, 5, 11, 345, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (752, 9, 24, 345, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (753, 2, 17, 346, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (754, 5, 1, 346, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (755, 2, 17, 347, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (756, 9, 1, 347, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (757, 2, 17, 348, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (758, 9, 1, 348, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (759, 9, 2, 348, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (760, 8, 1, 349, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (761, 3, 2, 349, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (762, 5, 1, 349, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (763, 7, 3, 350, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (764, 7, 4, 350, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (765, 2, 7, 351, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (766, 5, 11, 351, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (767, 6, 6, 352, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (768, 6, 7, 352, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (769, 5, 11, 353, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (770, 1, 24, 353, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (771, 2, 17, 354, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (772, 5, 1, 354, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (773, 6, 22, 355, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (774, 2, 3, 355, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (775, 6, 4, 357, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (776, 2, 6, 356, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (777, 6, 7, 357, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (778, 6, 5, 356, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (779, 6, 8, 358, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (780, 7, 9, 358, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (781, 8, 10, 359, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (782, 5, 11, 359, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (783, 1, 12, 360, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (784, 5, 13, 360, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (785, 1, 14, 361, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (786, 5, 15, 361, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (787, 4, 16, 362, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (788, 5, 17, 362, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (789, 5, 18, 363, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (790, 5, 19, 363, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (791, 3, 20, 364, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (792, 3, 21, 364, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (793, 1, 22, 365, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (794, 9, 23, 365, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (795, 9, 24, 366, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (796, 2, 21, 366, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (797, 7, 1, 367, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (798, 7, 24, 367, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (799, 2, 3, 368, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (800, 7, 10, 368, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (801, 1, 24, 369, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (802, 1, 13, 369, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (803, 9, 2, 370, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (804, 9, 1, 370, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (805, 9, 3, 371, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (806, 2, 4, 371, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (807, 9, 9, 372, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (808, 1, 6, 372, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (809, 7, 7, 373, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (810, 8, 11, 373, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (811, 1, 6, 374, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (812, 9, 7, 374, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (813, 5, 11, 375, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (814, 8, 24, 375, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (815, 2, 17, 376, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (816, 5, 1, 376, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (817, 2, 17, 377, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (818, 5, 1, 377, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (819, 2, 17, 378, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (820, 5, 1, 378, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (821, 8, 2, 377, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (822, 5, 1, 379, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (823, 8, 2, 379, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (824, 5, 1, 379, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (825, 9, 17, 380, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (826, 5, 1, 380, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (827, 8, 2, 380, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (828, 2, 7, 381, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (829, 5, 11, 381, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (830, 2, 6, 382, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (831, 2, 7, 382, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (832, 5, 11, 383, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (833, 3, 24, 383, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (834, 2, 17, 384, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (835, 5, 1, 384, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (836, 3, 22, 385, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (837, 2, 3, 385, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (838, 3, 4, 387, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (839, 2, 6, 386, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (840, 3, 7, 387, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (841, 3, 5, 386, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (842, 1, 8, 388, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (843, 7, 9, 388, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (844, 2, 10, 389, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (845, 1, 11, 389, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (846, 1, 12, 390, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (847, 2, 13, 390, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (848, 2, 14, 391, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (849, 1, 15, 391, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (850, 3, 16, 392, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (851, 2, 17, 392, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (852, 3, 18, 393, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (853, 5, 19, 393, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (854, 5, 20, 394, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (855, 3, 21, 394, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (856, 5, 22, 395, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (857, 1, 23, 395, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (858, 2, 24, 396, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (859, 2, 21, 396, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (860, 6, 1, 397, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (861, 2, 24, 397, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (862, 2, 3, 398, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (863, 1, 10, 398, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (864, 5, 24, 399, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (865, 5, 13, 399, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (866, 3, 2, 400, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (867, 5, 1, 400, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (868, 1, 3, 401, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (869, 2, 4, 401, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (870, 5, 9, 402, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (871, 1, 6, 402, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (872, 2, 7, 403, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (873, 5, 11, 403, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (874, 5, 6, 404, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (875, 2, 7, 404, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (876, 5, 11, 405, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (877, 1, 24, 405, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (878, 2, 17, 406, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (879, 5, 1, 406, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (880, 2, 17, 407, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (881, 5, 1, 407, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (882, 2, 17, 408, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (883, 5, 1, 408, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (884, 3, 2, 407, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (885, 5, 1, 409, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (886, 3, 2, 409, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (887, 5, 1, 409, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (888, 2, 17, 410, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (889, 5, 1, 410, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (890, 3, 2, 410, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (891, 2, 7, 411, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (892, 5, 11, 411, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (893, 1, 6, 412, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (894, 2, 7, 412, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (895, 5, 11, 413, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (896, 1, 24, 413, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (897, 2, 17, 414, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (898, 5, 1, 414, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (899, 1, 22, 415, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (900, 2, 3, 415, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (901, 1, 4, 417, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (902, 2, 6, 416, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (903, 3, 7, 417, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (904, 6, 5, 416, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (905, 1, 8, 418, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (906, 7, 9, 418, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (907, 2, 10, 419, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (908, 5, 11, 419, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (909, 1, 12, 420, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (910, 2, 13, 420, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (911, 1, 14, 421, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (912, 1, 15, 421, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (913, 4, 16, 422, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (914, 2, 17, 422, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (915, 3, 18, 423, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (916, 4, 19, 423, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (917, 3, 20, 424, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (918, 3, 21, 424, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (919, 1, 22, 425, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (920, 1, 23, 425, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (921, 2, 24, 426, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (922, 2, 21, 426, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (923, 6, 1, 427, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (924, 2, 24, 427, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (925, 2, 3, 428, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (926, 1, 10, 428, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (927, 1, 24, 429, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (928, 1, 13, 429, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (929, 3, 2, 430, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (930, 5, 1, 430, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (931, 1, 3, 431, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (932, 2, 4, 431, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (933, 5, 9, 432, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (934, 1, 6, 432, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (935, 2, 7, 433, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (936, 5, 11, 433, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (937, 1, 6, 434, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (938, 2, 7, 434, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (939, 5, 11, 435, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (940, 1, 24, 435, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (941, 2, 17, 436, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (942, 5, 1, 436, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (943, 2, 17, 437, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (944, 5, 1, 437, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (945, 3, 2, 437, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (946, 2, 7, 438, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (947, 5, 11, 438, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (948, 2, 7, 438, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (949, 5, 11, 439, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (950, 2, 7, 439, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (951, 5, 11, 440, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (952, 2, 7, 440, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (953, 5, 11, 441, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (954, 1, 6, 442, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (955, 2, 7, 442, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (956, 5, 11, 443, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (957, 1, 24, 443, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (958, 2, 17, 444, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (959, 5, 1, 444, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (960, 1, 22, 445, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (961, 2, 3, 445, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (962, 1, 4, 447, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (963, 2, 6, 446, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (964, 3, 7, 447, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (965, 6, 5, 446, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (966, 1, 8, 448, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (967, 7, 9, 448, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (968, 2, 10, 449, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (969, 5, 11, 449, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (970, 1, 12, 450, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (971, 2, 13, 450, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (972, 1, 14, 451, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (973, 1, 15, 451, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (974, 4, 16, 452, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (975, 2, 17, 452, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (976, 3, 18, 453, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (977, 4, 19, 453, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (978, 3, 20, 454, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (979, 3, 21, 454, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (980, 1, 22, 455, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (981, 1, 23, 455, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (982, 2, 24, 456, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (983, 2, 21, 456, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (984, 6, 1, 457, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (985, 2, 24, 457, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (986, 2, 3, 458, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (987, 1, 10, 458, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (988, 1, 24, 459, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (989, 1, 13, 459, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (990, 3, 2, 460, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (991, 5, 1, 460, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (992, 1, 3, 461, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (993, 2, 4, 461, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (994, 5, 9, 462, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (995, 1, 6, 462, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (996, 2, 7, 463, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (997, 5, 11, 463, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (998, 1, 6, 464, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (999, 2, 7, 464, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1000, 5, 11, 465, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1001, 1, 24, 465, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1002, 2, 17, 466, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1003, 5, 1, 466, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1004, 2, 17, 467, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1005, 5, 1, 467, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1006, 3, 2, 467, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1007, 5, 1, 468, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1008, 3, 2, 468, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1009, 5, 1, 468, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1010, 5, 11, 469, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1011, 8, 7, 469, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1012, 5, 11, 470, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1013, 2, 7, 470, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1014, 5, 11, 471, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1015, 1, 6, 472, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1016, 8, 7, 472, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1017, 5, 11, 473, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1018, 1, 24, 473, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1019, 2, 17, 474, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1020, 8, 1, 474, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1021, 1, 22, 475, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1022, 2, 3, 475, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1023, 8, 4, 477, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1024, 2, 6, 476, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1025, 8, 7, 477, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1026, 6, 5, 476, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1027, 1, 8, 478, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1028, 7, 9, 478, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1029, 8, 10, 479, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1030, 5, 11, 479, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1031, 8, 12, 480, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1032, 2, 13, 480, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1033, 1, 14, 481, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1034, 1, 15, 481, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1035, 8, 16, 482, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1036, 2, 17, 482, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1037, 3, 18, 483, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1038, 4, 19, 483, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1039, 3, 20, 484, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1040, 3, 21, 484, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1041, 1, 22, 485, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1042, 1, 23, 485, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1043, 2, 24, 486, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1044, 2, 21, 486, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1045, 6, 1, 487, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1046, 2, 24, 487, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1047, 2, 3, 488, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1048, 8, 10, 488, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1049, 1, 24, 489, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1050, 8, 13, 489, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1051, 3, 2, 490, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1052, 5, 1, 490, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1053, 1, 3, 491, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1054, 2, 4, 491, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1055, 5, 9, 492, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1056, 1, 6, 492, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1057, 2, 7, 493, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1058, 8, 11, 493, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1059, 1, 6, 494, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1060, 2, 7, 494, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1061, 8, 11, 495, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1062, 1, 24, 495, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1063, 2, 17, 496, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1064, 5, 1, 496, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1065, 8, 17, 497, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1066, 5, 1, 497, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1067, 8, 2, 497, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1068, 5, 1, 498, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1069, 8, 2, 498, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1070, 5, 1, 498, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1071, 5, 11, 499, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1072, 8, 7, 499, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1073, 5, 11, 500, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1074, 2, 7, 500, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1075, 5, 11, 501, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1076, 1, 6, 502, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1077, 8, 7, 502, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1078, 5, 11, 502, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1079, 1, 24, 503, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1080, 2, 17, 504, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1081, 8, 1, 504, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1082, 1, 22, 505, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1083, 2, 3, 505, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1084, 8, 4, 507, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1085, 2, 6, 506, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1086, 8, 7, 507, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1087, 6, 5, 506, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1088, 1, 8, 508, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1089, 7, 9, 508, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1090, 8, 10, 509, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1091, 5, 11, 509, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1092, 8, 12, 510, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1093, 2, 13, 510, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1094, 1, 14, 511, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1095, 1, 15, 511, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1096, 8, 16, 512, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1097, 2, 17, 512, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1098, 3, 18, 513, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1099, 4, 19, 513, 1)
GO
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1100, 3, 20, 514, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1101, 3, 21, 514, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1102, 1, 22, 515, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1103, 1, 23, 515, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1104, 2, 24, 516, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1105, 2, 21, 516, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1106, 6, 1, 517, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1107, 2, 24, 517, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1108, 2, 3, 518, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1109, 8, 10, 518, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1110, 1, 24, 519, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1111, 8, 13, 519, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1112, 3, 2, 520, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1113, 5, 1, 520, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1114, 1, 3, 521, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1115, 2, 4, 521, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1116, 5, 9, 522, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1117, 1, 6, 522, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1118, 2, 7, 523, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1119, 8, 11, 523, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1120, 1, 6, 523, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1121, 2, 7, 524, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1122, 8, 11, 525, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1123, 1, 24, 525, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1124, 2, 17, 526, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1125, 5, 1, 526, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1126, 8, 17, 527, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1127, 5, 1, 527, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1128, 8, 2, 527, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1129, 5, 1, 528, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1130, 8, 2, 528, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1131, 5, 1, 528, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1132, 1, 24, 529, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1133, 8, 13, 529, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1134, 3, 2, 530, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1135, 5, 1, 530, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1136, 1, 3, 531, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1137, 2, 4, 531, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1138, 5, 9, 532, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1139, 1, 6, 532, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1140, 1, 1, 533, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1141, 1, 1, 534, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1142, 1, 5, 535, 1)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1143, 1, 2, 536, 2)
INSERT [dbo].[Orders_Detail] ([id], [amount], [id_product], [id_order], [id_size]) VALUES (1144, 1, 4, 537, 1)
SET IDENTITY_INSERT [dbo].[Orders_Detail] OFF
SET IDENTITY_INSERT [dbo].[Orders_status] ON 

INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (1, 2, N'success', 1)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (2, 2, N'success', 2)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (3, 2, N'success', 3)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (4, 0, N'fail', 4)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (5, 2, N'success', 5)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (6, 2, N'success', 6)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (7, 2, N'success', 7)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (8, 2, N'success', 8)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (9, 0, N'fail', 9)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (10, 0, N'fail', 10)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (11, 2, N'success', 11)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (12, 2, N'success', 12)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (13, 2, N'success', 13)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (14, 2, N'success', 14)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (15, 2, N'success', 15)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (16, 0, N'fail', 16)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (17, 2, N'success', 17)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (18, 2, N'success', 18)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (19, 2, N'success', 19)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (20, 0, N'fail', 20)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (21, 2, N'success', 21)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (22, 2, N'success', 22)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (23, 2, N'success', 23)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (24, 2, N'success', 24)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (25, 0, N'fail', 25)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (26, 0, N'fail', 26)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (27, 2, N'success', 27)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (28, 2, N'success', 28)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (29, 2, N'success', 29)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (30, 2, N'success', 30)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (31, 2, N'success', 31)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (32, 0, N'fail', 32)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (33, 2, N'success', 33)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (34, 2, N'success', 34)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (35, 2, N'success', 35)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (36, 0, N'fail', 36)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (37, 2, N'success', 37)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (38, 2, N'success', 38)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (39, 2, N'success', 39)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (40, 2, N'success', 40)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (41, 0, N'fail', 41)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (42, 0, N'fail', 42)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (43, 2, N'success', 43)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (44, 2, N'success', 44)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (45, 2, N'success', 45)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (46, 2, N'success', 46)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (47, 2, N'success', 47)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (48, 0, N'fail', 48)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (49, 2, N'success', 49)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (50, 0, N'fail', 50)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (51, 2, N'success', 51)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (52, 2, N'success', 52)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (53, 2, N'success', 53)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (54, 2, N'success', 54)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (55, 0, N'fail', 55)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (56, 0, N'fail', 56)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (57, 2, N'success', 57)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (58, 2, N'success', 58)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (59, 2, N'success', 59)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (60, 2, N'success', 60)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (61, 2, N'success', 61)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (62, 0, N'fail', 62)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (63, 2, N'success', 63)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (64, 2, N'success', 64)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (65, 2, N'success', 65)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (66, 0, N'fail', 66)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (67, 2, N'success', 67)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (68, 2, N'success', 68)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (69, 2, N'success', 69)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (70, 2, N'success', 70)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (71, 0, N'fail', 71)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (72, 0, N'fail', 72)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (73, 2, N'success', 73)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (74, 2, N'success', 74)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (75, 2, N'success', 75)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (76, 2, N'success', 76)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (77, 2, N'success', 77)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (78, 2, N'success', 78)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (79, 2, N'success', 79)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (80, 0, N'fail', 80)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (81, 2, N'success', 81)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (82, 2, N'success', 82)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (83, 2, N'success', 83)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (84, 2, N'success', 84)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (85, 0, N'fail', 85)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (86, 0, N'fail', 86)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (87, 2, N'success', 87)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (88, 2, N'success', 88)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (89, 2, N'success', 89)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (90, 2, N'success', 90)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (91, 2, N'success', 91)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (92, 0, N'fail', 92)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (93, 2, N'success', 93)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (94, 2, N'success', 94)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (95, 2, N'success', 95)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (96, 0, N'fail', 96)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (97, 2, N'success', 97)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (98, 2, N'success', 98)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (99, 2, N'success', 99)
GO
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (100, 2, N'success', 100)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (101, 0, N'fail', 101)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (102, 0, N'fail', 102)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (103, 2, N'success', 103)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (104, 2, N'success', 104)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (105, 2, N'success', 105)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (106, 2, N'success', 106)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (107, 2, N'success', 107)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (108, 2, N'success', 108)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (109, 2, N'success', 109)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (110, 0, N'fail', 110)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (111, 2, N'success', 111)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (112, 2, N'success', 112)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (113, 2, N'success', 113)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (114, 2, N'success', 114)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (115, 0, N'fail', 115)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (116, 0, N'fail', 116)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (117, 2, N'success', 117)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (118, 2, N'success', 118)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (119, 2, N'success', 119)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (120, 2, N'success', 120)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (121, 2, N'success', 121)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (122, 0, N'fail', 122)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (123, 2, N'success', 123)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (124, 2, N'success', 124)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (125, 2, N'success', 125)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (126, 0, N'fail', 126)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (127, 2, N'success', 127)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (128, 2, N'success', 128)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (129, 2, N'success', 129)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (130, 2, N'success', 130)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (131, 0, N'fail', 131)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (132, 0, N'fail', 132)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (133, 2, N'success', 133)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (134, 2, N'success', 134)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (135, 2, N'success', 135)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (136, 2, N'success', 136)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (137, 2, N'success', 137)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (138, 2, N'success', 138)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (139, 2, N'success', 139)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (140, 0, N'fail', 140)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (141, 2, N'success', 141)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (142, 2, N'success', 142)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (143, 2, N'success', 143)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (144, 2, N'success', 144)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (145, 0, N'fail', 145)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (146, 0, N'fail', 146)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (147, 2, N'success', 147)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (148, 2, N'success', 148)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (149, 2, N'success', 149)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (150, 2, N'success', 150)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (151, 2, N'success', 151)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (152, 0, N'fail', 152)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (153, 2, N'success', 153)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (154, 2, N'success', 154)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (155, 2, N'success', 155)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (156, 0, N'fail', 156)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (157, 2, N'success', 157)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (158, 2, N'success', 158)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (159, 2, N'success', 159)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (160, 2, N'success', 160)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (161, 0, N'fail', 161)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (162, 0, N'fail', 162)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (163, 2, N'success', 163)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (164, 2, N'success', 164)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (165, 2, N'success', 165)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (166, 2, N'success', 166)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (167, 2, N'success', 167)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (168, 2, N'success', 168)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (169, 2, N'success', 169)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (170, 0, N'fail', 170)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (171, 2, N'success', 171)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (172, 2, N'success', 172)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (173, 2, N'success', 173)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (174, 2, N'success', 174)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (175, 0, N'fail', 175)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (176, 0, N'fail', 176)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (177, 2, N'success', 177)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (178, 2, N'success', 178)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (179, 2, N'success', 179)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (180, 2, N'success', 180)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (181, 2, N'success', 181)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (182, 0, N'fail', 182)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (183, 2, N'success', 183)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (184, 2, N'success', 184)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (185, 2, N'success', 185)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (186, 0, N'fail', 186)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (187, 2, N'success', 187)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (188, 2, N'success', 188)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (189, 2, N'success', 189)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (190, 2, N'success', 190)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (191, 0, N'fail', 191)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (192, 0, N'fail', 192)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (193, 2, N'success', 193)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (194, 2, N'success', 194)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (195, 2, N'success', 195)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (196, 2, N'success', 196)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (197, 2, N'success', 197)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (198, 2, N'success', 198)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (199, 2, N'success', 199)
GO
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (200, 0, N'fail', 200)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (201, 2, N'success', 201)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (202, 2, N'success', 202)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (203, 2, N'success', 203)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (204, 2, N'success', 204)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (205, 0, N'fail', 205)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (206, 0, N'fail', 206)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (207, 2, N'success', 207)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (208, 2, N'success', 208)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (209, 2, N'success', 209)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (210, 2, N'success', 210)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (211, 2, N'success', 211)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (212, 0, N'fail', 212)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (213, 2, N'success', 213)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (214, 2, N'success', 214)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (215, 2, N'success', 215)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (216, 0, N'fail', 216)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (217, 2, N'success', 217)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (218, 2, N'success', 218)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (219, 2, N'success', 219)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (220, 2, N'success', 220)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (221, 0, N'fail', 221)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (222, 0, N'fail', 222)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (223, 2, N'success', 223)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (224, 2, N'success', 224)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (225, 2, N'success', 225)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (226, 2, N'success', 226)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (227, 2, N'success', 227)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (228, 2, N'success', 228)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (229, 2, N'success', 229)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (230, 0, N'fail', 230)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (231, 2, N'success', 231)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (232, 2, N'success', 232)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (233, 2, N'success', 233)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (234, 2, N'success', 234)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (235, 0, N'fail', 235)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (236, 0, N'fail', 236)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (237, 2, N'success', 237)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (238, 2, N'success', 238)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (239, 2, N'success', 239)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (240, 2, N'success', 240)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (241, 2, N'success', 241)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (242, 0, N'fail', 242)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (243, 2, N'success', 243)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (244, 2, N'success', 244)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (245, 2, N'success', 245)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (246, 0, N'fail', 246)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (247, 2, N'success', 247)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (248, 2, N'success', 248)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (249, 2, N'success', 249)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (250, 2, N'success', 250)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (251, 0, N'fail', 251)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (252, 0, N'fail', 252)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (253, 2, N'success', 253)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (254, 2, N'success', 254)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (255, 2, N'success', 255)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (256, 2, N'success', 256)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (257, 2, N'success', 257)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (258, 2, N'success', 258)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (259, 2, N'success', 259)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (260, 2, N'success', 260)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (261, 2, N'success', 261)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (262, 2, N'success', 262)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (263, 2, N'success', 263)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (264, 2, N'success', 264)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (265, 0, N'fail', 265)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (266, 0, N'fail', 266)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (267, 2, N'success', 267)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (268, 2, N'success', 268)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (269, 2, N'success', 269)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (270, 2, N'success', 270)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (271, 2, N'success', 271)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (272, 0, N'fail', 272)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (273, 2, N'success', 273)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (274, 2, N'success', 274)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (275, 2, N'success', 275)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (276, 0, N'fail', 276)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (277, 2, N'success', 277)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (278, 2, N'success', 278)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (279, 2, N'success', 279)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (280, 2, N'success', 280)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (281, 0, N'fail', 281)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (282, 0, N'fail', 282)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (283, 2, N'success', 283)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (284, 2, N'success', 284)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (285, 2, N'success', 285)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (286, 2, N'success', 286)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (287, 2, N'success', 287)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (288, 2, N'success', 288)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (289, 2, N'success', 289)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (290, 0, N'fail', 290)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (291, 2, N'success', 291)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (292, 2, N'success', 292)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (293, 2, N'success', 293)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (294, 2, N'success', 294)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (295, 0, N'fail', 295)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (296, 0, N'fail', 296)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (297, 2, N'success', 297)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (298, 2, N'success', 298)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (299, 2, N'success', 299)
GO
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (300, 2, N'success', 300)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (301, 2, N'success', 301)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (302, 0, N'fail', 302)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (303, 2, N'success', 303)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (304, 2, N'success', 304)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (305, 2, N'success', 305)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (306, 0, N'fail', 306)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (307, 2, N'success', 307)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (308, 2, N'success', 308)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (309, 2, N'success', 309)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (310, 2, N'success', 310)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (311, 0, N'fail', 311)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (312, 0, N'fail', 312)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (313, 2, N'success', 313)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (314, 2, N'success', 314)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (315, 2, N'success', 315)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (316, 2, N'success', 316)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (317, 2, N'success', 317)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (318, 2, N'success', 318)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (319, 2, N'success', 319)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (320, 0, N'fail', 320)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (321, 2, N'success', 321)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (322, 2, N'success', 322)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (323, 2, N'success', 323)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (324, 2, N'success', 324)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (325, 0, N'fail', 325)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (326, 0, N'fail', 326)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (327, 2, N'success', 327)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (328, 2, N'success', 328)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (329, 2, N'success', 329)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (330, 2, N'success', 330)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (331, 2, N'success', 331)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (332, 0, N'fail', 332)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (333, 2, N'success', 333)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (334, 2, N'success', 334)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (335, 2, N'success', 335)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (336, 0, N'fail', 336)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (337, 2, N'success', 337)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (338, 2, N'success', 338)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (339, 2, N'success', 339)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (340, 2, N'success', 340)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (341, 0, N'fail', 341)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (342, 0, N'fail', 342)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (343, 2, N'success', 343)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (344, 2, N'success', 344)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (345, 2, N'success', 345)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (346, 2, N'success', 346)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (347, 2, N'success', 347)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (348, 2, N'success', 348)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (349, 2, N'success', 349)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (350, 0, N'fail', 350)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (351, 2, N'success', 351)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (352, 2, N'success', 352)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (353, 2, N'success', 353)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (354, 2, N'success', 354)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (355, 0, N'fail', 355)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (356, 0, N'fail', 356)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (357, 2, N'success', 357)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (358, 2, N'success', 358)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (359, 2, N'success', 359)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (360, 2, N'success', 360)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (361, 2, N'success', 361)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (362, 0, N'fail', 362)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (363, 2, N'success', 363)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (364, 2, N'success', 364)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (365, 2, N'success', 365)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (366, 0, N'fail', 366)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (367, 2, N'success', 367)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (368, 2, N'success', 368)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (369, 2, N'success', 369)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (370, 2, N'success', 370)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (371, 0, N'fail', 371)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (372, 0, N'fail', 372)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (373, 2, N'success', 373)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (374, 2, N'success', 374)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (375, 2, N'success', 375)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (376, 2, N'success', 376)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (377, 2, N'success', 377)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (378, 2, N'success', 378)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (379, 2, N'success', 379)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (380, 2, N'success', 380)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (381, 2, N'success', 381)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (382, 2, N'success', 382)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (383, 2, N'success', 383)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (384, 2, N'success', 384)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (385, 0, N'fail', 385)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (386, 0, N'fail', 386)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (387, 2, N'success', 387)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (388, 2, N'success', 388)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (389, 2, N'success', 389)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (390, 2, N'success', 390)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (391, 2, N'success', 391)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (392, 0, N'fail', 392)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (393, 2, N'success', 393)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (394, 2, N'success', 394)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (395, 2, N'success', 395)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (396, 0, N'fail', 396)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (397, 2, N'success', 397)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (398, 2, N'success', 398)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (399, 2, N'success', 399)
GO
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (400, 2, N'success', 400)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (401, 0, N'fail', 401)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (402, 0, N'fail', 402)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (403, 2, N'success', 403)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (404, 2, N'success', 404)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (405, 2, N'success', 405)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (406, 2, N'success', 406)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (407, 2, N'success', 407)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (408, 2, N'success', 408)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (409, 2, N'success', 409)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (410, 2, N'success', 410)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (411, 2, N'success', 411)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (412, 2, N'success', 412)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (413, 2, N'success', 413)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (414, 2, N'success', 414)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (415, 0, N'fail', 415)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (416, 0, N'fail', 416)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (417, 2, N'success', 417)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (418, 2, N'success', 418)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (419, 2, N'success', 419)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (420, 2, N'success', 420)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (421, 2, N'success', 421)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (422, 0, N'fail', 422)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (423, 2, N'success', 423)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (424, 2, N'success', 424)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (425, 2, N'success', 425)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (426, 0, N'fail', 426)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (427, 2, N'success', 427)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (428, 2, N'success', 428)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (429, 2, N'success', 429)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (430, 2, N'success', 430)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (431, 0, N'fail', 431)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (432, 0, N'fail', 432)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (433, 2, N'success', 433)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (434, 2, N'success', 434)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (435, 2, N'success', 435)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (436, 2, N'success', 436)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (437, 2, N'success', 437)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (438, 2, N'success', 438)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (439, 2, N'success', 439)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (440, 2, N'success', 440)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (441, 2, N'success', 441)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (442, 2, N'success', 442)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (443, 2, N'success', 443)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (444, 2, N'success', 444)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (445, 0, N'fail', 445)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (446, 0, N'fail', 446)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (447, 2, N'success', 447)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (448, 2, N'success', 448)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (449, 2, N'success', 449)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (450, 2, N'success', 450)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (451, 2, N'success', 451)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (452, 0, N'fail', 452)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (453, 2, N'success', 453)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (454, 2, N'success', 454)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (455, 2, N'success', 455)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (456, 0, N'fail', 456)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (457, 2, N'success', 457)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (458, 2, N'success', 458)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (459, 2, N'success', 459)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (460, 2, N'success', 460)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (461, 0, N'fail', 461)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (462, 0, N'fail', 462)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (463, 2, N'success', 463)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (464, 2, N'success', 464)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (465, 2, N'success', 465)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (466, 2, N'success', 466)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (467, 2, N'success', 467)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (468, 2, N'success', 468)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (469, 2, N'success', 469)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (470, 2, N'success', 470)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (471, 2, N'success', 471)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (472, 2, N'success', 472)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (473, 2, N'success', 473)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (474, 2, N'success', 474)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (475, 0, N'fail', 475)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (476, 0, N'fail', 476)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (477, 2, N'success', 477)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (478, 2, N'success', 478)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (479, 2, N'success', 479)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (480, 2, N'success', 480)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (481, 2, N'success', 481)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (482, 0, N'fail', 482)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (483, 2, N'success', 483)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (484, 2, N'success', 484)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (485, 2, N'success', 485)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (486, 0, N'fail', 486)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (487, 2, N'success', 487)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (488, 2, N'success', 488)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (489, 2, N'success', 489)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (490, 2, N'success', 490)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (491, 0, N'fail', 491)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (492, 0, N'fail', 492)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (493, 2, N'success', 493)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (494, 2, N'success', 494)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (495, 2, N'success', 495)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (496, 2, N'success', 496)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (497, 2, N'success', 497)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (498, 2, N'success', 498)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (499, 2, N'success', 499)
GO
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (500, 2, N'success', 500)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (501, 2, N'success', 501)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (502, 2, N'success', 502)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (503, 2, N'success', 503)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (504, 2, N'success', 504)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (505, 0, N'fail', 505)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (506, 0, N'fail', 506)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (507, 2, N'success', 507)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (508, 2, N'success', 508)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (509, 2, N'success', 509)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (510, 2, N'success', 510)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (511, 2, N'success', 511)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (512, 0, N'fail', 512)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (513, 2, N'success', 513)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (514, 2, N'success', 514)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (515, 2, N'success', 515)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (516, 0, N'fail', 516)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (517, 2, N'success', 517)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (518, 2, N'success', 518)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (519, 2, N'success', 519)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (520, 2, N'success', 520)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (521, 0, N'fail', 521)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (522, 0, N'fail', 522)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (523, 2, N'success', 523)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (524, 2, N'success', 524)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (525, 2, N'success', 525)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (526, 2, N'success', 526)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (527, 2, N'success', 527)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (528, 2, N'success', 528)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (529, 2, N'success', 529)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (530, 2, N'success', 530)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (531, 0, N'fail', 531)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (532, 0, N'fail', 532)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (533, 0, N'Fail', 533)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (534, 0, N'Fail', 534)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (535, 0, N'Fail', 535)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (536, 0, N'Fail', 536)
INSERT [dbo].[Orders_status] ([id], [status], [comment], [id_order]) VALUES (537, 0, N'Fail', 537)
SET IDENTITY_INSERT [dbo].[Orders_status] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (1, N'Cà Phê Đen Đá', N'https://res.cloudinary.com/minhchon/image/upload/v1619173944/Thecoffeeson/Cafeden_rwfthx.png', 28000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (2, N'Cà Phê Sữa Đá', N'https://res.cloudinary.com/minhchon/image/upload/v1619162162/Thecoffeeson/Cafesua_qvkole.png', 30000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (3, N'Bạc Xỉu', N'https://res.cloudinary.com/minhchon/image/upload/v1619162144/Thecoffeeson/Bacxiu_ci7zk5.png', 32000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (4, N'Capuchino', N'https://res.cloudinary.com/minhchon/image/upload/v1619162173/Thecoffeeson/Capuchino_vypirr.png', 35000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (5, N'Sữa Tươi Cà Phê', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162451/Thecoffeeson/Suatuoicafe_cv8qi2.png', 33000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (6, N'Trà Vải', N'https://res.cloudinary.com/minhchon/image/upload/v1619162325/Thecoffeeson/Travaihoahong_e8ctw7.png', 31000, 1, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (7, N'Trà Olong Sen Vàng', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162337/Thecoffeeson/Traolongsenvang_lb4ky6.png', 33000, 1, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (8, N'Trà Đào cam Sả', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162369/Thecoffeeson/Tradaocamsa_w1lmmf.png', 33000, 1, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (9, N'Trà Sữa Trân Châu	', N'https://res.cloudinary.com/minhchon/image/upload/v1619162811/Thecoffeeson/Trasuathapcam_bykpa5.png', 33000, 1, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (10, N'Yaourt Dâu', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162194/Thecoffeeson/yaourtdau_u33bnc.png', 33000, 1, 3)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (11, N'Yaourt Kiwi', N'https://res.cloudinary.com/minhchon/image/upload/v1619173560/Thecoffeeson/yaourtKiwi_th0lwc.png', 33000, 1, 3)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (12, N'Yaourt Việt Quất	', N'https://res.cloudinary.com/minhchon/image/upload/v1619173549/Thecoffeeson/yaourtvietquat_jbywen.png', 33000, 1, 3)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (13, N'Sữa Tươi', N'https://res.cloudinary.com/minhchon/image/upload/v1619173498/Thecoffeeson/Suatuoi_hc0rmq.png', 33000, 1, 4)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (14, N'Sinh Tố Phúc Bồn Tử', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162426/Thecoffeeson/Phucbontu_ixl8p0.png', 33000, 1, 5)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (15, N'Sinh Tố Việt Quất', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162302/Thecoffeeson/Vietquat_la7iz7.png', 33000, 1, 5)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (16, N'Sinh Tố Xoài', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162292/Thecoffeeson/Xoaidaxay_g5bxjs.png', 33000, 1, 5)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (17, N'Nước Chanh Dây', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162403/Thecoffeeson/Sodachanhday_gvliuy.png', 33000, 1, 6)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (18, N'Nước Ép Thơm', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162207/Thecoffeeson/Nuocepthom_lusahz.png', 33000, 1, 6)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (19, N'Nước Dưa Hấu	', N'https://res.cloudinary.com/minhchon/image/upload/v1619162246/Thecoffeeson/Nuocduahau_fdxksm.png', 33000, 1, 6)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (20, N'Nước Cam Ép', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162386/Thecoffeeson/Camvat_gweuve.png', 33000, 1, 6)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (21, N'Soda', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162184/Thecoffeeson/Soda_c1rixd.png', 33000, 1, 7)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (22, N'Khoai Môn Cream Cheese', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162437/Thecoffeeson/Khoaimonkemcheese_iuwzpx.png', 33000, 1, 7)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (23, N'Chocolate Đá Xay', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162798/Thecoffeeson/Chocolatedaxay_jxfii9.png', 33000, 1, 7)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (24, N'Chocolate Cream Cheese', N'	https://res.cloudinary.com/minhchon/image/upload/v1619162779/Thecoffeeson/Chocoladaxayphomai_w3sctu.png', 33000, 1, 7)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (25, N'Latte', N'https://product.hstatic.net/1000075078/product/latte-nong_ffcd92de11f74937bce4197823246d07_large.jpg', 38000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (26, N'Mocha', N'https://product.hstatic.net/1000075078/product/mocha-nong_66ebb6f03a874a4391fc80ad69264ea5_master.jpg', 42000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (27, N'Espresso', N'https://product.hstatic.net/1000075078/product/espresso-nong_4b32833e9a5f48768ea5d5d2a4df0303_large.jpg', 38000, 1, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (28, N'Cookies Đá Xay', N'https://product.hstatic.net/1000075078/product/cookie-da-xay_43c2bc99f313405aa253b803dcd59030_large.jpg', 33000, 2, 1)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (29, N'Hồng trà latte ', N'https://product.hstatic.net/1000075078/product/ht-latte-macchiato_fe7fa1571b974b48a5d750bd2e9e84eb_large.jpg', 31000, 2, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (30, N'Trà sưa Olong Nướng', N'https://product.hstatic.net/1000075078/product/oolong-nuong_6ce4a7b9cfba44509fcf1886a25e5b00_large.jpg', 33000, 2, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (31, N'Trà Đen MachiaTo cam Sả', N'https://product.hstatic.net/1000075078/product/tra-den-macchiato_facfdd980ca547be93974edb7f16d3b7_large.jpg', 33000, 2, 2)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (32, N'Chanh sả đá xay', N'https://product.hstatic.net/1000075078/product/chanh-sa-da-xay_fd95c70496714848a16a64c60b04ccf6_large.jpg', 38000, 2, 8)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (33, N'Đào việt quất đá xay', N'https://product.hstatic.net/1000075078/product/dao-viet-quat-da-xay_7e464338009c432985f8e3cdba6acb38_large.jpg', 42000, 2, 8)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (34, N'Phúc bồn tử đá xay', N'https://product.hstatic.net/1000075078/product/cam-pbt-da-xay_06ca55fce8e84389ab9d707f4bd753a7_large.jpg', 38000, 2, 8)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (35, N'Sinh tố việt quất', N'https://product.hstatic.net/1000075078/product/sinh-to-viet-quoc_75c00683e2aa4b8eb10e2b0d0e568a0c_large.jpg', 33000, 2, 8)
INSERT [dbo].[Products] ([id], [name], [image], [price], [status], [id_categories]) VALUES (36, N'Yogurt dưa lưới phát tài ', N'https://product.hstatic.net/1000075078/product/yogurt-dua-luoi_30e104322e764a8aa4794e09c1ff74ab_large.jpg', 31000, 2, 8)
SET IDENTITY_INSERT [dbo].[Products] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([id], [name]) VALUES (1, N'ROLE_ADMIN')
INSERT [dbo].[Roles] ([id], [name]) VALUES (3, N'ROLE_SUPERADMIN')
INSERT [dbo].[Roles] ([id], [name]) VALUES (2, N'ROLE_USER')
SET IDENTITY_INSERT [dbo].[Roles] OFF
SET IDENTITY_INSERT [dbo].[Ships_info] ON 

INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (1, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 1)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (2, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 2)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (3, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 3)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (4, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 4)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (5, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 5)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (6, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 6)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (7, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 7)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (8, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 8)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (9, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 9)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (10, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 10)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (11, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 11)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (12, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 12)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (13, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 13)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (14, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 14)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (15, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 15)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (16, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 16)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (17, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 17)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (18, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 18)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (19, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 19)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (20, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 20)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (21, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 21)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (22, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 22)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (23, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 23)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (24, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 24)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (25, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 25)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (26, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 26)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (27, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 27)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (28, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 28)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (29, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 29)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (30, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 30)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (31, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 31)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (32, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 32)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (33, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 33)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (34, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 34)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (35, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 35)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (36, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 36)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (37, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 37)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (38, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 38)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (39, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 39)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (40, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 40)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (41, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 41)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (42, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 42)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (43, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 43)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (44, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 44)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (45, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 45)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (46, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 46)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (47, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 47)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (48, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 48)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (49, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 49)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (50, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 50)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (51, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 51)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (52, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 52)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (53, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 53)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (54, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 54)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (55, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 55)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (56, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 56)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (57, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 57)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (58, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 58)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (59, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 59)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (60, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 60)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (61, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 61)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (62, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 62)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (63, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 63)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (64, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 64)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (65, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 65)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (66, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 66)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (67, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 67)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (68, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 68)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (69, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 69)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (70, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 70)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (71, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 71)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (72, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 72)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (73, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 73)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (74, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 74)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (75, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 75)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (76, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 76)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (77, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 77)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (78, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 78)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (79, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 79)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (80, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 80)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (81, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 81)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (82, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 82)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (83, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 83)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (84, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 84)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (85, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 85)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (86, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 86)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (87, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 87)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (88, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 88)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (89, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 89)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (90, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 90)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (91, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 91)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (92, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 92)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (93, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 93)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (94, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 94)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (95, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 95)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (96, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 96)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (97, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 97)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (98, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 98)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (99, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 99)
GO
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (100, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 100)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (101, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 101)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (102, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 102)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (103, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 103)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (104, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 104)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (105, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 105)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (106, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 106)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (107, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 107)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (108, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 108)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (109, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 109)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (110, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 110)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (111, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 111)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (112, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 112)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (113, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 113)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (114, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 114)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (115, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 115)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (116, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 116)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (117, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 117)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (118, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 118)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (119, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 119)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (120, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 120)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (121, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 121)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (122, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 122)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (123, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 123)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (124, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 124)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (125, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 125)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (126, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 126)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (127, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 127)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (128, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 128)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (129, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 129)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (130, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 130)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (131, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 131)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (132, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 132)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (133, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 133)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (134, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 134)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (135, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 135)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (136, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 136)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (137, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 137)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (138, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 138)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (139, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 139)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (140, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 140)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (141, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 141)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (142, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 142)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (143, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 143)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (144, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 144)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (145, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 145)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (146, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 146)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (147, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 147)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (148, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 148)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (149, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 149)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (150, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 150)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (151, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 151)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (152, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 152)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (153, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 153)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (154, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 154)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (155, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 155)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (156, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 156)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (157, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 157)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (158, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 158)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (159, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 159)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (160, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 160)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (161, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 161)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (162, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 162)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (163, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 163)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (164, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 164)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (165, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 165)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (166, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 166)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (167, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 167)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (168, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 168)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (169, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 169)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (170, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 170)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (171, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 171)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (172, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 172)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (173, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 173)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (174, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 174)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (175, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 175)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (176, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 176)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (177, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 177)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (178, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 178)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (179, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 179)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (180, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 180)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (181, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 181)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (182, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 182)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (183, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 183)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (184, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 184)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (185, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 185)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (186, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 186)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (187, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 187)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (188, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 188)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (189, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 189)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (190, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 190)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (191, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 191)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (192, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 192)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (193, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 193)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (194, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 194)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (195, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 195)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (196, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 196)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (197, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 197)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (198, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 198)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (199, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 199)
GO
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (200, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 200)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (201, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 201)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (202, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 202)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (203, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 203)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (204, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 204)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (205, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 205)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (206, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 206)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (207, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 207)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (208, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 208)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (209, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 209)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (210, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 210)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (211, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 211)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (212, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 212)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (213, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 213)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (214, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 214)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (215, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 215)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (216, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 216)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (217, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 217)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (218, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 218)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (219, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 219)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (220, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 220)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (221, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 221)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (222, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 222)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (223, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 223)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (224, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 224)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (225, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 225)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (226, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 226)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (227, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 227)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (228, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 228)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (229, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 229)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (230, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 230)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (231, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 231)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (232, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 232)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (233, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 233)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (234, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 234)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (235, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 235)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (236, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 236)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (237, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 237)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (238, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 238)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (239, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 239)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (240, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 240)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (241, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 241)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (242, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 242)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (243, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 243)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (244, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 244)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (245, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 245)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (246, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 246)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (247, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 247)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (248, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 248)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (249, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 249)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (250, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 250)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (251, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 251)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (252, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 252)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (253, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 253)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (254, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 254)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (255, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 255)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (256, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 256)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (257, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 257)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (258, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 258)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (259, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 259)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (260, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 260)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (261, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 261)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (262, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 262)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (263, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 263)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (264, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 264)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (265, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 265)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (266, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 266)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (267, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 267)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (268, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 268)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (269, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 269)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (270, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 270)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (271, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 271)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (272, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 272)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (273, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 273)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (274, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 274)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (275, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 275)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (276, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 276)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (277, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 277)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (278, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 278)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (279, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 279)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (280, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 280)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (281, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 281)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (282, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 282)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (283, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 283)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (284, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 284)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (285, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 285)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (286, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 286)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (287, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 287)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (288, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 288)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (289, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 289)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (290, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 290)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (291, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 291)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (292, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 292)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (293, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 293)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (294, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 294)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (295, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 295)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (296, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 296)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (297, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 297)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (298, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 298)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (299, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 299)
GO
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (300, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 300)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (301, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 301)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (302, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 302)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (303, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 303)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (304, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 304)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (305, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 305)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (306, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 306)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (307, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 307)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (308, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 308)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (309, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 309)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (310, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 310)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (311, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 311)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (312, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 312)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (313, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 313)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (314, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 314)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (315, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 315)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (316, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 316)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (317, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 317)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (318, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 318)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (319, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 319)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (320, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 320)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (321, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 321)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (322, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 322)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (323, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 323)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (324, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 324)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (325, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 325)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (326, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 326)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (327, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 327)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (328, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 328)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (329, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 329)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (330, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 330)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (331, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 331)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (332, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 332)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (333, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 333)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (334, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 334)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (335, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 335)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (336, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 336)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (337, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 337)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (338, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 338)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (339, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 339)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (340, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 340)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (341, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 341)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (342, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 342)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (343, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 343)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (344, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 344)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (345, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 345)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (346, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 346)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (347, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 347)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (348, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 348)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (349, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 349)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (350, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 350)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (351, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 351)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (352, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 352)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (353, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 353)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (354, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 354)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (355, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 355)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (356, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 356)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (357, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 357)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (358, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 358)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (359, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 359)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (360, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 360)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (361, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 361)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (362, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 362)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (363, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 363)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (364, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 364)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (365, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 365)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (366, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 366)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (367, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 367)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (368, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 368)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (369, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 369)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (370, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 370)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (371, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 371)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (372, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 372)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (373, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 373)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (374, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 374)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (375, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 375)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (376, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 376)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (377, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 377)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (378, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 378)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (379, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 379)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (380, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 380)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (381, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 381)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (382, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 382)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (383, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 383)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (384, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 384)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (385, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 385)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (386, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 386)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (387, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 387)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (388, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 388)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (389, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 389)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (390, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 390)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (391, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 391)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (392, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 392)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (393, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 393)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (394, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 394)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (395, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 395)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (396, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 396)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (397, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 397)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (398, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 398)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (399, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 399)
GO
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (400, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 400)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (401, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 401)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (402, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 402)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (403, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 403)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (404, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 404)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (405, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 405)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (406, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 406)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (407, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 407)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (408, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 408)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (409, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 409)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (410, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 410)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (411, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 411)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (412, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 412)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (413, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 413)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (414, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 414)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (415, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 415)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (416, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 416)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (417, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 417)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (418, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 418)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (419, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 419)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (420, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 420)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (421, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 421)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (422, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 422)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (423, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 423)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (424, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 424)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (425, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 425)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (426, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 426)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (427, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 427)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (428, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 428)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (429, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 429)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (430, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 430)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (431, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 431)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (432, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 432)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (433, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 433)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (434, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 434)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (435, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 435)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (436, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 436)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (437, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 437)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (438, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 438)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (439, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 439)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (440, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 440)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (441, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 441)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (442, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 442)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (443, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 443)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (444, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 444)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (445, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 445)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (446, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 446)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (447, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 447)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (448, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 448)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (449, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 449)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (450, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 450)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (451, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 451)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (452, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 452)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (453, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 453)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (454, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 454)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (455, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 455)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (456, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 456)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (457, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 457)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (458, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 458)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (459, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 459)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (460, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 460)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (461, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 461)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (462, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 462)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (463, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 463)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (464, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 464)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (465, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 465)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (466, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 466)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (467, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 467)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (468, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 468)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (469, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 469)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (470, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 470)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (471, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 471)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (472, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 472)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (473, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 473)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (474, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 474)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (475, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 475)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (476, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 476)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (477, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 477)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (478, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 478)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (479, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 479)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (480, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 480)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (481, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 481)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (482, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 482)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (483, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 483)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (484, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 484)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (485, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 485)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (486, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 486)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (487, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 487)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (488, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 488)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (489, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 489)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (490, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 490)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (491, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 491)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (492, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 492)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (493, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 493)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (494, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 494)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (495, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 495)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (496, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 496)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (497, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 497)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (498, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 498)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (499, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 499)
GO
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (500, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 500)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (501, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 501)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (502, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 502)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (503, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 503)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (504, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 504)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (505, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 505)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (506, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 506)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (507, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 507)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (508, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 508)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (509, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 509)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (510, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 510)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (511, N'Hắc Lịch', N'002 Thái Thượng Lãng Ong Quận 8', N'0966888444', N'Giao Bình thường', 15000, 511)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (512, N'Minh Ngọc', N'214/12 Lê Quang Định F1 Quận Gò Vấp', N'0933888426', N'Giao Bình thường', 15000, 512)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (513, N'Thái Vũ', N'11 Nguyên Hồng F1 Gò Vấp', N'0736652477', N'Giao Bình thường', 15000, 513)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (514, N'Minh Lan', N'22 Hoàng Sa F4 Quận 3', N'0778996542', N'Giao Bình thường', 15000, 514)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (515, N'Nguyên Thi Mai', N'214 Nguyễn Trãi Phường 2 Quận 5', N'0765258841', N'Giao nhanh', 15000, 515)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (516, N'Hải Nam', N'33 Ngô Gia Tự Phương 4 Quận 5', N'0111222322', N'Giao bình thường', 15000, 516)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (517, N'Nguyễn Thị Cúc', N'12 Đương quãn hàm f6  quận Gò Vấp', N'0111222311', N'Giao bình thường', 15000, 517)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (518, N'Trần thị Diệp', N'512/2/3 Lê Văn Sỹ Phường 6 Quận 3', N'0665559982', N'Giao Bình thường', 15000, 518)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (519, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 519)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (520, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 520)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (521, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 521)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (522, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 522)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (523, N'Nguyễn thị Ngọc Mai', N'14 Ngô Quyền f2 Quận tân Bình', N'0999856441', N'Giao Bình thường', 15000, 523)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (524, N'Đinh bá thành', N'112 Nở Trang Long f9 Quận Bình Thạnh', N'0989856332', N'Giao Bình thường', 15000, 524)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (525, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 525)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (526, N'Nguyễn Thị Phương Uyên', N'21 Nguyễn Thái Sơn f7 Gò Vấp', N'0989547761', N'Giao Bình thường', 15000, 526)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (527, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 527)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (528, N'Trần Hào', N'233 Quang Trung f5 Gò Vấp', N'0966855788', N'Giao Bình thường', 15000, 528)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (529, N'Ngô Lâm', N'004 Phan Văn Trị f1 Quận Gò Vấp', N'0254785541', N'Giao Bình thường', 15000, 529)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (530, N'Nguyên Hông Mai', N'11 Đinh Tiên Hoàng F2 Quận 1', N'065794452', N'Giao Bình thường', 15000, 530)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (531, N'Nguyễn Minh', N'312 Cống lở f6 Quận6', N'053258674', N'Giao Bình thường', 15000, 531)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (532, N'Trần Hoàng Yến', N'24 Trần Huy Liệu F1 Quận 3', N'0986865547', N'Giao Bình thường', 15000, 532)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (533, N'Nguyễn Tiến Đạt', N'225/33/34 Lê Văn Quới', N'0964138876', N'Giao hàng nhanh', 15000, 533)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (534, N'Nguyễn Tiến Đạt', N'', N'0964138876', N'', 15000, 534)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (535, N'Nguyễn Tiến Đạt', N'225/33/34 Lê Văn Quới', N'0964138876', N'Giao hàng nhanh', 15000, 535)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (536, N'Nguyễn Tiến Đạt', N'225/33/34 Lê Văn Quới', N'0964138876', N'Giao hàng nhanh', 15000, 536)
INSERT [dbo].[Ships_info] ([id], [name_cus], [address], [phone_cus], [note], [price], [id_order]) VALUES (537, N'Nguyễn Tiến Đạt', N'225/33/34 Lê Văn Quới', N'0964138876', N'Giao hàng nhanh', 15000, 537)
SET IDENTITY_INSERT [dbo].[Ships_info] OFF
SET IDENTITY_INSERT [dbo].[Sizes] ON 

INSERT [dbo].[Sizes] ([id], [Product_size], [status], [Product_price]) VALUES (1, N'S', 1, 1)
INSERT [dbo].[Sizes] ([id], [Product_size], [status], [Product_price]) VALUES (2, N'M', 1, 1.2)
INSERT [dbo].[Sizes] ([id], [Product_size], [status], [Product_price]) VALUES (3, N'L', 1, 1.25)
INSERT [dbo].[Sizes] ([id], [Product_size], [status], [Product_price]) VALUES (4, N'XL', 1, 1.3)
SET IDENTITY_INSERT [dbo].[Sizes] OFF
SET IDENTITY_INSERT [dbo].[Stores] ON 

INSERT [dbo].[Stores] ([id], [name], [phone], [email], [address]) VALUES (1, N'TheCoffeeSon', N'0989697995', N'TheSonCoffee@gmail.com', N'2 đường Trường Sơn Quận tân Bình TP.HCM')
INSERT [dbo].[Stores] ([id], [name], [phone], [email], [address]) VALUES (2, N'TheCoffeeLuan', N'0764265512', N'TheLuanCoffee@gmail.com', N'1 Đường Nguyên Hồng Phường 1 Quận Gò Vấp TP.HCM')
INSERT [dbo].[Stores] ([id], [name], [phone], [email], [address]) VALUES (3, N'TheCoffeeHau', N'0794751044', N'TheHauCoffee@gmail.com', N'225 Đường Lê Văn Quới  Phường Bình Trị Đông Quận Bình Tân TP.HCM')
INSERT [dbo].[Stores] ([id], [name], [phone], [email], [address]) VALUES (4, N'TheCoffeeMinh', N'0376571409', N'TheMinhCoffee@gmail.com', N'1 Đường Chương Dương Phường Linh Chiểu TP.ThuDuc TP.HCM')
INSERT [dbo].[Stores] ([id], [name], [phone], [email], [address]) VALUES (5, N'TheCoffeeDat', N'0964138876', N'ThedatCoffee@gmail.com', N' Đường số 9 Phường 6 Quận 4 TP.HCM')
SET IDENTITY_INSERT [dbo].[Stores] OFF
SET IDENTITY_INSERT [dbo].[Vouchers] ON 

INSERT [dbo].[Vouchers] ([id], [status], [value], [start_time], [end_time], [quanity]) VALUES (1, 1, 5000, CAST(N'2020-12-04' AS Date), CAST(N'2021-05-10' AS Date), 10)
INSERT [dbo].[Vouchers] ([id], [status], [value], [start_time], [end_time], [quanity]) VALUES (2, 1, 10000, CAST(N'2020-11-04' AS Date), CAST(N'2021-05-10' AS Date), 5)
INSERT [dbo].[Vouchers] ([id], [status], [value], [start_time], [end_time], [quanity]) VALUES (3, 1, 7000, CAST(N'2020-12-04' AS Date), CAST(N'2021-05-10' AS Date), 10)
INSERT [dbo].[Vouchers] ([id], [status], [value], [start_time], [end_time], [quanity]) VALUES (4, 1, 12000, CAST(N'2020-09-04' AS Date), CAST(N'2021-05-10' AS Date), 5)
INSERT [dbo].[Vouchers] ([id], [status], [value], [start_time], [end_time], [quanity]) VALUES (5, 1, 4000, CAST(N'2020-10-04' AS Date), CAST(N'2021-05-10' AS Date), 10)
INSERT [dbo].[Vouchers] ([id], [status], [value], [start_time], [end_time], [quanity]) VALUES (6, 1, 15000, CAST(N'2020-02-04' AS Date), CAST(N'2021-05-10' AS Date), 5)
SET IDENTITY_INSERT [dbo].[Vouchers] OFF
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Accounts__F3DBC572BF3513ED]    Script Date: 5/3/2021 7:04:07 PM ******/
ALTER TABLE [dbo].[Accounts_info] ADD UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UQ__Roles__72E12F1B67C91009]    Script Date: 5/3/2021 7:04:07 PM ******/
ALTER TABLE [dbo].[Roles] ADD UNIQUE NONCLUSTERED 
(
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Accounts_info]  WITH CHECK ADD FOREIGN KEY([id_role])
REFERENCES [dbo].[Roles] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Bills]  WITH CHECK ADD FOREIGN KEY([id_order])
REFERENCES [dbo].[Orders] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Feedbacks]  WITH CHECK ADD FOREIGN KEY([id_cus])
REFERENCES [dbo].[Accounts_info] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Feedbacks]  WITH CHECK ADD FOREIGN KEY([id_product])
REFERENCES [dbo].[Products] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([id_cus])
REFERENCES [dbo].[Accounts_info] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([id_voucher])
REFERENCES [dbo].[Vouchers] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders_Detail]  WITH CHECK ADD FOREIGN KEY([id_order])
REFERENCES [dbo].[Orders] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders_Detail]  WITH CHECK ADD FOREIGN KEY([id_product])
REFERENCES [dbo].[Products] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders_Detail]  WITH CHECK ADD FOREIGN KEY([id_size])
REFERENCES [dbo].[Sizes] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Orders_status]  WITH CHECK ADD FOREIGN KEY([id_order])
REFERENCES [dbo].[Orders] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD FOREIGN KEY([id_categories])
REFERENCES [dbo].[Categories] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Ships_info]  WITH CHECK ADD FOREIGN KEY([id_order])
REFERENCES [dbo].[Orders] ([id])
ON DELETE CASCADE
GO
/****** Object:  StoredProcedure [dbo].[allProduct]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  CREATE PROCEDURE [dbo].[allProduct]
 as
 select Product,sum(amount)as amout,sum(total)as total
 from orders_view 
 group by Product


GO
/****** Object:  StoredProcedure [dbo].[checkOrder]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--select * from VoucherUser_View

CREATE PROCEDURE [dbo].[checkOrder]
 as
select od.id,od.date_created ,ship.id as ship ,b.id as id_bill,ost.id as status 
--select*
from Orders od
--full join Orders_Detail odt on od.id = odt.id_order
full join Ships_info ship on ship.id_order=od.id
full join bills b on b.id_order = od.id
full join Orders_status ost on ost.id_order = od.id


GO
/****** Object:  StoredProcedure [dbo].[count_bill]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE PROCEDURE [dbo].[count_bill] 
AS
  select count(id_order) 
  from orders_viewsussecc

GO
/****** Object:  StoredProcedure [dbo].[doanhThuAll]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 --drop PROCEDURE doanhThuAll 
 --go
 CREATE PROCEDURE [dbo].[doanhThuAll] 
AS
 select MONTH(date_sale)as month, year(date_sale)as year ,sum(total) as total
 from orders_view
 group by year(date_sale),MONTH(date_sale)
  order by year(date_sale)desc,MONTH(date_sale) desc


GO
/****** Object:  StoredProcedure [dbo].[doanhthunam]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 --drop PROCEDURE doanhthunam
 CREATE PROCEDURE [dbo].[doanhthunam] @year int
AS
select MONTH(date_sale)as month ,sum(total) as total
 from orders_view
 where year(date_sale)= @year
 group by MONTH(date_sale)
 order by MONTH(date_sale) desc


GO
/****** Object:  StoredProcedure [dbo].[doanhthunhieunam]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--drop PROCEDURE doanhthunhieunam
 CREATE PROCEDURE [dbo].[doanhthunhieunam] 
AS
select year(date_sale)as year ,sum(total) as total
 from orders_view
 --where year(date_sale)= @year
 group by year(date_sale)
 order by year(date_sale)desc


GO
/****** Object:  StoredProcedure [dbo].[doanhThuToday]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 --drop PROCEDURE doanhThuToday 
 CREATE PROCEDURE [dbo].[doanhThuToday]
 as 
 select* from Today_view
 --select product, MONTH(date_sale)as month, year(date_sale)as year ,sum(total) as total
 --from orders_view
 --where day(date_sale)= day(getdate()) and MONTH(date_sale)= month(getdate()) and year(date_sale) =year(getdate())
 --group by product,MONTH(date_sale),year(date_sale)


GO
/****** Object:  StoredProcedure [dbo].[findFeedbacksbyProduct]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec findOrder_statusUser @status =1 ,@id_cus =7
--select*from orders_status
 --Voucher======================

  --select * from Vouchers

  --select * from Vouchers where status =1
--=======
-- select * from Feedbacks where id_cus =1
CREATE PROCEDURE [dbo].[findFeedbacksbyProduct] @id int
 as
select * from FeedBacks where id_product = @id


GO
/****** Object:  StoredProcedure [dbo].[findOrder]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ban tong
--drop PROCEDURE findOrder
CREATE PROCEDURE [dbo].[findOrder] 
AS
select * from orders_view 


GO
/****** Object:  StoredProcedure [dbo].[findOrder_status]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[findOrder_status] @status int
 as
 --select  b.id, b.date_create,sum( odt.amount*pr.price*sz.Product_price) as total,b.id_order
 select od.id ,od.date_created,od.payment,od.id_cus,od.id_voucher
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
where ost.status = @status
--group by b.id , b.date_create ,b.id_order 


GO
/****** Object:  StoredProcedure [dbo].[findOrder_statusUser]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec findOrder_status @status =2
--drop PROCEDURE findOrder_statusUser
CREATE PROCEDURE [dbo].[findOrder_statusUser] @status int ,@id_cus int
 as
 select ost.id,ost.comment,ost.status,ost.id_order
from Orders od
inner join Orders_Detail odt on od.id = odt.id_order
inner join Products pr on odt.id_product = pr.id
inner join Sizes sz on odt.id_size = sz.id
inner join bills b on b.id_order = od.id
inner join Orders_status ost on ost.id_order = od.id
where ost.status = @status and od.id_cus =@id_cus
--group by b.id , b.date_create ,b.id_order 


GO
/****** Object:  StoredProcedure [dbo].[findOrderByCustomer]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


  CREATE PROCEDURE [dbo].[findOrderByCustomer] @id_cus int
 as
 select * from orders where id_cus = @id_cus


GO
/****** Object:  StoredProcedure [dbo].[findOrderbyid]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[findOrderbyid] @idod int
AS
select * from orders_view where id_order = @idod


GO
/****** Object:  StoredProcedure [dbo].[findProduct]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --exec softProduct

 --find san pham theo ten
 CREATE PROCEDURE [dbo].[findProduct] @pr nvarchar(50)
 as
 select *
 from product_view 
 where name like '%'+@pr+'%' 


GO
/****** Object:  StoredProcedure [dbo].[findShipByIdOrder]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --exec findProduct @pr = ca



-- tìm kiếm thông tin ship theo đơn hàng
 CREATE PROCEDURE [dbo].[findShipByIdOrder] @id int
 as
select* from Ships_info
 where id_order= @id 


GO
/****** Object:  StoredProcedure [dbo].[product_statisticMon]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
  --drop PROCEDURE product_statisticMon
  --go

   CREATE PROCEDURE [dbo].[product_statisticMon] @month  int ,@year int
AS

 select Product,sum(amount)as amout,sum(total)as total
 from orders_view
 where Month(date_sale) =@month and year(date_sale) =@year
 group by Product,Month(date_sale)
 order by MONTH(date_sale) desc



GO
/****** Object:  StoredProcedure [dbo].[product_statisticYear]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

   CREATE PROCEDURE [dbo].[product_statisticYear] @year int
AS
 select Product,sum(amount)as amout,sum(total)as total
 from orders_view
  where  year(date_sale) = @year
 group by Product



GO
/****** Object:  StoredProcedure [dbo].[searchProduct]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- thong ke san pham------------------------------------------
--drop PROCEDURE searchProduct
--go
 CREATE PROCEDURE [dbo].[searchProduct] @pr nvarchar(50),@mon int ,@yr int
 as
 select Product,month(date_sale) as month ,sum(amount)as amout,sum(total)as total
 from orders_view 
 where Product like '%'+@pr+'%' and month(date_sale) = @mon and year(date_sale) = @yr
 group by Product , month(date_sale)
 order by MONTH(date_sale) desc


GO
/****** Object:  StoredProcedure [dbo].[softProduct]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

  CREATE PROCEDURE [dbo].[softProduct] 
 as
 select *
 from product_view 
 order by status desc 


GO
/****** Object:  StoredProcedure [dbo].[sumdoanhthu]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 CREATE PROCEDURE [dbo].[sumdoanhthu] 
AS
select sum(total) as total
 from orders_view
 --where year(date_sale)= @year
-- group by total


GO
/****** Object:  StoredProcedure [dbo].[yearCall]    Script Date: 5/3/2021 7:04:07 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --xuat year
  --drop PROCEDURE yearCall 
  --go
 CREATE PROCEDURE [dbo].[yearCall] 
AS
  select YEAR(date_sale) from orders_view 
group by YEAR(date_sale)
 order by year(date_sale) desc


GO
USE [master]
GO
ALTER DATABASE [thecoffeeson] SET  READ_WRITE 
GO
