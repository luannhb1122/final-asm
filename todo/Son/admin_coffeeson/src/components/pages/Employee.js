import React, { useEffect, useState, Fragment } from 'react';
import Employee from '../Axios/AxiosServicer/employee';
import DataTable from 'react-data-table-component';
import './Style.css'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Modal } from 'react-bootstrap';

export default function Emp(props) {
	// Khai báo giá trị
	const initialproducttate = {
		id: "",
		username: "",
		password: "",
		status: "",
		start_date: "",
		account_Info: {
			id: "",
			name: "",
			phone: "",
			email: "",
			birthday: "",
			gender: true,
			picture: ""
		}
	};

	const initialproducttate1 = {
		id: "",
		name: "",
		phone: "",
		email: "",
		birthday: "",
		gender: "",
		picture: ""
	};


	//Khai báo set và get mảng
	const [Emp, setEmp] = useState([]);
	//Khai báo set và get mảng
	const [Emps, setEmps] = useState([]);
	const [EmpItem, setEmpItem] = useState(initialproducttate);
	const [EmpItema, setEmpItema] = useState(initialproducttate1);
	const [index, setindex] = useState(0);
	const [message, setMessage] = useState('');
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALlemp();
	}, []);
	const GetALlemp = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Employee.getAllemployee()
			.then((response) => {
				setEmp(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	}
	const setActiveTutorial = (Emp, index) => {
		setEmpItem(Emp);
		setEmpItema(Emp.account_Info);
		setindex(index);
		handleShowModal();
		console.log(Emp);
		console.log(Emp.account_Info);
		console.log(EmpItema);
	};

	const UpdateProduct = () => {
		var data = {
			id: parseInt(EmpItem.id),
			username: EmpItem.username,
			password: EmpItem.password,
			status: true,
			start_date: EmpItem.start_date,
			account_Info: {
				id: EmpItema.id,
				name: EmpItema.name,
				phone: EmpItema.phone,
				email: EmpItema.email,
				birthday: EmpItema.birthday,
				gender: (EmpItema.gender === "Nam") ? true : false,
				picture: EmpItema.picture
			}
		}
		console.log(EmpItem)
		console.log(data);
		Employee.updateemployee(EmpItem.id, data)
			.then((response) => {
				console.log(response.data);
				console.log(data);
				/* 	setMessage('Thành Công!'); */
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};


	//Xóa
	const deleteTutorial = () => {
		Employee.removeemployee(EmpItem.id)
			.then((response) => {
				console.log(response.data);
				setMessage('Xóa Thành Công!');
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};


	function handleInputChange(evt) {
		const value = evt.target.value;
		setEmpItem({
			...EmpItem,
			[evt.target.name]: value,
		});
		setEmpItema({
			...EmpItema,
			[evt.target.name]: value,
		});
	}
	const [show, setShow] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'username',
			selector: 'username',
			sortable: true
		},
		{
			name: 'password',
			selector: 'password',
			sortable: true
		},
		{
			name: 'start_date',
			selector: 'start_date',
			sortable: true
		},
/* 		{
			name: 'Id',
			selector: 'account_Info.id',
			sortable: true
		}, */
		{
			name: 'Name',
			selector: 'account_Info.name',
			sortable: true
		},
		{
			name: 'phone',
			selector: 'account_Info.phone',
			sortable: true
		},
		{
			name: 'email',
			selector: 'account_Info.email',
			sortable: true
		},
		{
			name: 'birthday',
			selector: 'account_Info.birthday',
			sortable: true,
			
		},
		{
			name: 'picture',
			sortable: true,
			cell: (row) => (
				<img
					height="150px"
					width="256px"
					alt=""
					src={`https://res.cloudinary.com/minhchon/image/upload/v1617690557/Thecoffeeson/${row.image}`}
				/>
			)
		},
	/* 	{
			name: 'picture',
			selector: 'account_Info.picture',
			sortable: true,
		}, */
		{
			name: 'Chức Năng',
			key: 'action',
			text: 'Action',
			cell: (Emp, index) => {
				return (
					<Fragment>
						{/*  	<UpdateProduct/> */}
						<button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(Emp, index)}>
							<i className="ft-edit-1">Cập Nhật</i>
						</button>
					</Fragment>
				);
			}
		}

	];

	const refreshList = () => {
		GetALlemp();
	};
	const refreshTable = () => {
		GetALlemp();
	};

	//CSS DATATABLE
	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900',

			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},

		headCells: {
			style: {
				fontSize: '12px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'

			},
		},
		cells: {
			style: {
				fontSize: '12px',
				paddingLeft: '0 8px',
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			},
		},
	};


	return (
		<div>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
							<Link className="breadcrumb-item" to="/Product">Tất cả sản phẩm</Link>
						</li>
					</ul>
				</div>
			</div>
			<button
				className="btn btn-sm btn-outline-success"
				type="button"
				id="button-addon2"
				onClick={refreshList}
			>
				<i className="ft-refresh-ccw"></i>&nbsp;Refresh
						</button>
			{/*  Hiện thi Table */}
			<div className="card mt-2" >
				<DataTable
					customStyles={customStyles}
					title="Tất Cả Nhân Viên" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={Emp} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
				/* 	selectableRows */
				/>
			</div>
			{EmpItem ? (
				<Modal
					dialogClassName="dialogModal"
					show={show}
					onHide={handleCloseModal}
					backdrop="static"
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title className="tille_Modal">Cập Nhật Thông tin</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="container">
							<section className="panel panel-default">
								{/* // nhập tên sản phẩm */}
								<div className="form-group row ">
									<label className="col-sm-3 control-label">username:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="username"
											name="username"
											value={EmpItem.username}
											onChange={handleInputChange}
											placeholder="username"
										/>
									</div>
								</div>
								{/*    Giá */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">password:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="password"
											value={EmpItem.password}
											onChange={handleInputChange}
											name="password"
											placeholder="password"
										/>
									</div>
								</div>
								<div className="form-group row">
									<label className="col-sm-3 control-label">start_date:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="start_date"
											value={EmpItem.start_date}
											onChange={handleInputChange}
											name="start_date"
											placeholder="start_date"
										/>
									</div>
								</div>
								{/*   load hình anh */}
								{/* 		account_Info */}

								{/* name */}
{/* 								<div className="form-group row">
									<label className="col-sm-3 control-label">id:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="id"
											value={EmpItema.id}
											onChange={handleInputChange}
											name="id"
											placeholder="id"
										/>
									</div>
								</div> */}

								{/* name */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">name:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="name"
											value={EmpItema.name}
											onChange={handleInputChange}
											name="name"
											placeholder="name"
										/>
									</div>
								</div>
								{/* phone */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">phone:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="phone"
											value={EmpItema.phone}
											onChange={handleInputChange}
											name="phone"
											placeholder="phone"
										/>
									</div>
								</div>
								{/* email */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">email:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="email"
											value={EmpItema.email}
											onChange={handleInputChange}
											name="email"
											placeholder="email"
										/>
									</div>
								</div>

								{/* email */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">birthday:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="birthday"
											value={EmpItema.birthday}
											onChange={handleInputChange}
											name="birthday"
											placeholder="birthday"
										/>
									</div>
								</div>
								{/* gender */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">gender:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="gender"
											value={(EmpItema.gender === true) ? "Nam" : "Nữ"}
											onChange={handleInputChange
											}
											name="gender"
											placeholder="gender"
										/>
									</div>
								</div>

								{/* gender */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">picture:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="picture"
											value={EmpItema.picture}
											onChange={handleInputChange}
											name="picture"
											placeholder="picture"
										/>
									</div>
								</div>
							</section>
							{/*  Nút button Update */}
							<div className="form-group row pt-2 div-button">
								<div className="col-sm-12">
									<button type="submit" className="btn btn-sm btn-success" onClick={UpdateProduct}>
										Cập Nhật
									</button>
									<button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
										Xóa
									</button>
								</div>
							</div>
							<p>{message}</p>
						</div>
					</Modal.Body>
				</Modal>
			) : (
				<div>
					<br />
					<p>Please click on a Tutorial...</p>
				</div>
			)}
		</div>
	);
}

