import React, { useEffect, Fragment, useState } from 'react';
import Data from '../Axios/AxiosServicer/Product';
import Category from '../Axios/AxiosServicer/category';
import DataTable from 'react-data-table-component';
import { Modal } from 'react-bootstrap';
import AddProduct from '../pages/AddProduct';
import './Style.css'
/* import './UpdateProduct.css'; */
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
/* import { BsArrowRepeat, BsCalendar, BsSearch } from 'react-icons/bs'; */

export default function Customer(props) {
	// Khai báo giá trị
	const initialproducttate = {
		id: "",
		name: "",
		image: "",
		price: "",
		status:"",
		categories:{
			id: ""
		}
	};
	
	//Khai báo set và get mảng
	const [ product, setproduct ] = useState([]);
	const [ productItem, setproductItem ] = useState(initialproducttate);
	const [ index, setindex ] = useState(0);
	const [ message, setMessage ] = useState('');
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetAllProduct();
	}, []);
	const GetAllProduct = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Data.getAllproduct()
			.then((response) => {
				setproduct(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const setActiveTutorial = (product, index) => {
		setproductItem(product);
		setindex(index);
		handleShowModal();
	};
	// Sự kiện nhập giá trị vào form
	function handleInputChange(evt) {
		const value = evt.target.value;
		setproductItem({
			...productItem,
			[evt.target.name]: value
		});
	}

	// cập nhật dữ liệu sản phẩm
	const UpdateProduct = () => {
		var data = {
			name: productItem.name,
			price: parseFloat(productItem.price),
			image: productItem.image,
			categories: {
				id: parseInt(productItem.categories)
			}
		};
		Data.updateproduct(productItem.id, data)
			.then((response) => {
				console.log(response.data);
				setMessage('Thành Công!');
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//duyệt vòng lặp loại sản phẩm
	const [ list, setList ] = React.useState([]);
	useEffect(() => {
		getAlltype();
	}, []);
	const getAlltype = () => {
		Category.getAllcategory()
			.then((response) => {
				setList(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Set selected default value
	const optionCategory = list.map((item) => {
		if (item.id === productItem.categories.id) {
			return (
				<option value={item.id} selected>
					{item.type}
				</option>
			);
		} else {
			return <option value={item.id}>{item.type}</option>;
		}
	});
	const selectCategory = () => {
		return (
			<select
				className="form-control"
				id="categories"
				name="categories"
				onChange={handleInputChange}
			>
				{optionCategory}
			</select>
		);
	};
	//Xóa
	const deleteTutorial = () => {
		Data.removeproduct(productItem.id)
			.then((response) => {
				console.log(response.data);
				setMessage('Xóa Thành Công!');
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};

	//hiện thị Modal
	const [ show, setShow ] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'ID',
			selector: 'id',
			sortable: true
		},
		{
			name: 'Hình',
			sortable: true,
			cell: (row) => (
				<img
					height="150px"
					width="256px"
					alt=""
					src={`https://res.cloudinary.com/minhchon/image/upload/v1617690557/Thecoffeeson/${row.image}`}
				/>
			)
		},
		{
			name: 'Tên Sản Phẩm',
			selector: 'name',
			sortable: true
		},

		{
			name: 'Giá',
			selector: 'price',
			sortable: true
		},
		{
			name: 'Trạng Thái',
			selector: 'status',
			sortable: true
		},
		{
			name: 'Loại',
			selector: 'categories.type',
			sortable: true
		},
		{
			name: 'Chức Năng',
			key: 'action',
			text: 'Action',
			cell: (product, index) => {
				return (
					<Fragment>
						{/*  	<UpdateProduct/> */}
						<button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(product, index)}>
						<i className="ft-edit-1">Cập Nhật</i>
						</button>
					</Fragment>
				);
			}
		}
	];
	//CSS DATATABLE
	const customStyles = {
		title: {
		  style: {
			fontColor: 'red',
			fontWeight: '900',
			
		  }
		},
		rows: {
		  style: {
			minHeight: '72px', // override the row height
			backgroundColor:'#F7F7F8',
			justifyContent: 'center',
			alignitems: 'center'
		  }
		},
	
		headCells: {
		  style: {
			fontSize: '12px',
			fontWeight: '500',
			textTransform: 'uppercase',
			paddingLeft: '0 8px',
			backgroundColor:'#fff',
			fontColor:'black',
			justifyContent: 'center',
			alignitems: 'center'
		
		  },
		},
		cells: {
		  style: {
			fontSize: '12px',
			paddingLeft: '0 8px',
			backgroundColor:'#F7F7F8',
			justifyContent: 'center',
			alignitems: 'center'
		  },
		},
	  };
	// Checkbox tất cả table
	const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
		<div className="custom-control custom-checkbox">
			<input type="checkbox" className="custom-control-input" ref={ref} {...rest} />
			<label className="custom-control-label" onClick={onClick} />
		</div>
	));
	// ô search
	const [ search, setSearch ] = useState('');
	const onChangeSearchTitle = (e) => {
		const search = e.target.value;
		setSearch(search);
	};
	//Tìm tên sản phẩm
	const findByProduct = () => {
		Data.getFindProduct(search)
			.then((response) => {
				setproduct(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Tìm theo loại
	const findByType = () => {
		Data.getFindTypeProduct(search)
			.then((response) => {
				setproduct(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Tìm theo nguồn
	const findByItem = () => {
		Data.getFindItemProduct(search)
			.then((response) => {
				setproduct(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Reset lại Table
	const refreshList = () => {
		setSearch('');
		GetAllProduct();
	};
	const refreshTable = () => {
		GetAllProduct();
	};

	
	return (
		<div>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
							<Link className="breadcrumb-item" to="/Product">Tất cả sản phẩm</Link>
						</li>
					</ul>
				</div>
			</div>
			<div className="flex-wrap mb-4">
				<h6 className="small-text">Tìm Kiếm</h6>
				<div className="input-group mb-3">
					<input
						type="text"
						className="form-control"
						placeholder="Tìm kiếm theo sản phẩm, loại"
						aria-label="Search Product"
						aria-describedby="button-addon2"
						value={search}
						onChange={onChangeSearchTitle}
					/>
					<div className="input-group-append">
						<button
							className="btn btn-sm btn-outline-vk"
							type="button"
							id="button-addon2"
							onClick={() => {
								findByProduct();
								findByType();
								findByItem();
							}}
						>

							<i className="ft-filter"></i>&nbsp;Tìm Kiếm
						</button>
						<button
							className="btn btn-sm btn-outline-success"
							type="button"
							id="button-addon2"
							onClick={refreshList}
						>
						<i className="ft-refresh-ccw"></i>&nbsp;Refresh
						</button>
						<AddProduct />
					</div>
				</div>
			</div>

			{/*  Hiện thi Table */}
			<div className="card mt-2" >
				<DataTable 
		 			customStyles={customStyles}
					title="Tất Cả Sản Phẩm" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={product} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
					/* 	selectableRows */
					selectableRowsComponent={BootyCheckbox} // checkbox all
				/>
			</div>
			{productItem ? (
				<Modal
					dialogClassName="dialogModal"
					show={show}
					onHide={handleCloseModal}
					backdrop="static"
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title className="tille_Modal">Cập Nhật Sản Phẩm</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="container">
							<section className="panel panel-default">
								<div className="form-group row pt-3">
									<label className="col-sm-3 control-label">ID:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="id"
											name="id"
											value={productItem.id}
											onChange={handleInputChange}
											placeholder="ID"
										/>
									</div>
								</div>
								{/* // nhập tên sản phẩm */}
								<div className="form-group row ">
									<label className="col-sm-3 control-label">Tên Sản Phẩm:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="name"
											name="name"
											value={productItem.name}
											onChange={handleInputChange}
											placeholder="Tên Sản Phẩm"
										/>
									</div>
								</div>
								{/*    Giá */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">Giá:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="price"
											value={productItem.price}
											onChange={handleInputChange}
											name="price"
											placeholder="Giá"
										/>
									</div>
								</div>
								<div className="form-group row">
									<label className="col-sm-3 control-label">image:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="image"
											value={productItem.image}
											onChange={handleInputChange}
											name="image"
											placeholder="Upload an image"
										/>
									</div>
								</div>
								{/*   load hình anh */}
								
								{/* id-type */}
								<div className="form-group row">
									<label for="name" className="col-sm-3 control-label">
										Loại:
									</label>
									<div className="col-sm-9">
									{selectCategory()}
								</div></div>
							</section>
							{/*  Nút button Update */}
							<div className="form-group row pt-2 div-button">
								<div className="col-sm-12">
									<button type="submit" className="btn btn-sm btn-success" onClick={UpdateProduct}>
										Cập Nhật
									</button>
									<button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
										Xóa
									</button>
								</div>
							</div>
							<p>{message}</p>
						</div>
					</Modal.Body>
				</Modal>
			) : (
				<div>
					<br />
					<p>Please click on a Tutorial...</p>
				</div>
			)}
			        
		</div>
	);
}
