import React, { useEffect, useState, Fragment } from 'react';
import Allbill from '../Axios/AxiosServicer/bill';
import DataTable from 'react-data-table-component';
import './Style.css'
import { Modal } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import {ExportToExcel} from './ExportToExcel'
import axios from 'axios';

export default function Bill(props) {
	// Khai báo giá trị
	//Khai báo set và get mảng
	const [bill, setbill] = useState([]);
	const [id, setid] = useState(0);
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALlbill();
	}, []);
	const GetALlbill = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		Allbill.getAllallbill()
			.then((response) => {
				setbill(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	// Hiển thị title theo cột trong table
	const columns = [
		{
			name: 'id',
			selector: 'id',
			sortable: true
		},
		{
			name: 'date_create',
			selector: 'date_create',
			sortable: true
		},
		{
			name: 'Tổng Tiền',
			selector: 'total',
			sortable: true
		},
		{
			name: 'Tên Khách Hàng',
			selector: 'orders.customers.account_Info.name',
			sortable: true
		},
		{
			name: 'Tên Nhân Viên',
			selector: 'orders.employees.account_Info.name',
			sortable: true
		},
		{
			name: 'Chức Năng',
			key: 'action',
			text: 'Action',
			cell: (bill, index) => {
				return (
					<Fragment>
						{/*  	<UpdateProduct/> */}
						<button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(bill.id)}>
							<i className="ft-edit-1" >Chi Tiết đơn Hàng</i>
						</button>
					</Fragment>
				);
			}
		}

	];
	const setActiveTutorial = (id) => {
		setid(id);
		handleShowModal();
	};
	//CSS DATATABLE
	const customStyles = {
		title: {
			style: {
				fontColor: 'red',
				fontWeight: '900',

			}
		},
		rows: {
			style: {
				minHeight: '72px', // override the row height
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			}
		},

		headCells: {
			style: {
				fontSize: '12px',
				fontWeight: '500',
				textTransform: 'uppercase',
				paddingLeft: '0 8px',
				backgroundColor: '#fff',
				fontColor: 'black',
				justifyContent: 'center',
				alignitems: 'center'

			},
		},
		cells: {
			style: {
				fontSize: '12px',
				paddingLeft: '0 8px',
				backgroundColor: '#F7F7F8',
				justifyContent: 'center',
				alignitems: 'center'
			},
		},
	};




	const [show, setShow] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);

	const menuItems = bill.map((item) => {
		const subMenuItems = item.orders.orders_Detail.map((subItem, index) => {
			if (item.id === id) {
				return (
					<tr key={index}>

						<td>{subItem.products.name}</td>
						<td>{subItem.sizes.type}</td>
						<td>{subItem.amount}</td>
						<td>{subItem.products.price}</td>
						<td>{subItem.products.categories.type}</td>
					</tr>
				);
			} else
				return (<></>);
		});

		return (
			<>{subMenuItems}</>
		);
	});


	// xuất ra ex
	const data1={
		id:"",
		date_create:"",
		total:"",
		orders:{
			customers:{
				account_Info:{
					name:"",
				}

			},
			employees:{
				account_Info:{
					name:"",
				}
			},
			orders_Detail:{
				amount:"",
				sizes:{
					type:""
				},
				products:{
					name:"",
					price:"",
					categories:{
						type:""
					}
				}
			}
		}
	}

	const [data, setData] = React.useState([])
	const fileName = "myfile"; // here enter filename for your excel file
  
	React.useEffect(() => {
	  const fetchData = () =>{
	   axios.get('http://localhost:8081/api/order_detail').then(r => setData(r.data) )
	  }
	  fetchData()
	}, [])


	return (
		<div>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
							<Link className="breadcrumb-item" to="/Product">Tất cả Đơn hàng</Link>
						</li>
					</ul>
				</div>
			</div>
{/* 	xuất extends */}
			<div className="App">
     			 <ExportToExcel apiData={data} fileName={fileName} />
    		</div>
			{/*  Hiện thi Table */}
			<div className="card mt-2" >
				<DataTable
					customStyles={customStyles}
					title="Tất cả Đơn hàng" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={bill} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
				/* 	selectableRows */
				/>
			</div>

			<Modal
				dialogClassName="dialogModalBill"
				show={show}
				onHide={handleCloseModal}
				keyboard={false}
			>	<div style={{ textAlign: "center", marginTop: "25px" }}>
					<h4>CHI TIẾT ĐƠN HÀNG</h4>
				</div>
				<Modal.Body>
					<div class="table">
						<table className="table-striped text-center">
							<thead>
								<tr>

									<th scope="col">Tên Sản Phẩm</th>
									<th scope="col">Size</th>
									<th scope="col">Số Lượng</th>
									<th scope="col">Giá</th>
									<th scope="col">Loại</th>
								</tr>
							</thead>
							<tbody>	{menuItems}</tbody>
						</table>

					</div>

				</Modal.Body>
			</Modal>
		</div>
	);
}
