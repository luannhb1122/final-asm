import React, { useState,useEffect } from 'react';
import Data from '../Axios/AxiosServicer';
import { Modal, Button } from 'react-bootstrap';
export default function Account() {
	const initialTutorialState = {
		type: "",
	
	};
	const [tutorial, setTutorial] = useState(initialTutorialState);

	const [submitted, setSubmitted] = useState(false);

	function handleInputChange(evt) {
		const value = evt.target.value;
		setTutorial({
			...tutorial,
			[evt.target.id]: value
		});
	}

	const saveTutorial = (data) => {
		var data = {
			type: (tutorial.type),
		};
		Data.createType(data)
			.then(response => {
				console.log(response.data)
				setSubmitted(true);
			})
			.catch(e => {
				alert('Lưu Thất Bại');
			});
	};

	const newTutorial = () => {
		setTutorial(initialTutorialState);
		setSubmitted(false);
	};
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	return (
		<>
			<Button variant="primary" className="btn btn-sm btn-outline-warning" onClick={handleShow}>
			<i className="ft-database">&nbsp;Thêm Loại</i>
  </Button>

			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm Loai</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
						<div className="submit-form">
							{submitted ? (
								<div>
									<h4>Thêm Thành Công!</h4>
									<button className="btn btn-success" onClick={newTutorial}>
										Thêm tiếp
          </button>
								</div>
							) : (
								<div>
									<section className="panel panel-default">
										{/*Form Type */}
										<div className='form-group row'>
											<label for="name" className="col-sm-3 control-label">Loại:</label>
											<div className="col-sm-7">
												<input type="text"
													className="form-control"
													id="type"
													required
													value={tutorial.type}
													onChange={handleInputChange}
													name="type" placeholder="Loại" />

											</div>
										
											<button className="btn btn-outline-success col-sm-2 " onClick={saveTutorial} >Thêm</button>
										</div>
									</section>
								</div>
							)}
						</div>
					</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
