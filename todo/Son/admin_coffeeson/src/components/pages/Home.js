
import React, { useEffect, Fragment, useState } from 'react';
import Feedback from '../Axios/AxiosServicer/feedback';
import BillView from '../Axios/AxiosServicer/Bill_view';
import './Style.css'
import './Home.css'
export default function Home() {
    const [feedback, setfeedback] = useState([]);
    // sử dụng useEffect để chạy vòng đời
    useEffect(() => {
        GetAllfeedback();
    }, []);
    const GetAllfeedback = () => {
        //Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
        Feedback.getAllfeedback()
            .then((response) => {
                setfeedback(response.data);
                console.log(response.data);
            })
            .catch((e) => {
                console.log(e);
            });
    };
    // duyệt vòng lặp feedabck
    const ProductList = feedback.slice(0,5).map((item, index) => {
        return (
            <tr key={index}>
                <td><img
					height="70px"
					width="70px"
					alt=""
					src={`https://res.cloudinary.com/minhchon/image/upload/v1617690557/Thecoffeeson/${item.products.image}`}
				/></td>
                <td>{item.products.name}</td>
                <td>{item.products.price}</td>
                <td>{item.products.categories.type}</td>
                <td>{item.rate}</td>
                <td>{item.note}</td>
            </tr>
        )
    });


    // vòng lặp billview
    const [billView, setbillView] = useState([]);
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALlbillView();
	}, []);
	const GetALlbillView = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getAllallbillView()
			.then((response) => {
				setbillView(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};


    return (
        <div>
            <div className="content-overlay"></div>
            <div className="content-wrapper">
                {/*               <!--Statistics cards Starts--> */}
                <div className="row">
                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-purple-love">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0">$2,156</h3>
                                            <span>Total</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-activity font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-ibiza-sunset">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0">$15,678</h3>
                                            <span>Total Cost</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-percent font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart1" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>

                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-mint">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0">$45,668</h3>
                                            <span>Total Sales</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-trending-up font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart2" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-3 col-lg-6 col-md-6 col-12">
                        <div className="card gradient-king-yna">
                            <div className="card-content">
                                <div className="card-body py-0">
                                    <div className="media pb-1">
                                        <div className="media-body white text-left">
                                            <h3 className="font-large-1 white mb-0">$32,454</h3>
                                            <span>Total Earning</span>
                                        </div>
                                        <div className="media-right white text-right">
                                            <i className="ft-credit-card font-large-1"></i>
                                        </div>
                                    </div>
                                </div>
                                <div id="Widget-line-chart3" className="height-75 WidgetlineChart WidgetlineChartshadow mb-2">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*                     <!--Statistics cards Ends-->

                    <!--Line with Area Chart 1 Starts--> */}
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-title">PRODUCTS SALES</h4>
                            </div>
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="chart-info mb-3 ml-3">
                                        <span className="gradient-purple-bliss d-inline-block rounded-circle mr-1"></span>
                                            Sales
                                            <span className="gradient-mint d-inline-block rounded-circle mr-1 ml-2"></span>
                                            Visits
                                        </div>
                                    <div id="line-area" className="height-350 lineArea">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/*       <!--Line with Area Chart 1 Ends--> */}

                <div className="row match-height">
                    <div className="col-xl-4 col-lg-12 col-12">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-title">Số Lượng</h4>
                            </div>
                            <div className="card-content">
                                <div className="card-body">
                                    <p className="font-medium-2 text-center my-2">Last 6 Months Sales</p>
                                    <div id="Stack-bar-chart" className="height-300 Stackbarchart mb-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-8 col-lg-12 col-12">
                        <div className="card shopping-cart">
                            <div className="card-header pb-2">
                                <h4 className="card-title mb-1" style={{color:'#BB0000'}}><i className="fa fa-shopping-cart" aria-hidden="true"></i> Mặt Hàng Nổi Bật</h4>
                            </div>
                            <div className="card-content">
                                <div className="card-body p-0">
                                    <div className="table-responsive">
                                        <table className="table text-center m-0">
                                            <thead>
                                                <tr>
                                                    <th>Image</th>
                                                    <th>Name</th>
                                                    <th>Price</th>
                                                    <th>Type</th>
                                                    <th>Rate</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {ProductList}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row match-height">
                    <div className="col-xl-8 col-lg-12 col-12">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-title mb-0">Visit & Sales Statistics</h4>
                            </div>
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="chart-info mb-2">
                                        <span className="text-uppercase mr-3"><i className="fa fa-circle success font-small-2 mr-1"></i> Sales</span>
                                        <span className="text-uppercase"><i className="fa fa-circle info font-small-2 mr-1"></i> Visits</span>
                                    </div>
                                    <div id="line-area2" className="height-400 lineAreaDashboard">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-4 col-lg-12 col-12">
                        <div className="card gradient-purple-bliss">
                            <div className="card-content">
                                <div className="card-body">
                                    <h4 className="card-title white">Statistics</h4>
                                    <div className="p-2 text-center">
                                        <a className="white font-medium-1">Month</a>
                                        <a className="btn round bg-light-info mx-3 px-3">Week</a>
                                        <a className="white font-medium-1">Day</a>
                                    </div>
                                    <div className="my-3 text-center white">
                                        <div className="font-large-2 d-block mb-1">
                                            <span>$78.89</span>
                                            <i className="ft-arrow-up font-large-2"></i>
                                        </div>
                                        <span className="font-medium-1">Week2 +15.44</span>
                                    </div>
                                </div>
                                <div id="lineChart" className="height-250 lineChart lineChartShadow"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row match-height">
                    <div className="col-xl-4 col-md-6 col-12">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-title">Statistics</h4>
                            </div>
                            <div className="card-content">

                                <p className="font-medium-2 text-center mb-0 mt-2">Hobbies</p>
                                <div id="bar-chart" className="height-250 BarChartShadow BarChart">
                                </div>

                                <div className="card-body">
                                    <div className="row">
                                        <div className="col text-center">
                                            <span className="gradient-starfall d-block rounded-circle mx-auto mb-2"></span>
                                            <span className="font-medium-4 d-block mb-2">48</span>
                                            <span className="font-small-3">Sport</span>
                                        </div>
                                        <div className="col text-center">
                                            <span className="gradient-mint d-block rounded-circle mx-auto mb-2"></span>
                                            <span className="font-medium-4 d-block mb-2">9</span>
                                            <span className="font-small-3">Music</span>
                                        </div>
                                        <div className="col text-center">
                                            <span className="gradient-purple-bliss d-block rounded-circle mx-auto mb-2"></span>
                                            <span className="font-medium-4 d-block mb-2">26</span>
                                            <span className="font-small-3">Travel</span>
                                        </div>
                                        <div className="col text-center">
                                            <span className="gradient-ibiza-sunset d-block rounded-circle mx-auto mb-2"></span>
                                            <span className="font-medium-4 d-block mb-2">17</span>
                                            <span className="font-small-3">News</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-4 col-md-6 col-12">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-title mb-0" style={{color:'#000088'}}><i className="fa fa-user" aria-hidden="true"></i> User List</h4>
                            </div>
                            <div className="card-content">
                                <div className="card-body">
                                    <div className="media pt-0 pb-2">
                                        <img className="mr-3 avatar" src={process.env.PUBLIC_URL + '/assets/images/VoTruongSon.jpg'} alt="Avatar" width="40" height="40" />
                                        <div className="media-body">
                                            <h4 className="font-medium-1 mb-0">Ngô Hoàng Bảo Luân</h4>
                                            <p className="text-muted font-small-3 m-0">Back-End Developer</p>
                                        </div>
                                        <div className="mt-1">
                                            <div className="checkbox">
                                                <input type="checkbox" id="dash-checkbox1" checked="" />
                                                <label for="dash-checkbox1"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="media py-2">
                                        <img className="mr-3 avatar" src={process.env.PUBLIC_URL + '/assets/images/VoTruongSon.jpg'} alt="Avatar" width="40" height="40"/>
                                        <div className="media-body">
                                            <h4 className="font-medium-1 mb-0">Nguyễn Tiến Đạt</h4>
                                            <p className="text-muted font-small-3 m-0">Back-End Developerr</p>
                                        </div>
                                        <div className="mt-1">
                                            <div className="checkbox">
                                                <input type="checkbox" id="dash-checkbox2" />
                                                <label for="dash-checkbox2"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="media py-2">
                                        <img className="mr-3 avatar" src={process.env.PUBLIC_URL + '/assets/images/VoTruongSon.jpg'} alt="Avatar" width="40" height="40" />
                                        <div className="media-body">
                                            <h4 className="font-medium-1 mb-0">Nguyễn Thanh Hậu</h4>
                                            <p className="text-muted font-small-3 m-0">Front-End Devloper</p>
                                        </div>
                                        <div className="mt-1">
                                            <div className="checkbox">
                                                <input type="checkbox" id="dash-checkbox3" />
                                                <label for="dash-checkbox3"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="media py-2">
                                        <img className="mr-3 avatar" src={process.env.PUBLIC_URL + '/assets/images/VoTruongSon.jpg'} alt="Avatar" width="40" height="40" />
                                        <div className="media-body">
                                            <h4 className="font-medium-1 mb-0">Võ Trường Sơn</h4>
                                            <p className="text-muted font-small-3 m-0">Front-End Devloper/Tester</p>
                                        </div>
                                        <div className="mt-1">
                                            <div className="checkbox">
                                                <input type="checkbox" id="dash-checkbox4" checked="" />
                                                <label for="dash-checkbox4"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="media py-2">
                                        <img className="mr-3 avatar" src={process.env.PUBLIC_URL + '/assets/images/VoTruongSon.jpg'} alt="Avatar" width="40" height="40"/>
                                        <div className="media-body">
                                            <h4 className="font-medium-1 mb-0">Nguyễn Nhật Minh</h4>
                                            <p className="text-muted font-small-3 m-0">Design</p>
                                        </div>
                                        <div className="mt-1">
                                            <div className="checkbox">
                                                <input type="checkbox" id="dash-checkbox5" />
                                                <label for="dash-checkbox5"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="text-center mt-2">
                                        <button type="button" className="btn bg-light-primary">Add New</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-xl-4 col-12">
                        <div className="card">
                            <div className="card-header">
                                <h4 className="card-title">Project Stats</h4>
                            </div>
                            <div className="card-content">

                                <p className="font-medium-2 text-center mb-0 mt-2">Project Tasks</p>
                                <div id="donut-dashboard-chart" className="height-250 donut">
                                </div>

                                <div className="card-body">
                                    <div className="row mb-3">
                                        <div className="col">
                                            <span className="mb-1 text-muted d-block">23% - Started</span>
                                            <div className="progress" >
                                                <div className="progress-bar bg-info" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <span className="mb-1 text-muted d-block">28% - Done</span>
                                            <div className="progress" >
                                                <div className="progress-bar bg-success" role="progressbar" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mb-2">
                                        <div className="col">
                                            <span className="mb-1 text-muted d-block">35% - Remaining</span>
                                            <div className="progress" >
                                                <div className="progress-bar bg-primary" role="progressbar" aria-valuenow="35" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <span className="mb-1 text-muted d-block">14% - In Progress</span>
                                            <div className="progress" >
                                                <div className="progress-bar bg-warning" role="progressbar" aria-valuenow="14" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    )
}
