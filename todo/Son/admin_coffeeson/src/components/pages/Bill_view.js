import React, { useEffect, useState, Fragment } from 'react';
import BillView from '../Axios/AxiosServicer/Bill_view';
import DataTable from 'react-data-table-component';
import './Style.css'
import { Nav, Col, Tab } from 'react-bootstrap';
import Tabs from 'react-bootstrap/Tabs'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { ExportToExcel } from './ExportToExcel'
import { Line } from "react-chartjs-2";
import axios from 'axios';
import { Chart } from "react-google-charts";

export default function Bill(props) {

	const datachart = [
		['Tháng', "Doanh thu 2020"]
	];

	const datachart1 = [
		['Tháng', "Doanh thu 2021"]
	];
	// Khai báo giá trị
	//Khai báo set và get mảng
	const [billView, setbillView] = useState([]);
	const [billViewa, setbillViewa] = useState([]);
	// sử dụng useEffect để chạy vòng đời
	useEffect(() => {
		GetALlbillView();
	}, []);
	const GetALlbillView = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getAllallbillView()
			.then((response) => {
				setbillView(response.data);
				console.log(response.data)
			})
			.catch((e) => {
				console.log(e);
			});
	};

	useEffect(() => {
		GetALlbillViewa();
	}, []);
	const GetALlbillViewa = () => {
		//Load sản phẩm từ api Product và sử dung useEffect để quản lí vòng đời
		BillView.getAllallbillViewa()
			.then((response) => {
				setbillViewa(response.data);
				console.log(response.data)
			})
			.catch((e) => {
				console.log(e);
			});
	};

	const list1 = datachart.concat(billView);
	const list2 = datachart1.concat(billViewa);


	const list = billView.map((item, index) => {
		return (
			<tr key={index}>
				<td>{item[0]}</td>
				<td>{item[1]}</td>
			</tr>
		)
	});

	return (
		<div>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
							<Link className="breadcrumb-item" to="/Product">Tổng Số Doanh Thu</Link>
						</li>
					</ul>
				</div>
			</div>
			<Tabs defaultActiveKey="home" transition={false} id="noanim-tab-example">
				<Tab eventKey="home" title="Biểu Đồ Thể hiện Doanh Thu">
					<h1>Dankmemes</h1>
					<div>
						<Chart
							width={'1200px'}
							height={'400px'}
							chartType="Bar"
							loader={<div>Loading Chart</div>}
							data={list1}
							options={{
								// Material design options
								chart: {
									title: 'Company Performance',
									subtitle: 'Biểu Đồ Doanh thu',
								},
							}}
							// For tests
							rootProps={{ 'data-testid': '2' }}
						/>

					</div>
					<h1>Dankmemes</h1>
					<div>
						<Chart
							width={'1200px'}
							height={'400px'}
							chartType="Bar"
							loader={<div>Loading Chart</div>}
							data={list2}
							options={{
								// Material design options
								chart: {
									title: 'Company Performance',
									subtitle: 'Biểu Đồ Doanh thu',
								},
							}}
							// For tests
							rootProps={{ 'data-testid': '2' }}
						/>

					</div>
				</Tab>
				<Tab eventKey="profile" title="Biểu Đồ Thể hiện Doanh Thu">

				</Tab>
				<Tab eventKey="contact" title="Năm" >
					<table class="table text-center">
						<thead class="table-dark">
							<tr>
								<th scope="col">Tháng </th>
								<th scope="col">Tổng Tiền</th>
							</tr>
						</thead>
						<tbody>
							{list}
						</tbody>
					</table>
				</Tab>
			</Tabs>
			{/* 	xuất extends */}
			{/* 			<div className="App">
     			 <ExportToExcel apiData={data} fileName={fileName} />
    		</div> */}
			{/*  Hiện thi Table */}

		</div>
	);
}
