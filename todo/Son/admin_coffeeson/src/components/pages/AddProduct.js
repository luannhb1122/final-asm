import React, { useState, useEffect } from 'react';
import ProductDataService from '../Axios/AxiosServicer/Product';
import Category from '../Axios/AxiosServicer/category';
import axios from 'axios';
import { Modal, Button } from 'react-bootstrap';

import './Style.css'
export default function Account(props) {
	const initialProductState = {
		name: '',
		price: '',
		image: '',
		categories: {
			id: ''
		}
	};
	const [Product, setProduct] = useState(initialProductState);
	const [submitted, setSubmitted] = useState(false);

	function handleInputChange(evt) {
		const value = evt.target.value;
		setProduct({
			...Product,
			[evt.target.name]: value
		});
	}


	const saveProduct = () => {
		var data = {
			name: Product.name,
			price: parseFloat(Product.price),
			image: Product.image,
			categories: {
				id: parseInt(Product.categories)
			}
		};
		console.log(data)
		ProductDataService.createproduct(data)
			.then((response) => {
				console.log(response.data)
				setSubmitted(true);
				
			})
			.catch((e) => {
				alert('Thêm Thất Bại');
			});
		
	};

	const [list, setList] = React.useState([]);
	useEffect(() => {
		GetType();
	}, []);
	const GetType = () => {
		Category.getAllcategory()
			.then((response) => {
				setList(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const optionCategory = list.map((item) => {
		return (
			<option value={item.id}>{item.type}</option>
		)
	});
	const selectCategory = () => {
		return (

			<select className='form-control' id="categories" name="categories" onChange={handleInputChange}>
				{optionCategory}
			</select>
		)
	};

	const newProduct = () => {
		setProduct(initialProductState);
		setSubmitted(false);
	};
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	///hình ảnh
	/* const [image, setImage] = useState('')
	const [loading, setLoading] = useState(false)

	const uploadImage = async e => {
		const files = e.target.files
		const data = new FormData()
		data.append('file', image)
		data.append('upload_preset', 'dqrutcek')
		setLoading(true)
		const res = await fetch(
			'https://api.cloudinary.com/v1_1/minhchon/image/upload',
			{
				method: 'POST',
				body: data
			}
		)
		
		const file = await res.json()

		setImage(file.secure_url)
		setLoading(false)
	} */

	const [image, setImage] = useState('')
	const uploadImage = () => {
		const formData = new FormData()
		formData.append('file', image)
		formData.append('upload_preset', 'Thecoffeeson')
		

		axios.post('https://api.cloudinary.com/v1_1/minhchon/image/upload',
			formData,
		).then(response => {
			console.log(response);
		})
	
	};


	return (
		<>
			<Button variant="primary" className="btn btn-sm btn-outline-danger" onClick={handleShow}>
				<i className="ft-database">&nbsp;Thêm Sản Phẩm</i>
			</Button>

			<Modal dialogClassName="mymodal" show={show} onHide={handleClose}>
				<Modal.Header closeButton>
					<Modal.Title >Thêm Sản phẩm</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<div className='container'>
						<div className="submit-form">
							{submitted ? (
								<div>
									<h4>Thêm Thành Công!</h4>
									<button className="btn btn-success" onClick={newProduct}>
										Thêm tiếp
          </button>
								</div>
							) : (
								<div>
									<section className="panel panel-default">

										{/* // nhập tên sản phẩm */}
										<div className='form-group row pt-4'>
											<label for="name" className="col-sm-3 control-label">Tên Sản Phẩm:</label>
											<div className="col-sm-9">
												<input type="text"
													className="form-control"
													id="name"
													required
													value={Product.name}
													onChange={handleInputChange}
													name="name" placeholder="Tên Sản Phẩm" />
											</div>
										</div>
										{/*    Giá */}
										<div className='form-group row'>
											<label for="name" className="col-sm-3 control-label">Giá:</label>
											<div className="col-sm-9">
												<input type="text"
													className="form-control"
													id="price"
													required
													value={Product.price}
													onChange={handleInputChange}
													name="price" placeholder="Giá" />
											</div>
										</div>
										{/*   load hình anh */}
										<div className="form-group row">
											<label for="name" className="col-sm-3 control-label" >Hình Ảnh</label>
											<div className="row justify-content">
												{/* 	<div className="App">
													<h1>Upload Image</h1>
													<input
														type="file"
														name="file"
														placeholder="Upload an image"
														value={Product.file}
														onChange={uploadImage}
										
													/>
													{loading ? (
														<h3>Loading...</h3>
													) : (
														<img src={image} style={{ width: '150px' }} />
													)}
												</div> */}
												<input
												
													type="file"
													name="image"
													placeholder="Upload an image"
													value={Product.image}
													onChange={(event) => {
														setImage(event.target.files[0]);
														handleInputChange(event)
													}}
												/>
											</div>

										</div>
							{/* 			<div className='form-group row'>
											<label for="name" className="col-sm-3 control-label">image:</label>
											<div className="col-sm-9">
												<input type="file"
													className="form-control"
													id="image"
													required
													value={Product.file}
													onChange={handleInputChange}
													name="image" placeholder="image" />
											</div>
										</div> */}
										{/* 	id_type */}
										<div className='form-group row'>
											<label for="name" className="col-sm-3 control-label">Loại:	</label>

											<div className="col-sm-9">
												{selectCategory()}
											</div>
										</div>
									</section>
									<div className='form-group row pt-2 div-button'>
										<div className='col-sm-12'>
											<button className="btn btn-outline-success mr-2 " onClick={()=> {uploadImage(); saveProduct()}} >Thêm</button>
											<button className="btn btn-outline-primary " onClick={newProduct} >Clear</button>
										</div>
									</div>
								</div>
							)}
						</div>
					</div>
				</Modal.Body>
			</Modal>
		</>

	);
}
