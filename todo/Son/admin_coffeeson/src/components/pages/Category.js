import React, { useEffect, Fragment, useState } from 'react';
import DataType from '../Axios/AxiosServicer/category';
import DataTable from 'react-data-table-component';
import AddType from '../pages/AddCategory';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
export default function Customer() {
	// Khai báo giá trị
	const initialTypeProduct = {
		id:"",
		type:"",
		
	};
	//Khai báo set và get mảng
	const [ TypeProduct, setTypeProduct ] = useState([]);
	const [ currentTutorial, setCurrentTutorial ] = useState(initialTypeProduct);
	const [ message, setMessage ] = useState('');
	// sử dụng useEffect để chạy vòng đời
    useEffect(() => {
		GetType();
	}, []);
	const GetType = () => {
		DataType.getAllcategory()
			.then((response) => {
                setTypeProduct(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	const setActiveTutorial = (TypeProduct, index) => {
		setCurrentTutorial(TypeProduct);
		handleShowModal();
	};
	// Sự kiện nhập giá trị vào form
	function handleInputChange(evt) {
		const value = evt.target.value;
		setCurrentTutorial({
			...currentTutorial,
			[evt.target.name]: value
		});
	}
    //Tìm Kiếm ID Type
    const [ searchID, setSearchID ] = useState('');
	const onChangeSearchType = (e) => {
		const searchID = e.target.value;
		setSearchID(searchID);
	};
    //Function Tìm Kiếm
	const findByType= () => {
		DataType.getNamecategory(searchID)
			.then((response) => {
				setTypeProduct(response.data);
				console.log(response.data);
			})
			.catch((e) => {
				console.log(e);
			});
	};
	// Cập nhật dữ liệu Type
	const updateTutorial = () => {
		DataType.updatecategory(currentTutorial.id, currentTutorial)
			.then((response) => {
				console.log(response.data);
				handleCloseModal();
				refreshTable();
			})
			.catch((e) => {
				console.log(e);
			});
	};
	//Xóa
	const deleteTutorial = () => {
		DataType.removecategory(currentTutorial.id)
			.then(response => {
				console.log(response.data);
				handleCloseModal();
				refreshTable();
			})
			.catch(e => {
				console.log(e);
			});
	};

	
	const refreshList = () => {
		setSearchID('');
		GetType();
	};
	//hiện thị Modal
	const [ show, setShow ] = useState(false);
	const handleCloseModal = () => setShow(false);
	const handleShowModal = () => setShow(true);
	// Hiển thị title theo cột trong table
	const columns = [
        {
			name: 'ID',
			selector: 'id',
			sortable: true
		},
		{
			name: 'Loại',
			selector:'type',
			sortable: true
		},
		{
			name: 'Chức Năng',
			key: 'action',
			text: 'Action',
			cell: (TypeProduct, index) => {
				return (
					<Fragment>
						<button className="btn btn-primary btn-sm" onClick={() => setActiveTutorial(TypeProduct, index)}>
						<i className="ft-edit-1">Cập Nhật</i>
						</button>
					</Fragment>
				);
			}
		}
	];
	//CSS DATATABLE
	const customStyles = {
		title: {
		  style: {
			fontColor: 'red',
			fontWeight: '900',
			
		  }
		},
		rows: {
		  style: {
			minHeight: '72px', // override the row height
			backgroundColor:'#F7F7F8',
			justifyContent: 'center',
			alignitems: 'center'
		  }
		},
	
		headCells: {
		  style: {
			fontSize: '12px',
			fontWeight: '500',
			textTransform: 'uppercase',
			paddingLeft: '0 8px',
			backgroundColor:'#fff',
			fontColor:'black',
			justifyContent: 'center',
			alignitems: 'center'
		
		  },
		},
		cells: {
		  style: {
			fontSize: '12px',
			paddingLeft: '0 8px',
			backgroundColor:'#F7F7F8',
			justifyContent: 'center',
			alignitems: 'center'
		  },
		},
	  };
	const refreshTable = () => {
        GetType();
	};
	return (
		<div>
			{/*     <!--Tiêu Đề--> */}
			<div class="input-group">
				<div className="breadcrumbs-area clearfix">
					<ul className="breadcrumbs float-left list-unstyled">
						<li class="breadcrumb-item" aria-current="page">
							<Link className="breadcrumb-item" to="/">Trang Chủ</Link>
							<Link className="breadcrumb-item" to="/Category">Tất cả loại sản phẩm</Link>
						</li>
					</ul>
				</div>
			</div>
                <div className="flex-wrap mb-4">
                    <h6>Tìm Kiếm</h6>
				<div className="input-group mb-3"> 				
				<input
								type="text"
								className="form-control"
								placeholder="Tìm Theo Loại"
								aria-label="Search Product"
								aria-describedby="button-addon2"
                                value={searchID}
                                onChange={onChangeSearchType}
							/> 
					<div className="input-group-append">
						<button className="btn btn-sm btn-outline-vk" type="button" id="button-addon2" onClick={findByType}>
						<i className="ft-filter"></i>&nbsp; <strong>Tìm Kiếm</strong>
						</button>
						<button
							className="btn btn-sm btn-outline-success"
							type="button"
							id="button-addon2"
							onClick={refreshList}>
						<i className="ft-refresh-ccw"></i>&nbsp; <strong>Refresh</strong>
						</button>
                        <AddType />
					</div>
				</div>
			</div>

			{/*  Hiện thi Table */}
			<div className="card mt-2">
				<DataTable
                customStyles={customStyles}
					title="Loại Sản Phẩm" //Tiêu đề bảng
					columns={columns} // Hiển thị cột
					data={TypeProduct} // Dữ liệu đổ vào bảng
					defaultSortField="id"
					pagination // Hiển thị phân trang hay không
					
				/>
			</div>
			{currentTutorial ? (
				<Modal
					dialogClassName="dialogModal"
					show={show}
					onHide={handleCloseModal}
					backdrop="static"
					keyboard={false}
				>
					<Modal.Header closeButton>
						<Modal.Title className="tille_Modal">Cập Nhật Loại</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<div className="container">
							<section className="panel panel-default">
								<div className="form-group row pt-3">
									<label className="col-sm-3 control-label">ID:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="id"
											name="id"
											required
											value={currentTutorial.id}
											
											placeholder="ID"
										/>
									</div>
								</div>
								{/* id-type */}
								<div className="form-group row">
									<label className="col-sm-3 control-label">Loại:</label>
									<div className="col-sm-9">
										<input
											type="text"
											className="form-control"
											id="type"
											required
											name="type"
											value={currentTutorial.type}
											onChange={handleInputChange}
											placeholder="Loại"
										/>
									</div>
								</div>
							</section>
							{/*  Nút button Update */}
							<div className="form-group row pt-2 div-button">
								<div className="col-sm-12">
									<button type="submit" className="btn btn-sm btn-success" onClick={updateTutorial}>
										Cập Nhật
									</button>
									<button className="btn btn-danger btn-sm" type="button" onClick={deleteTutorial}>
										Xóa
									</button>
								</div>
							</div>
							<p>{message}</p>
						</div>
					</Modal.Body>
				</Modal>
			) : (
				<div>
					<br />
					<p>Please click on a Tutorial...</p>
				</div>
			)}
		</div>
	);
}
