import React from 'react'
import Product from '../pages/Product';
import Home from '../pages/Home';
import AllOrder from '../pages/AllOrder';
import Category from '../pages/Category';
import Customer from '../pages/Customer';
import Employee from '../pages/Employee';
import Billview from '../pages/Bill_view';
/* import TTTK from '../pages/TTTK'; */

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
export default function Main() {
    return (
        <div>
        <nav className="navbar navbar-expand-lg navbar-light header-navbar navbar-static">
        <div className="container-fluid navbar-wrapper">
            <div className="navbar-header d-flex">
                <div className="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center" data-toggle="collapse"><i className="ft-menu font-medium-3"></i></div>
                <ul className="navbar-nav">
                    <li className="nav-item mr-2 d-none d-lg-block"><a className="nav-link apptogglefullscreen" id="navbar-fullscreen" href="javascript:;"><i className="ft-maximize font-medium-3"></i></a></li>
                    <li className="nav-item nav-search"><a className="nav-link nav-link-search" href="javascript:"><i className="ft-search font-medium-3"></i></a>
                        <div className="search-input">
                            <div className="search-input-icon"><i className="ft-search font-medium-3"></i></div>
                            <input className="input" type="text" placeholder="Tìm Kiếm" tabindex="0" data-search="template-search"/>
                            <div className="search-input-close"><i className="ft-x font-medium-3"></i></div>
                            <ul className="search-list"></ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div className="navbar-container">
                <div className="collapse navbar-collapse d-block" id="navbarSupportedContent">
                    <ul className="navbar-nav">
                        <li className="i18n-dropdown dropdown nav-item mr-2"><a className="nav-link d-flex align-items-center dropdown-toggle dropdown-language" id="dropdown-flag" href="javascript:;" data-toggle="dropdown"><img className="langimg selected-flag" src="../../../app-assets/img/flags/us.png" alt="flag"/><span className="selected-language d-md-flex d-none">English</span></a>
                            <div className="dropdown-menu dropdown-menu-right text-left" aria-labelledby="dropdown-flag"><a className="dropdown-item" href="javascript:;" data-language="en"><img className="langimg mr-2" src="../../../app-assets/img/flags/us.png" alt="flag"/><span className="font-small-3">English</span></a><a className="dropdown-item" href="javascript:;" data-language="es"><img className="langimg mr-2" src="../../../app-assets/img/flags/es.png" alt="flag"/><span className="font-small-3">Spanish</span></a><a className="dropdown-item" href="javascript:;" data-language="pt"><img className="langimg mr-2" src="../../../app-assets/img/flags/pt.png" alt="flag"/><span className="font-small-3">Portuguese</span></a><a className="dropdown-item" href="javascript:;" data-language="de"><img className="langimg mr-2" src="../../../app-assets/img/flags/de.png" alt="flag"/><span className="font-small-3">German</span></a></div>
                        </li>
                        <li className="dropdown nav-item"><a className="nav-link dropdown-toggle dropdown-notification p-0 mt-2" id="dropdownBasic1" href="javascript:;" data-toggle="dropdown"><i className="ft-bell font-medium-3"></i><span className="notification badge badge-pill badge-danger">4</span></a>
                            <ul className="notification-dropdown dropdown-menu dropdown-menu-media dropdown-menu-right m-0 overflow-hidden">
                                <li className="dropdown-menu-header">
                                    <div className="dropdown-header d-flex justify-content-between m-0 px-3 py-2 white bg-primary">
                                        <div className="d-flex"><i className="ft-bell font-medium-3 d-flex align-items-center mr-2"></i><span className="noti-title">7 New Notification</span></div><span className="text-bold-400 cursor-pointer">Mark all as read</span>
                                    </div>
                                </li>
                                <li className="scrollable-container"><a className="d-flex justify-content-between" href="javascript:void(0)">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="mr-3"><img className="avatar" src="../../../app-assets/img/portrait/small/avatar-s-20.png" alt="avatar" height="45" width="45"/></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Kate Young</span><small className="grey lighten-1 font-italic float-right">5 mins ago</small></h6><small className="noti-text">Commented on your photo</small>
                                                <h6 className="noti-text font-small-3 m-0">Great Shot John! Really enjoying the composition on this piece.</h6>
                                            </div>
                                        </div>
                                    </a><a className="d-flex justify-content-between" href="javascript:void(0)">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="mr-3"><img className="avatar" src="./app-assets/img/portrait/small/avatar-s-11.png" alt="avatar" height="45" width="45"/></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Andrew Watts</span><small className="grey lighten-1 font-italic float-right">49 mins ago</small></h6><small className="noti-text">Liked your album: UI/UX Inspo</small>
                                            </div>
                                        </div>
                                    </a><a className="d-flex justify-content-between read-notification" href="javascript:void(0)">
                                        <div className="media d-flex align-items-center py-0 pr-0">
                                            <div className="media-left">
                                                <div className="mr-3"><img src="./app-assets/img/icons/sketch-mac-icon.png" alt="avatar" height="45" width="45"/></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0">Update</h6><small className="noti-text">MyBook v2.0.7</small>
                                            </div>
                                            <div className="media-right">
                                                <div className="border-left">
                                                    <div className="px-4 py-2 border-bottom">
                                                        <h6 className="m-0 text-bold-600">Update</h6>
                                                    </div>
                                                    <div className="px-4 py-2 text-center">
                                                        <h6 className="m-0">Close</h6>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a><a className="d-flex justify-content-between read-notification" href="javascript:void(0)">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="avatar bg-primary bg-lighten-3 mr-3 p-1"><span className="avatar-content font-medium-2">LD</span></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Registration done</span><small className="grey lighten-1 font-italic float-right">6 hrs ago</small></h6>
                                            </div>
                                        </div>
                                    </a>
                                    <div className="cursor-pointer">
                                        <div className="media d-flex align-items-center justify-content-between">
                                            <div className="media-left">
                                                <div className="media-body">
                                                    <h6 className="m-0">New Offers</h6>
                                                </div>
                                            </div>
                                            <div className="media-right">
                                                <div className="custom-control custom-switch">
                                                    <input className="switchery" type="checkbox" checked id="notificationSwtich" data-size="sm"/>
                                                    <label for="notificationSwtich"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="d-flex justify-content-between cursor-pointer read-notification">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="avatar bg-danger bg-lighten-4 mr-3 p-1"><span className="avatar-content font-medium-2"><i className="ft-heart text-danger"></i></span></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Application approved</span><small className="grey lighten-1 font-italic float-right">18 hrs ago</small></h6>
                                            </div>
                                        </div>
                                    </div><a className="d-flex justify-content-between read-notification" href="javascript:void(0)">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="mr-3"><img className="avatar" src="./app-assets/img/portrait/small/avatar-s-6.png" alt="avatar" height="45" width="45"/></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Anna Lee</span><small className="grey lighten-1 font-italic float-right">27 hrs ago</small></h6><small className="noti-text">Commented on your photo</small>
                                                <h6 className="noti-text font-small-3 text-bold-500 m-0">Woah!Loving these colors! Keep it up</h6>
                                            </div>
                                        </div>
                                    </a>
                                    <div className="d-flex justify-content-between cursor-pointer read-notification">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="avatar bg-info bg-lighten-4 mr-3 p-1">
                                                    <div className="avatar-content font-medium-2"><i className="ft-align-left text-info"></i></div>
                                                </div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Report generated</span><small className="grey lighten-1 font-italic float-right">35 hrs ago</small></h6>
                                            </div>
                                        </div>
                                    </div><a className="d-flex justify-content-between read-notification" href="javascript:void(0)">
                                        <div className="media d-flex align-items-center">
                                            <div className="media-left">
                                                <div className="mr-3"><img className="avatar" src="./app-assets/img/portrait/small/avatar-s-7.png" alt="avatar" height="45" width="45"/></div>
                                            </div>
                                            <div className="media-body">
                                                <h6 className="m-0"><span>Oliver Wright</span><small className="grey lighten-1 font-italic float-right">2 days ago</small></h6><small className="noti-text">Liked your album: UI/UX Inspo</small>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li className="dropdown-menu-footer">
                                    <div className="noti-footer text-center cursor-pointer primary border-top text-bold-400 py-1">Read All Notifications</div>
                                </li>
                            </ul>
                        </li>
                        <li className="dropdown nav-item mr-1"><a className="nav-link dropdown-toggle user-dropdown d-flex align-items-end" id="dropdownBasic2" href="javascript:;" data-toggle="dropdown">
                                <div className="user d-md-flex d-none mr-2"><span className="text-right">No Name</span><span className="text-right text-muted font-small-3">Available</span></div><img className="avatar" src="../../../app-assets/img/portrait/small/avatar-s-1.png" alt="avatar" height="35" width="35"/>
                            </a>
                            <div className="dropdown-menu text-left dropdown-menu-right m-0 pb-0" aria-labelledby="dropdownBasic2"><a className="dropdown-item" href="app-chat.html">
                                    <div className="d-flex align-items-center"><i className="ft-message-square mr-2"></i><span>Chat</span></div>
                                </a><a className="dropdown-item" href="page-user-profile.html">
                                    <div className="d-flex align-items-center"><i className="ft-edit mr-2"></i><span>Thông tin cá nhân</span></div>
                                </a><a className="dropdown-item" href="app-email.html">
                                    <div className="d-flex align-items-center"><i className="ft-mail mr-2"></i><span>Tin Nhắn</span></div>
                                </a>
                                <div className="dropdown-divider"></div><a className="dropdown-item" href="auth-login.html">
                                    <div className="d-flex align-items-center"><i className="ft-power mr-2"></i><span>Đăng Xuất</span></div>
                                </a>
                            </div>
                        </li>
                        <li className="nav-item d-none d-lg-block mr-2 mt-1"><a className="nav-link notification-sidebar-toggle" href="javascript:;"><i className="ft-align-right font-medium-3"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
{/* Side Bar*/}
    <div className="wrapper">
{/*         <!-- main menu--> */}
        <div className="app-sidebar menu-fixed" data-background-color="man-of-steel" data-image="../../../app-assets/img/sidebar-bg/01.jpg" data-scroll-to-active="true">
  
            <div className="sidebar-header">
                <div className="logo clearfix active"><Link className="logo-text float-left" to="/" data-toggle="pill" role="tab">
                        <div className="logo-img">
                            <img src="../../../app-assets/img/Asset 2.png" alt="Logo" style={{width:40}} /><span class="text">COFFEE SON</span></div>
                    </Link>
                    <a className="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;"><i className="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a><a className="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i className="ft-x"></i></a></div>
            </div>
 {/*            <!-- Sidebar Header Ends-->
            <!-- / main menu header-->
            <!-- main menu content--> */}
            <div className="sidebar-content main-menu-content">
                <div className="nav-container">
                    <ul className="navigation navigation-main" id="v-pills-tab" data-menu="menu-navigation" role="tablist"
							aria-orientation="vertical">
                        <li><Link to='/'  data-toggle="pill" role="tab"><i className="ft-home"></i><span className="menu-title" data-i18n="Home">Trang Chủ</span></Link></li>
                        <li className="has-sub nav-item"><a href="javascript:;"><i className="ft-aperture"></i><span className="menu-title" data-i18n="Product">Sản Phẩm</span>{/* <span className="tag badge badge-pill badge-success float-right mr-1 mt-1">6</span> */}</a>
                            <ul className="menu-content">
                                <li><Link to="/Product"data-toggle="pill" role="tab"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="ProductAll">Tất cả sản phẩm</span></Link>
                                </li>
                                <li><Link to="/Category"data-toggle="pill" role="tab"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="ProductAll">Tất cả loại sản phẩm</span></Link>
                                </li>
                            </ul>
                        </li>
                        <li className="has-sub nav-item"><a href="javascript:;"><i className="ft-box"></i><span className="menu-title" data-i18n="Order">Đơn hàng</span></a>
                            <ul className="menu-content">
                                <li><Link to='/allorder' data-toggle="pill" role="tab"/*  href="javascript:;" */><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="OrderAll">Tất cả đơn hàng</span></Link>
                                    
                                </li>
                                <li><Link to="/customer"data-toggle="pill" role="tab"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="Customer">Khách Hàng</span></Link>
                                </li>
                            </ul>
                        </li>
                        <li className="has-sub nav-item"><a href="javascript:;"><i className="ft-edit"></i><span className="menu-title" data-i18n="Staff">Nhân Viên</span></a>
                            <ul className="menu-content">
                                <li> <Link to="/employee"data-toggle="pill" role="tab"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="StaffAll">Tất Cả Nhân Viên</span></Link>
                                </li>
                                
                              {/*   <li> <Link to="/tttk"data-toggle="pill" role="tab"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="StaffAll">Thông Nhân Viên</span></Link>
                                </li> */}
                            </ul>
                        </li>
                        <li className="has-sub nav-item"><a href="javascript:;"><i className="ft-grid"></i><span className="menu-title" data-i18n="Tables">Đăng Bài</span></a>
                            <ul className="menu-content">
                                <li><a href="table-basic.html"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="About">Giới Thiệu</span></a>
                                </li>
                                <li><a href="table-extended.html"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="News">Tin Tức</span></a>
                                </li>
                                <li><a href="table-extended.html"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="CV">Tuyển Dụng</span></a>
                                </li>
                            </ul>
                        </li>
                        <li className="has-sub nav-item"><a href="javascript:;"><i className="ft-layout"></i><span className="menu-title" data-i18n="Staticial">Thống Kê</span></a>
                            <ul className="menu-content">
                            <li><Link to="/billview"data-toggle="pill" role="tab"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="Customer">Doanh Thu</span></Link>
                                </li>
                                <li><a href="dt-advanced-initialization.html"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="Advanced">Sản Phẩm</span></a>
                                </li>
                                <li><a href="dt-data-sources.html"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="Data Sources">Đơn Hàng</span></a>
                                </li>
                            </ul>
                        </li>
                        <li className="has-sub nav-item"><a href="javascript:;"><i className="ft-layers"></i><span className="menu-title" data-i18n="Cards">Hỗ Trợ</span></a>
                            <ul className="menu-content">
                                <li><a href="cards-basic.html"><i className="ft-arrow-right submenu-icon"></i><span className="menu-item" data-i18n="Basic Cards">Liên Hệ</span></a>
                                </li>
                            </ul>
                        </li>
                    
                    </ul>
                </div>
            </div>
       {/*      <!-- main menu content--> */}
            <div className="sidebar-background"></div>
{/*             <!-- main menu footer-->
            <!-- include includes/menu-footer-->
            <!-- main menu footer-->
            <!-- / main menu--> */}
        </div>

        <div className="main-panel">
      {/*       <!-- BEGIN : Main Content--> */}
            <div className="main-content">
            <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/">
								<Home />
							</Route>
						</div>
            <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/Product">
								<Product />
							</Route>
						</div>
                        <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/Category">
								<Category />
							</Route>
						</div>
                        <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/allorder">
								<AllOrder />
							</Route>
						</div>
                        <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/customer">
								<Customer />
							</Route>
						</div>
                        <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/employee">
								<Employee />
							</Route>
						</div>
                        <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/billview">
								<Billview />
							</Route>
						</div>
                      {/*   <div className="tab-content" id="v-pills-tabContent">
							<Route exact path="/tttk">
							<TTTK />
							</Route>
						</div> */}
            </div>
       {/*      <!-- END : End Main Content--> */}
{/* 
            <!-- BEGIN : Footer--> */}
            <footer className="footer undefined undefined">
                <p className="clearfix text-muted m-0"><span>Copyright &copy; 2021 &nbsp;</span><a href="#" id="pixinventLink" target="_blank">Team Coffee Sơn</a><span className="d-none d-sm-inline-block">, All rights reserved.</span></p>
            </footer>
     {/*        <!-- End : Footer--> */}
 {/*            <!-- Scroll to top button --> */}
            <button className="btn btn-primary scroll-top" type="button"><i className="ft-arrow-up"></i></button>

        </div>
    </div>
{/* 
    <!-- START Notification Sidebar--> */}
    <aside className="notification-sidebar d-none d-sm-none d-md-block" id="notification-sidebar"><a className="notification-sidebar-close"><i className="ft-x font-medium-3 grey darken-1"></i></a>
        <div className="side-nav notification-sidebar-content">
            <div className="row">
                <div className="col-12 notification-nav-tabs">
                    <ul className="nav nav-tabs">
                        <li className="nav-item"><a className="nav-link active" id="base-tab1" data-toggle="tab" aria-controls="activity-tab" href="#activity-tab" aria-expanded="true">Activity</a></li>
                        <li className="nav-item"><a className="nav-link" id="base-tab2" data-toggle="tab" aria-controls="settings-tab" href="#settings-tab" aria-expanded="false">Settings</a></li>
                    </ul>
                </div>
                <div className="col-12 notification-tab-content">
                    <div className="tab-content">
                        <div className="row tab-pane active" id="activity-tab" role="tabpanel" aria-expanded="true" aria-labelledby="base-tab1">
                            <div className="col-12" id="activity">
                                <h5 className="my-2 text-bold-500">System Logs</h5>
                                <div className="timeline-left timeline-wrapper mb-3" id="timeline-1">
                                    <ul className="timeline">
                                        <li className="timeline-line mt-4"></li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><i className="ft-download primary"></i></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>New Update Available</span><span className="float-right grey font-italic font-small-2">1 min ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Android Pie 9.0.0_r52v availabe (658MB).</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><span className="text-bold-500">Download Now!</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><img className="avatar" src="../../../app-assets/img/portrait/small/avatar-s-15.png" alt="avatar" width="40"/></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Reminder!</span><span className="float-right grey font-italic font-small-2">52 min ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Your meeting is scheduled with Mr. Derrick Walters at 16:00.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><span className="text-bold-500">Snooze</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><img className="avatar" src="../../../app-assets/img/portrait/small/avatar-s-16.png" alt="avatar" width="40"/></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Recieved a File</span><span className="float-right grey font-italic font-small-2">4 hours ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Christina Rogers sent you a file for the next conference.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><img src="./app-assets/img/icons/sketch-mac-icon.png" alt="icon" width="20"/><span className="text-bold-500 ml-2">Diamond.sketch</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><i className="ft-mic primary"></i></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Voice Message</span><span className="float-right grey font-italic font-small-2">10 hours ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Natalya Parker sent you a voice message.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><span className="text-bold-500">Listen</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><i className="ft-cloud-drizzle primary"></i></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Weather Update</span><span className="float-right grey font-italic font-small-2">Yesterday</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Hi John! It is a rainy day with 16&deg;C.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <h5 className="my-2 text-bold-500">Applications Logs</h5>
                                <div className="timeline-left timeline-wrapper" id="timeline-2">
                                    <ul className="timeline">
                                        <li className="timeline-line mt-4"></li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><img className="avatar" src="../../../app-assets/img/portrait/small/avatar-s-26.png" alt="avatar" width="40"/></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Gmail</span><span className="float-right grey font-italic font-small-2">Just now</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Victoria Hampton sent you a mail and has a file attachment with it.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><img src="./app-assets/img/icons/pdf.png" alt="pdf icon" width="20"/><span className="text-bold-500 ml-2">Register.pdf</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><i className="ft-droplet primary"></i></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>MakeMyTrip</span><span className="float-right grey font-italic font-small-2">7 hours ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">Your next flight for San Francisco will be on 24th March.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><span className="text-bold-500">Important</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><img className="avatar" src="../../../app-assets/img/portrait/small/avatar-s-23.png" alt="avatar" width="40"/></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>CNN</span><span className="float-right grey font-italic font-small-2">16 hours ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">U.S. investigating report says email account linked to CIA Director was hacked.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><span className="text-bold-500">Read full article</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><i className="ft-map primary"></i></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Maps</span><span className="float-right grey font-italic font-small-2">Yesterday</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">You visited Walmart Supercenter in Chicago.</p>
                                                <div className="notification-note">
                                                    <div className="p-1 pl-2"><span className="text-bold-500">Write a Review!</span></div>
                                                </div>
                                            </div>
                                        </li>
                                        <li className="timeline-item">
                                            <div className="timeline-badge"><span className="bg-primary bg-lighten-4" data-toggle="tooltip" data-placement="right" title="Portfolio project work"><i className="ft-package primary"></i></span></div>
                                            <div className="activity-list-text">
                                                <h6 className="mb-1"><span>Updates Available</span><span className="float-right grey font-italic font-small-2">2 days ago</span></h6>
                                                <p className="mt-0 mb-2 font-small-3">19 app updates found.</p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="row tab-pane" id="settings-tab" aria-labelledby="base-tab2">
                            <div className="col-12" id="settings">
                                <h5 className="mt-2 mb-3">General Settings</h5>
                                <ul className="list-unstyled mb-0 mx-2">
                                    <li className="mb-3">
                                        <div className="mb-1"><span className="text-bold-500">Notifications</span>
                                            <div className="float-right">
                                                <div className="custom-switch">
                                                    <input className="custom-control-input" id="noti-s-switch-1" type="checkbox"/>
                                                    <label className="custom-control-label" for="noti-s-switch-1"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">Use switches when looking for yes or no answers.</p>
                                    </li>
                                    <li className="mb-3">
                                        <div className="mb-1"><span className="text-bold-500">Show recent activity</span>
                                            <div className="float-right">
                                                <div className="checkbox">
                                                    <input id="noti-s-checkbox-1" type="checkbox" checked/>
                                                    <label for="noti-s-checkbox-1"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">The "for" attribute is necessary to bind checkbox with the input.</p>
                                    </li>
                                    <li className="mb-3">
                                        <div className="mb-1"><span className="text-bold-500">Product Update</span>
                                            <div className="float-right">
                                                <div className="custom-switch">
                                                    <input className="custom-control-input" id="noti-s-switch-4" type="checkbox" checked/>
                                                    <label className="custom-control-label" for="noti-s-switch-4"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">Message and mail me on weekly product updates.</p>
                                    </li>
                                    <li className="mb-3">
                                        <div className="mb-1"><span className="text-bold-500">Email on Follow</span>
                                            <div className="float-right">
                                                <div className="custom-switch">
                                                    <input className="custom-control-input" id="noti-s-switch-3" type="checkbox"/>
                                                    <label className="custom-control-label" for="noti-s-switch-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">Mail me when someone follows me.</p>
                                    </li>
                                    <li className="mb-3">
                                        <div className="mb-1"><span className="text-bold-500">Announcements</span>
                                            <div className="float-right">
                                                <div className="checkbox">
                                                    <input id="noti-s-checkbox-2" type="checkbox" checked/>
                                                    <label for="noti-s-checkbox-2"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">Receive all the news and announcements from my clients.</p>
                                    </li>
                                    <li className="mb-3">
                                        <div className="mb-1"><span className="text-bold-500">Date and Time</span>
                                            <div className="float-right">
                                                <div className="checkbox">
                                                    <input id="noti-s-checkbox-3" type="checkbox"/>
                                                    <label for="noti-s-checkbox-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">Show date and time on top of every page.</p>
                                    </li>
                                    <li>
                                        <div className="mb-1"><span className="text-bold-500">Email on Comments</span>
                                            <div className="float-right">
                                                <div className="custom-switch">
                                                    <input className="custom-control-input" id="noti-s-switch-2" type="checkbox" checked/>
                                                    <label className="custom-control-label" for="noti-s-switch-2"></label>
                                                </div>
                                            </div>
                                        </div>
                                        <p className="font-small-3 m-0">Mail me when someone comments on my article.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </aside>
{/*     <!-- END Notification Sidebar--> */}
    <div className="sidenav-overlay">hihi</div>
    <div className="drag-target"></div>
    </div>
    )
}
