import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8081/api/',
    headers: {
		'Content-Type': 'application/json',
	}

});
axiosInstance.interceptors.response.use(response => {
    // code to be excuted.
    return response;
}, error => {
    
});

axiosInstance.interceptors.request.use(request => {
    // code to be executed
    return request;
}, error => {
    
});

export default axiosInstance;