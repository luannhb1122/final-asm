import url from '../AxiosConfig';
//Lấy dữ liệu toàn sản phẩm
const getAllcategory = () => {
  return url.get("/category");
};
//Tìm sản phẩm theo tên sản phẩm
const getcategory = (id,data) => {
  return url.get(`/category/${id}`,data);
};

//Thêm Sản Phẩm
const createcategory = data => {
    return url.post('/category', data);
  };

  //Cập Nhật Sản Phẩm Theo ID
const updatecategory = (id, data) => {
    return url.put(`/category/${id}`,data);
  };

  //Xóa Sản phẩm theo ID
const removecategory = id => {
    return url.delete(`/category/${id}`);
  };
//Tìm Loại theo Loại
  const getNamecategory = type=> {
    return url.get(`/category/${type}`);
  };

  export default {
    getAllcategory,
    getcategory,
    createcategory,
    updatecategory,
    removecategory,
    getNamecategory,
  };