import url from '../AxiosConfig';
//Lấy dữ liệu toàn sản phẩm
const getAllproduct = () => {
  return url.get("/product");
};
//Tìm sản phẩm theo tên sản phẩm
const getproduct = (id,data) => {
  return url.get(`/product/${id}`,data);
};

//Thêm Sản Phẩm
const createproduct = data => {
    return url.post('/product', data);
  };

  //Cập Nhật Sản Phẩm Theo ID
const updateproduct = (id, data) => {
    return url.put(`/product/${id}`,data);
  };

  //Xóa Sản phẩm theo ID
const removeproduct = id => {
    return url.delete(`/product/${id}`);
  };

  export default {
    getAllproduct,
    getproduct,
    createproduct,
    updateproduct,
    removeproduct,
  };