import url from '../AxiosConfig';
//Lấy dữ liệu toàn sản phẩm
const getAllCustomer = () => {
  return url.get("/customer");
};

const updatCustomer = (id, data) => {
  return url.put(`/customer/${id}`,data);
};

const removCustomer = id => {
  return url.delete(`/customer/${id}`);
};

export default {
    getAllCustomer,
    updatCustomer,
    removCustomer
  };