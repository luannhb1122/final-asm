import url from '../AxiosConfig';
//Lấy dữ liệu toàn sản phẩm
const getAllemployee = () => {
  return url.get("/employee");
};

const updateemployee = (id, data) => {
  return url.put(`/employee/${id}`,data);
};
const removeemployee = id => {
  return url.delete(`/employee/${id}`);
};

export default {
    getAllemployee,
    updateemployee,
    removeemployee,


  };