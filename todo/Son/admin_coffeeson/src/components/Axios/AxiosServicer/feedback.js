import url from '../AxiosConfig';
//Lấy dữ liệu toàn sản phẩm
const getAllfeedback = () => {
  return url.get("/feedback");
};

const updatfeedback = (id, data) => {
  return url.put(`/feedback/${id}`,data);
};
const removfeedback = id => {
  return url.delete(`/feedback/${id}`);
};

export default {
    getAllfeedback,
    updatfeedback,
    removfeedback,
  };