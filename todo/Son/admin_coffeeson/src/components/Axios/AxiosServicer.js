import url from './AxiosConfig';
//Lấy dữ liệu toàn sản phẩm
const getAll = () => {
  return url.get("/product");
};
//Tìm sản phẩm theo tên sản phẩm
const get = (id,data) => {
  return url.get(`/Product/${id}`,data);
};
//Tìm sản phẩm theo tên sản phẩm
const getCustomerID = id => {
  return url.get(`/Customer/${id}`);
};
//Lấy dữ liệu khách Hàng
const getCustomer = ()=> {
  return url.get(`/Customer`);
};
//Lấy dữ liệu khách Hàng
const getStaff = ()=> {
  return url.get(`/Account`);
};
//Lấy dữ liệu Loại
const getType= ()=> {
  return url.get(`/category`);
};
//Lấy dữ liệu Thống Kê Sản Phẩm Đã Bán
const getProductBuy = ()=> {
  return url.get(`/Productlistall`);
};
//Lấy dữ liệu Thống Kê Hóa Đơn Đã Bán
const getBill = ()=> {
  return url.get(`/Billlistall`);
};
//Lấy dữ liệu Sản Phẩm Đã Đặt
const getOrder = () => {
  return url.get(`/Order`);
};
//Xuất Dữ Liệu Đơn Hàng đặt
const getOrderExcel = () => {
  return url.get(`/ExportExcel`);
};
//Thêm Sản Phẩm
const create = data => {
  return url.post('/product', data);
};
//Thêm loại
const createType = id => {
  return url.post('/category', id);
};
//Cập Nhật Sản Phẩm Theo ID
const update = (id, data) => {
  return url.put(`/product/${id}`,data);
};
//Cập Nhật Loại Sản Phẩm Theo ID
const updateType = (id, data) => {
  return url.put(`/category/${id}`, data);
};
//Xóa Sản phẩm theo ID
const remove = id => {
  return url.delete(`/product/${id}`);
};
//Xóa Loại theo ID
const removetype = id => {
  return url.delete(`/category/${id}`);
};
//Tìm đơn hàng theo ID
const findByTitle = id => {
  return url.get(`/getallOrders/${id}`);
};
//Tìm Khách Hàng theo ID
const findByIDCustomer= id => {
  return url.get(`/Customer/${id}`);
};
//Tìm Loại theo Loại
const getNameType = type=> {
  return url.get(`/Type/${type}`);
};
//Tìm Sản phẩm 
const getFindProduct = name=> {
  return url.get(`/Product/${name}`);
};
//Tìm Nhân Viên
const getFindIDStaff= id=> {
  return url.get(`/Account/${id}`);
};
//Tìm Loại Sản phẩm
const getFindTypeProduct = type=> {
  return url.get(`/Products/${type}`);
};
//Tìm Loại Sản phẩm
const getFindItemProduct = item=> {
  return url.get(`/Productss/${item}`);
};
//Thống Kê Sản Phẩm đã bán theo ngày,tháng,năm
const getStatistialDate = date=> {
  return url.get(`/Productlistday/${date}`);
};
//Thống Kê Sản Phẩm đã bán theo tháng,năm
const getStatistialMonth = month=> {
  return url.get(`/Productlistmonth/${month}`);
};
//Thống Kê Sản Phẩm đã bán theo năm
const getStatistialYear = year=> {
  return url.get(`/Productlistyear/${year}`);
};
//Thống Kê Sản Phẩm đã bán theo ngày tháng năm
const getStatistialBillDate = date=> {
  return url.get(`/Billlistday/${date}`);
};
//Thống Kê Sản Phẩm đã bán theo tháng,năm
const getStatistialBillMonth = month=> {
  return url.get(`/Billlistmonth/${month}`);
};
//Thống Kê Sản Phẩm đã bán theo năm
const getStatistialBillYear = year=> {
  return url.get(`/Billlistyear/${year}`);
};


export default {
  getAll,
  get,
  getOrder,
  getBill,
  getType,
  getStaff,
  getOrderExcel,
  getCustomer,
  getCustomerID,
  getProductBuy,
  getStatistialDate,
  getStatistialMonth,
  getStatistialYear,
  getStatistialBillDate,
  getStatistialBillMonth,
  getStatistialBillYear,
  getNameType,
  getFindProduct,
  getFindTypeProduct,
  getFindItemProduct,
  getFindIDStaff,
  create,
  createType,
  update,
  updateType,
  remove,
  removetype,
  findByTitle,findByIDCustomer
};