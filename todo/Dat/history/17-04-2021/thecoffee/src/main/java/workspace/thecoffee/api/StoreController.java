package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Stores;
import workspace.thecoffee.service.StoreService;
// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/store")
@RestController
public class StoreController {
    private final StoreService storeService;

    @Autowired
    public StoreController(StoreService storeService){
        this.storeService= storeService; 
    }

    @GetMapping
    public List<Stores> getAllStore(){
       return storeService.getAllStore();
    }

    @GetMapping(path ="{id}" )
    public Stores  getStoreById(@PathVariable("id") Integer id){
        return storeService.getStoreById(id).orElse(null);
    }

    @PostMapping
    public void addStore(@RequestBody Stores store){
        storeService.addStore(store);
    }

    @DeleteMapping(path ="{id}")
    public void deleteStore(@PathVariable("id") Integer id){
        storeService.deleteStore(id);
    }

    @PutMapping(path ="{id}")
    public void updateStore(@PathVariable("id") Integer id,@RequestBody Stores newStore){
        storeService.updateStore(id,newStore);
    }
}
