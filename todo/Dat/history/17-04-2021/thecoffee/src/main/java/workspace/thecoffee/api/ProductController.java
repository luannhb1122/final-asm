package workspace.thecoffee.api;

import java.util.List;

import javax.validation.*;
import javax.validation.constraints.*;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Products;
import workspace.thecoffee.service.ProductService;

@CrossOrigin
@RequestMapping("api/product")
@RestController
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService= productService; 
    }

    @PostMapping
    public void addProduct(@RequestBody Products products){
        productService.addProduct(products);
      }

    @GetMapping
    public List<Products> getAllProduct(){
       return productService.getAllProduct();
    }

    @GetMapping(path = "{id}")
    public Products getProductById(@PathVariable ("id") Integer id){
        return productService.getProductById(id)
                .orElse(null);
    }

     @DeleteMapping(path = "{id}")
     public void deleteProductById(@PathVariable ("id") Integer id){
        productService.deleteProduct(id);
     }

    @PutMapping(path = "{id}")
    public void updateProductById(@PathVariable ("id") Integer id,@Valid @NotNull @RequestBody Products productToUpdate){
        productService.updateProduct(id, productToUpdate);
    }
}
