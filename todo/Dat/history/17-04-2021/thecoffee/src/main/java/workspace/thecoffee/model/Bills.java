package workspace.thecoffee.model;


import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Bills")
public class Bills {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "date_create")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date_create;

    @Column(name = "total")
    private Float total;
    
    @OneToOne
    @JoinColumn(name="id_order", referencedColumnName = "id")
    // @JoinColumn(name="id_order")
    private Orders orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Date getDate_create() {
        return date_create;
    }

    public void setDate_create(Date date_create) {
        this.date_create = date_create;
    }

    public Bills(Date date_create, Float total, Orders orders) {
        this.date_create = date_create;
        this.total = total;
        this.orders = orders;
    }

    public Bills() {
    }

    public Bills(Object setDate_create, Double total2, Orders orders2) {
    }
    
    
}
