package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Vouchers;
import workspace.thecoffee.service.VoucherService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/voucher")
@RestController
public class VoucherController {
    private final VoucherService voucherService;

    @Autowired
    public VoucherController(VoucherService voucherService){
        this.voucherService= voucherService; 
    }

    @GetMapping
    public List<Vouchers> getAllVoucher(){
       return voucherService.getAllVoucher();
    }

    @GetMapping(path ="{id}" )
    public Vouchers  getVoucherById(@PathVariable("id") Integer id){
        return voucherService.getVoucherById(id).orElse(null);
    }

    @PostMapping
    public void addVoucher(@RequestBody Vouchers Vou){
        voucherService.addVoucher(Vou);
    }

    @DeleteMapping(path ="{id}")
    public void deleteVoucher(@PathVariable("id") Integer id){
        voucherService.deleteVoucher(id);
    }

    @PutMapping(path ="{id}")
    public void updatVoucher(@PathVariable("id") Integer id,@RequestBody Vouchers newVou){
        voucherService.updatVoucher(id,newVou);
    }
}
