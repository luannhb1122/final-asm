package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.Order_StatusDao;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Orders_Status;

@Service
public class Order_StatusService {
    private final Order_StatusDao order_StatusDao;
    @Autowired
    public Order_StatusService(Order_StatusDao order_StatusDao) {
        this.order_StatusDao = order_StatusDao;
    }

    public List<Orders_Status> getAllOrder_Status() {
        return order_StatusDao.findAll();
    }

    public Optional<Orders_Status> getOrders_StatusById(Integer id) {
        return order_StatusDao.findById(id);
    }

    public void addOrders_Status(Orders od) {
        Orders_Status ods = new Orders_Status();                 
            ods.setStatus(true);
            ods.setComment("on working");
            ods.setOrders(od);
            order_StatusDao.save(ods);
            
    }
    // public void addOrders_Status(Orders_Status ods) {
    //     order_StatusDao.save(new Orders_Status(           
    //         ods.isStatus(),
    //         ods.getComment(),
    //         ods.getOrders()
    //         ));
    // }

    public void updatOrders_Status(Integer id, Orders_Status newOds){
        Optional<Orders_Status> Orders_StatusData = getOrders_StatusById(id);
        Orders_Status oldOds = Orders_StatusData.get();
        oldOds.setStatus(newOds.isStatus());
        oldOds.setComment(newOds.getComment());
        oldOds.setOrders(newOds.getOrders());
        order_StatusDao.save(oldOds);
    }

    public void deleteOrders_Status(Integer id) {
        order_StatusDao.deleteById(id);
    }

}
