package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders_Status;
import workspace.thecoffee.service.Order_StatusService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/order_status")
@RestController
public class Order_StatusController {
    private final Order_StatusService order_StatusService;

    @Autowired
    public Order_StatusController(Order_StatusService order_StatusService){
        this.order_StatusService= order_StatusService; 
    }

    @GetMapping
    public List<Orders_Status> getAllOrder_Status(){
       return order_StatusService.getAllOrder_Status();
    }

    @GetMapping(path ="{id}" )
    public Orders_Status  getOrders_StatusById(@PathVariable("id") Integer id){
        return order_StatusService.getOrders_StatusById(id).orElse(null);
    }

    // @PostMapping
    // public void addOrders_Status(@RequestBody Orders_Status ods){
    //     order_StatusService.addOrders_Status(ods);
    // }

    @DeleteMapping(path ="{id}")
    public void deleteOrders_Status(@PathVariable("id") Integer id){
        order_StatusService.deleteOrders_Status(id);
    }

    @PutMapping(path ="{id}")
    public void updatOrders_Status(@PathVariable("id") Integer id,@RequestBody Orders_Status newods){
        order_StatusService.updatOrders_Status(id,newods);
    }
}
