package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Orders;


public interface OrderDao extends JpaRepository<Orders,Integer>{
    
}
