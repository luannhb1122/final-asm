package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders_Detail;
import workspace.thecoffee.service.Order_DetailService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/order_detail")
@RestController
public class Order_DetailController {
    private final Order_DetailService order_DetailService;

    @Autowired
    public Order_DetailController(Order_DetailService order_DetailService){
        this.order_DetailService= order_DetailService; 
    }

    @GetMapping
    public List<Orders_Detail> getAllOrder_Detail(){
       return order_DetailService.getAllOrder_Detail();
    }
}
