package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Bills;
import workspace.thecoffee.service.BillService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/bill")
@RestController
public class BillController {
    private final BillService billService;

    @Autowired
    public BillController(BillService billService){
        this.billService= billService; 
    }

    @GetMapping
    public List<Bills> getAllBill(){
       return billService.getAllBill();
    }
}
