package workspace.thecoffee.service;

import workspace.thecoffee.dao.CustomerDao;
import workspace.thecoffee.model.Customers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    private final CustomerDao customerDao;
    @Autowired
    public CustomerService(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    public List<Customers> getAllCustomer() {
        return customerDao.findAll();
    }
}
