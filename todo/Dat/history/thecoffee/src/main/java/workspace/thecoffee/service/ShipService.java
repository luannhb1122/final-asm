package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.ShipDao;
import workspace.thecoffee.model.Ships_Info;

@Service
public class ShipService {
    private final ShipDao shipDao;
    @Autowired
    public ShipService(ShipDao shipDao) {
        this.shipDao = shipDao;
    }

    public List<Ships_Info> getAllShip() {
        return shipDao.findAll();
    }
}
