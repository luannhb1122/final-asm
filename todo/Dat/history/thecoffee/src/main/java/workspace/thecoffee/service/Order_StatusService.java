package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.Order_StatusDao;
import workspace.thecoffee.model.Orders_Status;

@Service
public class Order_StatusService {
    private final Order_StatusDao order_StatusDao;
    @Autowired
    public Order_StatusService(Order_StatusDao order_StatusDao) {
        this.order_StatusDao = order_StatusDao;
    }

    public List<Orders_Status> getAllOrder_Status() {
        return order_StatusDao.findAll();
    }
}
