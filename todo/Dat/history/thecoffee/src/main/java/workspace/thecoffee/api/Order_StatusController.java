package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders_Status;
import workspace.thecoffee.service.Order_StatusService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/order_status")
@RestController
public class Order_StatusController {
    private final Order_StatusService order_StatusService;

    @Autowired
    public Order_StatusController(Order_StatusService order_StatusService){
        this.order_StatusService= order_StatusService; 
    }

    @GetMapping
    public List<Orders_Status> getAllOrder_Status(){
       return order_StatusService.getAllOrder_Status();
    }
}
