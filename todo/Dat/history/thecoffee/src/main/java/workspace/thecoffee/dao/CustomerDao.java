package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Customers;

public interface CustomerDao extends JpaRepository<Customers,Integer> {
    
}
