package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.FeedbackDao;
import workspace.thecoffee.model.Feedbacks;

@Service
public class FeedbackService {
    private final FeedbackDao feedbackDao;
    @Autowired
    public FeedbackService(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }

    public List<Feedbacks> getAllFeedback() {
        return feedbackDao.findAll();
    }
}
