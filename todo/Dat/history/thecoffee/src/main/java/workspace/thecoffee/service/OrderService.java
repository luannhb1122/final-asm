package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.OrderDao;
import workspace.thecoffee.model.Orders;

@Service
public class OrderService {
    private final OrderDao orderDao;
    @Autowired
    public OrderService(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    public List<Orders> getAllOrder() {
        return orderDao.findAll();
    } 
}
