package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import workspace.thecoffee.dao.EmployeeDao;
import workspace.thecoffee.model.Employees;

@Service
public class EmployeeService {
    private final EmployeeDao employeeDao;
    @Autowired
    public EmployeeService(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    public List<Employees> getAllEmployee() {
        return employeeDao.findAll();
    }

/*     @Transactional(rollbackFor = Exception.class) 
    public String saveDto(UserDto userDto) { 
        userDto.setPassword(bCryptPasswordEncoder
               .encode(userDto.getPassword())); 
        return save(new User(userDto)).getId(); 
    } */
}
