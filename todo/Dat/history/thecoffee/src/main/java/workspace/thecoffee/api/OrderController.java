package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders;
import workspace.thecoffee.service.OrderService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/order")
@RestController
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService){
        this.orderService= orderService; 
    }

    @GetMapping
    public List<Orders> getAllOrder(){
       return orderService.getAllOrder();
    }
}
