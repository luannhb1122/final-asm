package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Vouchers;
import workspace.thecoffee.service.VoucherService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/voucher")
@RestController
public class VoucherController {
    private final VoucherService voucherService;

    @Autowired
    public VoucherController(VoucherService voucherService){
        this.voucherService= voucherService; 
    }

    @GetMapping
    public List<Vouchers> getAllVoucher(){
       return voucherService.getAllVoucher();
    }
}
