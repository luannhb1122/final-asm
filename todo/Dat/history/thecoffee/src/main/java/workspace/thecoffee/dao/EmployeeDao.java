package workspace.thecoffee.dao;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Employees;

public interface EmployeeDao extends JpaRepository<Employees,Integer> {
    
    Optional<Employees> findByUsername(String username);
    
}
