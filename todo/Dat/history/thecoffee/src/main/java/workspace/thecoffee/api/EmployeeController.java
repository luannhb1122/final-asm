package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Employees;
import workspace.thecoffee.service.EmployeeService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/employee")
@RestController
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService){
        this.employeeService= employeeService; 
    }

    @GetMapping
    public List<Employees> getAllEmployee(){
       return employeeService.getAllEmployee();
    }
}
