package workspace.thecoffee.service;

import workspace.thecoffee.dao.SizeDao;
import workspace.thecoffee.model.Sizes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SizeService {
    private final SizeDao sizeDao;
    @Autowired
    public SizeService(SizeDao sizeDao) {
        this.sizeDao = sizeDao;
    }

    public List<Sizes> getAllSize() {
        return sizeDao.findAll();
    }
}
