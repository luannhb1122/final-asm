package workspace.thecoffee.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import workspace.thecoffee.model.Products;

public interface ProductDao extends JpaRepository<Products, Integer> {
    
   /*  @Query(value ="", nativeQuery =true)
	List<Products> findProductByCategory(@Param("name") String name); */
}
