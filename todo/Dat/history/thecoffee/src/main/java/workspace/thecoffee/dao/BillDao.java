package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Bills;

public interface BillDao extends JpaRepository<Bills,Integer> {
    
}
