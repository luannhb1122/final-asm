package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.BillDao;
import workspace.thecoffee.model.Bills;

@Service
public class BillService {
    private final BillDao billDao;
    @Autowired
    public BillService(BillDao billDao) {
        this.billDao = billDao;
    }

    public List<Bills> getAllBill() {
        return billDao.findAll();
    }
}
