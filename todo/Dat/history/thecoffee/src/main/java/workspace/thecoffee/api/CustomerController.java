package workspace.thecoffee.api;

import workspace.thecoffee.model.Customers;
import workspace.thecoffee.service.CustomerService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/customer")
@RestController
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService){
        this.customerService= customerService; 
    }

    @GetMapping
    public List<Customers> getAllCustomer(){
       return customerService.getAllCustomer();
    }
}
