package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.VoucherDao;
import workspace.thecoffee.model.Vouchers;

@Service
public class VoucherService {
    private final VoucherDao voucherDao;
    @Autowired
    public VoucherService(VoucherDao voucherDao) {
        this.voucherDao = voucherDao;
    }

    public List<Vouchers> getAllVoucher() {
        return voucherDao.findAll();
    }
}
