package workspace.thecoffee.model;

import java.util.List;

import javax.persistence.*;

@Entity
@Table(name = "Sizes")
public class Sizes {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "product_size")
    private String type;

    @Column(name = "product_price")
    private Double price;

    @Column(name = "status")
    private Boolean status;

    @OneToMany(mappedBy="sizes")
	List<Orders_Detail> orders_Detail;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }
}
