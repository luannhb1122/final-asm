package workspace.thecoffee.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Accounts_Info;

public interface AccountDao extends JpaRepository<Accounts_Info,Integer> {
    
}
