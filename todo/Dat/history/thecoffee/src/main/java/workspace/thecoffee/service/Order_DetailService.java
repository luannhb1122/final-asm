package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.Order_DetailDao;
import workspace.thecoffee.model.Orders_Detail;

@Service
public class Order_DetailService {
    private final Order_DetailDao order_DetailDao;
    @Autowired
    public Order_DetailService(Order_DetailDao order_DetailDao) {
        this.order_DetailDao = order_DetailDao;
    }

    public List<Orders_Detail> getAllOrder_Detail() {
        return order_DetailDao.findAll();
    }
}
