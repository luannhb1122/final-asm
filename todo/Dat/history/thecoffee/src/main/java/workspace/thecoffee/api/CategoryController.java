package workspace.thecoffee.api;

import java.util.List;

import javax.validation.*;
import javax.validation.constraints.*;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Categories;
import workspace.thecoffee.service.CategoryService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/category")
@RestController
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService typeProductService){
        this.categoryService= typeProductService; 
    }

    @PostMapping
    public void addTypeProduct(@RequestBody Categories typeProducts){
        categoryService.addCategory(typeProducts);
      }

    @GetMapping
    public List<Categories> getAllCategory(){
       return categoryService.getAllCategory();
    }

    @GetMapping(path = "{id}")
    public Categories getTypeProductById(@PathVariable ("id") Integer id){
        return categoryService.getCategoryById(id)
                .orElse(null);
    }

     @DeleteMapping(path = "{id}")
     public void deleteTypeProductById(@PathVariable ("id") Integer id){
        categoryService.deleteCategory(id);
     }

    @PutMapping(path = "{id}")
    public void updateCategoryById(@PathVariable ("id") Integer id,@Valid @NotNull @RequestBody Categories categoryToUpdate){
        categoryService.updateCategory(id, categoryToUpdate);
    }
}
