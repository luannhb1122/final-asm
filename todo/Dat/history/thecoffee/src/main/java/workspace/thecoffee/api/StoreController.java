package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Stores;
import workspace.thecoffee.service.StoreService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/store")
@RestController
public class StoreController {
    private final StoreService storeService;

    @Autowired
    public StoreController(StoreService storeService){
        this.storeService= storeService; 
    }

    @GetMapping
    public List<Stores> getAllStore(){
       return storeService.getAllStore();
    }
}
