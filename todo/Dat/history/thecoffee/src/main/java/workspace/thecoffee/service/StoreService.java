package workspace.thecoffee.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.StoreDao;
import workspace.thecoffee.model.Stores;

@Service
public class StoreService {
    private final StoreDao storeDao;
    @Autowired
    public StoreService(StoreDao storeDao) {
        this.storeDao = storeDao;
    }

    public List<Stores> getAllStore() {
        return storeDao.findAll();
    }
}
