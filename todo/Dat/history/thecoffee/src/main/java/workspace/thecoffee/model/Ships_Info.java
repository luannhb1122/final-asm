package workspace.thecoffee.model;

import javax.persistence.*;


@Entity
@Table(name = "Ships_Info")
public class Ships_Info {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_cus")
    private String name_cus;

    @Column(name = "address")
    private String address;

    @Column(name = "phone_cus")
    private String phone_cus;
    
    @Column(name = "note")
    private String note;

    @Column(name = "price")
    private Double price;

    @OneToOne
    @JoinColumn(name="id_order", referencedColumnName = "id")
    private Orders orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName_cus() {
        return name_cus;
    }

    public void setName_cus(String name_cus) {
        this.name_cus = name_cus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone_cus() {
        return phone_cus;
    }

    public void setPhone_cus(String phone_cus) {
        this.phone_cus = phone_cus;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }
}
