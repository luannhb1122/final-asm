package workspace.thecoffee.model;


import java.util.Date;
import java.util.List;

import javax.persistence.*;


import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="accounts_info")
public class Accounts_Info {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    @Column(name = "email")
    private String email;
    
    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private Date birthday;

    @Column(name = "gender")
    private Boolean gender;

    @Column(name = "picture")
    private String picture;
    
    @OneToOne
    @JoinColumn(name="id_cus", referencedColumnName = "id")
    private Customers customers;

    @OneToOne
    @JoinColumn(name="id_emp", referencedColumnName = "id")
    private Employees employees;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }
   

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Accounts_Info(){
    }  

    public Accounts_Info( 
        String name,
        String phone,
        String email, 
        Date birthday, 
        Boolean gender,
        String picture
      
    ){
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.birthday = birthday;
        this.gender = gender;
        this.picture = picture;
    }

 
}
