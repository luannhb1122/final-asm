package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Orders_Detail;

public interface Order_DetailDao extends JpaRepository<Orders_Detail,Integer> {
    
}
