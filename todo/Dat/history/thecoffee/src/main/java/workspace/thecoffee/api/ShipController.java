package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Ships_Info;
import workspace.thecoffee.service.ShipService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/ship")
@RestController
public class ShipController {
    private final ShipService shipService;

    @Autowired
    public ShipController(ShipService shipService){
        this.shipService= shipService; 
    }

    @GetMapping
    public List<Ships_Info> getAllShip(){
       return shipService.getAllShip();
    }
}
