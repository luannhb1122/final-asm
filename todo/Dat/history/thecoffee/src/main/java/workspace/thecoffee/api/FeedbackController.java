package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Feedbacks;
import workspace.thecoffee.service.FeedbackService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/feedback")
@RestController
public class FeedbackController {
    private final FeedbackService feedbackService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService){
        this.feedbackService= feedbackService; 
    }

    @GetMapping
    public List<Feedbacks> getAllFeedback(){
       return feedbackService.getAllFeedback();
    }
}
