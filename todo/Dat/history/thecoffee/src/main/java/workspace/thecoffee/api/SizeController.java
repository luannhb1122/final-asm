package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Sizes;
import workspace.thecoffee.service.SizeService;

@CrossOrigin(origins = "http://localhost:8082")
@RequestMapping("api/size")
@RestController
public class SizeController {
    private final SizeService sizeService;

    @Autowired
    public SizeController(SizeService sizeService){
        this.sizeService= sizeService; 
    }

    @GetMapping
    public List<Sizes> getAllSize(){
       return sizeService.getAllSize();
    }
}
