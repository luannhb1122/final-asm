package workspace.thecoffee.api;

import java.util.List;

import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Accounts_Info;
import workspace.thecoffee.service.AccountService;

@CrossOrigin
@RequestMapping("api/account")
@RestController
public class AccountController {
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    // @GetMapping("test")
    // public void addAccountNull(){
    // accountService.addAccountNull();
    // }
    // @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    // @PreAuthorize("hasRole('isAdmin')")
    @GetMapping
    public List<Accounts_Info> getAllAccount() {
        return accountService.getAllAccount();
    }

    @GetMapping("emp")
    public List<Accounts_Info> getAllEmp() {
        return accountService.getAllEmp();
    }

    @GetMapping("cus")
    public List<Accounts_Info> getAllCus() {
        return accountService.getAllCus();
    }

    @GetMapping(path = "{id}")
    public Accounts_Info getAccountById(@PathVariable("id") Integer id) {
        return accountService.getAccountById(id).orElse(null);
    }

    @GetMapping(path = "searchAccountByUsername/{username}")
    public Accounts_Info getAccountByUserName(@PathVariable("username") String username) {
        return accountService.getAccountByUsername(username);
    }

    @GetMapping("forgotPassword")
    public String ForgotPassword(@RequestParam("username") String username, @RequestParam("email") String email) {
        
        return accountService.forgotPassword(username, email);
    }

    @GetMapping("forgotPasswordConfirm")
    public String ForgotPasswordConfirm(@RequestParam("otp") Integer otp, @RequestParam("username") String username,
            @RequestParam("password") String password) {
        return accountService.forgotPasswordConfirm(otp, username, password);
    }

    @GetMapping("changePassword")
    public String ChangePassword(@RequestParam("username") String username,
            @RequestParam("oldpassword") String oldPassword, @RequestParam("newpassword") String newPassword) {

        return accountService.ChangePassword(username, oldPassword, newPassword);
    }

    @PostMapping("registerCustomer")
    public void addCustomerAccount(@RequestBody Accounts_Info acc) {
        accountService.addCustomerAccount(acc);
    }

    @PostMapping("registerAdmin")
    public void addEmployeeAccount(@RequestBody Accounts_Info acc) {
        accountService.addEmployeeAccount(acc);
    }

    @DeleteMapping(path = "{id}")
    public void deleteAccountById(@PathVariable("id") Integer id) {
        accountService.deleteAccount(id);
    }

    @PutMapping(path = "{id}")
    public void updateAccount(@PathVariable("id") Integer id, @RequestBody Accounts_Info acc) {
        accountService.updateAccount(id, acc);
    }

    // @GetMapping("max")
    // public Accounts_Info findMaxId(){
    // return accountService.findMaxId().orElse(null);
    // }
    // @PostMapping("cus")
    // public void CusaddAccount(@RequestBody Customers acc){
    // accountService.CusaddAccount(acc);
    // }
}
