package workspace.thecoffee.model;

import java.util.Date;
// import java.util.List;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "accounts_info")
public class Accounts_Info {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    @Value("${stuff.value:@null}")
    private String name;

    @Column(name = "phone")
    @Value("${stuff.value:@null}")
    private String phone;

    @Column(name = "email")
    @Value("${stuff.value:@null}")
    private String email;

    @Column(name = "picture")
    @Value("${stuff.value:@null}")
    private String picture;

    @Column(name = "birthday")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Value("${stuff.value:@null}")
    private Date birthday;

    @Column(name = "gender")
    @Value("${stuff.value:@null}")
    private Boolean gender;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "create_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Value("${stuff.value:@null}")
    private Date create_date;

    @Column(name = "status")
    private boolean status;

    @ManyToOne
    @JoinColumn(name = "id_role")
    Roles roles;
    // getter setter---------------------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Boolean getGender() {
        return gender;
    }

    public void setGender(Boolean gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Accounts_Info(String name, String phone, String email, String picture, Date birthday, Boolean gender,
            String username, String password, Roles roles, Date create_date, boolean status) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.picture = picture;
        this.birthday = birthday;
        this.gender = gender;
        this.username = username;
        this.password = password;
        this.roles = roles;
        this.create_date = create_date;
        this.status = status;
    }

    public Accounts_Info(String name, String phone, String email, String picture, Date birthday, Boolean gender) {
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.picture = picture;
        this.birthday = birthday;
        this.gender = gender;
    }

    public Accounts_Info(String username, String password, String email, Integer role) {
        this.username = username;
        this.password = password;
        this.email=email;
        this.status=true;
        Roles roles = new Roles();
        roles.setId(role);
        this.roles = roles;
    }

    public Accounts_Info(Roles roles) {
        this.roles = roles;
    }

    public Accounts_Info(boolean status) {
        this.status = status;
    }

    public Accounts_Info() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
