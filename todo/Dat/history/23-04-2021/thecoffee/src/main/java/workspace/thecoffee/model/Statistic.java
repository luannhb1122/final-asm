package workspace.thecoffee.model;

import static javax.persistence.GenerationType.TABLE;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import org.springframework.data.annotation.Immutable;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Immutable
@Table(name = "Orders_view")
public class Statistic {
    
    
    @Id
    @GeneratedValue(strategy = TABLE)
    @Column(name = "id",updatable = false, nullable = false)    
    private Integer id;

    @Column(name = "id_order")    
    private Integer id_order;

    // @Column(name = "id_order")
    // private Integer id_order;

    @Column(name = "payment")
    private Integer payment;

    @Column(name = "product")
    private String product;

    @Column(name = "image")
    private String image;

    @Column(name = "price")
    private float price;

    @Column(name = "status")
    private String status;

    @Column(name = "amount")
    private Integer amount;
    
    @Column(name = "total")
    private float total;

    // public Integer getId() {
    //     return id;
    // }

    // public void setId(Integer id) {
    //     this.id = id;
    // }

    public Integer getPayment() {
        return payment;
    }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public Integer getId_order() {
        return id_order;
    }

    public void setId_order(Integer id_order) {
        this.id_order = id_order;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}

    // @OneToMany(mappedBy="orders")
	//  List<Orders_Detail> orders_Detail;
    //  private Orders_Detail orders_Detail;
    
 
