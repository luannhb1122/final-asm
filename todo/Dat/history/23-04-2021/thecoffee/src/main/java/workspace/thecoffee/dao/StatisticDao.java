package workspace.thecoffee.dao;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

//import workspace.thecoffee.model.Product_Statistic;
// import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Statistic;


public interface StatisticDao extends JpaRepository<Statistic,Integer>{
    @Query(value ="exec yearCall", nativeQuery = true)
    public List<Object> callYear();
    
// thong ke doanh thu
    @Query(value ="exec doanhThuAll", nativeQuery = true)
    public List<Object> doanhThuAll();

    @Query(value ="exec doanhThuToday", nativeQuery = true)
    public List<Object> doanhThuToday();

    @Query(value ="exec doanhthunam @year= :year", nativeQuery = true)
    public List<Object> doanhthunam(@Param("year")int year);

    @Query(value ="exec  doanhthunhieunam", nativeQuery = true)
    public List<Object> doanhthunhieunam();

    @Query(value ="exec  sumdoanhthu", nativeQuery = true)
    public List<Object> sumdoanhthu();


// thong ke hoa don
    @Query(value ="exec findOrder", nativeQuery = true)
    public List<Statistic> findAllOrder();

    @Query(value ="exec findOrderbyid @idod = :id", nativeQuery = true)
    public List<Statistic> findOrderId(@Param("id")int idod);


// thong ke theo san pham Dao
    @Query(value ="exec allProduct", nativeQuery = true)
    public List<Object> allProductSales();

    @Query(value ="exec searchProduct @pr = :pr, @mon =:mon ,@yr= :year ", nativeQuery = true)
    public List<Object> findProduct_statisticMon(@Param("pr")String pr,@Param("mon")int month,@Param("year")int year);

    @Query(value ="exec product_statisticMon @month= :mon,@year= :year ", nativeQuery = true)
    public List<Object> product_statisticMon(@Param("mon")int month,@Param("year")int year);

    @Query(value ="exec product_statisticYear @year= :year", nativeQuery = true)
    public List<Object> product_statisticYear(@Param("year")int year);

    // @Query(value ="exec billsbymonth @month= :mon", nativeQuery = true)
    // public List<Object> billsInMon(@Param("mon")int month);

    // @Query(value ="exec billsAll", nativeQuery = true)
    // public List<Object> billsAll();


}
