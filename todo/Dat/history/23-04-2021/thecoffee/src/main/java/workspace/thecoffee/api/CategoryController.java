package workspace.thecoffee.api;

import java.util.List;

import javax.validation.*;
import javax.validation.constraints.*;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Categories;
import workspace.thecoffee.service.CategoryService;

@CrossOrigin
@RequestMapping("api/category")
@RestController
public class CategoryController {
    private final CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService){
        this.categoryService= categoryService; 
    }

// lấy toàn bộ thông tin Category trong hệ thống
     /*
    Input Get URL= http://localhost:8081/api/category
    OutPut Data:
     [category{
        "id": value,
        "type": "value",
        "status": value
    },
    category{},..
    category{}
    ]
    */
    @GetMapping
    public List<Categories> getAllCategory(){
       return categoryService.getAllCategory();
    }
// tìm Category  theo id
    /*
    Input Get URL= http://localhost:8081/api/Category/{id} 
    OutPut Data:
     Category [id]{
        "id": value,
        "type": "value",
        "status": value
    }    
    */
    @GetMapping(path = "{id}")
    public Categories getCategoryById(@PathVariable ("id") Integer id){
        return categoryService.getCategoryById(id)
                .orElse(null);
    }

// thêm một Category vào hệ thống
     /*
    Input Post URL= http://localhost:8081/api/Category/{body} 
    Input Data:
     body{        
        "type": "value"     
    } 
    */
    @PostMapping
    public void addCategory(@RequestBody Categories categories){
        categoryService.addCategory(categories);
      }

//xóa một Category khỏi hệ thống Input Delete URL= http://localhost:8081/api/Category/{id}   
    //  @DeleteMapping(path = "{id}")
    //  public void deleteCategoryById(@PathVariable ("id") Integer id){
    //     categoryService.deleteCategory(id);
    //  }

     @DeleteMapping(path = "{id}")
     public void deleteCategoryAdmin(@PathVariable ("id") Integer id){
        categoryService.deleteCategoryAdmin(id);
     }
 // chính sửa thông tin tài khoản theo id 
    /*
    Input Put URL= http://localhost:8081/api/Category/{id} 
    Input Data:
     {
        "id":{id},
        "type": "repvalue",
        "status": "repvalue"
    }
    OutPut data 
     {
        "id":{id},
        "type": "repvalue",
        "status": "repvalue"
    } 
    */
    @PutMapping(path = "{id}")
    public void updateCategoryById(@PathVariable ("id") Integer id,@Valid @NotNull @RequestBody Categories categoryToUpdate){
        categoryService.updateCategory(id, categoryToUpdate);
    }
}
