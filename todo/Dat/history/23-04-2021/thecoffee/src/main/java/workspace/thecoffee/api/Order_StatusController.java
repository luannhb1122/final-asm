package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders_Status;
import workspace.thecoffee.service.Order_StatusService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/order_status")
@RestController
public class Order_StatusController {
    private final Order_StatusService order_StatusService;

    @Autowired
    public Order_StatusController(Order_StatusService order_StatusService){
        this.order_StatusService= order_StatusService; 
    }
// lấy toàn bộ thông tin order_status trong hệ thống
     /*
    Input Get URL= http://localhost:8081/api/order_status
    OutPut Data:
     [order_status {
        "id": value,
        "status": value,
        "comment": "value",
        "orders": value
    },
    order_status{},..
    order_status{}
    ]
    */
    @GetMapping
    public List<Orders_Status> getAllOrder_Status(){
       return order_StatusService.getAllOrder_Status();
    }

// tìm Orders_Status theo id
    /*
    Input Get URL= http://localhost:8081/api/Orders_Status/{id} 
    OutPut Data:
     Orders_Status[id]{
        "id": value,
        "status": value,
        "comment": "value",
        "orders": value
    }
    */

    @GetMapping(path ="{id}" )
    public Orders_Status  getOrders_StatusById(@PathVariable("id") Integer id){
        return order_StatusService.getOrders_StatusById(id).orElse(null);
    }
   
//xóa một Orders_Status khỏi hệ thống Input Delete URL= http://localhost:8081/api/Orders_Status/{id} 
  
    @DeleteMapping(path ="{id}")
    public void deleteOrders_Status(@PathVariable("id") Integer id){
        order_StatusService.deleteOrders_Status(id);
    }
// chính sửa thông tin Orders_Status theo id 
    /*
    Input Put URL= http://localhost:8081/api/Orders_Status/{id} 
    Input Data:
     {        
        {
        "id": {id},
        "status": repvalue,
        "comment": "repvalue",
        "orders": value
    }
    OutPut data 
        {
        "id": {id},
        "status": repvalue,
        "comment": "repvalue",
        "orders": value
    }   
    */
    @PutMapping(path ="{id}")
    public void updatOrders_Status(@PathVariable("id") Integer id,@RequestBody Orders_Status newods){
        order_StatusService.updatOrders_Status(id,newods);
    }
}
