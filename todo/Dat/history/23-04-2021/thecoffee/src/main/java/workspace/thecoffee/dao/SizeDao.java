package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Sizes;

public interface SizeDao extends JpaRepository<Sizes,Integer>{
    
}
