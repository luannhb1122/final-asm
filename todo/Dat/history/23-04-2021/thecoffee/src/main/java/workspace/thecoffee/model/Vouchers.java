package workspace.thecoffee.model;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Vouchers")
public class Vouchers {  
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "value")
    private Double value;

    @Column(name = "start_time")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date start_time;

    @Column(name = "end_time")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date end_time;

    @Column(name = "quanity")
    private Integer quanity;

    @OneToMany(mappedBy="vouchers")
	List<Orders> orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Date getStart_time() {
        return start_time;
    }

    public void setStart_time(Date start_time) {
        this.start_time = start_time;
    }

    public Date getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Date end_time) {
        this.end_time = end_time;
    }

    public Integer getQuanity() {
        return quanity;
    }

    public void setQuanity(Integer quanity) {
        this.quanity = quanity;
    }

    // public List<Orders> getOrders() {
    //     return orders;
    // }

    // public void setOrders(List<Orders> orders) {
    //     this.orders = orders;
    // }

    public Vouchers(Boolean status, Double value, Date start_time, Date end_time, Integer quanity) {
        this.status = status;
        this.value = value;
        this.start_time = start_time;
        this.end_time = end_time;
        this.quanity = quanity;
    }

    public Vouchers() {
    }
    
}
