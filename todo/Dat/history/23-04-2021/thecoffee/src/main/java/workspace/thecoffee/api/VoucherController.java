package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Vouchers;
import workspace.thecoffee.service.VoucherService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/voucher")
@RestController
public class VoucherController {
    private final VoucherService voucherService;

    @Autowired
    public VoucherController(VoucherService voucherService){
        this.voucherService= voucherService; 
    }
// lấy toàn bộ thông tin voucher trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/voucher
    OutPut Data:
     [voucher{
        "id": "value",
        "status": "value",
        "value": "value",
        "start_time": "value",
        "end_time": "value",
        "quanity": "value"
    },
    voucher{},..
    voucher{}
    ]
    */
    @GetMapping
    public List<Vouchers> getAllVoucher(){
       return voucherService.getAllVoucher();
    }

 // tìm voucher theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/voucher/{id} 
    OutPut Data:
     voucher[id]{
        "id": "id",
        "status": "value",
        "value": "value",
        "start_time": "value",
        "end_time": "value",
        "quanity": "value"
    },
    */   
    @GetMapping(path ="{id}" )
    public Vouchers  getVoucherById(@PathVariable("id") Integer id){
        return voucherService.getVoucherById(id).orElse(null);
    }

    // thêm một voucher vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/voucher/{body} 
    Input Data:
     body{
        "id": "newvalues",
        "status": "newvalues",
        "value": "newvalues",
        "start_time": "newvalues",
        "end_time": "newvalues",
        "quanity": "newvalues"
    },
    */
    @PostMapping
    public void addVoucher(@RequestBody Vouchers Vou){
        voucherService.addVoucher(Vou);
    }

    //xóa một voucher khỏi hệ thống Input Delete URL= http://localhost:8081/api/voucher/{id} 
     @DeleteMapping(path ="{id}")
    public void deleteVoucher(@PathVariable("id") Integer id){
        voucherService.deleteVoucher(id);
    }
// chính sửa thông tin voucher theo id 
    /*
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng 
    Input Put URL= http://localhost:8081/api/voucher/{id} 
    Input Data:
     {
        "id": "value",
        "status": "repvalue",
        "value": "repvalue",
        "start_time": "repvalue",
        "end_time": "repvalue",
        "quanity": "repvalue"
    }
    OutPut data 
        {
        "id": "values",
        "status": "repvalue",
        "value": "repvalue",
        "start_time": "repvalue",
        "end_time": "repvalue",
        "quanity": "repvalue"
    } 
    */
    @PutMapping(path ="{id}")
    public void updatVoucher(@PathVariable("id") Integer id,@RequestBody Vouchers newVou){
        voucherService.updatVoucher(id,newVou);
    }
}
