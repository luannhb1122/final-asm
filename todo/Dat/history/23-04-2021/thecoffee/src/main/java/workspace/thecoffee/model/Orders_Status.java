package workspace.thecoffee.model;

// import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "Orders_Status")
public class Orders_Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "status")
    private boolean status;

    @Column(name = "comment")
    private String comment;

    @OneToOne
    @JoinColumn(name="id_order", referencedColumnName = "id")
    private Orders orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }   

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public Orders_Status() {
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Orders_Status(boolean status, String comment, Orders orders) {
        this.status = status;
        this.comment = comment;
        this.orders = orders;
    }

    
}
