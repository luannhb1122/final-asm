package workspace.thecoffee.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import workspace.thecoffee.config.MailConfig;
import workspace.thecoffee.dao.AccountDao;
import workspace.thecoffee.model.Accounts_Info;

@Service
public class AccountService {
    private final AccountDao accountDao;

    @Autowired
    public AccountService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Autowired
    public MailConfig mailConfig;

    @Autowired
    public OTPService otpService;

    public String randomPassword() {
        String upperAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String numbers = "0123456789";

        // combine all strings
        String alphaNumeric = upperAlphabet + numbers;

        // create random string builder
        StringBuilder sb = new StringBuilder();

        // create an object of Random class
        Random random = new Random();

        // specify length of random string
        int length = 10;

        for (int i = 0; i < length; i++) {

            // generate random index number
            int index = random.nextInt(alphaNumeric.length());

            // get character specified by index
            // from the string
            char randomChar = alphaNumeric.charAt(index);

            // append the character to string builder
            sb.append(randomChar);

        }

        String randomString = sb.toString();
        return randomString;
    }

    public List<Accounts_Info> getAllAccount() {
        return accountDao.findAll();
    }

    public List<Accounts_Info> getAllEmp() {
        return accountDao.searchAllEmp();
    }

    public List<Accounts_Info> getAllCus() {
        return accountDao.searchAllCus();
    }
    // public void addAccount(Accounts_Info account) {
    // accountDao.save(new Accounts_Info
    // ( account.getName(),
    // account.getPhone(),
    // account.getEmail(),
    // account.getBirthday(),
    // account.getGender(),
    // account.getPicture()

    // )
    // );
    // }

    // public void addAccountNull(){
    // accountDao.save(new Accounts_Info(null,null,null,null,null,null));
    // }

    public Accounts_Info getAccountByUsername(String username) {

        return accountDao.searchAccountByUsername(username);

    }

    public String hashPassword(String password){
        String hashPassword = BCrypt.hashpw(password, BCrypt.gensalt(10));
        return hashPassword;
    }

    
    public Boolean hashPasswordCompare(String text, String textHashed){
        Boolean checkValue = BCrypt.checkpw(text, textHashed);
        return checkValue;
    }

    public void addCustomerAccount(Accounts_Info acc) {
        String hashPassword = hashPassword(acc.getPassword());
        Accounts_Info accounts_Info = new Accounts_Info(acc.getUsername(), hashPassword, acc.getEmail(), 2);
        accountDao.save(accounts_Info);
    }

    public String addEmployeeAccount(Accounts_Info acc) {
        String randomPassword = this.randomPassword();
        String hashPassword = hashPassword(randomPassword);
        Accounts_Info accounts_Info = new Accounts_Info(acc.getUsername(), hashPassword, acc.getEmail(), 1);
        accountDao.save(accounts_Info);

            
            String titleMail = "The coffee Son create random Admin password";

            String content = "Password của bạn là: "+ randomPassword;

            mailConfig.sendEmail(acc.getEmail(), titleMail, content);
            
            return"Success"; 
    }

    public String forgotPassword(String username, String email) {
        Accounts_Info accounts_Info = this.getAccountByUsername(username);
        if (accounts_Info != null && accounts_Info.getEmail().equals(email)) {
            int otp = otpService.generateOTP(username);
            Map<String, String> replacements = new HashMap<String, String>();

            replacements.put("user", username);
            replacements.put("otpnum", String.valueOf(otp));

            String titleMail = "The coffee Son Forgotpassword";

            String content = "Mã OTP của bạn là: "+ otp;

            mailConfig.sendEmail(email, titleMail, content);
            
            return"Success"; 
        }
        else
        return"Failed"; 
    }

    public String forgotPasswordConfirm(int otpnum,String username, String password) {
        //Validate the Otp 
		if(otpnum >= 0){
            int serverOtp = otpService.getOtp(username);
              if(serverOtp > 0){
                if(otpnum == serverOtp){
                    otpService.clearOTP(username);
                    Accounts_Info accounts_Info = this.getAccountByUsername(username);
                    String hashPassword = hashPassword(password);
                    accounts_Info.setPassword(hashPassword);
                    accountDao.save(accounts_Info);
                    return"Success";
                  }
                  else
                  return"Failed"; 
           }
           else
           return"Failed"; 
        }
        else
        return"Failed"; 
    }

    public String ChangePassword(String username, String oldPassword, String newPassword) {
        Accounts_Info accounts_Info = this.getAccountByUsername(username);
        if (accounts_Info != null && hashPasswordCompare(oldPassword, accounts_Info.getPassword())) {
            String hashNewPassword = hashPassword(newPassword);
            accounts_Info.setPassword(hashNewPassword);
            accountDao.save(accounts_Info);
            return"Success";
        }
        else
        return"Failed";
    }

    public void addAccount(Accounts_Info acc) {
        accountDao.save(acc);
    }

    public Optional<Accounts_Info> getAccountById(Integer id) {
        return accountDao.findById(id);
    }

    public Optional<Accounts_Info> findMaxId() {
        return accountDao.findMaxId();
    }

    public void deleteAccount(Integer id) {
        accountDao.deleteById(id);
    }

    // public void CusaddAccount(Accounts_Info cus){
    // Optional<Accounts_Info> typeAccountData = findMaxId();
    // Accounts_Info oldAccount = typeAccountData.get();
    // System.out.println(oldAccount);
    // oldAccount.setCustomers(cus);
    // accountDao.save(oldAccount);
    // }

    // public void EmpaddAccount(Employees emp){
    // addAccountNull();
    // Optional<Accounts_Info> typeAccountData = findMaxId();
    // Accounts_Info oldAccount = typeAccountData.get();
    // System.out.println(oldAccount);
    // oldAccount.setEmployees(emp);
    // accountDao.save(oldAccount);
    // }

    public void updateAccount(Integer id, Accounts_Info newAccount) {
        Optional<Accounts_Info> typeAccountData = getAccountById(id);
        Accounts_Info oldAccount = typeAccountData.get();
        System.out.println(oldAccount);
        oldAccount.setName(newAccount.getName());
        oldAccount.setPhone(newAccount.getPhone());
        oldAccount.setEmail(newAccount.getEmail());
        oldAccount.setBirthday(newAccount.getBirthday());
        oldAccount.setGender(newAccount.getGender());
        oldAccount.setPicture(newAccount.getPicture());
        accountDao.save(oldAccount);
    }

}
