package workspace.thecoffee.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Roles;

public interface  RoleDao extends JpaRepository<Roles,Integer> {
    
}
