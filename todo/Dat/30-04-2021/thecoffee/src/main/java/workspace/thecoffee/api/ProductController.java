package workspace.thecoffee.api;

import java.util.List;

import javax.validation.*;
import javax.validation.constraints.*;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Products;
import workspace.thecoffee.service.ProductService;

@CrossOrigin
@RequestMapping("api")
@RestController
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService){
        this.productService= productService; 
    }

   // lấy toàn bộ product trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/product
    OutPut Data:
     [product{
        "id": "value",
        "name": "value",
        "price": value,
        "image": "value",
        "status": "value",
        "categories":"value"
    },
    product{},..
    product{}
    ]
    */
    @GetMapping("/admin/product")
    public List<Products> AllProduct(){
       return productService.AllProduct();
    }

    // lấy toàn bộ product còn kinh doanh trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/product
    OutPut Data:
     [product{
        "id": "value",
        "name": "value",
        "price": value,
        "image": "value",
        "status": "value",
        "categories":"value"
    },
    product{},..
    product{}
    ]
    */
    @GetMapping("/user/product/true")
    public List<Products> getAllProduct(){
       return productService.getAllProduct();
    }
    
    // tìm product theo tên
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/product/search
    Input Data: {param pr= ?}
    
    OutPut Data:
      [product{
        "id": "value",
        "name": "*?*",
        "price": value,
        "image": "value",
        "status": "value",
        "categories":"value"
    },
    product{},..
    product{}
    ]
    */
    @GetMapping("/user/product/search")
    public List<Products> findProduct(@RequestParam("pr") String pr){
        System.out.println(pr);
        return productService.findProduct(pr);              
    }

// tìm product theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/product/{id} 
    OutPut Data:
    product{
        "id": "{id}",
        "name": "value",
        "price": value,
        "image": "value",
        "status": "value",
        "categories":"value"
    }
    */
    @GetMapping("/user/product/{id}")
    public Products getProductById(@PathVariable ("id") Integer id){
        return productService.getProductById(id)
                .orElse(null);
    }


// thêm một product vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/product/{body} 
    Input Data:
      body:{       
        "name": "newvalues",
        "price": newvalues,
        "image": "newvalues",       
        "categories":"newvalues"
    }
    */
    @PostMapping("/admin/product")
    public void addProduct(@RequestBody Products products){
        productService.addProduct(products);
    }

//xóa một product khỏi hệ thống Input Delete URL= http://localhost:8081/api/product/{id} 
    @DeleteMapping("/admin/product/{id}")
     public void deleteProductById(@PathVariable ("id") Integer id){
        productService.deleteProduct(id);
     }

     // chính sửa thông tinproduct theo id 
    /*
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng 
    Input Put URL= http://localhost:8081/api/product/{id} 
    Input Data:
        product{
        "id": "{id}",
        "name": "repvalue",
        "price": repvalue,
        "image": "repvalue",
        "status": "repvalue",
        "categories":"repvalue"
    }
    OutPut data 
          product{
        "id": "{id}",
        "name": "repvalue",
        "price": repvalue,
        "image": "repvalue",
        "status": "repvalue",
        "categories":"repvalue"
    }
    */
    @PutMapping("/admin/product/{id}")
    public void updateProductById(@PathVariable ("id") Integer id,@Valid @NotNull @RequestBody Products productToUpdate){
        productService.updateProduct(id, productToUpdate);
    }
}
