package workspace.thecoffee.api;

import java.util.List;

import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Orders;
import workspace.thecoffee.service.OrderService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api")
@RestController
public class OrderController {
    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService){
        this.orderService= orderService; 
    }
// lấy toàn bộ thông tin Orders trong hệ thống
     /*
      *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/order
    OutPut Data:
     [Orders {
        "id": "value",
        "date_created": "value",
        "payment": "value",
        "customers": "value" ,
        "vouchers": "value",
        "orders_Detail": [{order1},{order2},..,{orderD}]
     }
    Orders{},..
    Orders{}
    ]
    */
    @GetMapping("admin/order")
    public List<Orders> getAllOrder(){
       return orderService.getAllOrder();
    }
// tìm Orders theo id
    /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/order/{id} 
    OutPut Data:
     Orders[id]{
        "id": "value",
        "date_created": "value",
        "payment": "value",
        "customers": "value",
        "vouchers": "value",
        "orders_Detail": [{order1},{order2},..,{orderD}]
     }   
    */
    @GetMapping("admin/order/{id}" )
    public Orders getOrderById (@PathVariable("id") Integer id){
        return orderService.getOrderById(id).orElse(null);
    }
    //lấy order theo khách hàng
    @GetMapping("user/order" )
    public List <Orders> getAllOrderByIdCustomer (@RequestParam("id_cus") Integer id){
        return orderService.getAllOrderByIdCustomer(id);
    }

// thêm một Order vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/order/{body} 
    Input Data:
     body{
        
        "date_created": "newvalue",
        "payment": "newvalue",
        "customers": "newvalue",
        "vouchers": "newvalue",
        "orders_Detail": [{neworderD1},{neworderD2},..,{orderD}]
     }
    */
    @PostMapping("user/order" )
    public void addOrder (@RequestBody Orders od){
        orderService.addOrder(od);
    }
//xóa một Order khỏi hệ thống Input Delete URL= http://localhost:8081/api/order/{id} 
 
    @DeleteMapping("user/order/{id}")
    public void deleteOrder(@PathVariable("id") Integer id){
        orderService.deleteOrder(id);
    }
// chính sửa thông tin order theo id 
    /* 
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng    

    Input Put URL= http://localhost:8081/api/order/{id} 
    Input Data:
    {
        "id": "{id}",
        "date_created": "value",
        "payment": "repvalue",
        "customers": "value",
        "vouchers": "repvalue",
        "orders_Detail": [{reporderD1},{reporderD2},..,{reporderD}]
     }
    OutPut data 
         {
        "id": "{id}",
        "date_created": "value",
        "payment": "repvalue",
        "customers": "value",
        "vouchers": "repvalue",
        "orders_Detail": [{reporderD1},{reporderD2},..,{reporderD}]
     }
    */
    @PutMapping("user/order/{id}" )
    public void updateOrder(@PathVariable("id") Integer id,@RequestBody Orders newod){
        orderService.updateOrder(id, newod);
    }
    // @GetMapping("max")
    // public Orders findMaxId(){
    //     return orderService.findMaxId().orElse(null);
    // }
}
