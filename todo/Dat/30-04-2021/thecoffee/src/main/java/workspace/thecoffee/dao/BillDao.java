package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;
import workspace.thecoffee.model.Bills;

public interface BillDao extends JpaRepository<Bills,Integer> {
    @Query(value = "select * from bills_view", nativeQuery = true)
    public List<Bills> findAllBill();

}
