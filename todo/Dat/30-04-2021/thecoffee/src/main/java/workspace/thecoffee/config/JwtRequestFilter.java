package workspace.thecoffee.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {

		try {
			// JWT Token is in the form "Bearer token". Remove Bearer word and
			// get only the Token
			String jwtToken = extractJwtFromRequest(request);

			if (StringUtils.hasText(jwtToken) && jwtTokenUtil.validateToken(jwtToken)) {
				UserDetails userDetails = new User(jwtTokenUtil.getUsernameFromToken(jwtToken), "",
						jwtTokenUtil.getRolesFromToken(jwtToken));

				UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
						userDetails, null, userDetails.getAuthorities());
				// After setting the Authentication in the context, we specify
				// that the current user is authenticated. So it passes the
				// Spring Security Configurations successfully.
				SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
			} else {
				System.out.println("Cannot set the Security Context");
			}
		} catch (ExpiredJwtException ex) {

			String isRefreshToken = request.getHeader("isRefreshToken");
			String requestURL = request.getRequestURL().toString();
			// allow for Refresh Token creation if following conditions are true.
			if (isRefreshToken != null && isRefreshToken.equals("true") && requestURL.contains("refreshtoken")) {
				allowForRefreshToken(ex, request);
			} else
				request.setAttribute("exception", ex);

		} catch (BadCredentialsException ex) {
			request.setAttribute("exception", ex);
		} catch (Exception ex) {
			System.out.println("cmm");
		}
		chain.doFilter(request, response);
	}

	private void allowForRefreshToken(ExpiredJwtException ex, HttpServletRequest request) {

		// create a UsernamePasswordAuthenticationToken with null values.
		UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
				null, null, null);
		// After setting the Authentication in the context, we specify
		// that the current user is authenticated. So it passes the
		// Spring Security Configurations successfully.
		SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
		// Set the claims so that in controller we will be using it to create
		// new JWT
		request.setAttribute("claims", ex.getClaims());

	}

	private String extractJwtFromRequest(HttpServletRequest request) {
		String bearerToken = request.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
			return bearerToken.substring(7, bearerToken.length());
		}
		return null;
	}

	// @Autowired
	// private JwtLoginService jwtLoginService;

	// @Autowired
	// private JwtTokenUtil jwtTokenUtil;

	// @Override
	// protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
	// 		throws ServletException, IOException {

	// 	final String requestTokenHeader = request.getHeader("Authorization");

	// 	String username = null;
	// 	String jwtToken = null;
	// 	// JWT Token is in the form "Bearer token". Remove Bearer word and get only the Token
	// 	if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
	// 		jwtToken = requestTokenHeader.substring(7);
	// 		try {
	// 			username = jwtTokenUtil.getUsernameFromToken(jwtToken);
	// 		} catch (IllegalArgumentException e) {
	// 			System.out.println("Unable to get JWT Token");
	// 		} catch (ExpiredJwtException e) {
	// 			System.out.println("JWT Token has expired");
	// 		}
	// 	} else {
	// 		logger.warn("JWT Token does not begin with Bearer String");
	// 	}

	// 	//Once we get the token validate it.
	// 	if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

	// 		UserDetails userDetails = this.jwtLoginService.loadUserByUsername(username);

	// 		// if token is valid configure Spring Security to manually set authentication
	// 		if (jwtTokenUtil.validateToken(jwtToken, userDetails)) {

	// 			UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
	// 					userDetails, null, userDetails.getAuthorities());
	// 			usernamePasswordAuthenticationToken
	// 					.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
	// 			// After setting the Authentication in the context, we specify
	// 			// that the current user is authenticated. So it passes the Spring Security Configurations successfully.
	// 			SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
	// 		}
	// 	}
	// 	chain.doFilter(request, response);
	// }

}