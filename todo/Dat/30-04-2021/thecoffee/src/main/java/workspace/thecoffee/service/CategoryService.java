package workspace.thecoffee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.CategoryDao;
import workspace.thecoffee.model.Categories;

@Service
public class CategoryService {
    private final CategoryDao categorydao;

    @Autowired
    public CategoryService(CategoryDao categorydao) {
        this.categorydao = categorydao;
    }

    public void addCategory(Categories categories) {
        categorydao.save(categories);
    }

    public List<Categories> getAllCategory() {
        return categorydao.findAll();
    }

    public Optional<Categories> getCategoryById(Integer id) {
        return categorydao.findById(id);
    }

    public void deleteCategoryAdmin(Integer id) {
        categorydao.deleteById(id);
    }

    public void updateCategory(Integer id, Categories newCategory){
        Optional<Categories> categoryData = getCategoryById(id);
        Categories oldCategory = categoryData.get();
        oldCategory.setType(newCategory.getType());
        categorydao.save(oldCategory);
    }

    public void deleteCategory(Integer id){
        Optional<Categories> categoryData = getCategoryById(id);
        Categories oldCategory = categoryData.get();
        oldCategory.setStatus(false);
        categorydao.save(oldCategory);
    }
}