package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Stores;
import workspace.thecoffee.service.StoreService;
// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api")
@RestController
public class StoreController {
    private final StoreService storeService;

    @Autowired
    public StoreController(StoreService storeService){
        this.storeService= storeService; 
    }

// lấy toàn bộ thông tin store trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/store
    OutPut Data:
     [store {
        "id": value,
        "name": "value",
        "phone": "value",
        "email": "value",
        "address": "value"
    },
    store{},..
    store{}
    ]
    */
    @GetMapping("/admin/store")
    public List<Stores> getAllStore(){
       return storeService.getAllStore();
    }

// tìm store theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/store/{id} 
    OutPut Data:
     store[id]{
        "id": value,
        "name": "value",
        "phone": "value",
        "email": "value",
        "address": "value"
    }
    */    
    @GetMapping("/admin/store/{id}" )
    public Stores  getStoreById(@PathVariable("id") Integer id){
        return storeService.getStoreById(id).orElse(null);
    }

// thêm một store vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/store/{body} 
    Input Data:
     body{
        "id": newvalues,
        "name": "newvalues",
        "phone": "newvalues",
        "email": "newvalues",
        "address": "newvalues"
    }
    */   
    @PostMapping("/admin/store")
    public void addStore(@RequestBody Stores store){
        storeService.addStore(store);
    }

//xóa một store khỏi hệ thống Input Delete URL= http://localhost:8081/api/store/{id} 
    
    @DeleteMapping("/admin/store/{id}" )
    public void deleteStore(@PathVariable("id") Integer id){
        storeService.deleteStore(id);
    }

    // chính sửa thông tin store theo id 
    /*
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng 
    Input Put URL= http://localhost:8081/api/store/{id} 
    Input Data:
 store{
        "id": id,
        "name": "repvalue",
        "phone": "repvalue",
        "email": "repvalue",
        "address": "repvalue"
    }
    OutPut data 
  store{
        "id": id,
        "name": "repvalue",
        "phone": "repvalue",
        "email": "repvalue",
        "address": "repvalue"
    }  
    */
    @PutMapping("/admin/store/{id}" )
    public void updateStore(@PathVariable("id") Integer id,@RequestBody Stores newStore){
        storeService.updateStore(id,newStore);
    }
}
