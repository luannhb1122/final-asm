package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.Order_StatusDao;
import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Orders_Status;

@Service
public class Order_StatusService {
    private final Order_StatusDao order_StatusDao;
    private final BillService billService;
    
    @Autowired
    public Order_StatusService(Order_StatusDao order_StatusDao, BillService billService) {
        this.order_StatusDao = order_StatusDao;
        this.billService= billService; 
    }

    public List<Orders_Status> getAllOrder_Status() {
        return order_StatusDao.findAll();
    }

    public List<Orders_Status> findOrder_statusUser(int st, int id_cus) {
        return order_StatusDao.findOrder_statusUser(st,id_cus);
    }

    public Optional<Orders_Status> getOrders_StatusById(Integer id) {
        return order_StatusDao.findById(id);
    }

    public void addOrders_Status(Orders od) {
        Orders_Status ods = new Orders_Status();                 
            ods.setStatus(1);
            ods.setComment("on working");
            ods.setOrders(od);
            order_StatusDao.save(ods);
            
    }
    public void successOrders_Status(Integer id){
        Optional<Orders_Status> Orders_StatusData = getOrders_StatusById(id);
        Orders_Status oldOds = Orders_StatusData.get();                 
            oldOds.setStatus(2);
            oldOds.setComment("success");
            billService.addNewBill(oldOds.getOrders());            
            order_StatusDao.save(oldOds);            
    }

    public void failOrders_Status(Integer id){
        Optional<Orders_Status> Orders_StatusData = getOrders_StatusById(id);
        Orders_Status oldOds = Orders_StatusData.get();                 
            oldOds.setStatus(0);
            oldOds.setComment("Fail");
            // billService.addNewBill(oldOds.getOrders());            
            order_StatusDao.save(oldOds);            
    }


    public void updatOrders_Status(Integer id, Orders_Status newOds){
        Optional<Orders_Status> Orders_StatusData = getOrders_StatusById(id);
        Orders_Status oldOds = Orders_StatusData.get();
        oldOds.setStatus(newOds.isStatus());
        oldOds.setComment(newOds.getComment());
        oldOds.setOrders(newOds.getOrders());
        order_StatusDao.save(oldOds);
    }

    public void deleteOrders_Status(Integer id) {
        order_StatusDao.deleteById(id);
    }

}
