package workspace.thecoffee.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.FeedbackDao;
import workspace.thecoffee.model.Feedbacks;

@Service
public class FeedbackService {
    private final FeedbackDao feedbackDao;
    @Autowired
    public FeedbackService(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }

    public List<Feedbacks> getAllFeedback() {
        return feedbackDao.findAll();
    }

    public List<Feedbacks> findFeedbacksbyProduct(int id_pr) {
        return feedbackDao.findFeedbacksbyProduct(id_pr);
    }

    public Optional<Feedbacks> getFeedbackById(Integer id) {
        return feedbackDao.findById(id);
    }

    public void deleteFeedback(Integer id) {
        feedbackDao.deleteById(id);
    }

    public void addFeedBack(Feedbacks fb){
        feedbackDao.save(fb);
    }

    public void checkFalseFeedBack(Integer id){
        Optional<Feedbacks> typeFeedbackData = getFeedbackById(id);
        Feedbacks fb = typeFeedbackData.get();
        fb.setStatus(false);
        feedbackDao.save(fb);
    }

    public void checkTrueFeedBack(Integer id){
        Optional<Feedbacks> typeFeedbackData = getFeedbackById(id);
        Feedbacks fb = typeFeedbackData.get();
        fb.setStatus(true);
        feedbackDao.save(fb);
    }
    public void updateFeedBack(Integer id, Feedbacks newfb){
        Optional<Feedbacks> typeFeedbackData = getFeedbackById(id);
        Feedbacks oldfb = typeFeedbackData.get();
        oldfb.setNote(newfb.getNote());
        oldfb.setStatus(newfb.getStatus());
        oldfb.setRate(newfb.getRate());
        oldfb.setDate_time(newfb.getDate_time());
        oldfb.setCustomers(newfb.getCustomers());
        oldfb.setProducts(newfb.getProducts());       
        feedbackDao.save(oldfb);
    }

}
