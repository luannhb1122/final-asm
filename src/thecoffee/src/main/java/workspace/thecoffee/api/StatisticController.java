package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import workspace.thecoffee.model.Product_Statistic;
// import workspace.thecoffee.model.Orders;
import workspace.thecoffee.model.Statistic;
// import workspace.thecoffee.service.OrderService;
import workspace.thecoffee.service.StatisticService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/admin/check")
@RestController
public class StatisticController {
    private final StatisticService statisticService;

    @Autowired
    public StatisticController(StatisticService statisticService){
        this.statisticService= statisticService; 
    }
// API call year
// lấy toàn bộ thông tin năm kinh doanh trong hệ thông trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/check/year
    OutPut Data:
    [
    2020,
    2021]
    */
    @GetMapping("/year" )
    public  List<Object> callYear (){
        return statisticService.callYear();
    }
// API call order
// lấy toàn bộ thông tin Đơn hàng  kinh doanh trong hệ thông trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/check
    OutPut Data:
      {
        "id_order": 2,
        "payment": 1,
        "product": "Cà Phê Sữa Baileys",
        "image": "https://res.cloudinary.com/minhchon/image/upload/v1617775807/Thecoffeeson/cfe.png",
        "price": 48000,
        "status": "onboard",
        "amount": 5,
        "total": 240000
    },{order2},..,{orderN}
    */
    @GetMapping
    public List<Statistic> getAllStatic(){
       return  statisticService.getAllStatic();
    }
// lấy toàn bộ thông tin năm kinh doanh trong hệ thông trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/check/{id}
    OutPut Data:
      {
        "id_order": {id},
        "payment": value,
        "product": "va"lue",
        "image": value",
        "price": value,
        "status": "value",
        "amount": "value",
        "total": "value"
    }
    */
    @GetMapping(path ="{id}" )
    public List<Statistic> findOrderId (@PathVariable("id") Integer id){
        return statisticService.findOrderId(id);
    }
// API Call Product
// lấy toàn bộ thông tin doanh thu theo sản phẩm trong hệ thông trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/check/product
    OutPut Data:
      ["bạch xỉu đá",1,32000],[tên sản phâm, số lượng bán ,tổng tiền bán của sản phẩm],..,[]
    */
    @GetMapping("/product" ) // call tat ca san pham da ban
    public  List<Object> allProductSales (){
        return statisticService.allProductSales();
    }
    // lấy toàn bộ thông tin doanh thu 1 sản phẩm trong tháng của hệ thống
     /*    
     *v giá trị param chuyền vào 
    Input Get URL= http://localhost:8081/api/check/product/find?pr=*v&mon=*v&year=*v
    OutPut Data:
      [tên sản phâm, số lượng bán ,tổng tiền bán của sản phẩm]
    */
    @GetMapping("/product/find" ) // call 1 san pham ban duoc trong thang
    public  List<Object> findProduct_statisticMon (@RequestParam("pr") String pr,@RequestParam("mon") Integer mon,@RequestParam("year") Integer year){
        return statisticService.findProduct_statisticMon(pr, mon, year);
    }

    // lấy thông tin doanh thu của sản phẩm theo tháng trong hệ thông
     /*
     *value giá trị Database của đối tượng 
      *v giá trị param chuyền vào
    Input Get URL= http://localhost:8081/api/check/product/mon?mon=*v&year=*v
    OutPut Data:
      ["bạch xỉu đá",1,32000],[tên sản phâm, số lượng bán ,tổng tiền bán của sản phẩm],..,[]
    */
    @GetMapping("/product/mon" ) //call tat ca san pham ban duoc trong thang
    public  List<Object> product_statisticMon (@RequestParam("mon") Integer mon,@RequestParam("year") Integer year){
        return statisticService.product_statisticMon(mon, year);
    }
     // lấy thông tin doanh thu của sản phẩm theo năm trong hệ thông
     /*
     *value giá trị Database của đối tượng 
      *v giá trị param chuyền vào
    Input Get URL= http://localhost:8081/api/check/product/year?&year=*v
    OutPut Data:
      ["bạch xỉu đá",1,32000],[tên sản phâm, số lượng bán ,tổng tiền bán của sản phẩm],..,[]
    */
    @GetMapping("/product/year" ) // call tat ca san pham ban duoc trong nam
    public  List<Object> product_statisticYear (@RequestParam("year") Integer year){
        return statisticService.product_statisticYear( year);
    }
//API call doanh thu
  // lấy thông tin doanh thu của ngày hôm nay
     /*
     *value giá trị Database của đối tượng 
      *v giá trị param chuyền vào
    Input Get URL= http://localhost:8081/api/check/doanhthu/today
    OutPut Data:
     [
        "Cà Phê Bailey",
        2,
        "2021-04-23",
        90000
    ],[trên sản phẩm hôm nay bán,số lượng bán,ngày,doanhthu],
    */
    @GetMapping("/doanhthu/today" )
    public  List<Object> doanhThuToday(){
        return statisticService.doanhThuToday();
    }

 // lấy thông tin doanh thu toàn bộ
     /*
     *value giá trị Database của đối tượng 
      *v giá trị param chuyền vào
    Input Get URL= http://localhost:8081/api/check/doanhthu/All
    OutPut Data:
    [
        2,
        2020,
        368000
    ],[tháng,năm,doanhthu tháng đó],[]...
    */    
    @GetMapping("/doanhthu/All" )
    public  List<Object> doanhThuAll (){
        return statisticService.doanhThuAll();
    }
 // lấy thông tin doanh thu của năm
     /*
     *value giá trị Database của đối tượng 
      *v giá trị param chuyền vào
    Input Get URL= http://localhost:8081/api/check/doanhthu/year?year=*v
    OutPut Data:
    [
        2,       
        368000
    ],[tháng,doanhthu tháng đó],[]...
    */ 
    @GetMapping("/doanhthu/year" )
    public  List<Object> doanhthunam (@RequestParam("year") Integer year){
        return statisticService.doanhthunam( year);
    }
 // lấy thông tin doanh thu của năm các năm
     /*
     *value giá trị Database của đối tượng 
      *v giá trị param chuyền vào
    Input Get URL= http://localhost:8081/api/check/doanhthu/years
    OutPut Data:
    [
        2,       
        368000
    ],[tháng,doanhthu tháng đó],[]...
    */ 
    @GetMapping("/doanhthu/years" )
    public  List<Object> doanhthunhieunam (){
        return statisticService.doanhthunhieunam();
    }
    //tông doanh thu của hệ thống
    @GetMapping("/doanhthu/total" )
    public  List<Object> sumdoanhthu (){
        return statisticService.sumdoanhthu();
    }
  

    // @GetMapping("/bill" )
    // public  List<Object> billsInMon (@RequestParam("mon") Integer id){
    //     return statisticService.billsInMon(id);
    // }
    // @GetMapping("/billall" )
    // public  List<Object> billsAll (){
    //     return statisticService.billsAll();
    // }

   
}
