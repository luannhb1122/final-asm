package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Sizes;
import workspace.thecoffee.service.SizeService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api")
@RestController
public class SizeController {
    private final SizeService sizeService;

    @Autowired
    public SizeController(SizeService sizeService){
        this.sizeService= sizeService; 
    }

// lấy toàn bộ thông tin size trong hệ thống
     /*
     *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/size
    OutPut Data:
     [size{
        "id": "value",
        "type": "value",
        "price": "value",
        "status": "value"
    },
    size{},..
    size{}
    ]
    */
    @GetMapping("/all/size")
    public List<Sizes> getAllSize(){
       return sizeService.getAllSize();
    }

    // tìm size theo id
    /*
    *value giá trị Database của đối tượng 
    Input Get URL= http://localhost:8081/api/size/{id} 
    OutPut Data:
     size[id]{
        "id": "[id]",
        "type": "value",
        "price": "value",
        "status": "value"
    }
    */
    @GetMapping("/admin/size/{id}")
    public Sizes  getSizeById(@PathVariable("id") Integer id){
        return sizeService.getSizeById(id).orElse(null);
    }

// thêm một size vào hệ thống
     /*
     *newvalues giá trị mới được thêm vào database
    Input Post URL= http://localhost:8081/api/size/{body} 
    Input Data:
     body{        
        "type": "newvalues",
        "price": "newvalues"       
    }
    */    
    @PostMapping("/admin/size")
    public void addSize(@RequestBody Sizes size){
        sizeService.addSize(size);
    }

//xóa một size khỏi hệ thống Input Delete URL= http://localhost:8081/api/size/{id} 
 
    @DeleteMapping("/admin/size/{id}")
    public void deleteSize(@PathVariable("id") Integer id){
        sizeService.deleteSize(id);
    }

// chính sửa thông tin size theo id 
    /*
    *repvalue giá trị thay thế
    *value giá trị Database của đối tượng 
    Input Put URL= http://localhost:8081/api/size/{id} 
    Input Data:
      size[id]{
        "id": "[id]",
        "type": "value",
        "price": "value",
        "status": "repvalue"
    }
    OutPut data 
         size[id]{
        "id": "[id]",
        "type": "value",
        "price": "value",
        "status": "repvalue"
    }
    */
    @PutMapping("/admin/size/{id}")
    public void updatSize(@PathVariable("id") Integer id,@RequestBody Sizes newSize){
        sizeService.updatSize(id,newSize);
    }
}
