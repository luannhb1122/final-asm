package workspace.thecoffee.dao;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import workspace.thecoffee.model.Ships_Info;

public interface ShipDao extends JpaRepository<Ships_Info,Integer> {
    @Query(value ="exec findShipByIdOrder @id = :id", nativeQuery = true)
    public Optional<Ships_Info>  findShipByOrder(@Param("id")int id);
}
