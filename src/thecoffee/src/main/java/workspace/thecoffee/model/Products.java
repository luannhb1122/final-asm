package workspace.thecoffee.model;
import javax.persistence.*;
@Entity
@Table(name = "Products")
public class Products {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Float price;

    @Column(name = "image")
    private String image;

    @Column(name = "status")
    private Integer status =1;

    @ManyToOne 
    @JoinColumn(name="id_categories")
	Categories categories;

    public Products() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public Products(String name, Float price, String image, Integer status, Categories categories) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.status = status;
      //  if(status!=null){ this.status = status;}else{this.status=1;};       
        this.categories = categories;
    }
}
