package workspace.thecoffee.service;

import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.ProductDao;
import workspace.thecoffee.model.Products;

@Service
public class ProductService {
    private final ProductDao productDao;
    @Autowired
    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public void addProduct(Products products) {
        productDao.save(new Products(products.getName(),products.getPrice(), products.getImage(),products.getStatus(), products.getCategories()));
    }   

    public List<Products> getAllProduct() {
        return productDao.findAllProduct();
    }  

    public List<Products> findProduct(String pr) {
        return productDao.findProduct(pr);       
    }

    public Optional<Products> getProductById(Integer id) {
        return productDao.findById(id);
    }

    public void deleteProduct(Integer id) {
        productDao.deleteById(id);
    }


    public void updateProduct(Integer id, Products newProduct){
        Optional<Products> typeProductData = getProductById(id);
        Products oldProduct = typeProductData.get();
        oldProduct.setName(newProduct.getName());
        oldProduct.setPrice(newProduct.getPrice());
        oldProduct.setImage(newProduct.getImage());
        oldProduct.setStatus(newProduct.getStatus());
        oldProduct.setCategories(newProduct.getCategories());
        productDao.save(oldProduct);
    }
}

