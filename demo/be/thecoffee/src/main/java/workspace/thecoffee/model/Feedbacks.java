package workspace.thecoffee.model;

import java.util.Date;

import javax.persistence.*;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Feedbacks")
public class Feedbacks {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "note")
    private String note;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "rate")
    private Integer rate;

    @Column(name = "date_time")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date date_time;

    @ManyToOne @JoinColumn(name="id_cus")
	Accounts_Info customers;

    @ManyToOne @JoinColumn(name="id_product")
	Products products;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public Date getDate_time() {
        return date_time;
    }

    public void setDate_time(Date date_time) {
        this.date_time = date_time;
    }

  

    public Products getProducts() {
        return products;
    }

    public void setProducts(Products products) {
        this.products = products;
    }

    public Feedbacks(String note, Boolean status, Integer rate, Date date_time, Accounts_Info customers,
            Products products) {
        this.note = note;
        this.status = status;
        this.rate = rate;
        this.date_time = date_time;
        this.customers = customers;
        this.products = products;
    }

    public Feedbacks() {
    }

    public Accounts_Info getCustomers() {
        return customers;
    }

    public void setCustomers(Accounts_Info customers) {
        this.customers = customers;
    }

    
}
