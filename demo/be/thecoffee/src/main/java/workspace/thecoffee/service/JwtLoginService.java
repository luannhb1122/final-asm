package workspace.thecoffee.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.AccountDao;
import workspace.thecoffee.model.Accounts_Info;


@Service
public class JwtLoginService implements UserDetailsService {
    private final AccountDao accountDao;

    @Autowired
    public JwtLoginService(AccountDao accountDao) {
        this.accountDao = accountDao;
    }


    public List<Accounts_Info> getAllAccount() {
        return accountDao.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Accounts_Info accounts_Info = accountDao.findByUsername(username);
        if (accounts_Info != null) {
            List<GrantedAuthority> authorities= new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority(accounts_Info.getRoles().getName())); 
            return new User(accounts_Info.getUsername(), accounts_Info.getPassword(), authorities);

        } else
            throw new UsernameNotFoundException("User not found with username: " + username);
    }

}
