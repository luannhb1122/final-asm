package workspace.thecoffee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import workspace.thecoffee.dao.RoleDao;
import workspace.thecoffee.model.Roles;

@Service
public class RoleService {
    private final RoleDao roleDao;

    @Autowired
    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public void addRole(Roles roles) {
        roleDao.save(new Roles(roles.getName()));
    }

    public List<Roles> getAllRole() {
        return roleDao.findAll();
    }

    public Optional<Roles> getRoleById(Integer id) {
        return roleDao.findById(id);
    }

    public void deleteRole(Integer id) {
        roleDao.deleteById(id);
    }

    public void updateRole(Integer id, Roles roles){
        Optional<Roles> roleData = getRoleById(id);
        Roles oldRole = roleData.get();
        oldRole.setName(roles.getName());
        roleDao.save(oldRole);
    }
}
