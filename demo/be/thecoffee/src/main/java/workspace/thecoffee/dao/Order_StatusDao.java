package workspace.thecoffee.dao;
import org.springframework.data.jpa.repository.JpaRepository;

import workspace.thecoffee.model.Orders_Status;

public interface Order_StatusDao extends JpaRepository<Orders_Status,Integer> {
    
}
