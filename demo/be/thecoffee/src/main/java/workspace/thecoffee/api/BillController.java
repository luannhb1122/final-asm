package workspace.thecoffee.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import workspace.thecoffee.model.Bills;
import workspace.thecoffee.service.BillService;

// @CrossOrigin(origins = "http://localhost:8082")
@CrossOrigin
@RequestMapping("api/bill")
@RestController
public class BillController {
    private final BillService billService;

    @Autowired
    public BillController(BillService billService){
        this.billService= billService; 
    }
// lấy toàn bộ thông tin Hóa Đơn trong hệ thống
/*
Input Get URL= http://localhost:8081/api/bill
    OutPut Data:
  [bill{
        "id": value,
        "date_create": "value",
        "total": value,
        "orders":value
 },
 bill{},..
 bill{}
*/
    @GetMapping
    public List<Bills> getAllBill(){
       return billService.getAllBill();
    }
// lấy thông tin Hóa Đơn trong hệ thống theo id
/*
Input Get URL= http://localhost:8081/api/bill/{id}
    OutPut Data:
  [bill{
        "id": value,
        "date_create": "value",
        "total": value,
        "orders":value
 },
 bill{},..
 bill{}
*/
    @GetMapping(path ="{id}" )
    public Bills  getBillById(@PathVariable("id") Integer id){
        return billService.getBillById(id).orElse(null);
    }
// thêm Hóa mới vào hệ thống
/*
Input Post URL= http://localhost:8081/api/bill
    Input Data:
  bill{        
        "orders":newvalue
 }
*/    
    @PostMapping
    public void addBill(@RequestBody Bills acc){
        billService.addBillTest(acc);
    }
//xóa Hóa đơn khỏi hệ thống Input Delete URL= http://localhost:8081/api/bill/{id}

    @DeleteMapping(path ="{id}")
    public void deleteBill(@PathVariable("id") Integer id){
        billService.deleteBill(id);
    }

    // @PutMapping(path ="{id}")
    // public void updatBill(@PathVariable("id") Integer id,@RequestBody Bills acc){
    //     billService.updatBill(id,acc);
    // }

}
