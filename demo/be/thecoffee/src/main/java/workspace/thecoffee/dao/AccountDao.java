package workspace.thecoffee.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import workspace.thecoffee.model.Accounts_Info;

public interface AccountDao extends JpaRepository<Accounts_Info,Integer> {

Accounts_Info findByUsername(String username);
// get new info account
@Query(value = "SELECT TOP 1 * from Accounts_info Order by id desc", nativeQuery = true)
public java.util.Optional<Accounts_Info> findMaxId();
// get all account of employee
@Query(value = "SELECT * from Accounts_info where role =1", nativeQuery = true)
public java.util.List<Accounts_Info> searchAllEmp();
// get all account of customer
@Query(value = "SELECT * from Accounts_info where role =0", nativeQuery = true)
public java.util.List<Accounts_Info> searchAllCus();


}
